package com.resilient.resourceserver

import java.util.HashMap
import java.util.List
import java.util.logging.Logger

import com.resilient.tn.api.ServiceException
import com.resilient.tn.config.ClientConfigurator
import com.resilient.tn.config.Configuration
import com.resilient.tn.dts.client.rest.ContainerPolicy
import com.resilient.tn.dts.client.tnsdk.DTSClient
import com.resilient.tn.dts.client.rest.WLCreator

class RSSettingsService {

   static transactional = true

	private static RSSettings settings

	private static final Logger LOG = Logger.getLogger(RSSettingsService.class.getName())

	private static final String DEFAULT_DATA_TOKEN_SERVICE_ADDRESS = "rtn://localhost:8080";
	
	private static final long DATA_TOKEN_SERVICE_TIMEOUT = 3 * 60 * 1000;

	private boolean isClientOnline;

	def servletContext
		
	RSSettings getInstance() {
		if (!settings) {
			def listSettings = RSSettings.findAll()
			if (listSettings.size() == 1) {
				settings = listSettings.getAt(0)
			} else {
				// the settings object should have been created in Bootstrap, throw an exception if this is not true
				throw new RuntimeException("There can only be one Settings object")
			}
		}
		return settings
	}
	
	RSSettings reloadSettings() {
		settings = null
		return getInstance()
	}
	
	boolean startupTNSDKClient() {
		if (isClientOnline) {
			LOG.warning("Attempting to start TNSDK Client listener when it is already online");
			return true;
		}
		LOG.info("Attempting to start the Primary TNSDK Client listener");
		Map<Configuration.Key, String> configParams = new HashMap<Configuration.Key, String>();
		RSSettings theSettings = getInstance();
		
		String tbURL = theSettings.tbScheme + "://" + theSettings.tbHostname + ":" + theSettings.tbPort + "/trustbroker";
		configParams.put(Configuration.Key.CLIENT_DEFAULT_TRUSTBROKER, tbURL);
		configParams.put(Configuration.Key.CLIENT_CONTEXT_PATH, "/RNSResourceServer/primary");
		configParams.put(Configuration.Key.DEFAULT_MSG_TIMEOUT, theSettings.defaultMessageTimeout.toString());
		configParams.put(Configuration.Key.TRUST_SESSION_TIMEOUT, theSettings.trustSessionTimeout.toString());
		ClientConfigurator c = 	new ClientConfigurator(configParams, "client");
		try {
			// start service
			c.startService();
			
			// TODO: 1- it is entirely insane that the configuration object is only available after the service start
			// TODO: 2- Also it does not make sense to use servletContext inside this object!!!
			if ( DTSClient.getInstance() == null ) {
				String externalAddress = c.getConfiguration().getProperty(Configuration.Key.EXTERNAL_ADDRESS);
				String jksPath = c.getConfiguration().getProperty(Configuration.Key.SSL_KEYSTORE_PATH);
				if ( externalAddress == null ) {
					LOG.severe("Cannot initialize DTSClient. Failed to find external address." );
					return false;
				}
				DTSClient.initialize(externalAddress, jksPath);
			}
			LOG.info("Successfully started the Primary TNSDK Client listener");
			isClientOnline = true;
			this.servletContext.setAttribute("Configurator", c)
		}
		catch (ServiceException e) {
			LOG.severe("Failed to start the Primary TNSDK client listener service. Reason: " + e.getLocalizedMessage())
			return false;
		}
		// now see if we should start the backup TNSDK Client Session Manager
		if (theSettings.backupTBHostname) {
			String backupTBURL = "rtn://" + theSettings.backupTBHostname + ":" + theSettings.backupTBPort + "/trustbroker";
			configParams.put(Configuration.Key.CLIENT_DEFAULT_TRUSTBROKER, backupTBURL);
			configParams.put(Configuration.Key.CLIENT_CONTEXT_PATH, "/RNSResourceServer/secondary");
			ClientConfigurator c2 = new ClientConfigurator(configParams, "client");
			try {
				c2.startService();
				LOG.info("Successfully started the Backup TNSDK Client listener");
				this.servletContext.setAttribute("BackupClientConfigurator", c2)
			} catch (ServiceException e) {
				LOG.severe("Failed to start the Backup TNSDK client listener service. Reason: " + e.getLocalizedMessage())
				return false;
			}
		}
		return true
	}
	
	boolean shutdownTNSDKClient() {
		if (!isClientOnline) {
			LOG.warning("Attempting to stop TNSDK Client listener when it is not online");
			return true;
		}
		LOG.info("Attempting to stop the Primary TNSDK Client listener");
		try {
			ClientConfigurator c = getPrimaryClientConfigurator()
			if (c)
				c.stopService();
			LOG.info("Successfully stopped the Primary TNSDK Client listener");
			isClientOnline = false;
		}
		catch (ServiceException e) {
			LOG.severe("Failed to stop the Primary TNSDK client listener service. Reason: " + e.getLocalizedMessage())
			return false;
		}
		try {
			ClientConfigurator c2 = getSecondaryClientConfigurator()
			if (c2) {
				((ClientConfigurator)c2).stopService()
				LOG.info("Successfully stopped the Backup TNSDK Client listener");
			}
		} catch (ServiceException e2) {
			LOG.severe("Failed to stop the Backup TNSDK client listener service. Reason: " + e2.getLocalizedMessage())
			return false;
		}
		return true
	}
	
	boolean restartTNSDKClient() {
		if (shutdownTNSDKClient()) {
			return startupTNSDKClient()
		} else {
			return false;
		}
	}
	
	ClientConfigurator getPrimaryClientConfigurator() {
		Object c = this.servletContext.getAttribute("Configurator")
		if (c && c instanceof ClientConfigurator) {
			return (ClientConfigurator)c
		}
		return null
	}
	
	ClientConfigurator getSecondaryClientConfigurator() {
		Object c = this.servletContext.getAttribute("BackupClientConfigurator")
		if (c && c instanceof ClientConfigurator) {
			return (ClientConfigurator)c
		}
		return null
		
	}
	
	/**
	* This is the API for creation of container token. There is one container token per CX.
	* prior to calling this API, the CX should be scanned for all Credential Elements that
	* has data token parameters and the list of resolve URIs should be the GA URIs of each
	* Credential Element that has data token parameters.
	* The create policy whitelist includes a single entry the address of this TN Service
	* the ClientSessionManager in this case
	*
	* @param resolveURIs - All the GA URIs that should be in the resolve whitelist
	* @param data - the data value to tokenize
	*
	* @return - the container token
	* @throws ServiceException
	*/
   public String createContainerToken(List<String> resolveURIs) throws ServiceException {
	   try {
		   ClientConfigurator c = (ClientConfigurator)this.servletContext.getAttribute("Configurator")
		   String dtsAddress = c.getConfiguration().getProperty(Configuration.Key.DATA_TOKEN_SERVICE_ADDRESS, DEFAULT_DATA_TOKEN_SERVICE_ADDRESS);
		   String externalAddress = c.getConfiguration().getProperty(Configuration.Key.EXTERNAL_ADDRESS)
		   String contextPath = c.getConfiguration().getProperty(Configuration.Key.CLIENT_CONTEXT_PATH)
		   String wlCreatorAddress = externalAddress + contextPath + "/ClientSessionManager"
		   DTSClient dtsClient = DTSClient.getInstance();
		   
		   WLCreator wlCreate = new WLCreator();
		   wlCreate.addEntry(wlCreatorAddress);
		   WLCreator wlResolve = new WLCreator();
		   for (String resolveURI : resolveURIs) {
			   wlResolve.addEntry(resolveURI.toString());
		   }
		   String token = dtsClient.containerCreate(
			   dtsAddress, new ContainerPolicy(wlCreate.generateJsonString(), wlResolve.generateJsonString()),
			   DATA_TOKEN_SERVICE_TIMEOUT);
		   if ( token == null ) {
			   String err = "Failed to create the container token. Cause: " + dtsClient.getLastError();
			   throw new ServiceException (err);
		   }
		   return token;
	   } catch (ServiceException se) {
			   throw se;
	   } catch (Exception ex) {
			   LOG.severe("Failed to create the container token");
			throw new ServiceException(ex.getLocalizedMessage());
	   }
   }
   
   /**
	* Creates a Data token given a container token
	* @param containerToken - the container token
	* @param datum - the data to tokenize
	* @return - the tokenized value
	*
	* @throws ServiceException
	*/
   public String createDataToken(String containerToken, String datum) throws ServiceException {
	   try {
		   ClientConfigurator c = (ClientConfigurator)this.servletContext.getAttribute("Configurator")
		   String externalAddress = c.getConfiguration().getProperty(Configuration.Key.EXTERNAL_ADDRESS)
		   String contextPath = c.getConfiguration().getProperty(Configuration.Key.CLIENT_CONTEXT_PATH)
		   String wlCreatorAddress = externalAddress + contextPath + "/ClientSessionManager"
		   DTSClient dtsClient = DTSClient.getInstance();

		   String token = dtsClient.tokenCreate(containerToken, datum, DATA_TOKEN_SERVICE_TIMEOUT);
		   if ( token == null ) {
			   String err = "Failed to create the data token. Cause: " + rdtsClient.getLastError();
			   throw new ServiceException (err);
		   }
		   return token;
	   } catch (ServiceException se) {
			  throw se;
	   } catch (Exception ex) {
			  LOG.severe("Failed to create the data token");
		   throw new ServiceException(ex.getLocalizedMessage());
	   }
   }

}
