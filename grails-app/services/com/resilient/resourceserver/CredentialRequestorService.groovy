package com.resilient.resourceserver

import java.util.Map

import javax.servlet.http.HttpServletRequest

import org.codehaus.jettison.json.JSONObject

import com.resilient.as.BaseResource
import com.resilient.as.CredentialExpression
import com.resilient.as.CredentialElement
import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers
import com.resilient.ta.md.impl.CredElmtMetaDataParameter
import com.resilient.ta.md.impl.CredentialElementMetaData
import com.resilient.tn.api.DisplayRequest
import com.resilient.tn.api.ServiceException
import com.resilient.tn.api.client.*
import com.resilient.tn.config.ClientConfigurator

/**
 * The Credential Requestor service implementation
 * Performs the following functionality:
 * 1. extracts the credential expression(s) from the Resource object and combines them to create the credential
 *    expression to send to the trust broker
 * 2. substitutes requests parameters into the credential expression using parameter mapping specified in
 *    the Resource object
 * 3. handles the trust broker display redirection
 * 4. returns the CredentialRequestor object that has the decision of the credential request
 * 
 * TODO: This class is written almost entirely in Java syntax. Incorporate groovyism into the code!   
 * 
 * @author sghosh
 *
 */
class CredentialRequestorService {
	
    static transactional = true
	
	def servletContext
	def RSSettingsService

	class FinishUp implements CredentialRequestListener {
//		HttpSession session;
		String restartURL;
		String callbackURL;
		String clientSessionId;
				
		FinishUp(String restartURL, String callbackURL, String clientSessionId) {
//			this.session = session;
			this.restartURL = restartURL;
			this.callbackURL = callbackURL;
			this.clientSessionId = clientSessionId;
		}
		
		public void credentialRequestUpdated(CredentialRequest credentialRequest) {
			if (credentialRequest.restartURLRequired()) {
				String url = null;
				String decision = "";
				if (credentialRequest.granted()) {
//					this.session.setAttribute(this.trustSessionId, "GRANT");
					decision = "GRANT";
				} else if (credentialRequest.denied()) {
//					this.session.setAttribute(this.trustSessionId, "DENY");
					decision = "DENY";
				} else if (credentialRequest.deniedWithError() ) {
					decision = "ERROR";
				}
				url = restartURL + "?callbackUrl=" + callbackURL + "&decision=" + decision + "&tsid=" + clientSessionId
//				log.info("Restart URL: " + url)
				credentialRequest.setRestartURL(url);
			}
			
		}
	
		public void credentialRequestTimeOut(CredentialRequest a_request) {
			// TODO Auto-generated method stub
			
		}
	
	}
	
	class LogoutFinishUp implements TrustSessionCloseRequestListener {
		String restartURL;
		String callbackURL;
		String clientSessionId;
		
		LogoutFinishUp(String restartURL, String callbackURL, String clientSessionId) {
			this.restartURL = restartURL;
			this.callbackURL = callbackURL;
			this.clientSessionId = clientSessionId;
		}
			
		public void requestUpdated(TrustSessionCloseRequest credentialRequest) {
		    if (credentialRequest.restartURLRequired()) {
				String url = null;
				String decision = "";
				if (credentialRequest.ack()) {
				    decision = "GRANT";
				}
				if (credentialRequest.nack()) {
				    decision = "ERROR";
				}
				url = restartURL + "?callbackUrl=" + callbackURL + "&decision=" + decision + "&tsid=" + clientSessionId
				credentialRequest.setRestartURL(url);
			}
		}
		
		public void requestTimeOut(final TrustSessionCloseRequest a_request) {
		}
	}

    CredentialRequestor requestCredential(BaseResource resource, HttpServletRequest request, ClientConfigurator cc) {
		JSONObject postData = request.JSON;
		log.info("Data posted to Resource Server: " + postData.toString())
		CredentialRequestor credReq = new CredentialRequestor()
		String credentialExpr = resource.credentialExpr();
		// Tokenize parameters that are of rns:data-token type
		Map<String, String> parameters = postData.get("parameters")
		replaceDataTokenParameters(resource, parameters)
		credentialExpr = CredentialExpressionHelpers.replaceParamsInCredentialExpr(credentialExpr, parameters)
//		log.info("Credential Expression being sent to Trust Broker for evaluation: " + credentialExpr)
		UUID clientSessionID = null;
		if (postData.has("sessionId") && !postData.getString("sessionId").isEmpty() && !postData.getString("sessionId").equals("null")) {
			clientSessionID = UUID.fromString(postData.getString("sessionId"));
		}
		boolean dropSession = false;
//		if (postData.getBoolean("newsession")) {
//			dropSession = true;
//		}
		ClientSession clientSession = null;
		ClientSessionManager sm = null;
		CredentialRequest credentialRequest = null;
		try {
			sm = cc.getClientSessionManager();
		}
		catch (ServiceException e1) {
			credReq.decision = "ERROR";
			credReq.message = e1.getLocalizedMessage()
		}
		try {
			if (clientSessionID == null) {
				clientSession = sm.createClientSession(600);
				clientSessionID = clientSession.getSessionId();
			} else {
				clientSession = sm.joinClientSession(clientSessionID, 600);
			}
			credentialRequest =
			clientSession.evaluateCredentialExpression(credentialExpr, 1000 * 60 * 10);
	
		} catch (ClientException e) {
			credReq.decision = "ERROR";
			credReq.message = e.getLocalizedMessage()
		} catch (Exception anyEx) {
			credReq.decision = "ERROR";
			credReq.message = e.getLocalizedMessage()
		}
		log.info("Client Session Id: " + clientSessionID.toString());
		while (!credentialRequest.complete()) {
			try {
				credentialRequest.waitWhilePending(0);
			}
			catch (InterruptedException e) {
				credReq.decision = "ERROR";
				credReq.message = e.getLocalizedMessage()
			}
			if (credentialRequest.displayRequestAvailable()) {
				DisplayRequest displayRequest = credentialRequest.getDisplayRequest();
				credReq.decision = "TB_DISPLAY";
				credReq.trustBrokerDisplayURL = displayRequest.getUrl();
				credReq.sessionId = clientSessionID.toString();
				String restartURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/RNSResourceServer/credentialRequestor/restart/" + resource.name; 
				log.info("Trust Broker Decision: TB_DISPLAY")
				log.info("Trust Broker Display URL: " + credReq.trustBrokerDisplayURL)
				log.info("Client Session ID for TBD: " + credReq.sessionId)
				log.info("CallbackURL: " + postData.get("callbackURL"))
				log.info("Restart URL: " + restartURL)
				credentialRequest.addListener(new FinishUp(restartURL, postData.get("callbackURL"), clientSessionID.toString()));
				return credReq;	
			}
		}
		if (credentialRequest.granted()) {
			credReq.decision = "GRANT";
			credReq.sessionId = clientSessionID.toString();
		}
		else if (credentialRequest.deniedWithError()) {
			credReq.decision = "ERROR";
			credReq.message = "Internal Error Detected";
		}
		else if (credentialRequest.timedOut()) {
			log.info("Timed out while waiting for response from Trust Broker");
			credReq.decision = "TIMEOUT";
			credReq.message = "Timed out while waiting for the response from Trust Broker";
		} else {
			credReq.decision = "DENY";
		}
		log.info("Trust Broker Decision: " + credReq.decision)
		return credReq;
    }
	
	CredentialRequestor logout(BaseResource resource, HttpServletRequest request, ClientConfigurator cc) {
		JSONObject postData = request.JSON;
		CredentialRequestor credReq = new CredentialRequestor()
		UUID clientSessionID = null;
		if (postData.has("sessionId") && !postData.getString("sessionId").isEmpty() && !postData.getString("sessionId").equals("null")) {
			clientSessionID = UUID.fromString(postData.getString("sessionId"));
		}
		ClientSessionManager sm = null;
		try {
			sm = cc.getClientSessionManager();
		}
		catch (ServiceException e1) {
			credReq.decision = "ERROR";
			credReq.message = e1.getLocalizedMessage()
		}
		ClientSession clientSession = null;
		TrustSessionCloseRequest logOutRequest = null;
		try {
			if (clientSessionID == null) {
				clientSession = sm.createClientSession(600);
				clientSessionID = clientSession.getSessionId();
			} else {
				clientSession = sm.joinClientSession(clientSessionID, 600);
			}
			logOutRequest = clientSession.closeTrustSession(1000 * 60 * 10);
		} catch (ClientException e) {
			credReq.decision = "ERROR";
			credReq.message = e.getLocalizedMessage()
		}
		while (!logOutRequest.complete()) {
			try {
				logOutRequest.waitWhilePending(0);
			} catch (InterruptedException e) {
				credReq.decision = "ERROR";
				credReq.message = e.getLocalizedMessage()
			}
			if (logOutRequest.displayRequestAvailable()) {
				DisplayRequest displayRequest = logOutRequest.getDisplayRequest();
				credReq.decision = "TB_DISPLAY";
				credReq.trustBrokerDisplayURL = displayRequest.getUrl();
				credReq.sessionId = clientSessionID.toString();
				String restartURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/RNSResourceServer/credentialRequestor/restart/" + resource.name;
				logOutRequest.addListener(new LogoutFinishUp(restartURL, postData.get("callbackURL"), clientSessionID.toString()));
				return credReq;

			}
		}
		if (logOutRequest.ack()) {
			credReq.decision = "GRANT";
			credReq.sessionId = clientSessionID.toString();
		} else if (logOutRequest.timedOut()) {
			log.info("Timed out while waiting for response from Trust Broker");
			credReq.decision = "TIMEOUT";
			credReq.message = "Timed out while waiting for the response from Trust Broker";
		} else {
			credReq.decision = "ERROR";
		}
		return credReq;
	}
	
	private void replaceDataTokenParameters(BaseResource res, Map<String, String> parameters) throws Exception 
	{
		try {
			String credExpr = res.credentialExpr()
			boolean hasDataTokens = CredentialExpressionHelpers.hasDataTokenParameters(credExpr);
			if (hasDataTokens) {
				if (res.containerToken == null) {
					List<String> wlResolveURIs = CredentialExpressionHelpers.getDTSResolveURIs(credExpr);
					res.containerToken = RSSettingsService.createContainerToken(wlResolveURIs);
				}
				CredentialExpression cx = res.credentialExpr
				for (CredentialElement cred: cx.credentialElements) {
					CredentialElementMetaData credMD = CredentialExpressionHelpers.getCredentialElementMetaData(cred.name);
					Map<String, CredElmtMetaDataParameter> mdParams = credMD.getParameters();
					for (Map.Entry<String, CredElmtMetaDataParameter> entry: mdParams.entrySet()) {
						String paramName = entry.getKey();
						CredElmtMetaDataParameter paramMD = entry.getValue();
						if (paramMD.getType().equals(CredentialExpressionHelpers.DATA_TOKEN_TYPE)) {
							if (parameters.containsKey(paramName)) {
								URI resolveURI = new URI(cred.grantingAuthority)
								String dataTokenURI = RSSettingsService.createDataToken(res.containerToken, parameters.get(paramName));
								parameters.put(paramName, dataTokenURI);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			log.error("Failed to replace data token parameter value into data token URI. Reason: " + ex.getLocalizedMessage());
			throw ex;
		}
	}

}
