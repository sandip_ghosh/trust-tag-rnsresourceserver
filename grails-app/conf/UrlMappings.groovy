class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller:"resource", action:"settings")
		"/TrustTag/js/"(view:'/js/TrustTag.js')
		"/LABjs/js/"(view:'/js/LAB.min.js')
		"/TrustTag/css/"(view:'/css/TrustTag.css')
		"/erefferal/admin"(view:'/test/erefferal_demo')
		"500"(view:'/error')
//		"/credentialRequestor/requestCredential/" (controller: "credentialRequestor", parseRequest: true)
	}
}
