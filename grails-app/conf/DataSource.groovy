import com.resilient.ts.util.ConfigUtils
import com.resilient.commonutils.protectedstore.RNSProtectedStoreLabels
import com.resilient.commonutils.protectedstore.RNSProtectedStore
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import com.resilient.tn.util.DerbyInitializer
import java.util.Properties;

dataSource {
    pooled = true
    driverClassName = "org.hsqldb.jdbcDriver"
    username = "sa"
    password = ""
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}

// environment specific settings
environments {
	
	String propFileName = "${appName}-config.properties"
	// get the key which will be used to decrypt the database user name and password.
	Properties unEncryptedProps = ConfigUtils.loadPropertiesFile(this.class, propFileName)
	
	// just reading property values here
	dbhost = unEncryptedProps.getAt("ressvr.database.host")
	dbname = unEncryptedProps.getAt("ressvr.database.name")
	
        // Some of the environment elements are in the protected storage facility.
        RNSProtectedStore storage = new RNSProtectedStore ();
        String usernameLabel = RNSProtectedStoreLabels.getRNSLabel (
          RNSProtectedStoreLabels.RNSLabelType.DATABASE_USERNAME);
        String passwordLabel = RNSProtectedStoreLabels.getRNSLabel (
          RNSProtectedStoreLabels.RNSLabelType.DATABASE_PASSWORD);
        String bootLabel = RNSProtectedStoreLabels.getRNSLabel (
          RNSProtectedStoreLabels.RNSLabelType.DERBY_BOOT_PASSWORD);

        uname = storage.retrieveEntryString (usernameLabel);
        pword = storage.retrieveEntryString (passwordLabel);

	// MySql check enable DB and set data dir for derby db
	enableMysql = props.getAt("ressvr.database.mysql.enable")

	// Setup DB props (Derby)
	if ( enableMysql != 'true' ) {
                String bootPassword = storage.retrieveEntryString (bootLabel);
		
		if ( bootPassword == null || bootPassword.length() < 5 )
			throw new Exception ("Missing or invalid bootPassword in config file");
	
		dbUsername = uname
		dbPassword = pword
		
		// init derby initializer
		DerbyInitializer.initialize ("${appName}", bootPassword, dbUsername, dbPassword );
		// If created new DB then shutdown
		if ( DerbyInitializer.getInstance().createNewDB() ) {
			DerbyInitializer.getInstance().shutdown();
		}

		dbname = DerbyInitializer.getInstance().getDbName();
		dbDriverClassName = "org.apache.derby.jdbc.EmbeddedDriver"
		dbUrl =  "jdbc:derby:${dbname};dataEncryption=true;bootPassword="+bootPassword+";"
		dbDialect = org.hibernate.dialect.DerbyDialect
		
		dbProps = {}
	}
	else {	// MySQL
		dbDriverClassName = "com.mysql.jdbc.Driver"
		dbUrl =  "jdbc:mysql://${dbhost}/${dbname}";
		dbDialect = org.hibernate.dialect.MySQL5Dialect
		dbUsername = uname
		dbPassword = pword
		dbProps = {
			validationQuery="select 1"
			testWhileIdle=true
			timeBetweenEvictionRunsMillis=60000
		}
	}

	// Setup data srouce
	development {
		dataSource {
			dbCreate = "update" // one of 'create', 'create-drop','update'
			driverClassName = dbDriverClassName
			url =  dbUrl
			username = dbUsername
			password = dbPassword
			dialect = dbDialect
			properties = dbProps;
		}
	}
	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:hsqldb:mem:testDb"
		}
	}
	production {
		dataSource {
			dbCreate = "update" // one of 'create', 'create-drop','update'
			driverClassName = dbDriverClassName
			url =  dbUrl
			username = dbUsername
			password = dbPassword
			dialect = dbDialect
			properties = dbProps;
		}
	}
}
