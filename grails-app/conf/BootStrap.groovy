import grails.util.Environment

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.util.Properties
import com.resilient.resourceserver.RSSettings
import com.resilient.ts.util.ConfigUtils
import com.resilient.tn.util.DerbyInitializer
import com.resilient.commonutils.protectedstore.RNSProtectedStoreLabels
import com.resilient.commonutils.protectedstore.RNSProtectedStore
import com.resilient.commonutils.protectedstore.RNSProtectedStoreException;

class BootStrap {

	def RSSettingsService
	
    def init = { servletContext ->
		
		switch (Environment.current) {
			case Environment.DEVELOPMENT:
			case Environment.PRODUCTION:
				Properties clientProps = ConfigUtils.loadPropertiesFile(this.class, "client.properties")
				Properties rsProps = ConfigUtils.loadPropertiesFile(this.class, "RNSResourceServer-config.properties")
				setupSSLProperties(clientProps)
				createDefaultSettings(clientProps, rsProps)
				RSSettingsService.startupTNSDKClient()
				break
		}
	}

	def destroy = {
		switch (Environment.current) {
			case Environment.DEVELOPMENT:
			case Environment.PRODUCTION:
				RSSettingsService.shutdownTNSDKClient()
			
				// shutdown Derby correctly (Grails 1.3.7 does not do it well at all!)
				if ( DerbyInitializer.getInstance() != null ) {
					DerbyInitializer.getInstance().shutdown ( true )
				}
				break
		}
	}

	void createDefaultSettings(Properties clientProps, Properties rsProps) {
		def listSettings = RSSettings.findAll()
		if (listSettings.size() == 0) {
			println "TNSDK Client Settings not set, setting it to default values"
			String tbHostname = "localhost"
			int tbPort = 8080
			String tbHost = clientProps.getProperty("client.default.trustbroker", "rtn://localhost:8080/trustbroker")
			String tbScheme = "rtn"
			try {
				URI tbHostURI = new URI(tbHost)
				tbHostname = tbHostURI.getHost()
				tbPort = tbHostURI.getPort()
				tbScheme = tbHostURI.getScheme()
			} catch (URISyntaxException ex) {
			}
			int defaultMessageTimeout = Integer.valueOf(clientProps.getProperty("default.msg.timeout", "100000"))
			int trustSessionTimeout = Integer.valueOf(clientProps.getProperty("trust.session.timeout", "100000"))
			
			String auditTrailLogFile = rsProps.getProperty("settings.auditTrail.logFile", "/tmp/as_http_api.log")
			String auditLogLevel = rsProps.getProperty("settings.auditTrail.logLevel", "basic")
			int auditRolloverSize = Integer.valueOf(rsProps.getProperty("settings.auditTrail.rolloverSize", "10240"))

			// SEANOTE: No need to convert to kbytes
			//auditRolloverSize = auditRolloverSize / 1024
			
			int auditRolloverBackupIndex = Integer.valueOf(rsProps.getProperty("settings.auditTrail.maxBackupIndex", "10"))
			
			def settings = new RSSettings(tbHostname: tbHostname, 
										  tbPort: tbPort, 
										  defaultMessageTimeout: defaultMessageTimeout, 
										  trustSessionTimeout: trustSessionTimeout,
										  auditTrailFile: auditTrailLogFile,
										  auditLogLevel: auditLogLevel,
										  auditTrailRolloverSize: auditRolloverSize,
										  maxBackupIndex: auditRolloverBackupIndex,
										  tbScheme: tbScheme,
										  backupTBHostname: null,
										  backupTBPort: tbPort)
			if (!settings.save(flush:true, failOnError:true)) {
				println "Failed to save the TNSDK Client settings"
			} 
		} else {
			println "TNSDK Client Settings has been set"
		}
	}
	
	/**
	 * This sets the Java System SSL properties for the RelyingParty API calls
	 * The HTTP Protocol requests that are sent using HttpClientUtil
	 * 
	 * @param props
	 */
	void setupSSLProperties(Properties props) 
	{
		String keyStorePath = props.getProperty("ssl.keystore.path");
		if (keyStorePath != null && !keyStorePath.isEmpty()) {
			System.setProperty("javax.net.ssl.trustStore", keyStorePath);
			System.setProperty("javax.net.ssl.keyStore", keyStorePath);
			// The SSL KeyStore password is in the protected store.
			RNSProtectedStore storage = null;
			String keyStorePassword = null;
			try {
				storage = new RNSProtectedStore ();
				String passwordLabel = RNSProtectedStoreLabels.getRNSLabel (RNSProtectedStoreLabels.RNSLabelType.SSL_KEYSTORE_PASSWORD);
				keyStorePassword = storage.retrieveEntryString (passwordLabel);
			} catch (RNSProtectedStoreException exc) {
				log.warn("Keystore password not in protected store. Using default keystore password");
				keyStorePassword = "password";
			} finally {
				if (keyStorePassword != null && !keyStorePassword.isEmpty()) {
					System.setProperty("javax.net.ssl.trustStorePassword", keyStorePassword);
					System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
				}
				if (storage != null) {
					storage.cleanup ();
				}
			}
			System.setProperty("javax.net.ssl.trustStoreType", "JKS");
		}
	}
	
}
