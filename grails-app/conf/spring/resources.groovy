import grails.util.Environment
import com.resilient.tn.util.DerbyInitializer
import com.resilient.tn.util.RNSDataSource

/**
 * Add RNSDataSource impl to control how SQLExceptions are thrown and sanitize the stack
 * trace (remove passwords, etc.)
 *
 * @author samiadranly
 */

beans = {
	switch ( Environment.current )
	{
		case Environment.PRODUCTION:
		case Environment.DEVELOPMENT:
			dataSource ( RNSDataSource ) {
				driverClassName = DerbyInitializer.getInstance().getJDBCDriverName()
				url = DerbyInitializer.getInstance().getJDBCConnectionUrl()
			}
			break
	}
}
