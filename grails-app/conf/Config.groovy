import org.apache.log4j.ConsoleAppender
import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.RollingFileAppender
import org.apache.log4j.PatternLayout
import org.apache.log4j.Level

import com.resilient.resourceserver.RSSettings

import com.resilient.ts.util.ConfigUtils

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ html: ['text/html','application/xhtml+xml'],
                      xml: ['text/xml', 'application/xml'],
                      text: 'text/plain',
                      js: 'text/javascript',
                      rss: 'application/rss+xml',
                      atom: 'application/atom+xml',
                      css: 'text/css',
                      csv: 'text/csv',
                      all: '*/*',
                      json: ['application/json','text/json'],
                      form: 'application/x-www-form-urlencoded',
                      multipartForm: 'multipart/form-data'
                    ]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable for AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// set per-environment serverURL stem for creating absolute links
environments {
    production {
        grails.serverURL = "http://www.changeme.com"
    }
    development {
        grails.serverURL = "http://localhost:8080/${appName}"
    }
    test {
        grails.serverURL = "http://localhost:8080/${appName}"
    }

}

//////////////////////////////////////////////////////////////////
// log4j configuration
//////////////////////////////////////////////////////////////////

def log4jConsoleLogLevel = "OFF"
def log4jRollingFileLogLevel = "INFO"
def log4jDailyRollingFileLogLevel = "OFF"
def log4jLogFileDirectory = "/tmp"
def log4jMaxFileSize = "10000KB"
def log4jMaxBackupIndex = 10
def log4jDatePattern = "'.'yyyy-MM-dd"

// Audit Trail Logging Properties
def httpAPILogFile = RSSettings.DEFAULT_AUDIT_TRAIL_FILE
int rolloverSize = RSSettings.DEFAULT_AUDIT_TRAIL_ROLLOVER_SIZE
int maxBackupIndex = RSSettings.DEFAULT_AUDIT_TRAIL_BACKUP_INDEX 

log4j = {
    
    String propFileName = "${appName}-config.properties"
    try {
        Properties configProps = ConfigUtils.loadPropertiesFile(this.class, propFileName)
        String x = configProps.getProperty("log4jConsoleLogLevel")
        if (x != null) log4jConsoleLogLevel = x
        x = configProps.getProperty("log4jRollingFileLogLevel")
        if (x != null) log4jRollingFileLogLevel = x
        x = configProps.getProperty("log4jDailyRollingFileLogLevel")
        if (x != null) log4jDailyRollingFileLogLevel = x    
        x = configProps.getProperty("log4jLogFileDirectory")
        if (x != null) log4jLogFileDirectory = x
        x = configProps.getProperty("log4jMaxFileSize")
        if (x != null) log4jMaxFileSize = x
        x = configProps.getProperty("log4jMaxBackupIndex")
        if (x != null) log4jMaxBackupIndex = Integer.parseInt(x)
        x = configProps.getProperty("log4jDatePattern")
        if (x != null) log4jDatePattern = x

		x = configProps.getProperty("settings.auditTrail.logFile")
		if (x != null) httpAPILogFile = x
		x = configProps.getProperty("settings.auditTrail.rolloverSize")
		if (x != null) rolloverSize = Integer.parseInt(x)
		x = Integer.parseInt(configProps.getProperty("settings.auditTrail.maxBackupIndex"))
		if (x != null) maxBackupIndex = Integer.parseInt(x)
	} catch (Exception e) {
        System.err.println(e)
        e.printStackTrace(System.err)
    }
    
	println "Properties read from " + propFileName + ": " + httpAPILogFile + ", " + rolloverSize + ", " + maxBackupIndex
	
	def httpAPILayoutPattern = new PatternLayout("%d{MMM d, yyyy hh:mm:ss a} [Trust Network HTTP API] - %m%n")

	appenders {
		
		appender new RollingFileAppender (
			name: 'httpAPI',
			maxFileSize: rolloverSize * 1024,
			maxBackupIndex: maxBackupIndex,
			file: httpAPILogFile,
			threshold: org.apache.log4j.Level.INFO,
			layout: httpAPILayoutPattern
			)

	}

	
    // always dump all info to console if in development or test env
    environments {
        development {
            log4jConsoleLogLevel = "DEBUG"
            log4jRollingFileLogLevel = "OFF"
            log4jDailyRollingFileLogLevel = "OFF"
        }
        test {
            log4jConsoleLogLevel = "DEBUG"
            log4jRollingFileLogLevel = "OFF"
            log4jDailyRollingFileLogLevel = "OFF"
        }
    }
    
    if ("${System.getProperty('GRAILS_BUILD')}".equalsIgnoreCase("true")) {
        log4jRollingFileLogLevel = "OFF"
        log4jDailyRollingFileLogLevel = "OFF"
    }
    
    println "Log4j consoleLevel: ${log4jConsoleLogLevel} " +
                "rollingFile Level: ${log4jRollingFileLogLevel} " +
                "dailyRollingFile Level: ${log4jDailyRollingFileLogLevel}"

    def log4jLayoutPattern = new PatternLayout("%d [%t] %-5p %c %x - %m%n")
    
    // Sets lowest log level for package specific loggers. 
    // Any package not listed will not output log messages.
    
    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
          'org.codehaus.groovy.grails.web.pages', //  GSP
          'org.codehaus.groovy.grails.web.sitemesh', //  layouts
          'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
          'org.codehaus.groovy.grails.web.mapping', // URL mapping
          'org.codehaus.groovy.grails.commons', // core / classloading
          'org.codehaus.groovy.grails.plugins', // plugins
          'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
          'org.springframework',
          'org.hibernate',
          'net.sf.ehcache.hibernate'
          'org.apache'

	info httpAPI:	'grails.app.controller.com.resilient.resourceserver.CredentialRequestorController'
		  
    all 'grails.app', 'com.resilient'

    appenders {
        
        // do not write to stacktrace.log
        'null' name:'stacktrace'
        
        appender new ConsoleAppender(name: "console",
            threshold: Level.toLevel(log4jConsoleLogLevel),
            layout: log4jLayoutPattern
        )
        
    }
    
    log4jLogFileDirectory = log4jLogFileDirectory.trim()
    if ((log4jLogFileDirectory.length() > 0) && (!log4jLogFileDirectory.endsWith("/"))) {
        log4jLogFileDirectory = log4jLogFileDirectory + "/"
    }
    
    if (!"OFF".equalsIgnoreCase(log4jRollingFileLogLevel)) {
        
        println "log4j Rolling files in directory '" + log4jLogFileDirectory + "'"

        appenders {
            
            appender new RollingFileAppender(name: "rollingFile",
                threshold: Level.toLevel(log4jRollingFileLogLevel),
                file: "${log4jLogFileDirectory}${appName}.log",
                maxFileSize: "${log4jMaxFileSize}",
                maxBackupIndex: log4jMaxBackupIndex,
                layout: log4jLayoutPattern
            )
            
        }
    
        root {
            error 'console', 'rollingFile'
            additivity = true
        }
        
    } else if (!"OFF".equalsIgnoreCase(log4jDailyRollingFileLogLevel)) {
    
        println "log4j Daily Rolling files in directory '" + log4jLogFileDirectory + "'"

        appenders {
            
            appender new DailyRollingFileAppender(name: "dailyRollingFile",
                threshold: Level.toLevel(log4jDailyRollingFileLogLevel),
                file: "${log4jLogFileDirectory}${appName}.log",
                datePattern: log4jDatePattern,
                layout: log4jLayoutPattern
            )
            
        }
    
        root {
            error 'console', 'dailyRollingFile'
            additivity = true
        }
    
    } else {
    
        root {
            error 'console'
            additivity = true
        }
        
    }   

}

grails.converters.json.default.deep=true
