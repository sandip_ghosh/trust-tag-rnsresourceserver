package com.resilient.resourceserver

/**
 * This class is a singleton and allows for overriding the TNSDK Configuration.Key properties
 * from the values set in client.properties. This class is a singleton and it should
 * only be accessed through SettingsService that ensure it is used as a singleton. 
 * 
 * @author sghosh
 *
 */
class RSSettings {
	
	public static final String DEFAULT_AUDIT_TRAIL_FILE = "/tmp/as_http_api.log"
	public static final int DEFAULT_AUDIT_TRAIL_ROLLOVER_SIZE = 10
	public static final int DEFAULT_AUDIT_TRAIL_BACKUP_INDEX = 10
	public static final String DEFAULT_AUDIT_TRAIL_LEVEL = "basic"
	public static final int DEFAULT_RETRY_COUNT = 3
	// 1 minute
	public static final int DEFAULT_RETRY_INTERVAL = 30
	
	int defaultMessageTimeout;
	int trustSessionTimeout;
	String tbHostname;
	int tbPort;
	String backupTBHostname;
	int backupTBPort;
	String tbScheme;
	// These two client.properties are added here but will not be exposed in the UI until later on
	String sslKeystorePassword = "password";
	boolean enableSSL = false;
	boolean showReserved = false
	
	String auditTrailFile =  DEFAULT_AUDIT_TRAIL_FILE
	int auditTrailRolloverSize = DEFAULT_AUDIT_TRAIL_ROLLOVER_SIZE
	int maxBackupIndex = DEFAULT_AUDIT_TRAIL_BACKUP_INDEX
	String auditLogLevel = DEFAULT_AUDIT_TRAIL_LEVEL
	
	int retryCount = DEFAULT_RETRY_COUNT
	int retryInterval = DEFAULT_RETRY_INTERVAL 

	static mapping = {
		table "rs_settings"
	}

    static constraints = {
		defaultMessageTimeout(minSize:1000)
		trustSessionTimeout(minSize:1000)
		tbHostname (blank: false, nullable: false)
		tbPort(range:1..65535)
		tbScheme (inList: ["rtn", "rtns"])
		backupTBHostname (nullable: true)
		backupTBPort (nullable: true)
		auditTrailFile (blank: false)
		auditLogLevel (inList: ["basic", "detailed"])
		auditTrailRolloverSize(range:1..100000)
		maxBackupIndex(range:1..100)
		retryCount(range:1..10)
		retryInterval(range:1..300)
    }
}
