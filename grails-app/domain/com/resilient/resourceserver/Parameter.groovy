package com.resilient.resourceserver

class Parameter {

	String name
	String type
	String label
	String description
	String validationType
	String customValidation
	String picklistValues
	
	static mapping = {
		table 'rs_parameter'
	}
	
	Resource resource
	
	static belongsTo = [Resource]

    static constraints = {
		name (blank: false, nullable: false, matches:"^[a-zA-Z_][a-zA-Z0-9_\\s]*")
		type (inList: ["text", "password", "date", "boolean", "picklist"])
		label (blank:false)
		validationType (inList: ["none", "email", "phone", "currency","number","zip_code", "custom"])
		customValidation (nullable: true)
		picklistValues (nullable: true) 
		description (nullable: true)
    }
}
