package com.resilient.resourceserver

class CredentialRequestor {

	String decision
	String message
	String trustBrokerDisplayURL
	String credentialExpr
	String sessionId
	
	
	static mapping = {
		table "rs_credential_requestor"
	}

    static constraints = {
		decision (inList: ["GRANT", "DENY", "TB_DISPLAY", "EXPAND", "ERROR", "TIMEOUT"])
		message (nullable: true)
		trustBrokerDisplayURL (nullable: true)
		credentialExpr (nullable: true)
		sessionId (nullable: true)
    }
}
