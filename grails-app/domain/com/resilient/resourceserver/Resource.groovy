package com.resilient.resourceserver

import grails.converters.JSON

import java.util.Date
import java.util.List

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.BaseResource

import java.net.URLEncoder

class Resource extends BaseResource {
	

	// Style sheet max size is approx 20K, see http://dev.mysql.com/doc/refman/5.0/en/string-type-overview.html for string type lengths
	// and the justification for this choice, briefly VARCHAR max in MySQL is 65535 (64K) and with UTF-8 encoding each character can take
	// upto 3 bytes, hence this limit.
	public static int STYLE_SHEET_MAX_SIZE = 21844;
	
	String styleData

	List parameters;
	static hasMany = [parameters:Parameter]

	String trustTagType = "client_only"
	String trustTagPostGrantURI

	static constraints = {
		type (inList: ["app", "data", "noProxy"])
		displayType (inList: ["download", "display", "rawData", "redirect"])
		styleData (nullable: true, maxSize: STYLE_SHEET_MAX_SIZE)
		trustTagType (inList: ["client_only", "client_server"])
		trustTagPostGrantURI (nullable: true)
	}
	
	def String resourceURLTemplate(boolean useWrapper) {
		if (type == "noProxy") {
			return ""
		} else if (useWrapper) {
			return "/RNSResourceProxy/requestCredential.jsp?path=rest/" + type + "/" + URLEncoder.encode(name, "UTF-8")
		} else {
			return "/RNSResourceProxy/rest/" + type + "/" + URLEncoder.encode(name, "UTF-8")
		}
	}

	def typeString() {
		if (type == "app") {
			return "Application"
		} else if (type == "data") {
			return "Protecting an asset"
		} else if (type == "noProxy"){
			return "Not protecting an asset"
		}
	}
	
	def outputJSON() {
		JSONObject outputJSONObj = new JSONObject();
		outputJSONObj.put("name", name);
		outputJSONObj.put("type", type);
		outputJSONObj.put("useWrapper", useWrapper);
		if (hostname) {
			outputJSONObj.put("hostname", hostname);
		}
		if (port) {
			outputJSONObj.put("port", port);
		}
		if (path) {
			outputJSONObj.put("path", path);
		}
		if (displayType) {
			outputJSONObj.put("displayType", displayType);
		} else {
			outputJSONObj.put("displayType", "download");
		}
		if (trustTagType) {
			outputJSONObj.put("trustTagType", trustTagType)
			if (trustTagType == "client_server") {
				outputJSONObj.put("trustTagPostGrantURI", trustTagPostGrantURI ? trustTagPostGrantURI : "")
			} else {
			outputJSONObj.put("trustTagPostGrantURI", "")
			}
		} else {
			outputJSONObj.put("trustTagType", "client_only")
			outputJSONObj.put("trustTagPostGrantURI", "")
		}
		if (credentialExpr) {
			outputJSONObj.put("joinType", credentialExpr.joinType);
			outputJSONObj.put("logicalCredExpr", credentialExpr.logicalCredExpr);
			def credExprJSON = new JSON(credentialExpr);
			JSONObject credExprJSONObj = new JSONObject(credExprJSON.toString(false));
			JSONArray credArray = new JSONArray(credExprJSONObj.getJSONArray("credentialElements"));
			for (int cred = 0; cred < credArray.size(); cred++) {
				JSONObject credObj = credArray.get(cred);
				CredentialElement tempCred = new CredentialElement(name: credObj.getString("name"))
				credObj.put("ta", tempCred.trustAuthority())
			}
			outputJSONObj.put("credentials", credArray);
		} else {
			outputJSONObj.put("joinType","And");
			JSONArray credArray = new JSONArray();
			outputJSONObj.put("credentials", credArray)
		}
		JSONArray paramsArray = new JSONArray();
		if (parameters && parameters.size() > 0) {
			for (Parameter param: parameters) {
				def paramJSON = new JSON(param);
				JSONObject paramObj = new JSONObject(paramJSON.toString(false));
				paramObj.remove("resource")
				paramsArray.add(paramObj)
			}
		}
		outputJSONObj.put("parameters", paramsArray)
		return outputJSONObj.toString();
	}
	
}
