package com.resilient.resourceserver

import grails.converters.JSON

import java.util.Map

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.BaseResource
import com.resilient.as.ParameterMap
import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers

import com.resilient.tn.config.ClientConfigurator;

import com.resilient.ta.md.impl.CredentialElementMetaData;
import com.resilient.ta.md.impl.CredElmtMetaDataParameter;


class CredentialRequestorController {

	static allowedMethods = [inputCredentials: "GET", requestCredential: "POST", logout: "POST"]

	def credentialRequestorService
	def RSSettingsService

	def inputCredentials = {
		def referer = request.getHeader("referer")
		log.info("inputCredentials called by " + referer + " for [" + request.getRequestURL() + "]")
		def res = BaseResource.findByNameIlike(params.id)
		def model = null
		if (res && res.credentialExpr) {
			List<ParameterMap> requiredParams = new ArrayList<ParameterMap>()
			for (CredentialElement cred: res.credentialExpr.credentialElements) {
				CredentialElementMetaData credMD = CredentialExpressionHelpers.getCredentialElementMetaData(cred.name)
				Map<String, CredElmtMetaDataParameter> credParams = credMD.getParameters();
				for (ParameterMap credParam : cred.parameterMapping) {
					CredElmtMetaDataParameter credParamMD = credParams.get(credParam.name)
					credParam.type = credParamMD.getType()
					if (credParam.isRequired) {
						requiredParams.add(credParam)
					}
				}
			}
			// populate the parameter values from hard coded values
			// or request params or session
			// and figure out if we need to go into a requiredInput mode
			JSONArray inputsNeeded = new JSONArray();
			Map<String, ParameterMap> paramsInputNeeded = new HashMap<String, ParameterMap>()
			for (ParameterMap requiredParam : requiredParams) {
				if (!requiredParam.isStatic) {
					if (!paramsInputNeeded.get(requiredParam.value)) {
						// only add to paramsInputNeeded if the mapping request param has not already been seen
						// e.g. if there are 2 Required input params (e.g. 2 user ID params that are mapping to
						// the same request param "userid", make sure there is only one of them in the list
						paramsInputNeeded.put(requiredParam.value, requiredParam)
					}
				}
			}
			for (Map.Entry<String, ParameterMap> entry: paramsInputNeeded.entrySet()) {
				String paramName = entry.getKey();
				ParameterMap paramMap = entry.getValue()
				JSONObject paramObj = new JSONObject()
				paramObj.put("name", paramName)
				paramObj.put("label", paramMap.displayName)
				paramObj.put("type", paramMap.type)
				inputsNeeded.add(paramObj)
			}
			response.status = 200
			model = [inputsNeeded: inputsNeeded]
			def resJSON = model as JSON
			JSONObject resJSONObj = new JSONObject(resJSON.toString(false))
			log.info("inputCredentials response for [" + request.getRequestURL() + "] is: \n" + resJSONObj.toString(4))
		} else {
			log.info("inputCredentials response for [" + request.getRequestURL() + "] is: " + params.id + " Not Found.")
			model = []
			response.status = 404
		}
		render model as JSON
	}

	def requestCredential = {
		def model = []
		def referer = request.getHeader("referer")
		def logId = "[" + request.getRequestURL() + "]"
		RSSettings settings = RSSettingsService.getInstance()
		ClientConfigurator primo = RSSettingsService.getPrimaryClientConfigurator()
		try {
			JSONObject postData = request.JSON
			def sessionId = null
			if (postData.has("sessionId") && !postData.getString("sessionId").isEmpty() && !postData.getString("sessionId").equals("null")) {
				sessionId = postData.getString("sessionId")
				logId = "[" + request.getRequestURL() + "::" + sessionId + "]"
			}
			log.info("requestCredential called by " + referer + " for " + logId)
			def resource = BaseResource.findByNameIlike(params.id)
			def credentialRequestor = null
			if (resource && resource.credentialExpr) {
				if (settings.auditLogLevel == "detailed") {
					def maskedPostData = new JSONObject(postData.toString())
					// check if the parameters has password, if so then mask it!
					if (postData.has("parameters") && !postData.getString("parameters").equals("null")) {
						JSONObject parameters = new JSONObject(maskedPostData.getString("parameters"))
						if (parameters.has("password")) {
							def password = parameters.get("password")
							password = password.encodeAsMD5()
							parameters.put("password", password)
						}
						maskedPostData.put("parameters", parameters)
					}
					log.info("Request Body posted to requestCredential API for " + logId + " is: \n" + maskedPostData.toString(4))
					String credentialExpr = resource.credentialExpr();
					if (!maskedPostData.getString("parameters").equals("null")) {
						credentialExpr = CredentialExpressionHelpers.replaceParamsInCredentialExpr(credentialExpr, maskedPostData.get("parameters"))
						credentialExpr = new JSONObject(credentialExpr).toString(4)
					}
					log.info("In requestCredential for " + logId + " evaluating credential expression: \n" + credentialExpr)
				}
				def badRequest = false
				if (!(postData.has("parameters") && postData.has("callbackURL"))) {
					badRequest = true;
				}
				if (!badRequest) {
					if (sessionId) {
						credentialRequestor = CredentialRequestor.findBySessionId(sessionId)
						if (!credentialRequestor) {
							credentialRequestor = credentialRequestorService.requestCredential(resource, request, primo)
						} else {
							credentialRequestor.delete()
						}
					} else {
						credentialRequestor = credentialRequestorService.requestCredential(resource, request, primo)
					}
					if (credentialRequestor.decision == "TIMEOUT") {
						for (int i = 0; i < settings.retryCount - 1; i++) {
							log.info("In requestCredential for " + logId + " retrying evaluation of credential request after time-out\n")
							Thread.sleep(settings.retryInterval * 1000)
							credentialRequestor = credentialRequestorService.requestCredential(resource, request, primo)
							if (credentialRequestor.decision != "TIMEOUT") {
								break;
							}
						}
						// See if we have a secondary Client Configurator, if so use it
						ClientConfigurator segundo = RSSettingsService.getSecondaryClientConfigurator()
						if (segundo) {
							log.info("In requestCredential for " + logId + " retrying evaluation of credential request using Secondary Trust Broker\n")
							credentialRequestor = credentialRequestorService.requestCredential(resource, request, segundo)
							if (credentialRequestor.decision == "TIMEOUT") {
								for (int i = 0; i < settings.retryCount - 1; i++) {
									log.info("In requestCredential for " + logId + " retrying evaluation of credential request after time-out using Secondary Trust Broker\n")
									Thread.sleep(settings.retryInterval * 1000)
									credentialRequestor = credentialRequestorService.requestCredential(resource, request, segundo)
									if (credentialRequestor.decision != "TIMEOUT") {
										break;
									}
								}
							}
						}	
					}
					model = [decision: credentialRequestor.decision]
					if (credentialRequestor.decision == "GRANT") {
						model.put("sessionId", credentialRequestor.sessionId)
						if (resource.type == "data") {
							def resourceConfig = [name: resource.name, type: resource.type, hostname: resource.hostname, port: resource.port, path: resource.path, displayType: resource.displayType, styleData: resource.styleData]
							model.put("resourceConfig", resourceConfig)
						}
						response.status = 200
					} else if (credentialRequestor.decision == "TB_DISPLAY") {
						model.put("sessionId", credentialRequestor.sessionId)
						model.put("trustBrokerDisplayURL", credentialRequestor.trustBrokerDisplayURL)
						credentialRequestor.save()
						response.status = 200
					} else if (credentialRequestor.decision == "DENY") {
						response.status = 401
					} else if (credentialRequestor.decision == "TIMEOUT") {
						// I would have liked to send back 408, timed out response code, but the browser automatically performs retries
						// if the server returns 408, this makes the credential request process get into an infinite loop (or until the browser gives up)!
						response.status = 401
						model.put("message", credentialRequestor.message)
					} else if (credentialRequestor.decision == "ERROR") {
						if (credentialRequestor.message) {
							model.put("message", credentialRequestor.message)
						}
						response.status = 500
					}
				} else {
					response.status = 400
					model = [decision: "ERROR", message: "The 'parameters' and 'callbackURL' parameters must be passed in the request to requestCredential API"]
				}
			} else {
				response.status = 404
				log.info("requestCredentials response for " + logId + " is: " + params.id + " Not Found.")
				model = [decision: "ERROR", message: "The specified resource could not be found"]
			}
			def resJSON = model as JSON
			if (model.containsKey("sessionId")) {
				logId = "[" + request.getRequestURL() + "::" + model.get("sessionId") + "]"
			}
			JSONObject resJSONObj = new JSONObject(resJSON.toString(false))
			log.info("requestCredentials response for " + logId + " is: \n" + resJSONObj.toString(4))
			render model as JSON
		} catch (Exception ex) {
			response.status = 500
			model = [decision: "ERROR", message: ex.getLocalizedMessage()]
			log.info("requestCredentials response for " + logId + " is: Trust Network exception occurred. Reason: " + ex.getLocalizedMessage())
			render model as JSON
		}
	}

	/*	
	 def checkStatus = {
	 def resource = Resource.findByNameIlike(params.id)
	 def credentialRequestor = null
	 def model = []
	 if (resource && resource.credentialExpr) {
	 JSONObject postData = request.JSON
	 def tsid = null
	 if (postData.has("sessionId") && !postData.getString("sessionId").isEmpty() && !postData.getString("sessionId").equals("null")) {
	 tsid = postData.get("sessionId")
	 }
	 if (tsid) {
	 credentialRequestor = CredentialRequestor.findBySessionId(tsid)
	 if (credentialRequestor) {
	 model = [decision: credentialRequestor.decision]
	 if (credentialRequestor.decision == "GRANT") {
	 model.put("sessionId", credentialRequestor.sessionId)
	 if (resource.type == "data") {
	 def resourceConfig = [ name: resource.name, type: resource.type, hostname: resource.hostname, port: resource.port, path: resource.path, displayType: resource.displayType, styleData: resource.styleData]
	 model.put("resourceConfig", resourceConfig)
	 }
	 response.status = 200
	 } else if (credentialRequestor.decision == "DENY") {
	 response.status = 401
	 } else if (credentialRequestor.decision == "TB_DISPLAY") {
	 response.status = 400	
	 } else if (credentialRequestor.decision == "ERROR") {
	 if (credentialRequestor.message) {
	 model.put("message", credentialRequestor.message)
	 }
	 response.status = 500
	 }
	 credentialRequestor.trustBrokerDisplayURL = null
	 credentialRequestor.delete()
	 } else {
	 response.status = 404
	 }
	 } else {
	 response.status = 400
	 }
	 } else {
	 response.status = 404
	 }
	 render model as JSON
	 }
	 */

	def restart = {
		def credentialRequestor = CredentialRequestor.findBySessionId(params.tsid)
		if (!credentialRequestor) {
			credentialRequestor = new CredentialRequestor(decision: params.decision, sessionId: params.tsid)
		}
		credentialRequestor.decision = params.decision
		credentialRequestor.save()
		def callbackUrl = params.callbackUrl
		if (callbackUrl) {
			// check if the callbackURL is referencing the Resource Proxy Credential Request Wrapper
			if (callbackUrl.indexOf("/requestCredential.jsp") != -1)
			callbackUrl += callbackUrl.indexOf("?") == -1 ? "?status=checkDecision" : "&status=checkDecision"
			redirect (url: callbackUrl)
		}
	}

/*	
	 def logoutRestart = {
		 def credentialRequestor = CredentialRequestor.findBySessionId(params.tsid)
		 if (!credentialRequestor) {
			 credentialRequestor = new CredentialRequestor(decision: params.decision, sessionId: params.tsid)
		 }
		 credentialRequestor.decision = params.decision
		 credentialRequestor.save()
		 JSONObject requestBody = new JSONObject()
		 JSONObject paramsObj = new JSONObject()
		 requestBody.put("sessionId", sessionId)
		 request.setContent(requestBody.toString().getBytes())
		 forward action:"logout", params: [id: params.id]
	 }
*/
	
	def logout = {
		def model = []
		def referer = request.getHeader("referer")
		def logId = "[" + request.getRequestURL() + "]"
		RSSettings settings = RSSettingsService.getInstance()
		ClientConfigurator primo = RSSettingsService.getPrimaryClientConfigurator()
		try {
			JSONObject postData = request.JSON
			def sessionId = null
			if (postData.has("sessionId") && !postData.getString("sessionId").isEmpty() && !postData.getString("sessionId").equals("null")) {
				sessionId = postData.getString("sessionId")
				logId = "[" + request.getRequestURL() + "::" + sessionId + "]"
			}
			log.info("logout called by " + referer + " for " + logId)
			def resource = BaseResource.findByNameIlike(params.id)
			def credentialRequestor = null
			if (resource) {
				if (settings.auditLogLevel == "detailed") {
					log.info("Request Body posted to logout API for " + logId + " is: \n" + postData.toString(4))
				}
				if (sessionId && postData.has("callbackURL")) {
					credentialRequestor = CredentialRequestor.findBySessionId(sessionId)
					if (!credentialRequestor) {
						credentialRequestor = credentialRequestorService.logout(resource, request, primo)
						if (credentialRequestor.decision == "TIMEOUT") {
							ClientConfigurator segundo = RSSettingsService.getSecondaryClientConfigurator()
							if (segundo) {
								log.info("In logout for " + logId + " retrying logout using Secondary Trust Broker\n")
								credentialRequestor = credentialRequestorService.logout(resource, request, segundo)
							}
						}
					} else {
						credentialRequestor.delete()
					}
					model = [decision: credentialRequestor.decision]
					if (credentialRequestor.decision == "GRANT") {
						model.put("sessionId", credentialRequestor.sessionId)
						response.status = 200
					} else if (credentialRequestor.decision == "TB_DISPLAY") {
						model.put("sessionId", credentialRequestor.sessionId)
						model.put("trustBrokerDisplayURL", credentialRequestor.trustBrokerDisplayURL)
						credentialRequestor.save()
						response.status = 200
					} else if (credentialRequestor.decision == "DENY") {
						response.status = 401
					} else if (credentialRequestor.decision == "TIMEOUT") {
						response.status = 401
					} else if (credentialRequestor.decision == "ERROR") {
						if (credentialRequestor.message) {
							model.put("message", credentialRequestor.message)
						}
						response.status = 500
					}
				} else {
					response.status = 400
					model = [decision: "ERROR", message: "The 'sessionId' and 'callbackURL' parameters must be passed in the request to logout API"]
				}
			} else {
				response.status = 404
				model = [decision: "ERROR", message: "The specified resource could not be found"]
			}
			def resJSON = model as JSON
			JSONObject resJSONObj = new JSONObject(resJSON.toString(false))
			log.info("logout response for " + logId + " is: \n" + resJSONObj.toString(4))
			render model as JSON
		} catch (Exception ex) {
			response.status = 500
			model = [decision: "ERROR", message: ex.getLocalizedMessage()]
			log.info("logout response for " + logId + " is: Trust Network exception occurred. Reason: " + ex.getLocalizedMessage())
			render model as JSON
		}
	}
}
