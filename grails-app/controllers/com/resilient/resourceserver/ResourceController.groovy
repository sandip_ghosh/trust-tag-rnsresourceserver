package com.resilient.resourceserver

import grails.converters.JSON

import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.Reader
import java.io.StringWriter
import java.io.Writer
import java.net.HttpURLConnection

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONException
import org.codehaus.groovy.grails.web.json.JSONObject

//import org.codehaus.groovy.grails.validation.routines.UrlValidator

import com.resilient.as.BaseResource
import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.ParameterMap
import com.resilient.as.ReservedResource
import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers
import com.resilient.as.cxbuilder.util.CredentialExpressionParser

import org.apache.commons.validator.routines.DomainValidator
import org.apache.commons.validator.routines.InetAddressValidator

import com.resilient.as.relyingparty.RelyingPartyUtils;

import com.resilient.ts.util.ConfigUtils

@Mixin(RelyingPartyUtils)
class ResourceController {

	def RSSettingsService
	
    def index = { 
		_renderView("index")
	}
	
	def settings = {
		_renderView("settings")
	}
	
	def credentials = {
		_renderView("credentials")
	}
	
	def update = {
		def res = Resource.get(params.id);
		if (!res) {
			res = new Resource();
		}
		if (res) {
			try {
				if (!params.useWrapper) {
					params.useWrapper = false
				}
				def prevDisplayType = res.displayType
				res.properties = params;
				if (params.type == "noProxy") {
					res.hostname = "";
					res.path = "";
				} else if (params.type == "data"){
					def displayType = params.displayType
					if (displayType.equals("display")) {
						def okContents = ['text/xml', 'text/plain', "application/xml", "text/xsl", "text/xslt", "application/xsl", "application/xslt"]
						def xslFile = request.getFile('stylesheet')
						def contentType =  xslFile.getContentType()
						def isEmptyFile = xslFile.isEmpty()
						if (xslFile.isEmpty() && res.styleData == null) {
							res.displayType = prevDisplayType
							throw new Exception("Please specify a XSL compliant Stylesheet file for the data transformation")
//							res.errors.reject("Please specify a XSLT compliant StyleSheet file for the data transformation")
//							flash.errorObj = res;
						} else if (!xslFile.isEmpty() && !okContents.contains(xslFile.getContentType())) {
							res.displayType = prevDisplayType
							throw new Exception("The content type for the Stylesheet file is incorrect. It should be an XML document.")
//							Object[] args = new Object[1]
//							args[0] = xslFile.getName()
//							res.errors.reject("com.resilient.resourceserver.Resource.styleData.invalidType", args, "The content type for the Stylesheet file is incorrect. It should be an XML document.") ;
//							flash.errorObj = res;
						} else if (xslFile && !xslFile.isEmpty() && xslFile.getInputStream()) {
							res.styleData = _getFileContents(xslFile.getInputStream(), Resource.STYLE_SHEET_MAX_SIZE)
						}
					} else {
						res.styleData = null
//						res.trustTagType = params.trustTagType ? params.trustTagType : "client_only"
//						if (res.trustTagType.equals("client_server") && params.trustTagPostGrantURI) {
//							res.trustTagPostGrantURI = params.trustTagPostGrantURI
//						}
					}
				}
				if (params.portStr) {
					try {
						res.port = Integer.parseInt(params.portStr);
					} catch (NumberFormatException ex) {
						throw new NumberFormatException("Failed to parse the 'Port' value to a number. Please enter a numeric value");
					}
				}
				CredentialExpression credExpr = res.credentialExpr
				if (credExpr && credExpr.credentialElements && credExpr.credentialElements.size()) {
					for(CredentialElement credential : credExpr.credentialElements) {
						credential.delete();
					} 
				}
				if (credExpr) {
					credExpr.delete();
					res.credentialExpr = null;
				}
				_createCredentialsFromFormData(res, params, true);
				if (res.parameters && res.parameters.size() > 0) {
					// TODO: revisit if perf is an issue, we are deleting all existing users and (re)creating
					// new ones when we save an org.
					for(Parameter param : res.parameters) {
						param.delete()
					}
				}
				res.parameters = []
				if (params.parametersJSON) {
					JSONArray paramsArray = new JSONArray(params.parametersJSON)
					for (int i = 0; i < paramsArray.size(); i++) {
						JSONObject paramObj = paramsArray.getJSONObject(i)
						def param = new Parameter(name: paramObj.getString("name"), 
												  type: paramObj.getString("type"), 
												  label: paramObj.getString("label"), 
												  validationType: paramObj.getString("validationType")) 
						if (paramObj.has("description")) {
							param.description = paramObj.getString("description");
						}
						if (paramObj.has("picklistValues")) {
							param.picklistValues = paramObj.getString("picklistValues");
						}
						if (paramObj.has("customValidation")) {
							param.customValidation = paramObj.getString("customValidation");
						}
						res.addToParameters(param)
					}
				}
				res.containerToken = null
				if (!res.save(flush:true)) {
					flash.errorObj = res;
				}
			} catch (Exception ex) {
				Object[] args = new Object[1];
				args[0] = ex.getLocalizedMessage();
				res.errors.reject("com.resilient.resourceserver.Resource.saveException", args, "Exception occurred while attempting to save the Resource.") ;
				flash.errorObj = res;
			} finally {
				flash.selectedResourceId = params.id
			    flash.status = "validated"
				redirect(action:'index');
			}
		}
	}

	def delete = {
		def res = BaseResource.get(params.id);
		if (res) {
			CredentialExpression credExpr = res.credentialExpr
			if (credExpr.credentialElements && credExpr.credentialElements.size()) {
				for(CredentialElement credential : credExpr.credentialElements) {
					credential.delete();
				}
			}
			res.delete();
		}
		flash.status = "validated"
		redirect(action:'index');
	}

	def saveSettings = {
		try {
			RSSettings settings = RSSettingsService.getInstance()
			def hasErrors = false
			settings.tbHostname = params.tbHostname.trim()
			try {
				settings.tbPort = Integer.parseInt(params.tbPort)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.tbPort.exception");
			}
			if (params.backupTBHostname) {
				settings.backupTBHostname = params.backupTBHostname.trim()
			}
			if (params.backupTBPort) {
				try {
					settings.backupTBPort = Integer.parseInt(params.backupTBPort)
				} catch (NumberFormatException ex) {
					settings.errors.reject("com.resilient.resourceserver.Settings.backupTBPort.exception");
				}
			}
			DomainValidator domainValidator = DomainValidator.getInstance(true)
			InetAddressValidator inetAddressValidator = InetAddressValidator.getInstance()
			if (!(domainValidator.isValid(settings.tbHostname) || inetAddressValidator.isValid(settings.tbHostname))) {
				settings.errors.reject("com.resilient.resourceserver.Settings.tbHostname.exception")
			}
			if (settings.backupTBHostname) {
				if (!(domainValidator.isValid(settings.backupTBHostname) || inetAddressValidator.isValid(settings.backupTBHostname))) {
					settings.errors.reject("com.resilient.resourceserver.Settings.backupTBHostname.exception")
				}
			}
			settings.tbScheme = params.tbScheme
			try {
				settings.defaultMessageTimeout = Integer.parseInt(params.defaultMessageTimeout)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.defaultMessageTimeout.exception");
			}
			try {
				settings.trustSessionTimeout = Integer.parseInt(params.trustSessionTimeout)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.trustSessionTimeout.exception");
			}
			try {
				settings.retryCount = Integer.parseInt(params.retryCount)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.retryCount.exception");
			}
			try {
				settings.retryInterval = Integer.parseInt(params.retryInterval)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.retryInterval.exception");
			}
			settings.showReserved = Boolean.parseBoolean(params.showReserved)
			// default rolloverSize = 1MB
			// SEANOTE: Because it is stored as KB in the log, do not convert (keep the units the same as UI)
			def rolloverSize = 1000
			try {
				settings.auditTrailRolloverSize = Integer.parseInt(params.auditTrailRolloverSize)
				rolloverSize = settings.auditTrailRolloverSize
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.auditTrailRolloverSize.exception");
			}
			try {
				settings.maxBackupIndex = Integer.parseInt(params.maxBackupIndex)
			} catch (NumberFormatException ex) {
				settings.errors.reject("com.resilient.resourceserver.Settings.maxBackupIndex.exception");
			}
			settings.auditTrailFile = params.auditTrailFile
			settings.auditLogLevel = params.auditLogLevel
			def success = true
			if (settings.errors.hasErrors()) {
				flash.errorObj = settings
				success = false
			}
			if (success && !settings.save(flush:true)) {
				flash.errorObj = settings
				success = false
			}
			if (success) {
				settings = RSSettingsService.reloadSettings()
				success = RSSettingsService.restartTNSDKClient()
				if (!success) {
					settings.errors.reject("Failed to restart the Resilient Network Client listener to the requested settings, please make sure the appropriate values are set for the Resilient Network Client properties")
					flash.errorObj = settings
				}
			}
			if (success) {
				// write the audit trail settings into ${appName}-config.properties
				def appName = grails.util.Metadata.current.'app.name'
				String pfn = "${appName}-config.properties"
				String propFileFullPath = ConfigUtils.getConfigFileLocation(pfn)
				Properties prop = ConfigUtils.loadPropertiesFile(this.class, pfn)
				FileOutputStream propOutStream = null
				try {
					prop.setProperty("settings.auditTrail.logFile", settings.auditTrailFile)
					prop.setProperty("settings.auditTrail.rolloverSize", Integer.toString(rolloverSize))
					prop.setProperty("settings.auditTrail.maxBackupIndex", Integer.toString(settings.maxBackupIndex))
					prop.setProperty("settings.auditTrail.logLevel", settings.auditLogLevel)
					File propFile = new File(propFileFullPath)
					propOutStream = new FileOutputStream(propFile)
					prop.store(propOutStream, "")
				} catch (Exception e) {
					Object[] args = new Object[1];
					args[0] = e.getLocalizedMessage();
					settings.errors.reject("com.resilient.resourceserver.Settings.saveException", args, "Exception occurred while attempting to save the Settings.") ;
					flash.errorObj = settings;
				} finally {
					propOutStream.flush()
					propOutStream.close()
				}
			}
		} catch (Exception ex) {
			Object[] args = new Object[1];
			args[0] = ex.getLocalizedMessage();
			def settings = RSSettingsService.getInstance()
			settings.errors.reject("com.resilient.resourceserver.Settings.saveException", args, "Exception occurred while attempting to save the Settings.") ;
			flash.errorObj = settings;
		} finally {
			flash.status = "validated"
			redirect(action:'settings');
		}
	}

	def resourceProxyURLTemplate = {
		JSONObject result = new JSONObject();
		if (params.resourceName && params.resourceType) {
			Resource tempRes = new Resource(name: params.resourceName, type: params.resourceType)
			def apiURL = tempRes.resourceURLTemplate(false)
			def webURL = tempRes.resourceURLTemplate(true)
			result.put("apiURL", apiURL)
			result.put("webURL", webURL)
		}
		render result.toString();
	}

	// TODO: How do you extract common code into a "private" controller method?
	private void _createCredentialsFromFormData(Resource res, Map params, boolean persist) throws Exception {
		if (params.selectedCredentials) {
			res.credentialExpr = new CredentialExpression(joinType: params.joinType, logicalCredExpr: params.logicalCredExpr)
			JSONArray selCredArray = new JSONArray(params.selectedCredentials);
			for (int i = 0; i < selCredArray.size(); i++) {
				JSONObject selCredentialObj = selCredArray.get(i);
				String credName = selCredentialObj.getString("name");
				String grantingAuths = selCredentialObj.getString("grantingAuthority");
				String logicalCredName = _getLogicalCredentialName(credName, grantingAuths);
				def credential = new CredentialElement(name: credName, grantingAuthority: grantingAuths);
				if (params.paramMapping) {
					JSONArray paramMappings = new JSONArray(params.paramMapping);
					for (int cred = 0; cred < paramMappings.size(); cred++) {
						JSONObject credParamsObj = paramMappings.getJSONObject(cred);
						String paramMappingCredName = _getLogicalCredentialName(credParamsObj.getString("name"), credParamsObj.getString("grantingAuthority"));
						if (paramMappingCredName.equals(logicalCredName)) {
							String frequencyType = "";
							int maxAge = 0;
							try {
								frequencyType = credParamsObj.getString("frequencyType");
								maxAge = Integer.parseInt(credParamsObj.getString("maxAge"));
								maxAge = maxAge * 1000 * 60
							} catch (JSONException ex) {
								// the frequencyType and maxAge was not set in the credential expression builder so set these values to the default values
								frequencyType = CredentialElement.DEFAULT_FREQUENCY_TYPE;
								maxAge = CredentialElement.DEFAULT_MAX_AGE;
							} catch (NumberFormatException ex) {
								throw new NumberFormatException("Failed to parse the 'Expires After' value to a number. Please enter a numeric value");
							}
							credential.frequencyType = frequencyType;
							credential.maxAge = maxAge;
							JSONArray paramsArray = credParamsObj.getJSONArray("parameters");
							for (int param = 0; param < paramsArray.size(); param++) {
								JSONObject paramObj = paramsArray.getJSONObject(param);
								JSONObject paramMapObj = paramObj.getJSONObject("mapping");
								String paramValue = paramMapObj.getString("value") ? paramMapObj.getString("value") : ""
								boolean isRequired = paramMapObj.getBoolean("isRequired")
								String paramType = paramObj.getString("type")
								boolean isStatic = paramMapObj.getBoolean("isStatic")
								boolean historyKey = true
								if (paramType == CredentialExpressionHelpers.DATA_TOKEN_TYPE) {
									isStatic = false
									paramValue = paramObj.getString("name")
									historyKey = false;
								}
								def paramMap = new ParameterMap(name: paramObj.getString("name"), displayName: paramObj.getString("displayName"), isRequired: isRequired, isStatic: isStatic, value: paramValue, historyKey : historyKey)
								if (persist) {
									if ((paramValue.isEmpty() || paramValue.equals("null")) && isRequired && !paramType.equals(CredentialExpressionHelpers.DATA_TOKEN_TYPE)) {
										throw new Exception("The value is not specified for parameter '" + paramObj.getString("displayName") + "' that is required before evaluating the credential expression")
									}
									credential.addToParameterMapping(paramMap);
								} else {
									credential.parameterMapping.add(paramMap);
								}
							}
							break;
						}
					}
				}

				if (persist) {
					res.credentialExpr.addToCredentialElements(credential);
				} else {
					res.credentialExpr.credentialElements.add(credential);
				}
			}
		}
	} 

	private String _getLogicalCredentialName(String credName, String grantingAuths) {
		CredentialElement credElement = new CredentialElement(name: credName, grantingAuthority: grantingAuths)
		return credElement.logicalName()
	}

	private String _getFileContents(InputStream instream, int sizeLimit) {
		Writer writer = new StringWriter();
		Writer out = new BufferedWriter(writer);
		int BUF_SIZE = 4096;
		try {
			char[] tmpBuf = new char[BUF_SIZE];
			int bytesRead = 0;
			Reader reader = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
			if (sizeLimit != -1 && sizeLimit < BUF_SIZE) {
				// read in one-shot
				bytesRead = reader.read(tmpBuf);
				out.write(tmpBuf, 0, bytesRead);
				instream.close();
				instream = null;
			} else {
				int read = 0;
				while ((read = reader.read(tmpBuf)) != -1) {
					if (sizeLimit != -1 && bytesRead + read >= sizeLimit) {
						out.write(tmpBuf, 0, (int)(sizeLimit - bytesRead));
						instream.close();
						instream = null;
						break;
					}
					out.write(tmpBuf, 0, read);
					bytesRead += read;
				}
			}
		} finally {
			if (instream != null) {
				instream.close();
			}
		}
		out.flush();
		return writer.toString();
	}
	
	def _renderView(String requestor) {
		def crVars = initRelyingPartyState("__RS_ADMIN", requestor)
		def model = [:]
		model.putAll(crVars)
		def settings = RSSettingsService.getInstance()
		List filteredResources = new ArrayList()
		if (requestor == "index") {
			def resources = Resource.list()
			for (Resource resource : resources) {
				if (resource.valid()) {
					filteredResources.add(resource)
				} else {
					log.error("The Resource: " + resource.name + " is not valid, a Credential MetaData referenced in the Credential Expression is not defined.")
				}	
			}
			if (settings.showReserved) {
				def reservedResources = ReservedResource.list()
				for (ReservedResource reservedRes : reservedResources) {
					if (reservedRes.valid()) {
						filteredResources.add(reservedRes)
				} else {
					log.error("The Resource: " + filteredResources.name + " is not valid, a Credential MetaData referenced in the Credential Expression is not defined.")
				}	
				}
			}
		}
		model.put("resources", filteredResources)
		model.put("settings", settings)
		def selectedResourceId = flash.selectedResourceId ? flash.selectedResourceId : 0
		model.put("selectedResourceId", selectedResourceId)
		
		render (view: requestor, model: model)
	}
}
