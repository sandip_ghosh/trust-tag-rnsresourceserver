<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Access Server | Resources</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'dotimer.js') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Utils.js', plugin: 'cx-builder') }"></script>
 		<script type="text/javascript" src="${resource(dir: 'js', file: 'Credential.js', plugin: 'cx-builder') }"></script>
 		<script type="text/javascript" src="${resource(dir: 'js', file: 'CXBuilder.js', plugin: 'cx-builder') }"></script>
 		<script type="text/javascript" src="${resource(dir: 'js', file: 'Parameter.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Resource.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'RelyingParty.js', plugin: 'relying-party') }"></script>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/start/jquery-ui.css" type="text/css" rel="stylesheet" media="all" />
        <link href="${resource(dir: 'css', file: 'cxbuilder.css', plugin: 'cx-builder') }" type="text/css" rel="stylesheet" />
        <link href="${resource(dir: 'css', file: 'app.css') }" type="text/css" rel="stylesheet" />
    </head> 
	<script type="text/javascript">
	
	function pageCallback() {
	    var initialSelectRow = Resource.getInitialSelectRow($("#initialSelectId").val());
		var firstRow = $("#resources tr:nth-child(2)")[0];
		if (initialSelectRow) {
			Resource.selectResource(initialSelectRow);
			$(".saveNotif").show("slow");
			// add a handler to remove notification if there are any clicks
			// or remove notification after 1 minute
			var upHandler = function() {
				$(".saveNotif").hide("slow");
				$(document).unbind("mouseup", upHandler);
			};
			$(document).mouseup(upHandler);
			$.doTimeout( 60000, function() {
				$(".saveNotif").hide("slow");
			});					
		} else if (firstRow) {
			Resource.selectResource(firstRow);
		} else {
			$("#editResource").hide();
		}
	}
	
    $(document).ready(function(){

    	$("button").button();
    	$("#menu").buttonset();
		$( document ).tooltip({
			 position: {
			 	my: "center bottom-20",
			 	at: "center top",
			 	using: function( position, feedback ) {
			 		$( this ).css( position );
			 		$( "<div>" )
			 		.addClass( "arrow" )
			 		.addClass( feedback.vertical )
			 		.addClass( feedback.horizontal )
			 		.appendTo( this );
			 	}
			 }
		});

		Credential.initEditLogicalCXButton("#newResource");
		Credential.initEditLogicalCXButton("#editCredentialDlg");
		$("#newResource #grantingAuthorities").menu();
		$("#editCredentialDlg #grantingAuthorities").menu();

	    $("#credentialsTab").click(function(event) {
		    window.location = "/RNSResourceServer/resource/credentials/";
	    	Utils.preventDefault ( event );
		});
    	
	    $("#settingsTab").click(function(event) {
		    window.location = "/RNSResourceServer/resource/settings/";
	    	Utils.preventDefault ( event );
		});

		$("#new").click(function(event) {
			g_currPage = "ResourceConfig";
			Resource.clearResourceConfigProps("#newResource");
			Resource.showResourceConfigStep("#newResource");
			Credential.clearCredentials("#newResource");
			Credential.clearCredentialProps("#newResource");
			Parameter.initParameters(null);
			Parameter.showParameters("#newResource");
			$("#newResource #logicalCX").val("");
			$("#newResource #editLogicalCX").prop("disabled", true);
			$("#newResource #paramMapping").val("");
			var newResourceDlg = $('#newResource').dialog({autoOpen: false, title:'Create Resource', modal:true, 
				width:1024, minWidth:1024, height:600, minHeight:600,
				buttons:{'Cancel': function() {$(this).dialog('close'); },
					'Prev' : function() {
						g_currPage = Resource.showPrev("#newResource", g_currPage);
					},						
					'Next': function() {
						g_currPage = Resource.showNext("#newResource", g_currPage);
					}
				}});
			newResourceDlg.dialog("open");
			Utils.preventDefault ( event );
		});

		
		$("#save, #save1").click(function(event) {
			Resource.createOrUpdateResource(false);
			Utils.preventDefault ( event );
		});
			
		$("#editResource #editCredExpr").click(function(event) {
			g_currPage = "SelectCredentials";
			$("#editCredentialDlg #editJoinType option[value = '" + Resource.selectedResource.joinType + "']").prop("selected", true);
			$("#editCredentialDlg #parameterMapping").hide();
			$("#editCredentialDlg #selectCredentials").show();
			var editCredentialDlg = $('#editCredentialDlg').dialog({autoOpen: false, title:'Edit Credential Expression', modal:true, 
				width:1024, minWidth:1024, height:600, minHeight:600,
				buttons:{'Cancel': function() {$(this).dialog('close'); },
					'Prev' : function() {
						if (g_currPage == "ParameterMapping") {
							CXBuilder.showSelectCredentialsStep("#editCredentialDlg");
							g_currPage = "SelectCredentials";
						}
					},						
					'Next': function() {
						if (g_currPage == "SelectCredentials") {
							g_currPage = "ParameterMapping";
							CXBuilder.updateParameterMapping();
							CXBuilder.showParameterMappingStep("#editCredentialDlg");
						} else if (g_currPage == "ParameterMapping") {					
							CXBuilder.updateCredentialExpr();
							$(this).dialog('close');
						}				
					}
				}});
			editCredentialDlg.dialog("open");
			Utils.preventDefault ( event );
		});

		// to prevent a bubble up of event when displaying a warning dialog
		// for the Redirect URL feature, this event handle is defined here
		$("#newResource #displayTypeRedirect").click(function(event) {
			Resource.onChangeDisplayType("#newResource", "redirect", true);
			event.stopPropagation();
		});
				
		CXBuilder.basePath = $("#basePath").val();
		Credential.basePath = $("#basePath").val();
		RelyingParty.checkCredentials(pageCallback);	
    });

	</script>

	<body>
	   	<g:render template="header" model="[selected: 'resourcesTab']" />
	        <div class="sectionBody withShadow">
	        	<div class="sectionHeader sectionHeaderShadow">Resources</div> 
				<div class="buttons" style="margin-left:10px;margin-top:10px;">
					<button id="new">New</button>
				</div>
	        	<input type="hidden" name="initialSelectId" id="initialSelectId" value="${ selectedResourceId }" />
	        	<table id="resources" cellspacing="0" cellpadding="5">
	        		<tr>
	        			<th>Name</th>
	        			<th>Type</th>
	        			<th>Credentials</th>
	        			<th>Resource Proxy REST URL</th>
	        			<th>Last Updated</th>
	        			<th style="min-width:15px;"></th>
	       			</tr>
				<g:each in="${ resources }" var="resource" status="i" >
					<tr style="background-color:${ (i % 2) == 0 ? '#e9f2fe' : '#ffffff' }">
						<td>${ resource.name }</td>
						<td>${ resource.typeString() }</td>
						<td>${ resource.logicalCredExpr() }</td>
						<g:if test="resource.type != 'rnsReserved'"> 
							<td>${ resource.resourceURLTemplate(false) }</td>
						</g:if>
						<g:else>
							<td></td>
						</g:else>	
						<td><g:formatDate date="${ resource.lastUpdated }" format="dd MMM yyyy"/></td>
						<td class="delete" align="right"></td>
					</tr>
					<!--  store the credential expr and id for each row -->
					<input type="hidden" name="${ resource.name.replaceAll("\\.", "_").replaceAll("/", "_").replaceAll(" ", "_") }_id"  id="${ resource.name.replaceAll("\\.", "_").replaceAll("/", "_").replaceAll(" ", "_") }_id" value="${ resource.id}"/>
					<input type="hidden" name="${ resource.name.replaceAll("\\.", "_").replaceAll("/", "_").replaceAll(" ", "_") }_props" id="${ resource.name.replaceAll("\\.", "_").replaceAll("/", "_").replaceAll(" ", "_") }_props" value="${ resource.outputJSON().encodeAsHTML() }"/>
				</g:each>
	        	</table>
	        </div>
	        <div id="editResource">
	            <div class="sectionBody withShadow" style="margin-top:20px">
	            	<div class="sectionHeader sectionHeaderShadow">Edit Resource</div>
	            	<form action="/RNSResourceServer/resource/update" method="post" enctype="multipart/form-data">
						<div class="buttons" align="left" style="margin-left:15px;margin-top:10px;">
							<button id="save1">Save</button>
						</div>
		        		<g:render template="resourceConfig" model="[isNew:false]"/>
	            	    <input type="hidden" id="selectedCredentials" name="selectedCredentials" value="" />
	            	    <input type="hidden" name="paramMapping" id="paramMapping" value="" />
	            	    <input type="hidden" name="joinType" id="joinType" value="" />
	            	    <input type="hidden" name="logicalCredExpr" id="logicalCredExpr" value="" />
			        	<div class="sectionBody">
			        		<div class="sectionHeader">Credential Expression</div>
			        		<textarea id="savedCredExpr" readonly="readonly"></textarea>
				   			<div class="buttons" style="margin-left:20px">
				   				<button id="editCredExpr">Edit Credential Expression</button>
				   			</div>
			        	</div>
						<div id="editCredentialDlg" style="display:none">	
				        	<div id="selectCredentials">
				        		<g:render template="/cxBuilder/selectCredentials" model="[isNew:false]" plugin="cx-builder"/>
				        	</div>
				        	<div id="parameterMapping" style="display:none">
				        		<g:render template="/cxBuilder/parameterMapping" model="[isNew:false]" plugin="cx-builder"/>
				        	</div>
		        		</div>
						<div class="buttons" align="left" style="margin-left:10px;">
							<button id="save">Save</button>
						</div>
					</form>
	        	</div>	
	        </div>
	        <div id="newResource" style="display:none">
	        	<form action="/RNSResourceServer/resource/update" method="post" enctype="multipart/form-data">
		        	<div id="resourceConfig">
		        		<g:render template="resourceConfig" model="[isNew:true]"/>
		        	</div>
		        	<div id="paramConfig">
		        		<g:render template="paramConfig" model="[isNew:true]"/>
		        	</div>
		        	<div id="selectCredentials" style="display:none">
		        		<g:render template="/cxBuilder/selectCredentials" model="[isNew:true]" plugin="cx-builder"/>
		        	</div>
		        	<div id="parameterMapping" style="display:none">
		        		<g:render template="/cxBuilder/parameterMapping" model="[isNew:true]" plugin="cx-builder"/>
		        	</div>
	        	</form>
	        </div>
			<div id="credExpr" style="display:none">
				<textarea id="credExprJSON" style="height:90%;width:90%"></textarea>
			</div>
		<g:render template="footer" model="[]" />
	</body>
</html>	
