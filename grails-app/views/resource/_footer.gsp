<g:render template="/relyingParty/requestCredentialFooter" model="[]" plugin="relying-party"/>

<!-- The copyright footer --> 
<g:if test="${status != 'checkDecision' }"> 
	<g:render template="/relyingParty/requestCopyright" model="[]" plugin="relying-party"/>
</g:if>