<table cellspacing='0' cellpadding='0' width='100%'>
	<tr style="${ isNew ? 'display:block' : 'display:none'}">
		<td><div class="${selected == 'resourceConfig' ? 'stepLinkSelected' : 'stepLink'}" id="step1Link">Step 1:</div>
			<div class="helpBackground ui-icon ui-icon-help" style="margin-top:3px;" title="Specify the Resource Configurations. If protecting an asset, then specify where the asset should be retrieved from and how it should be presented."></div>
			<div class="${selected == 'resourceConfig' ? 'stepNameSelected' : 'stepName'}">Resource Configuration</div>
		</td>
	</tr>
	<tr>
		<td><div class="${selected == 'selectCredentials' ? 'stepLinkSelected' : 'stepLink'}" id="step3Link">${ isNew ? 'Step 2:' : 'Step 1:'}</div>
			<div class="helpBackground ui-icon ui-icon-help" style="margin-top:3px;" title="Select the Credential Elements and the Granting authorities that will make up the Credential expression. Also specify the structure of the credential expression in a logical form."></div>
			<div class="${selected == 'selectCredentials' ? 'stepNameSelected' : 'stepName'}">Select Credential Elements</div>
		</td>
	</tr>
	<tr>
		<td><div class="${selected == 'parameterMapping' ? 'stepLinkSelected' : 'stepLink'}" id="step4Link">${ isNew ? 'Step 3:' : 'Step 2:'}</div>
			<div class="helpBackground ui-icon ui-icon-help" style="margin-top:3px;" title="Specify how values will be supplied for the parameters of the selected Credential Elements. Also specify the duration each granted credential is good for."></div>
			<div class="${selected == 'parameterMapping' ? 'stepNameSelected' : 'stepName'}">Configure Credential Elements</div>
		</td>
	</tr>
</table> 