<g:if test="${status != 'checkDecision' }"> 
	<div class="header">
		<div class="headercellleft">
	 		<img src="${resource(dir:'images',file:'thetrust-rgb.png')}" alt="The Trust Network" width="300" height="79"/>
	 	</div>
	 	<div class="headercellmiddle">
		 	<span class="appHeader">Access Server</span>
	 	</div>
		<div class="headercellright">
			<img src="${resource(dir:'images',file:'Resilient-Logo-RGB-websafe.png')}" alt="Resilient Networks Logo" width="180" height="58" align="right"/>	   	 	
		</div>			 
	</div>
	<div id="menu" class="ui-widget-header" style="margin-top:10px;margin-right:0px;margin-bottom:20px">
		<span style="margin-left:30px"></span>
		<input type="radio" id="resourcesTab" name="radio" <%if (selected == 'resourcesTab') {  out << "checked='checked'"} %>/><label for="resourcesTab">Resources</label> 
		<div align="right" style="display:inline;margin-right:30px;float:right">
			<input type="radio" id="settingsTab" name="radio" align="right" <%if (selected == 'settingsTab') {  out << "checked='checked'"} %> /><label for="settingsTab">Settings</label>
	        <input type="radio" id="credentialsTab" name="radio" align="right" <%if (selected == 'credentialsTab') {  out << "checked='checked'"} %>/><label for="credentialsTab">Admin Access</label>	
			<button id="logout" style="display:none;">Log Out</button>
		</div>
	</div>
</g:if>	
	<div class="saveNotif" style="display:none">
		<div class="saveNotifBox">The Resource was successfully saved.</div>
	</div>
     <g:hasErrors bean="${ flash.errorObj }">
     	<div class="error" style="margin-bottom:20px">
        	<g:renderErrors bean="${ flash.errorObj }" as="list" />
        </div>
     </g:hasErrors>
     <g:render template="/relyingParty/requestCredential" model="[]" plugin="relying-party"/>
