  <input type='hidden' id='parametersJSON' name='parametersJSON' value=''/>
  <table id="navBarTable" cellspacing='0' cellpadding='0' width='100%' >
  	<tr>
  		<td class="navBar" valign="top" style="${ isNew ? 'display:block' : 'display:none' }">
			<g:render template='navBar' model='${[selected:"paramConfig", isNew:isNew]}'/>
 		</td>
 		<td valign="top" width="100%" style="margin-right:10px">
			<div class="sectionBody ${ isNew ? ' withShadow' : '' }"  style="margin-top:10px">
				<div class="sectionHeader ${ isNew ? ' sectionHeaderShadow' : '' }">Resource Parameters</div>
				<table id="parameterTable" cellspacing="0" cellpadding="5" width="100%" >
					<tr>
						<td valign="top">
				        	<table id="parameters" cellspacing="0" cellpadding="5">
				        		<tr>
					        		<th>Name</th>
					        		<th>Type</th>
					        		<th>Display Name</th>
					        		<th>Description</th>
					        		<th>Validation</th>
					        		<th></th>
				        		</tr>
				        	</table>
						</td>
					</tr>
					<tr id="topBtnBar">
						<td align="right">
							<div class="buttons" align="right">
								<button id="addParam" style="font-size:11px;">Add Parameter</button>
							</div>
						</td>
					</tr>
					<tr id="paramDetails" style="display:none;">
						<td>
					        <div class="sectionBody">
					        	<div class="sectionHeader">Parameter Details</div> 
					        	<table cellspacing="0" cellpadding="5" width="100%">
					        		<tr>
					        			<td>Type:</td>
					        			<td>
					        				<select id="paramType" name="paramType">
					        					<option value="text" selected="selected">Text</option>
					        					<option value="date" >Date</option>
					        					<option value="password" >Mask Input</option>
					        					<option value="boolean" >Boolean</option>
					        					<option value="picklist" >Picklist</option>
					        				</select><br/>
					        				<span id="picklistValuesSpan" style="display:none;margin-top:5px;margin-left:10px;">Picklist Values: <input type="text" name="picklistValues" id="picklistValues" value="" size="100" style="width:90%;"/></span>
					        			</td>
					        		</tr>
							   		<tr>
							   			<td>Parameter Name:</td>
							   			<td><input type="text" name="paramName" id="paramName" value="" size="100" style="width:90%"/>
							   		</tr>
							   		<tr>
							   			<td>Display Name:</td>
							   			<td><input type="text" name="paramLabel" id="paramLabel" value="" size="100" style="width:90%"/>
							   		</tr>
					        		<tr>
							   			<td>Description:</td>
							   			<td><textarea name="paramDesc" id="paramDesc" rows="3" cols="100" style="width:90%;height:40px;margin:0px;padding:0px"></textarea></td>
				        			</tr>
					        		<tr>
							   			<td>Validation Type:</td>
							   			<td>
					        				<select id="validationType" name="validationType">
					        					<option value="none" selected="selected" >None</option>
					        					<option value="email">Email</option>
					        					<option value="phone" >Phone</option>
					        					<option value="number" >Number</option>
					        					<option value="currency" >Currency</option>
					        					<option value="zip_code" >Zip Code</option>
					        					<option value="custom" >Custom</option>
					        				</select><br/>
					        				<span id="customValidationSpan" style="display:none;margin-top:5px;margin-left:10px;">Custom Validation Regular Expression: <input type="text" name="customValidation" id="customValidation" value="" size="100" style="width:90%;"/></span>					        				
							   			</td>
				        			</tr>
					        	</table>
					        </div>
						</td>
					</tr>
					<tr id="bottomBtnBar" style="display:none;">
						<td align="right">
							<div class="buttons" align="right">
								<button id="cancelSaveParam" style="font-size:11px;">Cancel</button>
								<button id="saveParam" style="font-size:11px;">Add Parameter</button>
							</div>
						</td>
					</tr>
				</table>			
			</div>			
		</td>
	</tr>
</table>    
			