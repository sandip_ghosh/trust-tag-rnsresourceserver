<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Access Server | Admin Access Credentials</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'dotimer.js') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Utils.js', plugin: 'cx-builder') }"></script>
 		<script type="text/javascript" src="${resource(dir: 'js', file: 'Credential.min.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Resource.min.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'RelyingParty.js', plugin: 'relying-party') }"></script>
        <link href="${resource(dir: 'css', file: 'app.css') }" type="text/css" rel="stylesheet" />
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/start/jquery-ui.css" type="text/css" rel="stylesheet" media="all" />
    </head> 
	<script type="text/javascript">

		function pageCallback() {
			var credExprFrame = document.getElementById('credExprFrame');
			if ( credExprFrame.contentWindow.document.body )			
				credExprFrame.style.height = (credExprFrame.contentWindow.document.body.scrollHeight + 10) + "px";
		}
	
	    $(document).ready(function(){
	    	$("button").button();
	    	$("#menu").buttonset();

	    	
		    $("#resourcesTab").click(function() {
			    window.location = "/RNSResourceServer/resource/index";
		    	Utils.preventDefault ( event );
			});

		    $("#settingsTab").click(function() {
			    window.location = "/RNSResourceServer/resource/settings";
		    	Utils.preventDefault ( event );
			});

		    RelyingParty.checkCredentials(pageCallback);
	    });
	</script>
	<body onload="resizeFrame(document.getElementById('credExprFrame'))">
		<script type="text/javascript">
			function resizeFrame(f) {
				f.style.height = (f.contentWindow.document.body.scrollHeight + 10) + "px";
			}
		</script>      
		<g:render template="header" model="[selected: 'credentialsTab']" />
			<p class="settingsText">Please click 'Edit Credential Expression' to configure the credential expression that will be used to authorize access of the Resource Server</p>
			<iframe id="credExprFrame" src="/RNSResourceServer/cxBuilder/editCredentialService/__RS_ADMIN?invalidateSessionOnSave=true" width="100%" style="border:none" frameborder="0" ></iframe>
		<g:render template="footer" model="[]" />
	</body>
</html>			