<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Access Server | Settings</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'dotimer.js') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Utils.js', plugin: 'cx-builder') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Resource.min.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'RelyingParty.js', plugin: 'relying-party') }"></script>
        <link href="${resource(dir: 'css', file: 'app.css') }" type="text/css" rel="stylesheet" />
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/start/jquery-ui.css" type="text/css" rel="stylesheet" media="all" />
    </head> 
	<script type="text/javascript">

		function pageCallback() {
		    if ($("#showReserved").val() === "true") {
		    	$("#showReserved").prop("checked", true);
			} else {
				$("#showReserved").prop("checked", false);
			}
			var auditLogLevel = $("#auditLogLevelSet").val();
			$("#auditLogLevel option[value='" + auditLogLevel + "']").attr("selected", "selected");
		}
		
    	$(document).ready(function(){
    		$("button").button();
	        $("#menu").buttonset();

		    $("#resourcesTab").click(function() {
			    window.location = "/RNSResourceServer/resource/index/";
		    	Utils.preventDefault ( event );
			});
	        
		    $("#credentialsTab").click(function() {
			    window.location = "/RNSResourceServer/resource/credentials/";
		    	Utils.preventDefault ( event );
			});

			$("#save").click(function(event) {
			    if ($("#showReserved").is(":checked")) {
			    	$("#showReserved").val("true");
			    } else {
				    $("#showReserved").val("false")
				}			
				$("#settingsForm").submit();
				Utils.preventDefault ( event );
			});

			RelyingParty.checkCredentials(pageCallback);			
    	});
    </script>
	<body>
 		<g:render template="header" model="[selected: 'settingsTab']" />	
 			<form action="/RNSResourceServer/resource/saveSettings" id="settingsForm">
			 	<div class="sectionBody withShadow">
			 		<div class="sectionHeader sectionHeaderShadow">Resilient Network Settings</div> 
				 	<table cellspacing="0" cellpadding="5" width="100%">
				 		<tr>
						 	<td>Trust Broker Hostname:</td>
							<td><input type="text" name="tbHostname" id="tbHostname" value="${settings.tbHostname }" size="100" style="width:90%"/>
						</tr>
				  		<tr>
				  			<td>Trust Broker Scheme:</td>
				  			<td>
				  				<select id="tbScheme" name="tbScheme">
				  					<option value="rtn" <% if (settings.tbScheme == 'rtn') { out << 'selected="selected"' } %> >RTN</option>
				  					<option value="rtns" <% if (settings.tbScheme == 'rtns') { out << 'selected="selected"' } %>>RTNS</option>
				  				</select>
				  			</td>
				  		</tr>
						<tr>
							<td>Trust Broker Port:</td>
							<td><input type="text" name="tbPort" id="tbPort" value="${settings.tbPort }" size="5"/>
						</tr>
						<tr>
							<td>Default Message Timeout (in milliseconds):</td>
							<td><input type="text" name="defaultMessageTimeout" id="defaultMessageTimeout" value="${settings.defaultMessageTimeout }" size="5"/>
						</tr>
						<tr>
							<td>Trust Session Timeout (in milliseconds):</td>
							<td><input type="text" name="trustSessionTimeout" id="trustSessionTimeout" value="${settings.trustSessionTimeout }" size="5"/>
						</tr>
						<tr>
							<td>Credential Request Timeout Retry Count:</td>
							<td><input type="text" name="retryCount" id="retryCount" value="${settings.retryCount }" size="5"/>
						</tr>
						<tr>
							<td>Credential Request Timeout Retry Interval (in seconds):</td>
							<td><input type="text" name="retryInterval" id="retryInterval" value="${settings.retryInterval }" size="5"/>
						</tr>
					</table>
				 </div>
				 <div class="sectionBody withShadow" style="margin-top:20px;">
				 	<div class="sectionHeader sectionHeaderShadow">Resource Display Settings</div> 
				 	<table cellspacing="0" cellpadding="5" width="100%">
				 		<tr>
						 	<td>Show Reserved Resources:&nbsp;&nbsp;<input type="checkbox" name="showReserved" id="showReserved" value="${settings.showReserved}"/></td>
						</tr>
					</table>
				</div>
				 <div class="sectionBody withShadow" style="margin-top:20px;">
				 	<div class="sectionHeader sectionHeaderShadow">HTTP API Audit Trail Settings</div> 
				 	<p class="settingsText">These settings will only take effect after the Access Server is re-deployed or the application server is restarted.</p>
				 	<table cellspacing="0" cellpadding="5" width="100%">
				 		<tr>
						 	<td>Log File Path:</td>
						 	<td><input type="text" name="auditTrailFile" id="auditTrailFile" value="${settings.auditTrailFile}" size="100" style="width:90%"/></td>
						</tr>
				 		<tr>
						 	<td>Log Level:</td>
						 	<td>
						 		<input type="hidden" name="auditLogLevelSet" id="auditLogLevelSet" value="${settings.auditLogLevel }"/>
		        				<select id="auditLogLevel" name="auditLogLevel">
			        				<option value="basic" >Basic</option>
		        					<option value="detailed" >Detailed</option>
		        				</select>
							</td>
						</tr>
						<tr>
							<td>Log File Rollover Size (in KB):</td>
							<td><input type="text" name="auditTrailRolloverSize" id="auditTrailRolloverSize" value="${settings.auditTrailRolloverSize }" size="5"/>
						</tr>
						<tr>
							<td>Maximum number of Rollover Files:</td>
							<td><input type="text" name="maxBackupIndex" id="maxBackupIndex" value="${settings.maxBackupIndex }" size="5"/>
						</tr>
					</table>
				</div>
				<div class="buttons" align="left" style="margin-left:50px;margin-top:10px;">
					<button id="save">Save</button>
				</div>
			</form>
		<g:render template="footer" model="[]" />
	</body>
</html>
