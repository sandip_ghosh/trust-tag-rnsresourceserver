<input type="hidden" name="id" id="id" value="" />
<input type="hidden" name="isOr" id="isOr" value="" />
<input type="hidden" name="credentialsToSet" id="credentialsToSet" value="" />
<input type="hidden" id="displayType" name="displayType" value=""/>  
<input type="hidden" id="trustTagType" name="trustTagType" value=""/>
<input type="hidden" id="trustTagPostGrantURI" name="trustTagPostGrantURI" value=""/>  
  <table id="navBarTable" cellspacing='0' cellpadding='0' width='100%' >
  	<tr>
  		<td class="navBar" valign="top" style="${ isNew ? 'display:block' : 'display:none' }">
			<g:render template='navBar' model='${[selected:"resourceConfig", isNew:isNew]}'/>
 		</td>
 		<td valign="top">
			<table id="resourceConfigTable" cellspacing="0" cellpadding="5" width="100%" style="margin-top:10px">
			  		<tr>
			  			<td style="padding-left:20px" colspan="2">
			  				Name:<input size="32" type="text" id="name" name="name" value="" style="margin-left:20px"/>
			  			</td>
			  		</tr>
	        		<tr>
	        			<td style="padding-left:20px" colspan="2">Type:
	        				<select id="resourceType" name="type" style="margin-left:20px">
		        				<option name="data" value="data" >Protecting an asset</option>
	        					<option name="noProxy" value="noProxy" >Not protecting an asset</option>
	        				</select>
	        			</td>
	        		</tr>
	        		<tr id="relyingPartyURL">
	        			<td style="padding-left:20px" width="200px">Relying Party URL:</td>
	        			<td style="margin-left:20px;"><span id="relyingPartyURI" title="The URL for requesting for the credential through the Trust Network. A Relying Party should use this URL to call the Access Server Relying Party API for requesting for the credentials of this Resource. The Relying Party must manage the interactions with the Trust Network for evaluating the credential as described in the Relying Party API Documentation."></span></td>
	        		</tr>
	        		<tr id="trustTagSettings">
						<td valign="top" colspan="2">
							<div class="sectionBody ${ isNew ? ' withShadow' : '' }" id="trustTagSection">
					        	<div class="sectionHeader  ${ isNew ? ' sectionHeaderShadow' : '' }">Trust Tag Configuration
					        	<g:if test="${isNew }">
					        		<div title="If this Resource is used in conjunction with Trust Tag the configuration for Trust Tag. Please refer to Trust Tag documentation for details." class="helpBackground ui-icon ui-icon-help" style="margin-top:2px"}></div>
					        	</g:if>	
					        	</div> 
					        	<table cellspacing="0" cellpadding="5" width="100%">
					        		<tr>
					        			<td colspan="2"><input type="radio" name="trustTagTypeRadio" id="trustTagClientOnly" value="client_only" checked="checked">Trust Enable with client side scripting only</input></td>
					        		</tr>
					        		<tr>
					        			<td colspan="2"><input type="radio" name="trustTagTypeRadio" id="trustTagClientServer" value="client_server">Trust Enable with client and server side scripting</input><br/>
					        				<span style="margin-left:20px;margin-right;10px;margin-top:10px;display:inline-block;">Server Script to POST the Granted Credential Info:</span><input type="text" name="trustTagScriptURI" id="trustTagScriptURI" size="80" style="margin-top:5px;margin-left:10px;"/>
					        			</td>
					        		</tr>
					        		<tr>
					        			<td>Trust Tag HTML:</td>
					        			<td style="margin-left:20px;"><span id="trustTagHTML" title="Include this SCRIPT tag as the first SCRIPT tag in the HEAD section of your application pages to trust enable the application"></span></td>
					        		</tr>
					        	</table>
		        			</div>
		        		</td>	
	        		</tr>
					<tr id="resourceProxyURLs">
						<td style="padding-left:20px" width="200px">Resource Proxy URLs:</td>
						<td><span id="rpURLs" style="margin-left:20px;display:inline-block;" title="The URLs for retrieving the asset through the Trust Network. Clients using the API URL will have to manage the dialog with the Trust Network that may request for additional credentials. If the Web URL is used, the request for additional credentials is handled by the Resource Proxy service."></span></td>
					</tr>
				  <tr>	  		 
					<td valign="top" colspan="2">
				        <div class="sectionBody ${ isNew ? ' withShadow' : '' }" id="assetLocation">
				        	<div class="sectionHeader  ${ isNew ? ' sectionHeaderShadow' : '' }">Asset Location
				        	<g:if test="${isNew }">
				        		<div title="The HTTP Address where the asset is located." class="helpBackground ui-icon ui-icon-help" style="margin-top:2px"}></div>
				        	</g:if>	
				        	</div> 
				        	<table cellspacing="0" cellpadding="5" width="100%">
						   		<tr>
						   			<td>Hostname:</td>
						   			<td><input type="text" name="hostname" id="hostname" value="" size="100" style="width:90%"/>
						   		</tr>
						   		<tr>
						   			<td>Port:</td>
						   			<td><input type="text" name="portStr" id="portStr" value="" size="5"/>
						   		</tr>
						   		<tr>
						   			<td>Path:</td>
						   			<td><input type="text" name="path" id="path" value="" size="100" style="width:90%" title="To specify a path with dynamic path segments, specify each dynamic segment using a variable in REST JAX-RS syntax, if a request parameter with the specified variable name is found, the value will be substituted for the variable."/>
						   		</tr>
				        	</table>
				        </div>
			        </td>
			    </tr>
				  <tr>	  		 
					<td valign="top" colspan="2">
				        <div class="sectionBody ${ isNew ? ' withShadow' : '' }" id="displaySettings" style="display:none">
				        	<div class="sectionHeader  ${ isNew ? ' sectionHeaderShadow' : '' }" >Asset Retrieval Settings
				        	<g:if test="${isNew }">
					        	<div title="Specify how the asset will be retrieved and presented to the user." class="helpBackground ui-icon ui-icon-help" style="margin-top:2px"}></div>
					        </g:if>	
				        	</div>
				        	<table cellspacing="0" cellpadding="5" width="100%">
				        		<tr>
				        			<td><input type="radio" name="displayTypeRadio" id="displayTypeRawData" value="rawData" checked="checked">Unmodified</input></td>
				        		</tr>
				        		<tr>
				        			<td><input type="radio" name="displayTypeRadio" id="displayTypeDisplay" value="display">Transform using Stylesheet (XSLT compliant style-sheet, max size 20KB)</input><br/>
				        				<input type="file" name="stylesheet" id="stylesheet" size="80" style="margin-top:5px;margin-left:10px;"/>
				        			</td>
				        		</tr>
				        		<tr>
				        			<td><input type="radio" name="displayTypeRadio" id="displayTypeDownload" value="download" checked="checked">Return URL to download to a file.</input></td>
				        		</tr>
				        		<tr>
				        			<td><input type="radio" name="displayTypeRadio" id="displayTypeRedirect" value="redirect">Return URL of the Asset.</input></td>
				        		</tr>
			        		</table>
		        		</div>
			    	</td>
		    	</tr>
			</table>
		</td>
	</tr>
</table>    
