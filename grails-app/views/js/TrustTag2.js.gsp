<%@ page contentType="text/javascript"%>
// TODO: Some bootstrap suff will happen here
/**
 * - Check in JQuery, JQueryUI and dotimer are present, if not fetch them
 * - Fetch the CSS styles that are required here
 */


(function ($) {

	// The websocket object
	var socket = null;
	var status = "inputCredentials";
	var contextId = null;
	var resourceName = null;
	var parameters = null;
	var wsURL = null;

	var wsConnect = function (wsURL) {
        if ('WebSocket' in window) {
            socket = new WebSocket(wsURL);
        } else if ('MozWebSocket' in window) {
            Chat.socket = new MozWebSocket(wsURL);
        } else {
            return false;
        }
		
        socket.onopen = function () {
        	// the onOpen handler
        };
        
        socket.onclose = function () {
        	// the onClose handler
        };
        
        socket.onmessage = function (message) {
        	if (status == "inputCredentials") {
        		var respJSON = eval('(' + message + ')');
        		if (respJSON.responseCode && respJSON.responseCode == 200) {
            		contextId = respJSON.requestContextId;
            		if (respJSON.inputsNeeded) {
    			    	$(".waiting").hide();
    			    	$("#requiredInputTable").empty();
    			    	for (i = 0; i < respJSON.inputsNeeded.length; i++) {
    			    		var inputLabel = respJSON.inputsNeeded[i].label;
    			    		var inputName = respJSON.inputsNeeded[i].name;
    			    		$("#requiredInputTable").append("<tr><td>" + inputLabel + "</td><td><input type='text' id='" + inputName + "' name='" + inputName + "' value='' size='40'/></td></tr>"); 
    			    	}
    			    	$("#requiredInput").show();
            		}
        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
        			// resource is not defined in AS, just show the page
        			$(".waiting").hide();
        			$("#pageContents").show();
        		} else {
        			showError(respJSON.message);
        		}
        	} else if (status == "requestCredentials" || status == "waitingCredentialInput") {
        		var respJSON = eval('(' + message + ')');
        		if (respJSON.responseCode && respJSON.responseCode == 200) {
        			if (respJSON.decision == "TB_DISPLAY") {
        				$('.waitingMessage').text("Waiting for Credential Input...");
        				var theCredInputDialog = $('#credentialInput').dialog({autoOpen: false, title:'Trust Broker Display', modal:true, width: 800, maxWidth: 1024, height:600, maxHeight:768});
        				status = "waitingCredentialInput";
        				respJSON.trustBrokerDisplayURL = respJSON.trustBrokerDisplayURL.replace("/?", "?");
		        		window.frames['tbDisplay'].location = respJSON.trustBrokerDisplayURL;
		        		theCredInputDialog.dialog('open');
        			} else {
        				postEvaluation(respJSON.decision);
        				if (respJSON.decision == "GRANT" && respJSON.grantGoodFor) {
        					// setup the idle timer
    						$.doTimeout( "sessionTimeout", respJSON.grantGoodFor + 1000, function() {
    							$("#pageContents").hide();
    							invalidateSession();
    						});					
    						var timeoutHandler = function() {
    							// kill the session Timout
    							$.doTimeout("sessionTimeout");
    							// start a new timeout 1 seccond after session Timeout
    							$.doTimeout( "sessionTimeout", data.timeout + 1000, function() {
        							$("#pageContents").hide();
        							invalidateSession();
    							});					
    						};
    						$(document).mouseup(timeoutHandler);
        				}
        			}
        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
        			// resource is not defined in AS, just show the page
        			$(".waiting").hide();
        			$("#pageContents").show();
        		} else {
        			showError(respJSON.message);
        		} 
        	}
        };
        return true;
	};
	
	var injectCxEvaluationMarkup = function() {
		// first insert all the contents of the body into a hidden div
		$("body").wrapAll("<div id='pageContents' style='display:none'></div>");
		// now inject the credential request markup into the div
		var crMarkup = "<div id='credentialRequestor'" +
					   		"<div id='requiredInput' style='display:none'>" +
					   			"<div class='loginBox'>" +
					   			"	<p style='margin-left:50px;'>Please enter the Credentials Required to gain access to the application</p>" +
					   			"	<div class='sectionBody' style='width:800px;'>" +
					   			"		<div class='sectionHeader sectionHeaderShadow'>Log In</div>" +
					   			"		<table id='requiredInputTable' cellspacing='0' cellpadding='5' valign='top' style='width:780px'></table>" +
					   			"	</div>" +
					   			"	<div class='buttons' style='margin-top:10px;margin-left:50px;'>" +
					   			"		<button id='requestCredentials' style='height:40px; font-size:14px; line-height:40px;'>Log In</button>" +
					   			"	</div>" +
					   		"</div>" +
					   		"<div class='waiting'><div class='waitingMessage'></div></div>" +
					   		"<div class='error' style='display:none'></div>" +
					   		"<div id='credentialInput' style='display:none'>" +
					   		"	<iframe id='tbDisplay' name='tbDisplay' frameborder=0 height='100%' width='100%'></iframe>" +
					   		"</div>" +
					   		"<div id='messageBox' style='display:none'><div id='message'></div></div>" +
					   	"</div>";	
		$("body").prepend(crMarkup);			   			
	};
	
	var getInputCredentials = function() {
		$('.waitingMessage').text("Retrieving Credential Input parameters, please wait...");
		$('.waiting').show();
		var postData = new Object();
		postData["resourceName"] = resourceName;
		var postDataStr = JSON.stringify(postData, null);
		socket.send(postDataStr);
	};
	
	var processCredentialInput = function() {
		var requiredInputs = $("#requiredInputTable input");
		var paramsObj = new Object();
		// check if the user has entered values for required input params
		var hasValues = true;
		var errorMessage = "Please enter values for the the parameters: ";
		var errParams = "";
		$.each(requiredInputs, function(i, requiredInput) {
			var inputName = $(requiredInput).attr("name");
			var inputValue = $(requiredInput).val();
			if (!inputValue) {
				hasValues = false;
				if (errParams) {
					errParams += ", ";
				}
				errParams += inputName;
			} else {
				paramsObj[inputName] = inputValue;
			}
		});
		if (!hasValues) {
			showMessageBox("Required Input Parameters Missing", errorMessage + errParams, "error");
		} else {
			parameters = JSON.stringify(paramsObj, null);
			requestCredentials();
		}	
	};
	
	var requestCredentials = function() {
		$('.waitingMessage').text("Requesting Credentials, please wait...");
		$('.waiting').show();
		status = "requestCredentials";
		var postData = new Object();
		postData["resourceName"] = resourceName;
		if (parameters) {
			postData["parameters"] = parameters;
		}
		postData["requestContextId"] = contextId;
		var postDataStr = JSON.stringify(postData, null);
		socket.send(postDataStr);
		// start the credential request timer
		$.doTimeout("credReqTimeout", 120000, function() {
			postEvaluation("TIMEOUT");
		});		
	};
	
	var postEvaluation = function(decision) {
		$(".waiting").hide();
		$('#credentialInput').dialog('close');
		$("#credentialInput").hide();
		$.doTimeout("credReqTimeout");
		if (decision == "GRANT") {
			$("#pageContents").show();
		} else if (decision == "DENY" || decision == "ERROR" || decision == "TIMEOUT") {
			showError(evaluationFailureMsg(decision));
		}
	};
	
	var showError = function(errorMsg) {
		$(".error").empty();
		$(".error").append("<p>" + errorMsg + "</p>");
		$(".error").show();
	};
	
	var evaluationFailureMsg = function(decision) {
		var msg = "";
		if (decision == "ERROR") {
			msg = "There was an error processing the credential request. Please check your configuration and try again, if this continues to occur, please contact technical support."
		} else if (decision == "DENY") {
			msg = "The requested credential was denied. You will need to gain the configured credential to use the application.";
		} else if (decision == "TIMEOUT") {
			msg = "The requested credential has timed out due to not getting a response from the Trust Broker. Please check your configuration and try again, if this continues to occur, please contact technical support."
		}
		msg += "<br/>To try again with different credentials, please <span class='changeCred'>click here</span>"
		return msg;	
	};
	
	var showMessageBox = function(title, msg, style) {
		this.setMessageBox(msg, style);
		var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:title, modal:true,
			width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
			buttons:{'OK': function() {$(this).dialog('close'); }}
		});
		confirmDlg.dialog("open");
	};

	// this is similar to logout except the trust session is not invalidated
	// will close the existing websocket connection and create a new one
	var invalidateSession = function() {
		status = "inputCredentials";
		parameters = null;
		contextId = null;
		socket.close();
		if (wsConnect(wsURL)) {
			getInputCredentials();
		}
	};

	$.fn['evaluateCredentials'] = function(options) {
		// TODO hide the current page contents
	
		if (options && options.action == "checkCredentials") {
//			injectCXEvaluationMarkup();
			
			var contextPath = location.pathname.split("/")[1];
			var theOptions = $.extend({
				// These are the defaults.
				accessServerHost: "${ request.getServerName() + ':' + request.getServerPort() }",
				resourceName: contextPath,
				contextId: null
				}, options );
			resourceName = theOptions.resourceName;
			<g:if test="${request.getScheme() == 'https' }">
				wsURL = "wss://" + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials"; 
			</g:if>
			<g:else>
				wsURL = "ws://" + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials"; 
			</g:else>
			if (wsConnect(wsURL)) {
				if (theOptions.contextId) {
					contextId = theOptions.contextId;
					status = "requestCredentials";
					requestCredentials();
				} else {
					getInputCredentials();
				}
			} else {
				showError("Your Browser does not support web sockets. Please run the application on a browser that supports websockets");
			}
		} else if (options && options.action == "processCredentialInput") {
			processCredentialInput();
		} else if (options && options.action == "invalidateSession") {
			invalidateSession();
		}
	}
	
	
} ( jQuery ));

$("#credentialRequestor #requestCredentials").live('click', function(event) {
	$.evaluateCredentials({action: "processCredentialInput"});
	
	// Prevent default does not exit on event in IE9
	if ( event.preventDefault )
		event.preventDefault()
	else 
		event.returnValue = false;
});

$(".changeCred").live("click", function() {
    $.evaluateCredentials({action: "invalidateSession"});
	return false;
});

// Document Ready event handler
$(document).ready(function() {
	$("button").button();
	$.evaluateCredentials({action: "checkCredentials"});
});