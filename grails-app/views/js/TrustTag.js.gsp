<%@ page contentType="text/javascript"%>
<%
	String callback = request.getParameter("callback");
	String resourceName = request.getParameter("resource");
	boolean hasServerScript = (request.getParameter("serverScript") != null) ? Boolean.valueOf(request.getParameter("serverScript")) : false;
	String scriptURI = request.getParameter("serverScriptURI");
%>
// TODO: Some bootstrap suff will happen here
/**
 * - Check in JQuery, JQueryUI and dotimer are present, if not fetch them
 * - Fetch the CSS styles that are required here
 */

(function(g,b,d){var c=b.head||b.getElementsByTagName("head"),D="readyState",E="onreadystatechange",F="DOMContentLoaded",G="addEventListener",H=setTimeout;
H(function(){if("item"in c){if(!c[0]){H(arguments.callee,25);return}c=c[0]}var a=b.createElement("script"),e=false;a.onload=a[E]=function(){if((a[D]&&a[D]!=="complete"&&a[D]!=="loaded")||e){return false}a.onload=a[E]=null;e=true;f()};
 
a.src="${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/LABjs/js";
 
c.insertBefore(a,c.firstChild)},0);if(b[D]==null&&b[G]){b[D]="loading";b[G](F,d=function(){b.removeEventListener(F,d,false);b[D]="complete"},false)}})(this,document);

function f(){
	$LAB
	.script('https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js').wait()
	.script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js')
	.script('${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/js/RTNModal.js')
	.script('https://raw.github.com/cowboy/jquery-dotimeout/master/jquery.ba-dotimeout.min.js')
	.wait(function() {
	    $('body').css('display', 'none');
	    
		// use JQuery to get the required CSS
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: "${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/TrustTag/css"
		}).appendTo("head");
		
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/start/jquery-ui.css"
		}).appendTo("head");
		
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: "${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/css/modal.css"
		}).appendTo("head");

		function TrustTagDef() {
		
			// The websocket object
			this.socket = null;
			this.status = "inputCredentials";
			this.contextId = null;
			this.resourceName = null;
			this.parameters = null;
			this.wsURL = null;
			this.callback = null;
			this.myModal = null;
			this.pageContents = null;
			this.hasServerScript = false;
			this.scriptURI = null;
			
			this.wsConnect = function (wsURL) {
		        if ('WebSocket' in window) {
		            this.socket = new WebSocket(wsURL);
		        } else if ('MozWebSocket' in window) {
		            this.socket = new MozWebSocket(wsURL);
		        } else {
		            return false;
		        }
				
				var theThis = this;
		        this.socket.onopen = function () {
		        	// the onOpen handler
		        	console.log("WebSocket connection opened");
		        	if (theThis.status != "ConnectFailed") {
		        		if (theThis.contextId != null) {
							theThis.status = "requestCredentials";
							theThis.requestCredentials();
						} else {
							theThis.getInputCredentials();
						}
					}
		        };
		        
		        this.socket.onclose = function () {
		        	// the onClose handler
		        	console.log("WebSocket connection closed");
		        };
		        
		        this.socket.onmessage = function (message) {
		        	var respJSON = eval('(' + message.data + ')');
		        	if (theThis.status == "inputCredentials") {
		        		if (respJSON.responseCode && respJSON.responseCode == 200) {
		            		theThis.contextId = respJSON.requestContextId;
		            		if (respJSON.inputsNeeded) {
		            			var credentialInput = '<form id="rnsForm" data-handler="handleCredentialSubmit" method="post" style="padding: 0; margin: 0;"><p style="margin: 0; padding: 0 0 15px 0;">Please provide the credential identifiers:</p>';
		    			    	for (i = 0; i < respJSON.inputsNeeded.length; i++) {
		    			    		var inputLabel = respJSON.inputsNeeded[i].label;
		    			    		var inputName = respJSON.inputsNeeded[i].name;
		    			    		credentialInput += '<input class="rns" id="' + inputName + '" name="' + inputName + '" tabindex="1" type="email" placeholder="' + inputLabel + '" autocomplete="off" autofocus required/>';
		    			    	}	
		    			    	credentialInput += '</form>';
		    			    	theThis.showCredentialInputForm(credentialInput);
		            		}
		        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
		        			// resource is not defined in AS, just show the page
//		        			$("#pageContents").show();
		        			theThis.postEvaluation("GRANT");
		        		} else {
		        			theThis.showError(respJSON.message);
		        		}
		        	} else if (theThis.status == "requestCredentials" || TrustTag.status == "waitingCredentialInput") {
		        		if (respJSON.responseCode && respJSON.responseCode == 200) {
		        			if (respJSON.decision == "TB_DISPLAY") {
		        				setTimeout(function() {
			        				theThis.myModal.hide();
			        				var theCredInputDialog = $('#credentialInput').dialog({autoOpen: false, title:'Trust Broker Display', modal:true, width: 800, maxWidth: 1024, height:600, maxHeight:768});
			        				theThis.status = "waitingCredentialInput";
			        				respJSON.trustBrokerDisplayURL = respJSON.trustBrokerDisplayURL.replace("/?", "?");
					        		window.frames['tbDisplay'].location = respJSON.trustBrokerDisplayURL;
					        		theCredInputDialog.dialog('open');
		        				}, 1000);
		        			} else {
		        				theThis.postEvaluation(respJSON.decision, respJSON.sessionId, respJSON.grantGoodFor);
/*		        				
		        				if (respJSON.decision == "GRANT" && respJSON.grantGoodFor) {
		        					// setup the idle timer
		    						$.doTimeout( "sessionTimeout", respJSON.grantGoodFor + 1000, function() {
		    							$("#pageContents").hide();
		    							theThis.invalidateSession();
		    						});					
		    						var timeoutHandler = function() {
		    							// kill the session Timout
		    							$.doTimeout("sessionTimeout");
		    							// start a new timeout 1 seccond after session Timeout
		    							$.doTimeout( "sessionTimeout", data.timeout + 1000, function() {
		        							$("#pageContents").hide();
		        							theThis.invalidateSession();
		    							});					
		    						};
		    						$(document).mouseup(timeoutHandler);
		        				}
*/		        				
		        			}
		        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
		        			// resource is not defined in AS, just show the page
		        			theThis.postEvaluation("GRANT");
//		        			$(".waiting").hide();
//		        			$("#pageContents").show();
		        		} else {
		        			theThis.showError(respJSON.message);
		        		} 
		        	}
		        };
		        return true;
			};
			
			this.injectCXEvaluationMarkup = function() {
				// first insert all the contents of the body into a hidden div
//				$("body").children().wrapAll("<div id='pageContents' style='display:none'></div>");
				if (!this.hasServerScript) {
					this.pageContents = $("body").children().detach();
				}	
				// now inject the credential request markup into the div
				var crMarkup = "<div id='credentialRequestor'>" +
				                    "<div style='height:50px'></div>" +
							   		"<div class='waiting'><div class='waitingMessage'></div></div>" +
							   		"<div class='error' style='display:none'></div>" +
							   		"<div id='credentialInput' style='display:none'>" +
							   		"	<iframe id='tbDisplay' name='tbDisplay' frameborder=0 height='100%' width='100%'></iframe>" +
							   		"</div>" +
							   		"<div id='messageBox' style='display:none'><div id='message'></div></div>" +
							   		"<iframe name='logoutFrame' style='width:0px;height:0px;border:0px;'></iframe>";
							   		if (this.hasServerScript) {
					crMarkup +=	   		"<form id='postCredentials' action='' method='post'>" +
								   		"	<input type='hidden' name='sessionId' id='sessionId' value=''/>" +	
								   		"	<input type='hidden' name='parameters' id='parameters' value=''/>" +
								   		"	<input type='hidden' name='expires' id='expires' value=''/>" +	
								   		"</form>";
								   	}
					crMarkup += 			   		
//							   		"<iframe id='logoutFrame' name='logoutFrame' frameborder=0 height='100%' width='100%' style='display:none'>" +
//							   		"	<form action='' target='logoutFrame'></form>" +
//							   		"</iframe>" +
							   	"</div>";	
				$("body").append(crMarkup);
				$('body').css('display', 'block');		
				$("button").button();	
				this.myModal = new RTNModal();   			
			};
			
			this.getInputCredentials = function() {
				var self = this;
				
				this.myModal.show(false, function() {
		          	self.myModal.setStatusTo('success', 'CREDENTIAL IDENTIFIERS');
		          	self.myModal.update(function () {
		            	self.myModal.showContent(function () {
		              		self.myModal.showSpinner();
		            	});
		          	});
		        });
				var postData = new Object();
				postData["resourceName"] = this.resourceName;
				var postDataStr = JSON.stringify(postData, null);
				this.socket.send(postDataStr);
			};
			
			this.processCredentialInput = function() {
				var requiredInputs = $("#rnsForm input");
				var paramsObj = new Object();
				// check if the user has entered values for required input params
				var hasValues = true;
				var errorMessage = "Please enter values for the the parameters: ";
				var errParams = "";
				$.each(requiredInputs, function(i, requiredInput) {
					var inputName = $(requiredInput).attr("name");
					var inputValue = $(requiredInput).val();
					if (!inputValue) {
						hasValues = false;
						if (errParams) {
							errParams += ", ";
						}
						errParams += inputName;
					} else {
						paramsObj[inputName] = inputValue;
					}
				});
				if (!hasValues) {
					showMessageBox("Required Input Parameters Missing", errorMessage + errParams, "error");
				} else {
					this.parameters = paramsObj;
				}	
			};
			
			this.requestCredentials = function() {
				this.status = "requestCredentials";
				var postData = new Object();
				postData["resourceName"] = this.resourceName;
				if (TrustTag.parameters) {
					postData["parameters"] = this.parameters;
				}
				postData["requestContextId"] = this.contextId;
				var postDataStr = JSON.stringify(postData, null);
				this.socket.send(postDataStr);
				// start the credential request timer
				$.doTimeout("credReqTimeout", 120000, function() {
					this.postEvaluation("TIMEOUT");
				});		
			};
			
			this.postEvaluation = function(decision, sessionId, expiry) {
				$(".waiting").hide();
				$('#credentialInput').dialog('close');
				$("#credentialInput").hide();
				$.doTimeout("credReqTimeout");
				if (decision == "GRANT") {
					var params = "";
					for (var key in this.parameters) {
  						if (this.parameters.hasOwnProperty(key)) {
  						    if (params.length) {
  						    	params += ", ";
  						    }
    						params += key + " : " + this.parameters[key];
  						}
					}
					if (!this.hasServerScript) {
						this.myModal.showHeaderBar(params);
						if (this.pageContents) {
							this.pageContents.appendTo("body");
						}
						// $("#pageContents").show();
						if (this.callback && $.isFunction(window[this.callback])) {
							window[this.callback].call(window);
						}
					} else {
						if (this.scriptURI) {
							$('#credentialRequestor #postCredentials').attr('action', this.scriptURI);
							$('#credentialRequestor #postCredentials #parameters').val(params);
							if (sessionId) {
								$('#credentialRequestor #postCredentials #sessionId').val(sessionId);
							}
							if (expiry) {
								$('#credentialRequestor #postCredentials #expires').val(expiry);
							}
							$('#credentialRequestor #postCredentials').submit();
						}
					}	
				} else if (decision == "DENY" || decision == "ERROR" || decision == "TIMEOUT") {
					this.showError(evaluationFailureMsg(decision));
				}
			};
			
			this.showError = function(errorMsg) {
				$(".waiting").hide();
				$('#credentialInput').dialog('close');
				$("#credentialInput").hide();
				$(".error").empty();
				$(".error").append("<p>" + errorMsg + "</p>");
				$(".error").show();
			};
			
			this.evaluationFailureMsg = function(decision) {
				var msg = "";
				if (decision == "ERROR") {
					msg = "There was an error processing the credential request. Please check your configuration and try again, if this continues to occur, please contact technical support."
				} else if (decision == "DENY") {
					msg = "The requested credential was denied. You will need to gain the configured credential to use the application.";
				} else if (decision == "TIMEOUT") {
					msg = "The requested credential has timed out due to not getting a response from the Trust Broker. Please check your configuration and try again, if this continues to occur, please contact technical support."
				}
				msg += "<br/>To try again with different credentials, please <span class='changeCred'>click here</span>"
				return msg;	
			};
			
			this.showMessageBox = function(title, msg, style) {
				$("#credentialRequestor #message").text(msg);
				if ($("#credentialRequestor #message").hasClass("error")) {
					$("#credentialRequestor #message").removeClass("error");
				} else if ($("#credentialRequestor #message").hasClass("warning")) {
					$("#credentialRequestor #message").removeClass("warning");
				} else if ($("#credentialRequestor #message").hasClass("success")) {
					$("#credentialRequestor #message").removeClass("success");
				}
				$("#credentialRequestor #message").addClass(style);
				var confirmDlg = $('#credentialRequestor #messageBox').dialog({autoOpen: false, title:title, modal:true,
					width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
					buttons:{'OK': function() {$(this).dialog('close'); }}
				});
				confirmDlg.dialog("open");
			};
		
			// this is similar to logout except the trust session is not invalidated
			// will close the existing websocket connection and create a new one
			this.invalidateSession = function() {
				this.status = "inputCredentials";
				this.parameters = null;
				this.contextId = null;
				this.socket.close();
				if (this.wsConnect(TrustTag.wsURL)) {
					this.getInputCredentials();
				}
			};

			this.showCredentialInputForm = function(credentialInputForm) {
	            var self = this;
	            setTimeout(function() {
		            self.myModal.hideContent(function () {
		            	self.myModal.settings.height = 250;
		            	self.myModal.settings.width = 470;
		            	self.myModal.settings.borderRadius = 3;
		            	self.myModal.settings.bg = '#fff';
		              
		              	// update content
		            	self.myModal.settings.contentColor = '#000';
		            	self.myModal.updateModalContentStyle();
		            	self.myModal.changeContentTo(credentialInputForm);
		              
		              	setTimeout(function () {
		                	var form = document.getElementById('rnsForm');
		                	form.addEventListener("submit", function (e) {
		                  		e.preventDefault();
		                  		e.returnValue = false;
		                  		return false;
		                	}, false);
		              	}, 1);
		              
		              	self.myModal.hideSpinner();
		              	self.myModal.showHeader();
		              	self.myModal.showFooter();
		              	self.myModal.setContentDimensionsByModal();
		              	self.myModal.update(function () {
		                	self.addCredentialFormEvents();
		                	self.myModal.showContent();
		                	self.myModal.updateModalContentStyle();
		              	});
		        	});      
	            }, 1000);
			};

	        this.addCredentialFormEvents = function () {

	        	var self = this;
		        // handle return click
		        document.addEventListener('keyup', function (e) {
	        
		            var formErrors = document.querySelectorAll(':invalid'); 
		            var	submitButton = document.getElementById('rnsFormSubmit'); 
		            var form = document.getElementById('rnsForm');
//		            var	formHandler = form.getAttribute('data-handler');
	          
	          		if (e.keyCode===13) {
	            		if (formErrors.length===0) {
	            			self.handleCredentialSubmit();
	            	        e.preventDefault();
	            		} else {
	              			console.log('form errors', formErrors);
	            		}
	          		} else {
	            		if (formErrors.length===0) {
	              			submitButton.removeClass('disabled');
	            		} else {
	              			if (!submitButton.hasClass('disabled')) {
	                			submitButton.addClass('disabled');
	              			}
	            		}
	            
	          		}
	          
	        	}, true);
		        
	      };  	

	      this.handleCredentialSubmit = function () {
	      	this.processCredentialInput();
			this.showSpinner('REQUESTING CREDENTIALS');
			this.requestCredentials();
	      };

		  this.showSpinner = function(message) {
	        this.myModal.settings.height = 200;
	        this.myModal.settings.width = 200;
	        this.myModal.settings.borderRadius = 100;
	        this.myModal.settings.bg = '#000';
	        this.myModal.settings.contentColor = '#fff';
	        var self = this;
	        self.myModal.hideContent(function() {
	        	self.myModal.hideHeader();
	          	self.myModal.hideFooter();
	          	self.myModal.setStatusTo('success', message);
	          	self.myModal.setContentDimensionsByModal();
	          	self.myModal.updateModalContentStyle();
	          	self.myModal.update(function () {
	            	self.myModal.showContent(function () {
	              		self.myModal.showSpinner();
	            	});
	          	});
	        });
		  };
		  	      
	      this.initState = function(checkStateURL) {
				var theThis = this;
				$.ajax({
				       type: 'GET',
				        url: checkStateURL,
				        async: false,
				        contentType: "application/json",
				        dataType: 'jsonp',
				        jsonpCallback: 'respJSON',		
				        success:
			   			function(data) {
			  				if (data) {
			  					theThis.status = data.status;
			  					if (theThis.status === "GRANTED") {
			  						if (data.parameters) {
			  							theThis.parameters = data.parameters;
			  						}
			  						theThis.postEvaluation("GRANT");
			  					} else {
				  					if (data.sessionId) {
				  						theThis.wsURL += "?sessionId=" + data.sessionId;
				  					}
				  					if (data.parameters) {
				  						var paramsStr = JSON.stringify(data.parameters, null);
				  						theThis.wsURL += (theThis.wsURL.indexOf("?") != -1 ? "&parameters=" : "?parameters=") + paramsStr;
				  					} else if (theThis.parameters) {
				  						var paramsStr = JSON.stringify(theThis.parameters, null);
				  						theThis.wsURL += (theThis.wsURL.indexOf("?") != -1 ? "&parameters=" : "?parameters=") + paramsStr;
				  						theThis.status = "requestCredentials";
				  					}
									if (!theThis.wsConnect(theThis.wsURL)) {
										theThis.showError("Your Browser does not support web sockets. Please run the application on a browser that supports websockets");
										theThis.status = "ConnectFailed";
									}
								}									
			  				}
			  			}
			  		});
			  				
			};

			this.logout = function(logoutURL) {
				this.showSpinner('LOGGING OUT');
				// do the get in the hidden iframe
				$("#logoutFrame").location = logoutURL;
//				$("#logoutFrame form").attr('action', logoutURL);
//				$("#logoutFrame form").submit();
//				setTimeout(function() {
//					window.location.reload();
//				}, 1000); 
				
/*
				$.get(logoutURL, 
					function(data) {
						window.location.reload();
					}
				);
*/				
				$.ajax({
				       type: 'GET',
				        url: logoutURL,
				        async: false,
				        contentType: "text/html",
				        dataType: 'jsonp',
				        jsonpCallback: 'respJSON',		
				        success:
			   			function(data) {
			  				if (data) {
			  					if (data.status == "SUCCESS") {
			  						window.location.reload();
			  					}
			  				}
			  			}		
			  		});
			};
		
			this.evaluateCredentials = function(options) {
				
				<g:if test="${request.getScheme() == 'https' }">
					var httpScheme = "https://"
					var wsScheme = "wss://";
				</g:if>
				<g:else>
					var httpScheme = "http://"
					var wsScheme = "ws://";
				</g:else>
			
				if (options && options.action == "checkCredentials") {
					this.hasServerScript = options.hasServerScript;
					if (options.scriptURI) {
						this.scriptURI = options.scriptURI;
					}
					this.injectCXEvaluationMarkup();
					
					var contextPath = location.pathname.split("/")[1];
					var theOptions = $.extend({
						// These are the defaults.
						accessServerHost: "${ request.getServerName() + ':' + request.getServerPort() }",
						resourceName: contextPath,
						}, options );
					this.resourceName = theOptions.resourceName;
					if (options.callback) {
						this.callback = options.callback;
					}
					// if parameters is in the query string then extract it into the URL
					// expect the parameters can be converted to a JSON object
					var qString = location.search.substring(1);
					var params = qString.split("&");
					var status = null;
					if (params) {
						for(var i = 0; i < params.length; i++) {
							var paramName = params[i].split("=")[0];
							var paramValue = params[i].split("=")[1];
							if (paramName === "parameters") {
								this.parameters = eval('(' + unescape(paramValue) + ')');
								break;
							}
						}
					}

					this.wsURL = wsScheme + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials";
					if (!this.hasServerScript) {
						var initStateURL = httpScheme + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials/checkDecision?resourceName=" + this.resourceName + "&action=checkStatus";
						this.initState(initStateURL);
					} else {
						if (!this.wsConnect(this.wsURL)) {
							this.showError("Your Browser does not support web sockets. Please run the application on a browser that supports websockets");
							this.status = "ConnectFailed";
						}
					}					 
				} else if (options && options.action == "processCredentialInput") {
					this.handleCredentialSubmit();
				} else if (options && options.action == "invalidateSession") {
					this.invalidateSession();
				} 
				else if (options && options.action == "logout") {
					var logoutURL = httpScheme + "${ request.getServerName() + ':' + request.getServerPort() }" + "/RNSResourceServer/evaluateCredentials/checkDecision?resourceName=" + this.resourceName + "&action=logout";
					this.logout(logoutURL);
				}
			};
			
		};
		

		$(".changeCred").live("click", function() {
		    TrustTag.evaluateCredentials({action: "invalidateSession"});
			return false;
		});

		$("#tnModalFooter #rnsFormSubmit").live('click', function(e) {
			TrustTag.evaluateCredentials({action: "processCredentialInput"});
			if ( e.preventDefault )
				e.preventDefault()
			else 
				e.returnValue = false;
		});

		$("#tnHeaderBar #rnsLogout").live('click', function(e) {
			TrustTag.evaluateCredentials({action: "logout"});
			if ( e.preventDefault )
				e.preventDefault()
			else 
				e.returnValue = false;
		});
		
		// Document Ready event handler
		$(document).ready(function() {

			TrustTag = new TrustTagDef();

			TrustTag.evaluateCredentials({action: 'checkCredentials'
			<g:if test="${callback != null }">
				, callback: '${callback}'
			</g:if>
			<g:if test="${resourceName != null }">
				, resourceName: '${resourceName}'
			</g:if>	
				, hasServerScript: ${ hasServerScript }
			<g:if test="${ scriptURI != null}">
				, scriptURI : '${scriptURI}'
			</g:if>			
			});
		});	
		
		
	});
}


