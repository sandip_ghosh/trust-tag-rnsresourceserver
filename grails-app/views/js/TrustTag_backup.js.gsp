<%@ page contentType="text/javascript"%>
<%
	String callback = request.getParameter("callback");
	String resourceName = request.getParameter("resource");
%>
// TODO: Some bootstrap suff will happen here
/**
 * - Check in JQuery, JQueryUI and dotimer are present, if not fetch them
 * - Fetch the CSS styles that are required here
 */

(function(g,b,d){var c=b.head||b.getElementsByTagName("head"),D="readyState",E="onreadystatechange",F="DOMContentLoaded",G="addEventListener",H=setTimeout;
H(function(){if("item"in c){if(!c[0]){H(arguments.callee,25);return}c=c[0]}var a=b.createElement("script"),e=false;a.onload=a[E]=function(){if((a[D]&&a[D]!=="complete"&&a[D]!=="loaded")||e){return false}a.onload=a[E]=null;e=true;f()};
 
a.src="${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/LABjs/js";
 
c.insertBefore(a,c.firstChild)},0);if(b[D]==null&&b[G]){b[D]="loading";b[G](F,d=function(){b.removeEventListener(F,d,false);b[D]="complete"},false)}})(this,document);

function f(){
	$LAB
	.script('https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js').wait()
	.script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js')
	.script('https://raw.github.com/cowboy/jquery-dotimeout/master/jquery.ba-dotimeout.min.js')
	.wait(function() {
	    $('body').css('display', 'none');
	    
		// use JQuery to get the required CSS
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: "${ request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort() }/RNSResourceServer/relyingParty/css"
		}).appendTo("head");
		
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/start/jquery-ui.css"
		}).appendTo("head");
		
		function RelyingPartyDef() {
		
			// The websocket object
			this.socket = null;
			this.status = "inputCredentials";
			this.contextId = null;
			this.resourceName = null;
			this.parameters = null;
			this.wsURL = null;
			this.callback = null;
			
			this.wsConnect = function (wsURL) {
		        if ('WebSocket' in window) {
		            this.socket = new WebSocket(wsURL);
		        } else if ('MozWebSocket' in window) {
		            this.socket = new MozWebSocket(wsURL);
		        } else {
		            return false;
		        }
				
				var theThis = this;
		        this.socket.onopen = function () {
		        	// the onOpen handler
		        	console.log("WebSocket connection opened");
		        	if (theThis.status != "ConnectFailed") {
		        		if (theThis.contextId != null) {
							theThis.status = "requestCredentials";
							theThis.requestCredentials();
						} else {
							theThis.getInputCredentials();
						}
					}
		        };
		        
		        this.socket.onclose = function () {
		        	// the onClose handler
		        	console.log("WebSocket connection closed");
		        };
		        
		        this.socket.onmessage = function (message) {
		        	var respJSON = eval('(' + message.data + ')');
		        	if (theThis.status == "inputCredentials") {
		        		if (respJSON.responseCode && respJSON.responseCode == 200) {
		            		theThis.contextId = respJSON.requestContextId;
		            		if (respJSON.inputsNeeded) {
		    			    	$(".waiting").hide();
		    			    	$("#requiredInputTable").empty();
		    			    	for (i = 0; i < respJSON.inputsNeeded.length; i++) {
		    			    		var inputLabel = respJSON.inputsNeeded[i].label;
		    			    		var inputName = respJSON.inputsNeeded[i].name;
		    			    		$("#requiredInputTable").append("<tr><td>" + inputLabel + "</td><td><input type='text' id='" + inputName + "' name='" + inputName + "' value='' size='40'/></td></tr>"); 
		    			    	}
		    			    	$("#requiredInput").show();
		            		}
		        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
		        			// resource is not defined in AS, just show the page
		        			$(".waiting").hide();
		        			$("#pageContents").show();
		        		} else {
		        			theThis.showError(respJSON.message);
		        		}
		        	} else if (theThis.status == "requestCredentials" || RelyingParty.status == "waitingCredentialInput") {
		        		if (respJSON.responseCode && respJSON.responseCode == 200) {
		        			if (respJSON.decision == "TB_DISPLAY") {
		        				$('.waitingMessage').text("Waiting for Credential Input...");
		        				var theCredInputDialog = $('#credentialInput').dialog({autoOpen: false, title:'Trust Broker Display', modal:true, width: 800, maxWidth: 1024, height:600, maxHeight:768});
		        				theThis.status = "waitingCredentialInput";
		        				respJSON.trustBrokerDisplayURL = respJSON.trustBrokerDisplayURL.replace("/?", "?");
				        		window.frames['tbDisplay'].location = respJSON.trustBrokerDisplayURL;
				        		theCredInputDialog.dialog('open');
		        			} else {
		        				theThis.postEvaluation(respJSON.decision);
		        				if (respJSON.decision == "GRANT" && respJSON.grantGoodFor) {
		        					// setup the idle timer
		    						$.doTimeout( "sessionTimeout", respJSON.grantGoodFor + 1000, function() {
		    							$("#pageContents").hide();
		    							theThis.invalidateSession();
		    						});					
		    						var timeoutHandler = function() {
		    							// kill the session Timout
		    							$.doTimeout("sessionTimeout");
		    							// start a new timeout 1 seccond after session Timeout
		    							$.doTimeout( "sessionTimeout", data.timeout + 1000, function() {
		        							$("#pageContents").hide();
		        							theThis.invalidateSession();
		    							});					
		    						};
		    						$(document).mouseup(timeoutHandler);
		        				}
		        			}
		        		} else if (respJSON.responseCode && respJSON.responseCode == 404) {
		        			// resource is not defined in AS, just show the page
		        			$(".waiting").hide();
		        			$("#pageContents").show();
		        		} else {
		        			theThis.showError(respJSON.message);
		        		} 
		        	}
		        };
		        return true;
			};
			
			this.injectCXEvaluationMarkup = function() {
				// first insert all the contents of the body into a hidden div
				$("body").children().wrapAll("<div id='pageContents' style='display:none'></div>");
				// now inject the credential request markup into the div
				var crMarkup = "<div id='credentialRequestor'>" +
				                    "<div style='height:50px'></div>" +
							   		"<div id='requiredInput' style='display:none'>" +
							   			"<div class='loginBox'>" +
							   			"	<p style='margin-left:50px;'>Please enter the Credentials Required to gain access to the application</p>" +
							   			"	<div class='sectionBody' style='width:800px;'>" +
							   			"		<div class='sectionHeader sectionHeaderShadow'>Log In</div>" +
							   			"		<table id='requiredInputTable' cellspacing='0' cellpadding='5' valign='top' style='width:780px'></table>" +
							   			"	</div>" +
							   			"	<div class='buttons' style='margin-top:10px;margin-left:50px;'>" +
							   			"		<button id='requestCredentials' style='height:40px; font-size:14px; line-height:40px;'>Log In</button>" +
							   			"	</div>" +
							   			"</div>" +
							   		"</div>" +
							   		"<div class='waiting'><div class='waitingMessage'></div></div>" +
							   		"<div class='error' style='display:none'></div>" +
							   		"<div id='credentialInput' style='display:none'>" +
							   		"	<iframe id='tbDisplay' name='tbDisplay' frameborder=0 height='100%' width='100%'></iframe>" +
							   		"</div>" +
							   		"<div id='messageBox' style='display:none'><div id='message'></div></div>" +
							   	"</div>";	
				$("body").append(crMarkup);
				$('body').css('display', 'block');		
				$("button").button();	   			
			};
			
			this.getInputCredentials = function() {
				var credElement = $("#credentialRequestor");
				$('.waitingMessage').text("Retrieving Credential Input parameters, please wait...");
				$('.waiting').show();
				var postData = new Object();
				postData["resourceName"] = this.resourceName;
				var postDataStr = JSON.stringify(postData, null);
				this.socket.send(postDataStr);
			};
			
			this.processCredentialInput = function() {
				var requiredInputs = $("#requiredInputTable input");
				var paramsObj = new Object();
				// check if the user has entered values for required input params
				var hasValues = true;
				var errorMessage = "Please enter values for the the parameters: ";
				var errParams = "";
				$.each(requiredInputs, function(i, requiredInput) {
					var inputName = $(requiredInput).attr("name");
					var inputValue = $(requiredInput).val();
					if (!inputValue) {
						hasValues = false;
						if (errParams) {
							errParams += ", ";
						}
						errParams += inputName;
					} else {
						paramsObj[inputName] = inputValue;
					}
				});
				if (!hasValues) {
					showMessageBox("Required Input Parameters Missing", errorMessage + errParams, "error");
				} else {
					this.parameters = paramsObj;
					this.requestCredentials();
				}	
			};
			
			this.requestCredentials = function() {
			    $('#requiredInput').hide();
				$('.waitingMessage').text("Requesting Credentials, please wait...");
				$('.waiting').show();
				this.status = "requestCredentials";
				var postData = new Object();
				postData["resourceName"] = this.resourceName;
				if (RelyingParty.parameters) {
					postData["parameters"] = this.parameters;
				}
				postData["requestContextId"] = this.contextId;
				var postDataStr = JSON.stringify(postData, null);
				this.socket.send(postDataStr);
				// start the credential request timer
				$.doTimeout("credReqTimeout", 120000, function() {
					this.postEvaluation("TIMEOUT");
				});		
			};
			
			this.postEvaluation = function(decision) {
				$(".waiting").hide();
				$('#credentialInput').dialog('close');
				$("#credentialInput").hide();
				$.doTimeout("credReqTimeout");
				if (decision == "GRANT") {
					$("#pageContents").show();
					if (this.callback && $.isFunction(window[this.callback])) {
						window[this.callback].call(window);
					}
				} else if (decision == "DENY" || decision == "ERROR" || decision == "TIMEOUT") {
					this.showError(evaluationFailureMsg(decision));
				}
			};
			
			this.showError = function(errorMsg) {
				$(".waiting").hide();
				$('#credentialInput').dialog('close');
				$("#credentialInput").hide();
				$(".error").empty();
				$(".error").append("<p>" + errorMsg + "</p>");
				$(".error").show();
			};
			
			this.evaluationFailureMsg = function(decision) {
				var msg = "";
				if (decision == "ERROR") {
					msg = "There was an error processing the credential request. Please check your configuration and try again, if this continues to occur, please contact technical support."
				} else if (decision == "DENY") {
					msg = "The requested credential was denied. You will need to gain the configured credential to use the application.";
				} else if (decision == "TIMEOUT") {
					msg = "The requested credential has timed out due to not getting a response from the Trust Broker. Please check your configuration and try again, if this continues to occur, please contact technical support."
				}
				msg += "<br/>To try again with different credentials, please <span class='changeCred'>click here</span>"
				return msg;	
			};
			
			this.showMessageBox = function(title, msg, style) {
				$("#credentialRequestor #message").text(msg);
				if ($("#credentialRequestor #message").hasClass("error")) {
					$("#credentialRequestor #message").removeClass("error");
				} else if ($("#credentialRequestor #message").hasClass("warning")) {
					$("#credentialRequestor #message").removeClass("warning");
				} else if ($("#credentialRequestor #message").hasClass("success")) {
					$("#credentialRequestor #message").removeClass("success");
				}
				$("#credentialRequestor #message").addClass(style);
				var confirmDlg = $('#credentialRequestor #messageBox').dialog({autoOpen: false, title:title, modal:true,
					width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
					buttons:{'OK': function() {$(this).dialog('close'); }}
				});
				confirmDlg.dialog("open");
			};
		
			// this is similar to logout except the trust session is not invalidated
			// will close the existing websocket connection and create a new one
			this.invalidateSession = function() {
				this.status = "inputCredentials";
				this.parameters = null;
				this.contextId = null;
				this.socket.close();
				if (this.wsConnect(RelyingParty.wsURL)) {
					this.getInputCredentials();
				}
			};
			
			this.initState = function(checkStateURL) {
				var theThis = this;
				$.ajax({
				       type: 'GET',
				        url: checkStateURL,
				        async: false,
				        contentType: "application/json",
				        dataType: 'jsonp',
				        jsonpCallback: 'respJSON',		
				        success:
			   			function(data) {
			  				if (data) {
			  					theThis.status = data.status;
			  					if (data.sessionId) {
			  						theThis.wsURL += "?sessionId=" + data.sessionId;
			  					}
			  					if (data.parameters) {
			  						var paramsStr = JSON.stringify(data.parameters, null);
			  						theThis.wsURL += (theThis.wsURL.indexOf("?") != -1 ? "&parameters=" : "?parameters=") + paramsStr;
			  					} else if (theThis.parameters) {
			  						var paramsStr = JSON.stringify(theThis.parameters, null);
			  						theThis.wsURL += (theThis.wsURL.indexOf("?") != -1 ? "&parameters=" : "?parameters=") + paramsStr;
			  						theThis.status = "requestCredentials";
			  					}
								if (!theThis.wsConnect(theThis.wsURL)) {
									theThis.showError("Your Browser does not support web sockets. Please run the application on a browser that supports websockets");
									theThis.status = "ConnectFailed";
								}
			  				}
			  			}
			  		});
			  				
			}
		
			this.evaluateCredentials = function(options) {
				// TODO hide the current page contents
			
				if (options && options.action == "checkCredentials") {
					this.injectCXEvaluationMarkup();
					
					var contextPath = location.pathname.split("/")[1];
					var theOptions = $.extend({
						// These are the defaults.
						accessServerHost: "${ request.getServerName() + ':' + request.getServerPort() }",
						resourceName: contextPath,
						}, options );
					this.resourceName = theOptions.resourceName;
					if (options.callback) {
						this.callback = options.callback;
					}
					// if parameters is in the query string then extract it into the URL
					// expect the parameters can be converted to a JSON object
					var qString = location.search.substring(1);
					var params = qString.split("&");
					var status = null;
					if (params) {
						for(var i = 0; i < params.length; i++) {
							var paramName = params[i].split("=")[0];
							var paramValue = params[i].split("=")[1];
							if (paramName === "parameters") {
								this.parameters = eval('(' + unescape(paramValue) + ')');
								break;
							}
						}
					}
					
					<g:if test="${request.getScheme() == 'https' }">
						var httpScheme = "https://"
						var wsScheme = "wss://";
					</g:if>
					<g:else>
						var httpScheme = "http://"
						var wsScheme = "ws://";
					</g:else>
					var initStateURL = httpScheme + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials/checkDecision?resourceName=" + this.resourceName;
					this.wsURL = wsScheme + theOptions.accessServerHost + "/RNSResourceServer/evaluateCredentials";
					this.initState(initStateURL);
				} else if (options && options.action == "processCredentialInput") {
					this.processCredentialInput();
				} else if (options && options.action == "invalidateSession") {
					this.invalidateSession();
				}
			};
			
		};
		
		var RelyingParty = new RelyingPartyDef();
		
		$("#credentialRequestor #requestCredentials").live('click', function(event) {
			RelyingParty.evaluateCredentials({action: "processCredentialInput"});
			
			// Prevent default does not exit on event in IE9
			if ( event.preventDefault )
				event.preventDefault()
			else 
				event.returnValue = false;
		});
		
		$(".changeCred").live("click", function() {
		    RelyingParty.evaluateCredentials({action: "invalidateSession"});
			return false;
		});
		
		// Document Ready event handler
		$(document).ready(function() {
			RelyingParty.evaluateCredentials({action: 'checkCredentials'
			<g:if test="${callback != null }">
				, callback: '${callback}'
			</g:if>
			<g:if test="${resourceName != null }">
				, resourceName: '${resourceName}'
			</g:if>			
			});
		});	
		
	});
}


