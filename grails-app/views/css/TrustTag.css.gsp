<%@ page contentType="text/css"%>

.waiting {
	background-image:url('${ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() }/RNSResourceServer/images/loading.gif');
	background-repeat:no-repeat;
	background-position: 10px center;
	display:none;
   	background-color: #EEEEEE !important;
    border: 1px solid #CCCCCC !important;
    border-radius: 3px 3px 3px 3px;
    line-height: 1.6 !important;
	padding:15px 10px 15px 50px;   
    margin-left:50px;
    margin-right:50px;      		
}

.waitingMessage {
	font-weight: bold;
    font-size: 12px !important;        		
	color: #06263C !important;
}

.warning, .error, .success {
	border: 1px solid;
	border-radius: 3px 3px 3px 3px;
	margin-left: 50px;
	margin-right: 50px;
	padding:15px 10px 15px 50px;
	background-repeat: no-repeat;
	background-position: 10px center;
	font-size:12px;
}
.warning {
	color: #9F6000;
	background-color: #FEEFB3;
	background-image: url('${ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() }/RNSResourceServer/images/warning.png');
}
.error {
	color: #D8000C;
	background-color: #FFBABA;
	background-image: url('${ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() }/RNSResourceServer/images/error.png');
}

.success {
	color: #4F8A10;
	background-color: #DFF2BF;
	background-image: url('${ request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() }/RNSResourceServer/images/success.png');
}

.buttons {
	position: relative;
	margin-bottom: 5px;
	margin-right: 10px;
}

#logout a:hover {
	cursor:pointer;
	text-decoration:underline;
}

.error .changeCred, .error .resetCred {
	text-decoration: underline;
	color: #0000ff;
	cursor:pointer;cursor:hand;
}


/** Styles from app.css, refactoring needed!! **/
div.loginBox
 {
    width:900px;
    padding:10px;
 	border-style:outset;
 	border-radius:15px;
 	border-color:grey;
	border-left: 1px solid #d8d8d8;
	border-right-width:0px;
	border-top-width:0px;
	border-bottom-width:0px;
 	-moz-box-shadow: 3px 3px 5px #888;
 	-webkit-box-shadow: 3px 3px 5px #888;
 	box-shadow: 3px 3px 5px #888;
 	margin-left:50px;
 }

.sectionHeader {
    /* background-color: #85b5d9; */
    /* background-color: #6396bc; */
    background-color: #46a3ca;
    color: #ffffff;
    /* color: #2e6e9e; */
    /* color: #e27817; */
    font-family: 'Verdana';
    font-size: 11px;
    font-weight: bold;
    padding: 3px 0 3px 4px;
    text-align: left;	
	 border-top-right-radius:5px;
	 border-top-left-radius:5px;
	 -webkit-border-top-right-radius: 5px;
	 -webkit-border-top-left-radius: 5px;
	 -moz-border-radius-topright: 5px;
	 -moz-border-radius-topright: 5px; 
}

.sectionBody {
	margin-left:50px;
/*	margin-top: 10px; */
	margin-bottom: 10px;
	margin-right: 50px;
/*	padding: 10px; */
	border: 1px solid #d8d8d8;  
	border-radius:5px;
}

.withShadow {
/*	border-style:outset; */
	border-color:grey;
/*	border-width:0px;  */
	border-left: 1px solid #d8d8d8;
	border-right-width:0px;
	border-top-width:0px;
	border-bottom-width:0px;
	-moz-box-shadow: 3px 3px 5px #888;
	-webkit-box-shadow: 3px 3px 5px #888;
	box-shadow: 3px 3px 5px #888;
	padding-bottom:5px;
}

.sectionHeaderShadow {
	height:20px;
	-moz-box-shadow: 0px 3px 5px #888;
	-webkit-box-shadow: 0px 3px 5px #888;
	box-shadow: 3px 3px 5px #888;
	padding-bottom:5px;
    font-size: 12px;
	text-shadow: 1px 1px 2px #888;
	line-height:20px;
}

.sectionBody table {
	padding: 10px;
	margin-bottom: 5px; 
/*	background-color: #ffffff; */
	width: 100%
}

.sectionBody .colHeader {
	background-color: #aaaaaa;
}

.sectionBody th {
	background-color: #46a3ca;
	color: #ffffff;
	font-family: 'Verdana';
	font-size: 11px;
	font-weight: bold;
	text-align: left;
	/* background-color: #788597; */
}

.sectionBody td {
    color: #1E1E1E;
    font-family: 'Verdana';
    font-size: 10pt;
    text-align: left;
/* border: 1px solid #DDDDDD; */        		
}

.buttons {
	position: relative;
	margin-bottom: 5px;
	margin-right: 10px;
}
