<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>RNS :: eReferral</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
	<link href="${resource(dir: 'css', file: 'modal.css') }" type="text/css" rel="stylesheet" />
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- 
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
    -->
  </head>
  <body>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="${resource(dir: 'js', file: 'RTNModal.js') }"></script> 
    <script>
    window.addEventListener('DOMContentLoaded', function (event) {
      var data = {
        emailForm: '<form id="rnsForm" data-handler="handleEmailSubmit" method="post" style="padding: 0; margin: 0;"><p style="margin: 0; padding: 0 0 15px 0;">Please provide an email address:</p><input class="rns" id="email" name="email" tabindex="1" type="email" placeholder="Email" autocomplete="off" autofocus required></form>',
        passwordForm: '<form id="rnsForm" data-handler="handlePasswordSubmit" method="post" style="padding: 0; margin: 0;"><p style="margin: 0; padding: 0 0 15px 0;">Please provide the password for <b>{0}</b>:</p><input class="rns" id="password" name="password" tabindex="2" type="password" placeholder="Password" autofocus required></form>'
      };
      myModal = new RTNModal();
      
      // handle login
      myModal.handleEmailSubmit = function (e) {
      
        var self = this, emailElement = document.getElementById('email');
        
        this.emailAddress = emailElement.value;
        
        localStorage.setItem('loggedInEmail', this.emailAddress);
        
        this.settings.height = 200;
        this.settings.width = 200;
        this.settings.borderRadius = 100;
        this.settings.bg = '#000';
        this.settings.contentColor = '#fff';
        this.hideContent(function() {
          self.hideHeader();
          self.hideFooter();
          self.setStatusTo('success', 'CHECKING ADDRESS');
          self.setContentDimensionsByModal();
          self.updateModalContentStyle();
          self.update(function () {
            self.showContent(function () {
              self.showSpinner();
            });
            setTimeout(function () {
              self.hideSpinner();
              self.setStatusTo('success', 'ACCOUNT FOUND');
              setTimeout(function () {
                self.showPasswordForm();
              }, 1500);
            }, 2000);
          });
        });
        
        e.preventDefault();
        
      };
      
      myModal.showPasswordForm = function (e) {
      
        myModal.hideContent(function () {
        
          myModal.dom.formSubmitButtonElement.className = 'disabled';
          myModal.settings.height = 250;
          myModal.settings.width = 470;
          myModal.settings.borderRadius = 3;
          myModal.settings.bg = '#fff';
          
          // update content
          myModal.settings.contentColor = '#000';
          myModal.updateModalContentStyle();
          myModal.changeContentTo(data.passwordForm.format(myModal.emailAddress));
          
          setTimeout(function () {
            var form = document.getElementById('rnsForm');
            form.addEventListener("submit", function (e) {
              e.preventDefault();
              e.returnValue = false;
              return false;
            }, false);
          }, 1);
              
          myModal.hideSpinner();
          myModal.showHeader();
          myModal.showFooter();
          myModal.setContentDimensionsByModal();
          myModal.update(function () {
            myModal.showContent();
            myModal.updateModalContentStyle();
          });
        });
      };
      
      myModal.handlePasswordSubmit = function (e) {
        
        var self = this, passwordElement = document.getElementById('password');
        var password = passwordElement.value;
        
        this.settings.height = 200;
        this.settings.width = 200;
        this.settings.borderRadius = 100;
        this.settings.bg = '#000';
        this.settings.contentColor = '#fff';
        
        this.hideContent(function() {
          self.hideHeader();
          self.hideFooter();
          self.setStatusTo('success', 'AUTHENTICATING');
          self.setContentDimensionsByModal();
          self.updateModalContentStyle();
          self.update(function () {
            self.showContent(function () {
              self.showSpinner();
            });
            setTimeout(function () {
              self.hideSpinner();
              
              if ( password==='bad' ) {
                self.setStatusTo('failure', 'WRONG PASSWORD');
                setTimeout(function () {
                  self.showPasswordForm();
                }, 1500);
              } else {
                self.setStatusTo('success', 'SUCCESS');
                setTimeout(function () {
                  location.href = 'app.php#home';
                }, 1500);
              }

            }, 2000);
          });
        });
        
        e.preventDefault();
        
      };
      
      myModal.addEmailFormEvents = function () {
      
        var self = this;
        
        // handle return click
        document.addEventListener('keyup', function (e) {
        
          var formErrors = document.querySelectorAll(':invalid'), 
              submitButton = document.getElementById('rnsFormSubmit'), 
              form = document.getElementById('rnsForm'),
              formHandler = form.getAttribute('data-handler');
          
          if (e.keyCode===13) {
            if (formErrors.length===0) {
              myModal[formHandler].call(self, e);
            } else {
              console.log('form errors', formErrors);
            }
            
          } else {
            
            if (formErrors.length===0) {
              submitButton.removeClass('disabled');
            } else {
              if (!submitButton.hasClass('disabled')) {
                submitButton.addClass('disabled');
              }
            }
            
          }
          
        }, true);
        
        // handle click events on buttons, mask, etc.
        document.addEventListener('click', function (e) {
        
          var button = e.target.getAncestorBy('rnsFormSubmit'),
              searchResult = e.target.getAncestorBy('rns-search-result'),
              mask = e.target.getAncestorBy(myModal.settings.maskId),
              formErrors = document.querySelectorAll(':invalid'),
              form = document.getElementById('rnsForm'),
              formHandler = form.getAttribute('data-handler');
        
          if (button && !button.hasClass('disabled')) {
            
            if (formErrors.length===0) {
              myModal[formHandler].call(self, e);
            } else {
              console.log('form errors', formErrors);
            }
            
          }
          
          if ( searchResult ) {
            self.handleSearchResultClick(searchResult);
          }
          
          if (mask) {
            self.hide();
          }
          
        }, false);
        
        
        
      };     
      
      setTimeout(function () {
      
        // show modal
        myModal.show(false, function () {
          setTimeout(function () {
            myModal.hideContent(function () {
              myModal.settings.height = 250;
              myModal.settings.width = 470;
              myModal.settings.borderRadius = 3;
              myModal.settings.bg = '#fff';
              
              // update content
              myModal.settings.contentColor = '#000';
              myModal.updateModalContentStyle();
              myModal.changeContentTo(data.emailForm);
              
              setTimeout(function () {
                var form = document.getElementById('rnsForm');
                form.addEventListener("submit", function (e) {
                  e.preventDefault();
                  e.returnValue = false;
                  return false;
                }, false);
              }, 1);
              
              myModal.hideSpinner();
              myModal.showHeader();
              myModal.showFooter();
              myModal.setContentDimensionsByModal();
              myModal.update(function () {
                myModal.addEmailFormEvents();
                myModal.showContent();
                myModal.updateModalContentStyle();
              });
            });
          }, ( ( 1000 * myModal.settings.transitionTime ) * 4 ) );
        });
        
      }, 300);
      
    }, false);
    </script>
  </body>
</html>
