package com.resilient.as.relyingparty

import grails.util.Environment

import com.resilient.as.relyingparty.util.ConfigUtils

class RelyingPartyUtils {

	def initRelyingPartyState(resourceName, requestor) {
		return initRelyingPartyState(resourceName, requestor, "sessionId", "parameters")	
	}
	
	def initRelyingPartyState(resourceName, requestor, sessionIdParam, parametersParam) {
		def checkCredentials = !flash.status || flash.status != 'validated';
		def sessionId = null
		if (params.sessionId) {
			sessionId = params.sessionId
		} else if (session.getAttribute(sessionIdParam)) {
			sessionId = session.getAttribute(sessionIdParam)
		} else {
			sessionId = ""
		}
		def status = "requiredInput"
		def parameters = ""
		if (params.parameters) {
			parameters = params.parameters
			session.setAttribute(parametersParam, parameters)
		} else if (session.getAttribute(parametersParam)) {
			parameters = session.getAttribute(parametersParam)
			status = "requestCredential"
		}
		if (params.status) {
			status = params.status
			if (params.status == "checkLogout") {
				sessionId = session.getAttribute(sessionIdParam)
				session.setAttribute(sessionIdParam, null)
				session.setAttribute(parametersParam, null)
				if (session.getAttribute("authCredential")) {
					session.setAttribute("authCredential", null)
				}
//				session.invalidate()
			}
		}
		// initialize the basePath
		def basePath = ""
		def requestorPath = ""
		Boolean canResetCredential = Boolean.FALSE
//		String url = request.getRequestURL().toString()
		String requestUri = request.getRequestURI()
		try {
			URI reqUri = new URI(requestUri)
			String path = reqUri.getPath()
			if (path.startsWith("/")) {
				path = path.substring(1);
			}
			String[] urlParts = path.split("/")
			basePath = urlParts[0]
			for (int i = 1; i < urlParts.length; i++) {
				if (!urlParts[i].equals("grails")) {
					requestorPath += "/"
					if (urlParts[i].indexOf(".dispatch") != -1) {
						requestorPath += urlParts[i].substring(0, urlParts[i].indexOf(".dispatch"))
					} else {
						requestorPath += urlParts[i]
					}
				}
			} 
			// if for some reason the 3rd segment of the URI is missing then assume it to be the index page
			String[] requestorPathSegments = requestorPath.split("/")
			if (requestorPathSegments.length == 2) {
				requestorPath = requestorPath + "/index"
			}
			if (Environment.current != Environment.TEST) {
				Properties configProps = ConfigUtils.loadPropertiesFile(this.class, basePath + "-config.properties")
				canResetCredential = Boolean.valueOf(configProps.getProperty("resetCredential.enable", "false"))
			}
		} catch (URISyntaxException ex) {
			// just swallow basePath will be an empty string
		}
		// set the no cache meta headers so browser will not cache the web pages
		// and user cannot get back to the app by clicking the back button
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);
		return [
			checkCredentials : checkCredentials,
			resourceName : resourceName,
			status : status,
			sessionId : sessionId,
			parameters: parameters,
			requestor: requestorPath,
			basePath: basePath,
			resetCredential: canResetCredential.toString()
		]
	}

}
