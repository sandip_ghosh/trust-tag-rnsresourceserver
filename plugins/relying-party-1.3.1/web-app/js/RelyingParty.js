function RelyingPartyDef() {

	this.pageCallback = null;
	
	this.basePath = null;
	
//	this.pageContents = null;
	
	this.afterGrant = function() {
		// set the first row as the selected row (the selector is selecting the 2nd tr row, since the first one is the column header row)
//		this.pageContents.appendTo("body");
		$("#pageContents").show();
		var sessionId = $("#credentialRequestor #sessionId").val();
		if (sessionId) {
		    $("#logout").show();
		    this.sessionTimeout();
		}
	    $("#credentialRequestor").hide();
	    $("#credentialRequestor #status").val("requestCredential");
 		if (this.pageCallback && $.isFunction(this.pageCallback)) {
 			this.pageCallback.call($(this));
 		}
	}
		
	this.logoutCallback = function(theThis, decision, theDoc) {
 		if (decision == "GRANT") {
 			parent.location = "/" + theThis.basePath + "/relyingParty/invalidateSession?requestor=" +  $("#credentialRequestor #requestor").val();
		} else if (decision == "DENY" || decision == "ERROR"){
			theThis.showError("#pageContents", theDoc, true, (decision === "DENY"))
		}	
	}

	this.credentialRequestCallback = function(theThis, decision, theDoc) {
 		if (decision == "GRANT") {
	 		theThis.afterGrant();
 		} else if (decision == "DENY" || decision == "ERROR"){
			theThis.showError("#pageContents", theDoc, false, (decision === "DENY"))
 		} else if (decision == "TIMEOUT") {
			theThis.showTimeoutError("#pageContents", theDoc, false);
		}
	}
	
	this.checkCredentials = function(pageCallback) {
	    // decide if we should be requesting credential or displaying the resource page
		this.basePath = $("#basePath").val();
	    if (pageCallback) {
	    	this.pageCallback = pageCallback;
	    }
		var checkCred = $("#checkCredentials").val() === "true";
	    if (checkCred) {
	    	 $("#pageContents").hide();
	    	var status = $("#credentialRequestor #status").val();
//		    this.pageContents = $("#pageContents").detach();
		    $("#credentialRequestor").show();
		    this.processCredentialRequest(status == "checkLogout" ? this.logoutCallback : this.credentialRequestCallback);
		} else {
			this.afterGrant();
		}
	}

	this.processCredentialRequest = function(callback) {
		var status = $("#credentialRequestor #status").val();
		var theThis = this;
		if (status == "requestCredential" || status == "checkDecision") {
		    $("#requiredInput").hide();
		    $(".waiting").show();
	 		this.requestCredentials(callback);
		} else if (status == "requestLogout" || status == "checkLogout") {
		    $("#requiredInput").hide();
		    $(".waiting").show();
			this.logout(callback);
		} else if (status == "waitingCredentialInput") {
			// We can only get in here if the user has closed the TBD dialog frame and reloaded the page
			// show the deny message
        	if (callback && $.isFunction(callback)) {
        		callback.call($(this), theThis, "DENY", window.document);
        	}
		} else {
			var url = "/" + this.basePath +	"/credentialRequestor/inputCredentials/" + $("#resourceName").val();			
			var sessionId = $("#credentialRequestor #sessionId").val();
			$.ajax({
				type: "GET",
				url: url,
				contentType: "application/json; charset=utf-8",
				data: {},
				dataType: "json",
				success: 
					function(data) {
						if (data.inputsNeeded) {
					    	$(".waiting").hide();
					    	$("#requiredInputTable").empty();
					    	$("#requiredInputTable").append("<tr><td colspan='2'>Please enter the values required before requesting credentials</td></tr>");
					    	for (i = 0; i < data.inputsNeeded.length; i++) {
					    		var inputLabel = data.inputsNeeded[i].label;
					    		var inputName = data.inputsNeeded[i].name;
					    		var inputType = "text";
					    		$("#requiredInputTable").append("<tr><td>" + inputLabel + "</td><td><input type='" + inputType + "' id='" + inputName + "' name='" + inputName + "' value='' size='40'/></td></tr>"); 
					    	}
					    	$("#requiredInput").show();
						}
					},
				error: 
					function(xmlHttpRequest, textStatus, errorThrown) {
						var responseStatus = xmlHttpRequest.status;
						if (responseStatus == 404) {
					    	var theContext = theThis;
	    		        	if (callback && $.isFunction(callback)) {
	    		        		callback.call(theContext, theThis, "GRANT", window.document);
	    		        	}
						}
					}
			});
		}
	}
	
	this.processCredentialInput = function(callback) {
		var requiredInputs = $("#requiredInputTable input");
		var parameters = new Object();
		// check if the user has entered values for required input params
		var hasValues = true;
		var errorMessage = "Please enter values for the the parameters: ";
		var errParams = "";
		$.each(requiredInputs, function(i, requiredInput) {
			var inputName = $(requiredInput).attr("name");
			var inputValue = $(requiredInput).val();
			if (!inputValue) {
				hasValues = false;
				if (errParams) {
					errParams += ", ";
				}
				errParams += inputName;
			} else {
				parameters[inputName] = inputValue;
			}
		});
		if (!hasValues) {
			Utils.showMessageBox("Required Input Parameters Missing", errorMessage + errParams, "error");
		} else {
			var paramsStr = JSON.stringify(parameters, null);
			var redirectURL = "/" + this.basePath;
			var requestor = $("#credentialRequestor #requestor").val();
			redirectURL += requestor ? requestor : "index";
			$("#credentialRequestor #parameters").val(paramsStr);
			$("#credentialRequestor #status").val("requestCredential");
			$("#credentialRequestor #submitInputCredentials").attr("action", redirectURL);
			$("#credentialRequestor #submitInputCredentials").submit();
//			redirectURL += "?parameters=" + paramsStr;
//			redirectURL += "&status=requestCredential";
//			location.href = redirectURL;
		}
	}
	
	this.requestCredentials = function(callback) {
   		var theCredInputDialog = $('#credentialInput').dialog({autoOpen: false, title:'Trust Broker Display', modal:true, width: 800, maxWidth: 1024, height:600, maxHeight:768});
		var theContext = $(this);
        var credentialStatus = $("#credentialRequestor #status").val();
		if (credentialStatus == "checkDecision") {
			window.parent.$('#credentialInput').dialog('close');
			window.parent.$('.waitingMessage').text("Retrieving Decision please wait...");
			theContext = window.parent.document;
		} else if (credentialStatus == "requestCredential") {
			$('.waitingMessage').text("Requesting Credentials please wait...");
			$('.waiting').show();
		}
		// initialize the jquery UI dialog
		if (credentialStatus == "requestCredential") {
			var credentialRequestURL = "/" + this.basePath + "/credentialRequestor/requestCredential/" + $("#resourceName").val();
			var requestor = $("#credentialRequestor #requestor").val();
			var callbackUrl = "/" + this.basePath + "/relyingParty/checkDecision?resourceName=" + $("#resourceName").val() + "&requestor=" + (requestor ? requestor : "index");
			callbackUrl = encodeURIComponent(callbackUrl)
			var sessionId = $("#credentialRequestor #sessionId").val();
			var parametersJSON = unescape($("#credentialRequestor #parameters").val());
			var parameters = new Object();
			if (parametersJSON) {
				parameters = eval('(' + parametersJSON + ')');
			}
			var postData = new Object();
			postData["callbackURL"] = callbackUrl;
			if (sessionId) {
				postData["sessionId"] = sessionId;
			}
			if (parameters) {
				postData["parameters"] = parameters;
			}
			var theThis = this;
			// set a time out of 2 minutes * retry interval * number of retries, if there is no activity in
			// that time, we will close the popup and show an error message
			$.doTimeout("credReqTimeout", 120000, function() {
				$.doTimeout("checkDecision");
				$('#credentialInput').dialog('close')
				var decision = "ERROR";
	        	if (callback && $.isFunction(callback)) {
	        		callback.call(theContext, theThis, decision, theContext == window.parent.document ? window.parent.document : window.document);
	        	}
			});
			var postDataStr = JSON.stringify(postData, null);
			$.ajax({
				type: "POST",
				url: credentialRequestURL,
				contentType: "application/json; charset=utf-8",
				data: postDataStr,
				dataType: "json",
				processData: false,
				success: 
	    			function(data) {
	    			    var decision = data.decision ? data.decision : null;
	    		        var tbDisplayUrl = data.RNS_API_CONTENT ? data.RNS_API_CONTENT : null;
	    		        if (data.decision) {
	    		        	if (data.decision == "TB_DISPLAY") {
	    		        		$("#credentialRequestor #sessionId").val(data.sessionId);
	    		        		$.post("/" + theThis.basePath + "/relyingParty/saveSessionId", {sessionId: data.sessionId}, function(data) { }, "json"); 
	    		        		$('.waitingMessage').text("Waiting for Credential Input...");
	    		        		$("#credentialRequestor #status").val("waitingCredentialInput");
	    		        		data.trustBrokerDisplayURL = data.trustBrokerDisplayURL.replace("/?", "?");
	    		        		window.frames['tbDisplay'].location = data.trustBrokerDisplayURL;
	    		        		theCredInputDialog.dialog('open');
	    		        		var decisionURL = "/" + theThis.basePath + "/relyingParty/getDecision"
	    		        		// now we should poll for the TBD input and the decision
	    		        		// after TBD input
	    		        		$.doTimeout( "checkDecision", 1000, function() {
	    		        			var sessionId = $("#credentialRequestor #sessionId").val();
	    		        			$.getJSON(decisionURL, { sessionId: sessionId, status: status}, 
	    		        					function(data) {
	    		        						if (data.decision && data.decision != "PENDING") {
	    		        							$.doTimeout("checkDecision");
	    		        							$.doTimeout("credReqTimeout");
	    		        	    		        	if (callback && $.isFunction(callback)) {
	    		        	    		        		callback.call(theContext, theThis, data.decision, theContext == window.parent.document ? window.parent.document : window.document);
	    		        	    		        	}
    		        	    		        		return false;
	    		        						}
	    		        						return true;
	    		        				});
	    		        			return true;
	    		        		});
	    		        		
	    		        	}
	    		        }
	    		        if (data.decision == "GRANT" || data.decision == "DENY" || data.decision == "TIMEOUT") {
	    		        	$.doTimeout("checkDecision");
							// kill the time out timer
							$.doTimeout("credReqTimeout");
	    		        	if (callback && $.isFunction(callback)) {
	    		        		callback.call(theContext, theThis, data.decision, theContext == window.parent.document ? window.parent.document : window.document);
	    		        	}
	    		        }	
	    			},
	    			error:
						function(xmlHttpRequest, textStatus, errorThrown) {
	    					$.doTimeout("checkDecision");
    						$.doTimeout("credReqTimeout");
    						var responseStatus = xmlHttpRequest.status;
	    					var responseData = eval('(' + xmlHttpRequest.responseText + ')')
	    					if (responseData) {
		    		        	if (callback && $.isFunction(callback)) {
		    		        		callback.call(theContext, theThis, responseData.decision, theContext == window.parent.document ? window.parent.document : window.document);
		    		        	}
	    					} else {
	    						var decision = "ERROR";
	    						if (responseStatus === 401) {
	    							decision = "DENY";
	    						} else if (responseStatus == "408") {
	    							decision = "TIMEOUT";
	    						}
		    		        	if (callback && $.isFunction(callback)) {
		    		        		callback.call(theContext, theThis, decision, theContext == window.parent.document ? window.parent.document : window.document);
		    		        	}
	    					}
	    				}	    			
			});
		}
		
	} 

	this.logout = function(callback) {
		var theContext = $(this);
        var credentialStatus = $("#credentialRequestor #status").val();
		if (credentialStatus == "checkLogout") {
			window.parent.$('.waitingMessage').text("Retrieving session close confirmation, please wait...");
			theContext = window.parent.document;
		} else if (credentialStatus == "requestLogout") {
			$('.waitingMessage').text("Closing session, please wait...");
			$('.waiting').show();
		}
		if (credentialStatus == "requestLogout" || credentialStatus == "checkLogout") {
			if (!this.basePath) {
				this.basePath = $("#basePath").val();
			}
			var credentialRequestURL = "/" + this.basePath + "/credentialRequestor/logout/" + $("#resourceName").val();
			var callbackUrl = "/" + $("#credentialRequestor #basePath").val();
			var requestor = $("#credentialRequestor #requestor").val();
			callbackUrl += requestor ? requestor : "index";
			callbackUrl += "?status=checkLogout";
			callbackUrl = encodeURIComponent(callbackUrl);
			var sessionId = $("#credentialRequestor #sessionId").val();
			var postData = new Object();
			postData["callbackURL"] = callbackUrl;
			if (sessionId) {
				postData["sessionId"] = sessionId;
			}
			var postDataStr = JSON.stringify(postData, null);
			var theThis = this;
			$.ajax({
				type: "POST",
				url: credentialRequestURL,
				contentType: "application/json; charset=utf-8",
				data: postDataStr,
				dataType: "json",
				processData: false,
				success: 
	    			function(data) {
	    			    var decision = data.decision ? data.decision : null;
	    		        var tbDisplayUrl = data.RNS_API_CONTENT ? data.RNS_API_CONTENT : null;
	    		        if (data.decision) {
	    		        	if (data.decision == "TB_DISPLAY") {
	    		        		$("#credentialRequestor #sessionId").val(data.sessionId);
	    		        		$("#credentialRequestor #status").val("checkLogout");
	    		        		data.trustBrokerDisplayURL = data.trustBrokerDisplayURL.replace("/?", "?");
	    		        		window.frames['tbDisplay'].location = data.trustBrokerDisplayURL;
	    		        	}
	    		        }
	    		        if (data.decision == "GRANT" || data.decision == "DENY") {
	    		        	if (callback && $.isFunction(callback)) {
	    		        		callback.call(theContext, theThis, data.decision, theContext == window.parent.document ? window.parent.document : window.document);
	    		        	}
	    		        }	
	    			}
			});
		}
		
	}
	
	this.showError = function(requestorPageId, theDoc, isLogout, isDeny) {
		var msg = isLogout ? "<p>An error occurred while attempting to close the trust session</p>" : 
		isDeny ? "<p>The requested credential was denied. You will need to gain the configured credential to use the application.</p>" :
			"<p>There was an error processing the credential request. Please check your configuration and try again, if this continues to occur, please contact technical support.</p>";
		var canRetry = $("#requestor").val().indexOf("testCredentials") == -1;
		if (canRetry) {
			msg += "<p>To try again with different credentials, please <span class='changeCred'>click here</span></p>";
		}
/*		
		var canReset = $("#resetCredential").val() === "true";
		if (canReset) {
			msg += "<p>To reset the credentials protecting admin access, please <span id='resetCred' class='changeCred'>click here</span></p>"
		}
*/
		$(requestorPageId, theDoc).hide();
	    $("#credentialRequestor", theDoc).show();
    	$(".error", theDoc).append(msg);
    	$("#requiredInput", theDoc).hide();
    	$(".waiting", theDoc).hide();
    	$(".error", theDoc).show();
	}

	this.showTimeoutError = function(requestorPageId, theDoc) {
		var msg = "<p>The requested credential has timed out due to not getting a response from the Trust Broker. Please check your configuration and try again, if this continues to occur, please contact technical support.<br/>To try again with different credentials, please <span class='changeCred'>click here</span></p>";
		$(requestorPageId, theDoc).hide();
	    $("#credentialRequestor", theDoc).show();
    	$(".error", theDoc).append(msg);
    	$("#requiredInput", theDoc).hide();
    	$(".waiting", theDoc).hide();
    	$(".error", theDoc).show();
	}
	
	// Will timeout the page after Credential Frequency + 1 second of inactivity
	this.sessionTimeout = function() {
		var theThis = this;
		$.getJSON("/" + this.basePath + "/cxBuilder/getCredentialFrequency", {id: $("#credentialRequestor #resourceName").val()},
				function(data) {
					if (data && data.timeout) {
						// setup a timeout timer
						$.doTimeout( "sessionTimeout", data.timeout + 1000, function() {
							window.location = "/" + theThis.basePath + "/relyingParty/invalidateSession?requestor=" +  $("#credentialRequestor #requestor").val();
						});					
						var timeoutHandler = function() {
							// kill the session Timout
							$.doTimeout("sessionTimeout");
							// start a new timeout 1 seccond after session Timeout
							$.doTimeout( "sessionTimeout", data.timeout + 1000, function() {
								window.location = "/" + theThis.basePath + "/relyingParty/invalidateSession?requestor=" +  $("#credentialRequestor #requestor").val();
							});					
						};
						$(document).mouseup(timeoutHandler);
					}
			});
	}
	
}

var RelyingParty = new RelyingPartyDef();

//Credential Request related event handlers
$("#credentialRequestor #requestCredentials").live('click', function(event) {
	RelyingParty.processCredentialInput();
	
	// Prevent default does not exit on event in IE9
	if ( event.preventDefault )
		event.preventDefault()
	else 
		event.returnValue = false;
});

$("#logout").live('click', function(event) {
    $("#pageContents").hide();
    $("#credentialRequestor").show();
	$("#credentialRequestor #status").val("requestLogout");
	RelyingParty.processCredentialRequest(RelyingParty.logoutCallback);
	
	// Prevent default does not exit on event in IE9
	if ( event.preventDefault )
		event.preventDefault()
	else 
		event.returnValue = false;
});

$(".changeCred").live("click", function() {
    window.location = "/" + RelyingParty.basePath + "/relyingParty/invalidateSession?requestor=" + $("#credentialRequestor #requestor").val();
	return false;
});
	
$("#resetCred").live("click", function() {
    window.location = "/" + RelyingParty.basePath + "/relyingParty/resetCredential?requestor=" + $("#credentialRequestor #requestor").val();
	return false;
});
