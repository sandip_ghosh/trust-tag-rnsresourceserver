package com.resilient.as.relyingparty

import grails.converters.JSON

import java.net.URI
import java.net.URISyntaxException

import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.relyingparty.util.ConfigUtils
import com.resilient.commonutils.httputils.HttpClientUtil
import com.resilient.commonutils.httputils.IHttpClientUtil

class RelyingPartyController {

 	def saveSessionId = {
		def sessionIdParam = "sessionId"
		if (params.sessionIdParam) {
			sessionIdParam = params.sessionIdParam
		} 
		if (session.getAttribute(sessionIdParam)) {
			def oldSessionId = session.getAttribute(sessionIdParam)
			session.removeAttribute(sessionIdParam)
			session.removeAttribute(oldSessionId + "_decision")
		}
		if (params.sessionId) {
			session.setAttribute(sessionIdParam, params.sessionId)
			session.setAttribute(params.sessionId + "_decision", "PENDING")
		}
		def model = []
		render model as JSON
	}

	def getDecision = {
		// force no cahce in response
		forceNoResponseCache();
		
		def sessionId = params.sessionId
		def model = []
		if (sessionId) {
			def decision = session.getAttribute(sessionId + "_decision")
			model = [decision: decision]
		}
		render model as JSON
	}

	def checkDecision = {
		def decision = ""
		def sessionIdParam = "sessionId"
		if (params.sessionIdParam) {
			sessionIdParam = params.sessionIdParam
		}
		def parametersParam = "parameters"
		if (params.parametersParam) {
			parametersParam = params.parametersParam
		}
		def sessionId = session.getAttribute(sessionIdParam)
		int rescode = 500
		try {
			def reqURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + _getBasePath() + "/credentialRequestor/requestCredential/" + params.resourceName
			def parameters = session.getAttribute(parametersParam)
			def jsonReq = [sessionId: sessionId, parameters: parameters, callbackURL: params.requestor ? params.requestor : ""]
			def jsonOut = jsonReq as JSON
			log.debug("doHttpRequest: " + jsonOut.toString())
			log.info("request URL " + reqURL)
			
			IHttpClientUtil httpClientUtil = new HttpClientUtil();
			httpClientUtil.setupRequest(IHttpClientUtil.Method.POST, reqURL)
			httpClientUtil.setPostData(jsonOut.toString())
			IHttpClientUtil.IResponse resp = httpClientUtil.executeRequest();
			rescode = resp.getResponseCode()
			
			if (rescode == 200) {
				JSONObject resJSON = new JSONObject(resp.getResponseString(-1))
				log.debug("httpResponseCode: " + rescode)
				log.debug("JSON response: " + resJSON.toString())
				decision = resJSON.getString("decision")
			} else if (rescode == 401) {
				decision = "DENY"
			} else {
				decision = "ERROR"
			}
		} catch (Exception ex) {
			decision = "ERROR"
			log.error("Unexpected exception occurred while trying to get the decision, Reason: " + ex.getLocalizedMessage())
		} finally {
			session.setAttribute(sessionId + "_decision", decision)
			session.setAttribute(sessionId + "_decision", decision)
			def requestor = params.requestor
			if (requestor) {
				String[] uriParts = requestor.split("/")
				if (uriParts.length >= 3) {
					forward controller: uriParts[1], action: uriParts[2], params: [status: "checkDecision"]
				}  else {
					redirect(url:requestor, params: [status: "checkDecision"]);
				}
			}
		}
	}

	def invalidateSession = {
		def sessionIdParam = "sessionId"
		if (params.sessionIdParam) {
			sessionIdParam = params.sessionIdParam
		}
		def parametersParam = "parameters"
		if (params.parametersParam) {
			parametersParam = params.parametersParam
		}
		if (session.getAttribute("authCredential")) {
			session.setAttribute("authCredential", null)
		}
		session.setAttribute(sessionIdParam, null)
		session.setAttribute(parametersParam, null)
		String[] uriParts = params.requestor.split("/")
		if (uriParts.length == 3) {
			forward controller: uriParts[1], action: uriParts[2]
		}  else {
			redirect(url:params.requestor);
		}
	}
	
/*	
	def resetCredential = {
		def basePath = _getBasePath()
		Properties configProps = ConfigUtils.loadPropertiesFile(this.class, basePath + "-config.properties")
		Boolean canResetCredential = Boolean.valueOf(configProps.getProperty("resetCredential.enable", "false"))
		// this ensures that the user cannot just point the browser to this URL and bypass credential evaluation
		if (canResetCredential) {
			def credentialUrl = configProps.getProperty("resetCredential.url", null)
			if (!credentialUrl) {
				String[] uriParts = params.requestor.split("/")
				uriParts[uriParts.length - 1] = "credentials"
				credentialUrl = StringUtils.join(uriParts, "/")
			}	
			flash.status = "validated"
			redirect(url: credentialUrl)
		} else {
			String[] uriParts = params.requestor.split("/")
			if (uriParts.length == 3) {
				forward controller: uriParts[1], action: uriParts[2]
			}  else {
				redirect(url:params.requestor);
			}
		}
	}
*/		
	def testCredentials = {
		def resName = params.id
		def testResName = params.id + "_TEST"
		def requestor = "/relyingParty/testCredentials/" + resName
		def parameters = ""
		def status = "requiredInput"
		if (params.parameters) {
			parameters = params.parameters
		}	
		if (params.status) {
			status = params.status
		}
		def sessionId = ""
		if (params.sessionId) {
			sessionId = params.sessionId
		}
		def basePath = _getBasePath()
		def model = [	resourceName: testResName,
						originalResourceName: resName,
					 	requestor: requestor,
						basePath: basePath,
						status: status,
						parameters: parameters,
						sessionId: sessionId
					]
		render (model: model, view: "testCredentials")
	}
	
	private String _getBasePath() {
		String basePath = ""
		String url = request.getRequestURL().toString()
		try {
			URI reqUri = new URI(url)
			String path = reqUri.getPath()
			if (path.startsWith("/")) {
				path = path.substring(1);
			}
			String[] urlParts = path.split("/")
			basePath = urlParts[0]
		} catch (URISyntaxException ex) {
			// just swallow basePath will be an empty string
		}
		return basePath
	} 
	
	private void forceNoResponseCache ()
	{
		response.setHeader("Pragma", "no-cache")
		response.setDateHeader("Expires", 1L )
		response.setHeader("Cache-Control", "no-cache")
		response.addHeader("Cache-Control", "no-store")
	}

}
