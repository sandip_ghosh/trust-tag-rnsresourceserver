<input type="hidden" id="checkCredentials" name="checkCredentials" value="${ checkCredentials }"/>
<div id="credentialRequestor" style="display:none">
	<div id="requiredInput" style="display:none">
		<form id="submitInputCredentials" action="" method="post"> 
		    <input type="hidden" id="status" name="status" value="${ status }"/>
		    <g:if test="${parameters != null }">
		    	<input type="hidden" id="parameters" name="parameters" value="${ parameters.encodeAsHTML() }"/> 
		    </g:if>
		    <g:else>
		    	<input type="hidden" id="parameters" name="parameters" value=""/> 
		    </g:else>	   
		    <input type="hidden" id="sessionId" name="sessionId" value="${ sessionId }"/>
		    <input type="hidden" id="resourceName" name="resourceName" value="${ resourceName }"/>
			<input type="hidden" id="requestor" name="requestor" value="${ requestor }"/>    
			<input type="hidden" id="redirect" name="redirect" value=""/>	
			<input type="hidden" id="basePath" name="basePath" value="${ basePath }"/>
			<input type="hidden" id="resetCredential" name="resetCredential" value="${ resetCredential }"/>		
		       <div class="sectionBody withShadow" style="width:800px">
		       	<div class="sectionHeader sectionHeaderShadow">Log In</div> 
		       	<table id="requiredInputTable" cellspacing="0" cellpadding="5" valign="top" style="width:780px">
		       		<tr><td colspan="2">Please enter the values required before requesting credentials</td></tr>
				</table>	        	
			</div>
			<div class="buttons" style="margin-left:50px;">
					<button id="requestCredentials" style="height:40px; font-size:14px; line-height:40px;">Log In</button>
			</div>
		</form>	
	</div>
	  	<!-- The waiting message box -->
	<g:if test="${status != 'checkDecision' }"> 
		<div class="waiting">
		  	<div class="waitingMessage"></div>
		</div>
	</g:if>	
	<!--  The error notification box -->
	<div class="error" style="display:none"></div>
	  <div id="credentialInput" style="display:none">
	      <iframe id="tbDisplay" name="tbDisplay" frameborder=0 width="100%" height="100%"></iframe>
	</div>
</div>
<div id="pageContents" style="display:none">

