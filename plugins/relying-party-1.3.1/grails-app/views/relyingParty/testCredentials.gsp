<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Credential Expression</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'dotimer.js') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Utils.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'RelyingParty.js') }"></script>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/start/jquery-ui.css" type="text/css" rel="stylesheet" media="all" />
        <link href="${resource(dir: 'css', file: 'RelyingParty.css') }" type="text/css" rel="stylesheet" />
    </head> 
	<script type="text/javascript">
    	$(document).ready(function() {

    		$("button").button();

        	$("#back").click(function(event) {
            	var backURL = "/" + RelyingParty.basePath + "/cxBuilder/editCredentialService/" + $("#originalResourceName").val() + "?invalidateSessionOnSave=true&afterTest=true";
            	location = backURL; 
        		Utils.preventDefault (event);
            });
            
            var status = $("status").val();
            if (status === "checkDecision") {
                $("#topPanel").hide();
            } 
    		RelyingParty.checkCredentials(null);
    	});		
	</script>
	<body>
		<div class="sectionBody withShadow" id="topPanel">
	    	<div class="sectionHeader sectionHeaderShadow">Test Credential Expression Evaluation</div>
	    	<!-- This is stuff from _requestCredentials, possibly can be refactored -->
			<input type="hidden" id="checkCredentials" name="checkCredentials" value="true"/>
			<div id="credentialRequestor" style="display:none">
			    <div style="height:20px"></div>
			    <form id="submitInputCredentials" action="" method="post"> 
					<div id="requiredInput" style="display:none">
						<input type="hidden" id="sessionId" name="sessionId" value="${ sessionId }"/>
						<input type="hidden" id="parameters" name="parameters" value="${ parameters.encodeAsHTML() }"/> 
					    <input type="hidden" id="status" name="status" value="${ status }"/>
					    <input type="hidden" id="requestor" name="requestor" value = "${ requestor }"/>
					    <input type="hidden" id="resourceName" name="resourceName" value="${ resourceName }"/>
					    <input type="hidden" id="originalResourceName" name="originalResourceName" value="${ originalResourceName }"/>
						<input type="hidden" id="redirect" name="redirect" value=""/>	
						<input type="hidden" id="basePath" name="basePath" value="${ basePath }"/>
					    <div class="sectionBody" style="width:800px">
					       	<div class="sectionHeader">Log In</div> 
					       	<table id="requiredInputTable" cellspacing="0" cellpadding="5" valign="top" style="width:780px">
					       		<tr><td colspan="2">Please enter the values required for evaluating the credentials</td></tr>
							</table>	        	
						</div>
						<div class="buttons" style="margin-left:50px;">
								<button id="requestCredentials" style="height:40px; font-size:14px; line-height:40px;">Log In</button>
						</div>
					</div>	
				</form>	
				
				  	<!-- The waiting message box -->
				<g:if test="${status != 'checkDecision' }"> 
					<div class="waiting">
					  	<div class="waitingMessage"></div>
					</div>
				</g:if>	
				<!--  The error notification box -->
				<div class="error" style="display:none"></div>
				  <div id="credentialInput" style="display:none">
				      <iframe id="tbDisplay" name="tbDisplay" frameborder=0 width="100%" height="100%"></iframe>
				</div>
			</div>
			<div id="pageContents" style="display:none">
				<div style="height:20px"></div>
				<div class="success">The Evaluation of the Credential Expression was successful</div>
			</div>	
			<div style="height:300px"></div>
			<div class="buttons" style="margin-left:10px">
				<button id="back">Back</button>
			</div>
	    </div>	
	</body>
</html>	        