package com.resilient.as.cxbuilder.util;

import java.text.ParseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;

/**
 * This class implements a robust parser for a Logical Credential Expression (CX)
 * A logical CX represents the boolean expression of the Credential Elements (CE)
 * selected and the logical operators (And, Or, OrderedAnd, OrderedOr and Not) that join them
 * A CE is represented by CE meta data name ([) CE instance name(s) (])e.g. LDAPPassword[resilient_auth,cisco_auth]
 * The parser is a recursive descent parser and generates an Abstract Syntax Tree (AST). It is
 * robust and will handle over or under parenthization in the expected manner for logical expressions
 * The operator precedence rules are similar to programming languages, it is Not, OrderedAnd, And, Or
 * 
 *    
 * @author sghosh
 *
 */
public class CredentialExpressionParser {
	private String credExpr;
	private JSONArray credElements;
	private String errorMessage;
	private int errorOffset;
	private Lexer lexer;
	private Parser parser;
	private boolean parseOK;
	
	private static final Log logger = LogFactory.getLog(CredentialExpressionParser.class);
	
	public CredentialExpressionParser(String credExpr, JSONArray credElts) {
		this.credExpr = credExpr.trim();
		this.credElements = credElts;
		this.lexer = new Lexer(credExpr, credElements);
		this.parser = new Parser(lexer);
		this.parseOK = false;
	}
	
	public boolean parseCredentialExpr() {
		// handle the null/empty cases
		if ((credExpr == null || credExpr.isEmpty()) && (credElements == null || credElements.isEmpty())) {
			return true;
		} else if (credExpr == null || credExpr.isEmpty()) {
			return false;
		} else if (credElements == null || credElements.isEmpty()) {
			return false;
		} else {
			try {
				parser.getExpr();
				if (parser.astRoot != null) {
					parseOK = true;
					logger.info("Successfully parsed the Credential Expression: " + credExpr);
					// parsed fine, now we need to make sure all the credential elements are referenced
					String parsedExpr = parser.toString();
					for (int i = 0; i < credElements.size(); i++) {
						JSONObject credElementObj = credElements.getJSONObject(i);
						String ceName = Lexer.getCredElementName(credElementObj);
						String oldCEName = Lexer.getCredElementName(credElementObj, false);
						if (parsedExpr.indexOf(ceName) == -1 && parsedExpr.indexOf(oldCEName) == -1) {
							errorMessage = "Did not find the Credential Element: " + ceName + " referenced in the Credential Expression";
							errorOffset = 0;
							logger.error(errorMessage);
							return false;
						}
					}
				}
				return true;
			} catch (ParseException ex) {
				errorMessage = ex.getLocalizedMessage();
				errorOffset = ex.getErrorOffset();
				logger.error("Failed to parse Credential Expression: " + credExpr + " Error: " + ex.getLocalizedMessage());
				return false;
			}
		}
	}
	
	public JSONObject getCredentialExprJSON() {
		if (!parseOK) {
			parseOK = parseCredentialExpr();
		}
		if (!parseOK) {
			return null;
		}
		return buildCredentialExprJSON(parser.astRoot);
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public int getErrorOffset() {
		return this.errorOffset;
	}
	
	public String getParsedExpression() {
		return parser.toString();
	}

	private JSONObject buildCredentialExprJSON(Parser.Node node) {
		if (node == null) {
			return null;
		}
		if (node.type == Parser.NodeType.CE_NODE) {
			JSONObject ceObj = getCredElementJSON(node.credElementName);
			if (ceObj == null) {
				return null;
			}
			return CredentialExpressionHelpers.buildCredentialElement(ceObj.toString());
		}
		JSONObject joinObj = new JSONObject();
		joinObj.put("nodeName", node.toString());
		JSONArray nodeChildren = new JSONArray();
		joinObj.put("children", nodeChildren);
		JSONObject leftChild = buildCredentialExprJSON(node.left);
		if (leftChild != null) {
			if (node.left.type != node.type) {
				nodeChildren.add(leftChild);
			} else {
				nodeChildren.addAll(leftChild.getJSONArray("children"));
			}
		}
		JSONObject rightChild = buildCredentialExprJSON(node.right);
		if (rightChild != null) {
			if (node.right.type != node.type) {
				nodeChildren.add(rightChild);
			} else {
				nodeChildren.addAll(rightChild.getJSONArray("children"));
			}
		}
		return joinObj;
	}
	
	private JSONObject getCredElementJSON(String credElementName) {
		for (int i = 0; i < credElements.size(); i++) {
			JSONObject credElementObj = credElements.getJSONObject(i);
			String ceName = Lexer.getCredElementName(credElementObj);
			String oldCEName = Lexer.getCredElementName(credElementObj, false);
			if (ceName.equals(credElementName) || oldCEName.equals(credElementName)) {
				return credElementObj;
			}
		}
		return null;
	}
	
	private static class Lexer {
		
		enum TokenType {
			AND_TOK,
			OR_TOK,
			NOT_TOK,
			ORDERED_AND_TOK,
			ORDERED_OR_TOK,
			CE_TOK,
			LP_TOK,
			RP_TOK,
			END_TOK
		}
		
		public static String AND_TOKEN = "And";
		public static String OR_TOKEN = "Or";
		public static String NOT_TOKEN = "Not";
		public static String ORDERED_AND_TOKEN = "OrderedAnd";
		public static String ORDERED_OR_TOKEN = "OrderedOr";
		public static String LP_TOKEN = "(";
		public static String RP_TOKEN = ")";
		
		private String credExpr;
		private JSONArray credElements;		
		private boolean init = true;
		private String nextTokenString;
		private int charPos = 0;
		
		Lexer(String credExpr, JSONArray credElements) {
			this.credExpr = credExpr;
			this.credElements = credElements;
		}
		
		private static class Token {
			TokenType tokenType;
			String credElementName;
		}
		
		String getTokenString() {
			StringBuilder tokString = new StringBuilder();
			while (charPos < credExpr.length() && Character.isWhitespace(credExpr.charAt(charPos))) {
				charPos++;
			}
			if (charPos == credExpr.length()) {
				return "";
			} else if (credExpr.charAt(charPos) == '(') {
				charPos++;
				return "(";
			} else if (credExpr.charAt(charPos) == ')') {
				charPos++;
				return ")";
			} else {
				while (charPos < credExpr.length() && !Character.isWhitespace(credExpr.charAt(charPos)) && credExpr.charAt(charPos) != ')') {
					tokString.append(credExpr.charAt(charPos));
					charPos++;
				}
				return tokString.toString();
			}
		}
		
		Token getToken() throws ParseException {
			String tokString = "";
			Token theToken = new Token();
			if (init) {
				init = false;
				tokString = getTokenString();
			} else {
				tokString = nextTokenString;
			}
			if (tokString.isEmpty()) {
				theToken.tokenType = TokenType.END_TOK;
			} else if (tokString.equalsIgnoreCase(LP_TOKEN)) {
				theToken.tokenType = TokenType.LP_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(RP_TOKEN)) {
				theToken.tokenType = TokenType.RP_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(ORDERED_AND_TOKEN)) {
				theToken.tokenType = TokenType.ORDERED_AND_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(AND_TOKEN)) {
				theToken.tokenType = TokenType.AND_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(ORDERED_OR_TOKEN)) {
				theToken.tokenType = TokenType.ORDERED_OR_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(OR_TOKEN)) {
				theToken.tokenType = TokenType.OR_TOK;
				nextTokenString = getTokenString();
			} else if (tokString.equalsIgnoreCase(NOT_TOKEN)) {
				theToken.tokenType = TokenType.NOT_TOK;
				nextTokenString = getTokenString();
			} else {
				// check if its a valid Credential Element name
				if (isValidCredElementName(tokString)) {
					theToken.tokenType = TokenType.CE_TOK;
					theToken.credElementName = tokString;
					nextTokenString = getTokenString();
				} else if (isValidOldCredElementName(tokString)) {
					theToken.tokenType = TokenType.CE_TOK;
					theToken.credElementName = convertToNewCredElementName(tokString);
					nextTokenString = getTokenString();
				} else {
					int errOffset = charPos - tokString.length();
					String errMsg = "Unexpected characters '" + tokString + "' found at position: " + errOffset + " while parsing the logical Credential Expression";
					throw new ParseException(errMsg, errOffset);
				}
			}
			return theToken;
		}
		
		static String getCredElementName(JSONObject ceObj) {
			return getCredElementName(ceObj, true);
		}
		
		static String getCredElementName(JSONObject ceObj, boolean newFormat) {
			String tsNames = "";
			if (newFormat) {
				String[] grantingAuthArray = ceObj.getString("grantingAuthority").split(",");
				for (int i = 0; i < grantingAuthArray.length; i++) {
					String[] gaParts = grantingAuthArray[i].split("/");
					String tsName = gaParts[gaParts.length - 1];
					if (tsNames.length() > 0) {
						tsNames += ",";
					}
					tsNames += tsName;
				}
			} else {
				// assume the GA list has a singleton authority
				String[] gaParts = ceObj.getString("grantingAuthority").split("/");
				String tsName = gaParts[gaParts.length - 1];
				tsNames = tsName;
			}
			String ceName = newFormat ? ceObj.getString("name") + "[" + tsNames + "]" : ceObj.getString("name") + "." + tsNames;
			return ceName;
		}

		private boolean isValidOldCredElementName(String tokString) throws JSONException{
			for (int i = 0; i < credElements.size(); i++) {
				JSONObject credElementObj = credElements.getJSONObject(i);
				String oldCEName = getCredElementName(credElementObj, false);
				if (tokString.equals(oldCEName)) {
					return true;
				}
			}
			return false;
		}
		
		private boolean isValidCredElementName(String tokString) throws JSONException{
			for (int i = 0; i < credElements.size(); i++) {
				JSONObject credElementObj = credElements.getJSONObject(i);
				String ceName = getCredElementName(credElementObj);
				if (tokString.equals(ceName)) {
					return true;
				}
			}
			return false;
		}
		
		private String convertToNewCredElementName(String tokString) throws JSONException {
			for (int i = 0; i < credElements.size(); i++) {
				JSONObject credElementObj = credElements.getJSONObject(i);
				String oldCEName = getCredElementName(credElementObj, false);
				if (tokString.equals(oldCEName)) {
					return getCredElementName(credElementObj);
				}
			}
			return null;
		}
	}
	
	
	/*
	 * The recursive descent parser. It parses an EBNF grammer defined as follows:
	 * Expr : OrExpr
	 * OrExpr : OrderedOrExpr { "Or" OrderedOrExpr }
	 * OrderedOrExpr : AndExr { "OrderedOr" AndExpr }
	 * AndExpr : OrderedAndExpr { "And" OrderedAndExpr }
	 * OrderedAndExpr :  NotExpr { "OrderedAndExpr" NotExpr }
	 * NotExpr : { "Not" } ParenExpr
	 * ParenExpr : CE | "(" Expr ")" 
	 */
	static class Parser {
		enum NodeType {
			NOT_NODE,
			AND_NODE,
			ORDERED_AND_NODE,
			ORDERED_OR_NODE,
			OR_NODE,
			CE_NODE
		}
		
		static class Node {
			NodeType type;
			String credElementName;
			Node left;
			Node right;
			
			public String toString() {
				switch(type) {
					case NOT_NODE:
						return Lexer.NOT_TOKEN;
					case AND_NODE:
						return Lexer.AND_TOKEN;
					case ORDERED_AND_NODE:
						return Lexer.ORDERED_AND_TOKEN;
					case ORDERED_OR_NODE:
						return Lexer.ORDERED_OR_TOKEN;
					case OR_NODE:
						return Lexer.OR_TOKEN;
					case CE_NODE:
						return credElementName;
					default:
						return "";
				}
			}
		}
		
		private Lexer.Token lookAhead;
		private Lexer lexer;
		private Node astRoot;
		
		public Parser(Lexer lexer) {
			this.lexer = lexer;
		}
		
		private Node newNode() {
			Node node = new Node();
			node.type = NodeType.CE_NODE;
			node.credElementName = "";
			node.left = null;
			node.right = null;
			return node;
		}
		
		private Node orExpr() throws ParseException {
			Node e = null;
			Node f = null;
			
			e = orderedOrExpr();
			if (lookAhead.tokenType == Lexer.TokenType.OR_TOK) {
				lookAhead = lexer.getToken();
				f = newNode();
				f.type = NodeType.OR_NODE;
				f.left = e;
				f.right = orExpr();
				return f;
			} else {
				return e;
			}
		}

		private Node orderedOrExpr() throws ParseException {
			Node e = null;
			Node f = null;
			
			e = andExpr();
			if (lookAhead.tokenType == Lexer.TokenType.ORDERED_OR_TOK) {
				lookAhead = lexer.getToken();
				f = newNode();
				f.type = NodeType.ORDERED_OR_NODE;
				f.left = e;
				f.right = orderedOrExpr();
				return f;
			} else {
				return e;
			}
		}

		private Node andExpr() throws ParseException {
			Node e = null;
			Node f = null;
			
			e = orderedAndExpr();
			if (lookAhead.tokenType == Lexer.TokenType.AND_TOK) {
				lookAhead = lexer.getToken();
				f = newNode();
				f.type = NodeType.AND_NODE;
				f.left = e;
				f.right = andExpr();
				return f;
			} else {
				return e;
			}
		}

		private Node orderedAndExpr() throws ParseException {
			Node e = null;
			Node f = null;
			
			e = notExpr();
			if (lookAhead.tokenType == Lexer.TokenType.ORDERED_AND_TOK) {
				lookAhead = lexer.getToken();
				f = newNode();
				f.type = NodeType.ORDERED_AND_NODE;
				f.left = e;
				f.right = orderedAndExpr();
				return f;
			} else {
				return e;
			}
		}
		
		private Node notExpr() throws ParseException {
			Node f = null;

			if (lookAhead.tokenType == Lexer.TokenType.NOT_TOK) {
				lookAhead = lexer.getToken();
				f = newNode();
				f.type = NodeType.NOT_NODE;
				f.left = notExpr();
				return f;
			} else {
				return parenExpr();
			}
		}
		
		private Node parenExpr() throws ParseException {
			Node e;
			if (lookAhead.tokenType == Lexer.TokenType.LP_TOK) {
				lookAhead = lexer.getToken();
				e = orExpr();
				if (lookAhead.tokenType != Lexer.TokenType.RP_TOK) {
					throw new ParseException("Credential Expression parsing error, ')' expected!", lexer.charPos);
				} else {
					lookAhead = lexer.getToken();
				}
				return e;
			} else if (lookAhead.tokenType == Lexer.TokenType.CE_TOK) {
				e = newNode();
				e.type = NodeType.CE_NODE;
				e.credElementName = lookAhead.credElementName;
				lookAhead = lexer.getToken();
				return e;
			} else {
				throw new ParseException("Unexpected token found, a Credential Element token was expected", lexer.charPos);
			}
		}
		
		public Node getExpr() throws ParseException {
			
			lookAhead = lexer.getToken();
			
			astRoot = orExpr();
			if (lookAhead.tokenType != Lexer.TokenType.END_TOK) {
				throw new ParseException("Extraneous character found after parsing the credential expression", lexer.charPos);
			}
			return astRoot;
		}
		
		private String printExpr(Node e) {
			StringBuilder exprBuilder = new StringBuilder();
			switch(e.type) {
				case OR_NODE:
					exprBuilder.append("(").append(printExpr(e.left)).append(" Or ").append(printExpr(e.right)).append(")");
					break;
				case ORDERED_OR_NODE:
					exprBuilder.append("(").append(printExpr(e.left)).append(" OrderedOr ").append(printExpr(e.right)).append(")");
					break;
				case AND_NODE:
					exprBuilder.append("(").append(printExpr(e.left)).append(" And ").append(printExpr(e.right)).append(")");
					break;
				case ORDERED_AND_NODE:
					exprBuilder.append("(").append(printExpr(e.left)).append(" OrderedAnd ").append(printExpr(e.right)).append(")");
					break;
				case NOT_NODE:
					exprBuilder.append("Not (").append(printExpr(e.left)).append(")");
					break;
				case CE_NODE:
					exprBuilder.append(e.credElementName);
					break;
				default:
					exprBuilder.append("ERROR: ").append(e.type);
			}
			return exprBuilder.toString();
		}
		
		public String toString() {
			if (astRoot == null) {
				return "()";
			} else {
				return printExpr(astRoot);
			}
		} 
	}

}
