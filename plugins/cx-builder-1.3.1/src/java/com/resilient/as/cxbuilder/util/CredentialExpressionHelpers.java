package com.resilient.as.cxbuilder.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;

import com.resilient.ta.TrustAuthority;
import com.resilient.ta.TrustAuthorityDiscoveryService;
import com.resilient.ta.TrustAuthorityDiscoveryServiceFactory;
import com.resilient.ta.md.impl.CredElmtMetaDataGrantingAuthority;
import com.resilient.ta.md.impl.CredElmtMetaDataParameter;
import com.resilient.ta.md.impl.CredentialElementMetaData;

/**
 * A set of Java helper methods that can be invoked from Groovy Code
 * All methods here are public static
 * 
 * @author sghosh
 *
 */
public class CredentialExpressionHelpers {
	
	public static final String DATA_TOKEN_TYPE = "rns:data-token";
	public static final String DATA_TOKEN_GENERATE_TEMPLATE = "<rns:data-token-service:generate-token>";
	
	private static final Log log = LogFactory.getLog(CredentialExpressionHelpers.class.getName());
	
	public static String extractNameFromOldCredExpr(String credExpr) {
		return extractPropFromOldCredExpr(credExpr, "TC");
	}
	
	public static String extractNameFromNewCredExpr(String credExpr) {
		try {
			JSONObject jsonObj = new JSONObject(credExpr);
			JSONObject credIdentifier = jsonObj.getJSONObject("credentialIdentifier");
			if (credIdentifier != null) {
				return credIdentifier.getString("name");
			}
		} catch (JSONException ex) {
			return "";
		}
		return "";
	}

	public static boolean isCredentialExprInOldFormat(String credExpr) {
		String credNameOld = extractPropFromOldCredExpr(credExpr, "TC");
		return credNameOld.length() != 0;
	}
	
	// This is just a temporary convenience converter for credential expressions
	// in the old format to the new one. It is not robust and is a temporary
	// testing tool for credential expression creation
	// DOES NOT SUPPORT COMPOUND CREDENTIAL EXPRESSIONS
	public static String convertCredExprToNewFormat(String credExpr) {
		String credName = extractPropFromOldCredExpr(credExpr, "TC");
		String grantingAuthority = extractPropFromOldCredExpr(credExpr, "TSP");
		Map<String, String> parameters = extractParametersFromOldCredExpr(credExpr);
		JSONObject newCredExpr = new JSONObject();
		newCredExpr.put("nodeName", "CredentialElement");
		JSONObject credIdentifier = new JSONObject().put("name", credName);
		newCredExpr.put("credentialIdentifier", credIdentifier);
		JSONObject grantingAuth = new JSONObject().put("uri", grantingAuthority);
		JSONArray grantingAuthorities = new JSONArray().put(new JSONArray().put(grantingAuth));
		newCredExpr.put("grantingAuthorities", grantingAuthorities);
		JSONArray paramsArray = new JSONArray();
		for (Map.Entry<String, String> entry: parameters.entrySet()) {
			String paramName = entry.getKey();
			String paramValue = entry.getValue();
			JSONObject paramObj = new JSONObject().put("historyKey", true);
			paramObj.put("key", paramName);
			paramObj.put("value", paramValue);
			paramsArray.add(paramObj);
		}
		newCredExpr.put("parameters", paramsArray);
		// static frequency array
		JSONObject frequency = new JSONObject().put("nodeName", "SameSession").put("maxAge", 60000);
		newCredExpr.put("frequency", new JSONArray().put(frequency));
		return newCredExpr.toString(4);
	}

	// Validates the credential expression, the validation criteria is:
	// 1. should be valid JSON
	// 2. the node name should be credential element
	// 3. should have a valid name
	// 4. should have atleast one granting authority
	public static boolean isValidCredentialExpr(String credExpr) {
		if (isCredentialExprInOldFormat(credExpr)) {
			return false;
		}
		JSONObject credExprObj = null;
		try {
			credExprObj = new JSONObject(credExpr);
			String nodeName = credExprObj.getString("nodeName");
			if (nodeName == null || nodeName.isEmpty() || !nodeName.equalsIgnoreCase("CredentialElement")) {
				return false;
			}
			String credName = extractNameFromNewCredExpr(credExpr);
			if (credName == null || credName.isEmpty()) {
				return false;
			}
			JSONArray grantingAuthorities = (JSONArray)credExprObj.getJSONArray("grantingAuthorities").get(0);
			if (grantingAuthorities == null || grantingAuthorities.length() == 0) {
				return false;
			}
			return true;
		} catch (JSONException ex) {
			return false;
		}
	}
	
	public static String replaceParamsInCredentialExpr(String credExpr, Map<String, String> paramValues) {
		JSONObject credExprObj = new JSONObject(credExpr);
		try {
			_replaceParamsInCredentialExpr(credExprObj, paramValues);
		} catch(JSONException ex) {
			log.error("Error replacing params in credential expression. Reason: " + ex.getLocalizedMessage());
			return credExpr;
		}
		return credExprObj.toString();
	}
	
	private static void _replaceParamsInCredentialExpr(JSONObject cxNode, Map<String, String> paramValues) throws JSONException{
		if (cxNode == null) {
			return;
		}
		if (cxNode.getString("nodeName").equals("CredentialElement")) {
			JSONArray parameters = cxNode.getJSONArray("parameters");
			for (int i = 0; i < parameters.length(); i++) {
				JSONObject parameter = parameters.getJSONObject(i);
				if (parameter.has("value")) {
					String paramValue = parameter.getString("value");
					if (paramValue.startsWith("<") && paramValue.endsWith(">")) {
						String paramName = paramValue.substring(1, paramValue.length() - 1);
						if (paramValues.containsKey(paramName)) {
							parameter.put("value", paramValues.get(paramName));
						}
					}
				} else if (parameter.has("link")) {
					JSONObject linkObj = parameter.getJSONObject("link");
					String hrefValue = linkObj.getString("href");
					String paramName = parameter.getString("key");
					if (hrefValue.equals("<" + paramName + ">")) {
						if (paramValues.containsKey(paramName)) {
							linkObj.put("href", paramValues.get(paramName));
						}
					}
				}
			}
		} else {
			JSONArray cxNodeChildren = cxNode.getJSONArray("children");
			for (int child = 0; child < cxNodeChildren.size(); child++) {
				JSONObject cxNodeChild = cxNodeChildren.getJSONObject(child);
				_replaceParamsInCredentialExpr(cxNodeChild, paramValues);
			}
		}
		
	}
	
	public static String getGrantingAuthorities(String credExpr) {
		JSONObject credExprObj = new JSONObject(credExpr);
		JSONArray grantingAuthorities = (JSONArray)credExprObj.getJSONArray("grantingAuthorities").get(0);
		StringBuilder grantingAuthsBuilder = new StringBuilder();
		for (int i = 0; i < grantingAuthorities.length(); i++) {
			JSONObject grantingAuth = grantingAuthorities.getJSONObject(i);
			if (grantingAuthsBuilder.length() != 0) {
				grantingAuthsBuilder.append(", ");
			}
			grantingAuthsBuilder.append(grantingAuth.get("uri"));
		}
		return grantingAuthsBuilder.toString();
	}
	
	public static String getParameterNames(String credName) {
		try {
			CredentialElementMetaData md = getCredentialElementMetaData(credName);
			if (md == null) {
				return "";
			}
			List<String> paramNames = md.getParameterKeys();
			StringBuilder paramsBuilder = new StringBuilder();
			for(String paramName: paramNames) {
				if (paramsBuilder.length() != 0) {
					paramsBuilder.append(", ");
				}
				paramsBuilder.append(paramName);
			}
			return paramsBuilder.toString();
//			JSONObject credExprObj = new JSONObject(credExpr);
//			JSONArray parameters = credExprObj.getJSONArray("parameters");
//			StringBuilder paramsBuilder = new StringBuilder();
//			for (int i = 0; i < parameters.length(); i++) {
//				JSONObject paramObj = parameters.getJSONObject(i);
//				if (paramsBuilder.length() != 0) {
//					paramsBuilder.append(", ");
//				}
//				paramsBuilder.append(paramObj.get("key"));
//			}
//			return paramsBuilder.toString();
		} catch (Exception ex) {
			return "";
		}
	}

	public static CredentialElementMetaData getCredentialElementMetaData(String name) {
		// TODO fix this quick hack!
		TrustAuthorityDiscoveryService discoveryService = TrustAuthorityDiscoveryServiceFactory.create();		
		Iterator<TrustAuthority> taIterator = discoveryService.find();
		while(taIterator.hasNext()) {
			TrustAuthority ta = taIterator.next();
			if (ta.getCredentialElementMetaData(name) != null) {
				return ta.getCredentialElementMetaData(name);
			}
		}
		return null;
	}

	public static String getCredentialElementMetaDataJSON(String name, boolean prettyPrint) {
		CredentialElementMetaData md = getCredentialElementMetaData(name);
		JSONObject mdJSON = getCredentialMDJSON(md);
		return prettyPrint ? mdJSON.toString(4) : mdJSON.toString();
	}

	public static Collection<CredentialElementMetaData> findCredentialElementMetaData(String partialName) {
		List<CredentialElementMetaData> foundMDs = null;
		TrustAuthorityDiscoveryService discoveryService = TrustAuthorityDiscoveryServiceFactory.create();		
		Iterator<TrustAuthority> taIterator = discoveryService.find();
		while(taIterator.hasNext()) {
			TrustAuthority ta = taIterator.next();
			Collection<CredentialElementMetaData> foundMDsInTA = ta.findCredentialElementMetaData(partialName);
			if (foundMDsInTA != null && foundMDs == null) {
				foundMDs = new ArrayList<CredentialElementMetaData>();
			}
			if (foundMDsInTA != null && foundMDsInTA.size() > 0) {
				foundMDs.addAll(foundMDsInTA);
			}
		}
		return foundMDs;
	}
	
	public static String findCredentialElementMetaDataJSON(String partialName) {
		Collection<CredentialElementMetaData> foundMDs = findCredentialElementMetaData(partialName);
		if (foundMDs == null) {
			return null;
		}
		JSONObject foundMDsJSON = new JSONObject();
		JSONArray foundMDsArray = new JSONArray();
		foundMDsJSON.put("count", foundMDs.size());
		if (foundMDs.size() > 0) {
			Iterator<CredentialElementMetaData> foundMDsIterator = foundMDs.iterator();
			while(foundMDsIterator.hasNext()) {
				CredentialElementMetaData foundMD = foundMDsIterator.next();
				JSONObject foundMDJSON = getCredentialMDJSON(foundMD);
				foundMDsArray.add(foundMDJSON);
			}
		}
		foundMDsJSON.put("mds", foundMDsArray);
		return foundMDsJSON.toString();
	}
	
	public static JSONObject buildCredentialElement(String credentialJSON) {
		try {
			JSONObject credObjJSON = new JSONObject(credentialJSON);
			String name = credObjJSON.getString("name");
			CredentialElementMetaData md = getCredentialElementMetaData(name);
			if (md == null) {
				return null;
			}
			JSONObject credExprJSON = new JSONObject();
			credExprJSON.put("nodeName", "CredentialElement");
			JSONObject credIdentifier = new JSONObject();
			credIdentifier.put("name", name);
			credExprJSON.put("credentialIdentifier", credIdentifier);
			String grantingAuthorities = credObjJSON.getString("grantingAuthority");
			String[] gaList = grantingAuthorities.split(",");
			JSONArray grantingAuthsArray = new JSONArray();
			for (String ga : gaList) {
				JSONObject grantingAuthObj = new JSONObject();
				grantingAuthObj.put("uri", ga);
				JSONArray routeList = new JSONArray(Collections.singletonList(grantingAuthObj));
				grantingAuthsArray.add(routeList);
			}
			credExprJSON.put("grantingAuthorities", grantingAuthsArray);
			JSONArray paramMapping = credObjJSON.getJSONArray("parameterMapping");
			JSONArray credExprParams = new JSONArray();
			Map<String, CredElmtMetaDataParameter> parametersMD = md.getParameters();
			for (Map.Entry<String, CredElmtMetaDataParameter> entry : parametersMD.entrySet()) {
				String paramName = entry.getKey();
				CredElmtMetaDataParameter paramMD = entry.getValue();
				String paramValue = paramMD.getValue();
				String paramType = paramMD.getType();
				// find the param mapping value for the param
				boolean foundMapping = false;
				for (int i = 0; i < paramMapping.size(); i++) {
					JSONObject paramMap = paramMapping.getJSONObject(i);
					if (paramMap.getString("name").equals(paramName)) {
						foundMapping = true;
						if (paramMap.getBoolean("isRequired")) {
							JSONObject paramObj = new JSONObject();
							paramObj.put("historyKey", paramMap.getBoolean("historyKey"));
							paramObj.put("key", paramName);
							if (paramType.equals(DATA_TOKEN_TYPE)) {
								JSONObject linkObj = new JSONObject();
								linkObj.put("rel", DATA_TOKEN_TYPE);
								linkObj.put("href", "<" + paramValue + ">");
								paramObj.put("link", linkObj);
							} else {
								paramValue = paramMap.getString("value");
								if (!paramMap.getBoolean("isStatic")) {
									paramValue = "<" + paramValue + ">";
								}
								paramObj.put("value", paramValue);
							}	
							credExprParams.put(paramObj);
						}
						break;
					}
				}
				if (!foundMapping) {
					if (paramMD.isRequired()) {
						JSONObject paramObj = new JSONObject();
						paramObj.put("historyKey", true);
						paramObj.put("key", paramName);
						if (paramType.equals(DATA_TOKEN_TYPE)) {
							JSONObject linkObj = new JSONObject();
							linkObj.put("rel", DATA_TOKEN_TYPE);
							linkObj.put("href", "<" + paramName + ">");
							paramObj.put("link", linkObj);
						} else {
							paramObj.put("value", paramValue);
						}
						credExprParams.put(paramObj);
					}
				}
			}
			credExprJSON.put("parameters", credExprParams);
			if (credObjJSON.getString("frequencyType") != null) {
				JSONObject frequencyObj = new JSONObject();
				frequencyObj.put("nodeName", credObjJSON.getString("frequencyType"));
				frequencyObj.put("maxAge", credObjJSON.getInt("maxAge"));
				credExprJSON.put("frequency", frequencyObj);
			}
			return credExprJSON;
		} catch (Exception ex) {
			return null;
		}
	}

	public static JSONObject buildCredentialParamsJSON(String credentialJSON) {
		try {
			JSONObject credJSONObj = new JSONObject(credentialJSON);
			JSONObject credParamJSON = new JSONObject();
			credParamJSON.put("name", credJSONObj.getString("name"));
			credParamJSON.put("grantingAuthority", credJSONObj.getString("grantingAuthority"));
			credParamJSON.put("frequencyType", credJSONObj.getString("frequencyType"));
			credParamJSON.put("maxAge", credJSONObj.getInt("maxAge"));
			CredentialElementMetaData md = getCredentialElementMetaData(credJSONObj.getString("name"));
			if (md == null) {
				return null;
			}
			JSONArray paramMapping = credJSONObj.getJSONArray("parameterMapping");
			JSONArray credParams = new JSONArray();
			Map<String, CredElmtMetaDataParameter> parametersMD = md.getParameters();
			for (Map.Entry<String, CredElmtMetaDataParameter> entry : parametersMD.entrySet()) {
				String paramName = entry.getKey();
				CredElmtMetaDataParameter paramMD = entry.getValue();
				JSONObject paramObj = new JSONObject();
				paramObj.put("name", paramName);
				paramObj.put("displayName", paramMD.getDisplayName());
				paramObj.put("type", paramMD.getType());
				boolean foundMapping = false;
				for (int i = 0; i < paramMapping.size(); i++) {
					JSONObject paramMap = paramMapping.getJSONObject(i);
					if (paramMap.getString("name").equals(paramName)) {
						foundMapping = true;
						paramObj.put("mapping", paramMap);
						break;
					}	
				}
				if (!foundMapping) {
					boolean isRequired = paramMD.isRequired();
					JSONObject paramMapJSONObj = new JSONObject();
					paramMapJSONObj.put("isStatic", true);
					paramMapJSONObj.put("isRequired", isRequired);
					paramMapJSONObj.put("historyKey", true);
					paramMapJSONObj.put("value", "");
					paramObj.put("mapping", paramMapJSONObj);
				}
				credParams.add(paramObj);
			}	
			credParamJSON.put("parameters", credParams);
			return credParamJSON;
		} catch (Exception ex) {
			return null;
		}
	}
	
	// some helpers for DTS Client
	public static boolean hasDataTokenParameters(String credExpr) {
		try {
			JSONObject credExprObj = new JSONObject(credExpr);
			return _hasDataTokenParameters(credExprObj);
		} catch (JSONException ex) {
			return false;
		}
	}
	
	private static boolean _hasDataTokenParameters(JSONObject cxNode) {
		if (cxNode == null) {
			return false;
		}
		if (cxNode.getString("nodeName").equals("CredentialElement")) {
			JSONArray parameters = cxNode.getJSONArray("parameters");
			for (int i = 0; i < parameters.length(); i++) {
				JSONObject parameter = parameters.getJSONObject(i);
				if (parameter.has("link")) {
					JSONObject linkObj = parameter.getJSONObject("link");
					if (linkObj.has("rel") && linkObj.getString("rel").equals(DATA_TOKEN_TYPE)) {
						return true;
					}
				}
			}
			return false;
		} else {
			JSONArray cxNodeChildren = cxNode.getJSONArray("children");
			for (int child = 0; child < cxNodeChildren.size(); child++) {
				JSONObject cxNodeChild = cxNodeChildren.getJSONObject(child);
				if (_hasDataTokenParameters(cxNodeChild)) {
					return true;
				}
			}
			return false;
		}
	}
	
	public static List<String> getDTSResolveURIs(String credExpr) {
		try {
			JSONObject credExprObj = new JSONObject(credExpr);
			return _getDTSResolveURIs(credExprObj);
		} catch(JSONException ex) {
			return null;
		}
	}
	
	private static List<String> _getDTSResolveURIs(JSONObject cxNode) {
		if (cxNode == null) {
			return new ArrayList<String>();
		}
		if (cxNode.getString("nodeName").equals("CredentialElement")) {
			List<String> resolveURIs = new ArrayList<String>();
			JSONArray parameters = cxNode.getJSONArray("parameters");
			for (int i = 0; i < parameters.length(); i++) {
				JSONObject parameter = parameters.getJSONObject(i);
				if (parameter.has("link")) {
					JSONObject linkObj = parameter.getJSONObject("link");
					if (linkObj.has("rel") && linkObj.getString("rel").equals(DATA_TOKEN_TYPE)) {
						String gaListStr = getGrantingAuthorities(cxNode.toString());
						String[] gaList = gaListStr.split(",");
						for (String ga : gaList) {
							resolveURIs.add(ga.trim());
						}
					}
					break;
				}
			}
			return resolveURIs;
		} else {
			List<String> resolveURIs = new ArrayList<String>();
			JSONArray cxNodeChildren = cxNode.getJSONArray("children");
			for (int child = 0; child < cxNodeChildren.size(); child++) {
				JSONObject cxNodeChild = cxNodeChildren.getJSONObject(child);
				resolveURIs.addAll(_getDTSResolveURIs(cxNodeChild));
			}
			return resolveURIs;
		}		
	}
	
	// TODO: maybe we should incorporate Jackson JSON conversions from MD elements to JSON
	// this is tedious code to construct JSON from MD elements
	private static JSONObject getCredentialMDJSON(CredentialElementMetaData md) {
		assert md != null;
		JSONObject mdJSON = new JSONObject();
		mdJSON.put("name", md.getName());
		List<CredElmtMetaDataGrantingAuthority> grantingAuths = md.getAuthorities();
		JSONArray grantingAuthsJSON = new JSONArray();
		for (CredElmtMetaDataGrantingAuthority grantingAuth: grantingAuths) {
			grantingAuthsJSON.add(grantingAuth.getRtnUri());
		}
		mdJSON.put("ta", md.getMdPublisher());
		mdJSON.put("desc", md.getDescription());
		mdJSON.put("auths", grantingAuthsJSON);
		mdJSON.put("dispname", md.getDisplayName());
		Map<String, CredElmtMetaDataParameter> params = md.getParameters();
		JSONArray paramsArray = new JSONArray();
		for(Map.Entry<String, CredElmtMetaDataParameter> entry: params.entrySet()) {
			String paramName = entry.getKey();
			CredElmtMetaDataParameter param = entry.getValue();
			JSONObject paramJSON = new JSONObject();
			paramJSON.put("name", paramName);
			paramJSON.put("displayName", param.getDisplayName());
			paramJSON.put("type", param.getType());
			paramJSON.put("value", param.getValue());
			paramJSON.put("required", param.isRequired());
			paramsArray.add(paramJSON);
		}
		mdJSON.put("parameters", paramsArray);
		return mdJSON;
	}
	
	private static Map<String, String> extractParametersFromOldCredExpr(String credExpr) {
		int startPos = -1;
		int endPos = -1;
		startPos = credExpr.indexOf("Parameters[");
		if (startPos != -1) {
			startPos = startPos + "Parameters[".length();
			endPos = credExpr.indexOf("]", startPos + 1);
			String paramsStr = credExpr.substring(startPos, endPos);
			String[] params = paramsStr.split(",");
			Map<String, String> paramsMap = new HashMap<String, String>();
			for (String param : params) {
				String[] paramParts = param.split("=");
				assert paramParts.length == 2;
				String paramName = paramParts[0];
				String paramValue = paramParts[1].replaceAll("\"", "");
				paramsMap.put(paramName, paramValue);
			}
			return paramsMap;
		}
		return null;
	}
	
	private static String extractPropFromOldCredExpr(String credExpr, String propName) {
		int startPos = -1;
		int endPos = -1;
		propName += "=\"";
		startPos = credExpr.indexOf(propName); 
		if (startPos != -1) {
			startPos = startPos + propName.length();
			endPos = credExpr.indexOf('\"', startPos + 1);
		}
		if (startPos != -1 && endPos != -1) {
			return credExpr.substring(startPos, endPos);
		}
		return "";
		
	}

}
