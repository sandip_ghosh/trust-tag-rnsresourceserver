var Utils = {

	getSafeIdFromName : function(name) {
		name = name.replace(/[^a-zA-Z0-9_]/g, "_");
		return name;
	},

	setMessageBox : function(msg, style) {
		$("#message").text(msg);
		if ($("#message").hasClass("error")) {
			$("#message").removeClass("error");
		} else if ($("#message").hasClass("warning")) {
			$("#message").removeClass("warning");
		} else if ($("#message").hasClass("success")) {
			$("#message").removeClass("success");
		}
		$("#message").addClass(style);
	},
	
	showMessageBox : function(title, msg, style) {
		this.setMessageBox(msg, style);
		var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:title, modal:true,
			width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
			buttons:{'OK': function() {$(this).dialog('close'); }}
		});
		confirmDlg.dialog("open");
	}, 	
	
	
	validateEmail : function(email) {

        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);

    },
    
	/**
	 * This method works across all browsers. For webkit browsers (Chrome, Safari, and FF) there is
	 * preventDefault call on Event. For IE, there is not and therefore using another 
	 * recommended approach
	 * 
	 * TODO: Need to replace event.preventDefaul to this call for all apps
	 */
	preventDefault : function ( event )
	{
		if ( event ) {
		    if ( event.preventDefault)
	    		event.preventDefault();
	    	else 
	    		event.returnValue = false;
		}
    }
};

