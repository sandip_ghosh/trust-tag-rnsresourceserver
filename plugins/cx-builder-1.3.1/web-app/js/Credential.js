var Credential = {

    selectedRow : null,
    
    selectedRowColor : null,

    selectedParameter : null,
    
    basePath : null,
    
	clearCredentialProps : function(prefix) {
	    $(prefix + " #credentialName").val("");
	    $(prefix + " #ta").val("");
	    $(prefix + " #ta").removeAttr("title");
	    $(prefix + " #desc").text("");
	    $(prefix + " #grantingAuthorities").empty();
	    $(prefix + " #parameters").empty();
	    $(prefix + " #paramName").val("");
	    $(prefix + " #paramType").val("");
	    $(prefix + " #paramValue").val("");
//		    $(prefix + " #viewCredExpr").prop("disabled", true);
//		    $(prefix + " #viewCredExpr").disabled = true;
	},
	
	clearCredentials : function(prefix) {
		var selCredentialsPrefix = prefix;
		if (prefix == "#editCredentialDlg") {
			selCredentialsPrefix = "#editResource";
		}
		$(selCredentialsPrefix + " #selectedCredentials").val("");
		$(prefix + " #credentials").empty();
		var tableHeaderHTML = "<tr><th>Name</th><th>Trust Authority</th><th>Granting Authority</th><th></th></tr>";
		$(prefix + " #logicalCX").val("");
		$(prefix + " #logicalCX").prop("disabled", true);
		$(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
		$(prefix + " #editLogicalCX").button("option", "disabled", true);
		$(prefix + " #credentials").append(tableHeaderHTML);
		$(prefix + " #credElements").empty();
		$(prefix + " #paramsTable").empty();
		tableHeaderHTML = "<tr><th>Name</th><th>Display Name</th><th>Type</th><th>In Credential Expression</th><th>Value</th></tr>";
		$(prefix + " #paramsTable").append(tableHeaderHTML);
		$(prefix + " #saveCred").prop("disabled", true);
	},
	
	initEditLogicalCXButton: function(prefix) {
        $(prefix + " #editLogicalCX").button("option", "text", false);
        $(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
        $(prefix + " #editLogicalCX").removeClass("ui-corner-all");
        $(prefix + " #editLogicalCX").addClass("ui-corner-right");
	},
	
	setCredentialProps : function(prefix, mdJSONStr) {
	    this.clearCredentialProps(prefix);
		var mdJSON = eval('(' + mdJSONStr + ')');
		$(prefix + " #credentialName").val(mdJSON.name);
		$(prefix + " #credentialName").attr("title", mdJSON.desc);
		$(prefix + " #ta").val(mdJSON.ta);
//		$(prefix + " #desc").text(mdJSON.desc);
		var credName = Utils.getSafeIdFromName(mdJSON.name);
		var credId = $("#" + credName + "_id").val();
		$(prefix + " input[name='id']").attr("value", credId);
		var selAuths = $("#" + credName + "_auth").val() ? $("#" + credName + "_auth").val().split(",") : null;
		var selectedGAs = "";
		var gaHTML = "<li><label class='ui-corner-all' for='_dynamic_value'><input type='checkbox' id='_dynamic_value' value='$dynamic_value'/><span>&lt;Dynamic Value&gt;</span></label></li>"
		$(prefix + " #grantingAuthorities").append(gaHTML);
		$.each(mdJSON.auths, function(i) {
			var authURI = mdJSON.auths[i];
			var selected = "";
			if (selAuths) {
				$.each(selAuths, function(j) {
					if (selAuths[j] === authURI) {
						selected = "checked='checked'";
						if (selectedGAs) {
							selectedGAs += ",";
						}
						selectedGAs += authURI;
					}
				});
			}
//			if (mdJSON.auths.length == 1) {
//				selected = "checked='checked'";
//				selectedGAs += authURI;
//			}
//			var gaHTML = "<li><a href='#'><label class='ui-corner-all' for='" + authURI + "'><input type='checkbox' id='" + authURI + "' value='" + authURI + "' " + selected + " /><span>" + authURI + "</span></label></a></li>"
			gaHTML = "<li><label class='ui-corner-all' for='" + authURI + "'><input type='checkbox' id='" + authURI + "' value='" + authURI + "' " + selected + " /><span>" + authURI + "</span></label></li>"
			$(prefix + " #grantingAuthorities").append(gaHTML);
		});
		$(prefix + " #grantingAuthorities").menu("refresh");
		$(prefix + " #grantingAuthorities").val(selectedGAs);
		if (!selectedGAs) {
			$(prefix + " #saveCred").prop("disabled", true);
		}
		var paramValuesJSON = $("#" + credName + "_paramValues").val();
		var paramValues = null;
		if (paramValuesJSON) {
			paramValues = eval('(' + paramValuesJSON + ')');
		}
		$.each(mdJSON.parameters, function(i) {
			var paramName = mdJSON.parameters[i].name;
			if (paramValues) {
				var paramValue = paramValues[paramName];
				if (paramValue) {
					mdJSON.parameters[i].value = paramValue;
				}
			}
			var encodedParamJSON = escape(JSON.stringify(mdJSON.parameters[i], null));
			var optionHTML = "<option name='" + paramName + "' value='" + encodedParamJSON + "'>" + paramName + "</option>";
			$(prefix + " #parameters").append(optionHTML);
		});
		this.selectedParameter = null;
	//	$("#newCredential #viewCredExpr").prop("disabled", false);
	},

	getParamValues : function(prefix) {
		var paramValues = "{";
		if (this.selectedParameter) {
			paramValues += $(prefix + " #paramName").val() + ": ";
			paramValues += "'" + $(prefix + " #paramValue").val() + "'";
		}
		var paramOptions = $(prefix + " #parameters").children();
		$.each(paramOptions, function(i, paramOption) {
			var paramJSONStr = unescape($(paramOption).attr("value"));
			var paramJSON = eval('(' + paramJSONStr + ')');
			if (!this.selectedParameter || this.selectedParameter != paramJSON.name) {
				if (paramValues != "{") {
					paramValues += ", ";
				}
				paramValues += paramJSON.name + ": ";
				paramValues += "'" + paramJSON.value + "'";
			}
		});
		paramValues += "}";
		return paramValues;
	},

	selectCredential : function(newSelRow) {
		if (newSelRow != this.selectedRow) {
			if (this.selectedRow) {
				$(this.selectedRow).css("background-color", this.selectedRowColor);
			}
			this.selectedRowColor = $(newSelRow).css("background-color");
			$(newSelRow).css("background-color", "#b8d4ff");
			var credName = $("td:first", newSelRow).text();
			credName = Utils.getSafeIdFromName(credName);
			var credMD = $("#" + credName + "_md").val();
			this.setCredentialProps("#editCredential", credMD);
			this.selectedRow = $(newSelRow);
		}
	},
	
	updateGrantingAuthorities : function(prefix, ga, checked) {
		var currGAList = $(prefix + " #grantingAuthorities").val();
		if (currGAList) {
			if (checked) {
				// we are adding, so just add to end of the list
				$(prefix + " #grantingAuthorities").val(currGAList + "," + ga);
			} else {
				// we are removing, so find the GA from the list and remove it
				var currGAs = currGAList.split(",");
				for (var i = 0; i < currGAs.length; i++) {
					if (currGAs[i] == ga) {
						currGAs.splice(i, 1);
						break;
					}
				}
				$(prefix + " #grantingAuthorities").val(currGAs.join());
			}
		} else if (checked) {
			// if the GA list is empty then we can only add to it
			$(prefix + " #grantingAuthorities").val(ga);	
		}
		var hasGAs = $(prefix + " #grantingAuthorities").val().length > 0;
		$(prefix + " #saveCred").prop("disabled", !hasGAs);
	},
	
	addCredential : function(prefix) {
		var selCredentialsPrefix = prefix;
		if (prefix == "#editCredentialDlg") {
			selCredentialsPrefix = "#editResource";
		}
		var credentialsJSON = unescape($(selCredentialsPrefix + " #selectedCredentials").val());
		var selCredentials = new Array();
		if (credentialsJSON) {
			selCredentials = eval('(' + credentialsJSON + ')');
		}
		var newCredential = { name: $(prefix + " #credentialName").val(), ta: $(prefix + " #ta").val(), grantingAuthority: $(prefix + " #grantingAuthorities").val()};
		var newCELogicalName = this.getLogicalCredElement(newCredential, true);
		for (i = 0; i < selCredentials.length; i++) {
			var currCELogicalName = this.getLogicalCredElement(selCredentials[i], true);
			if (currCELogicalName === newCELogicalName) {
				Utils.showMessageBox("Add Credential Element", "Can only add one Credential Element with the same meta-data and granting authorities", "warning");
				return;
			}
		}
		selCredentials.push(newCredential);
		var tableRowHTML = selCredentials.length % 2 ? "<tr style='background-color: #e9f2fe;'>" : "<tr style='background-color: #ffffff;'>";
		tableRowHTML += "<td>" + newCredential.name + "</td>";
//		tableRowHTML += "<td>" + newCredential.ta + "</td>";
		tableRowHTML += "<td>" + newCredential.grantingAuthority + "</td>";
		tableRowHTML += "<td class='delete'></td></tr>";
		$(prefix + " #credentials").append(tableRowHTML);
		$(selCredentialsPrefix + " #selectedCredentials").val(escape(JSON.stringify(selCredentials, null)));
		$(prefix + " #newCredential").hide();
		$(prefix + " #newBtnBar").show();
		var currCX = $(prefix + " #logicalCX").val();
		if (currCX) {
			var joinTypeSelector = prefix == "#editCredentialDlg" ? prefix + " #editJoinType" : prefix + " #joinType";
			var newCX = currCX + " " + $(joinTypeSelector + " option:selected").attr("value") + " " + this.getLogicalCredElement(newCredential);
			$(prefix + " #logicalCX").val(newCX);
		} else {
			$(prefix + " #logicalCX").val(this.getLogicalCredElement(newCredential));
		}
		$(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
		$(prefix + " #editLogicalCX").button("option", "disabled", selCredentials.length <= 1 ? true : false);
		$(prefix + " #logicalCX").prop("disabled", true);
	},
	
	removeCredential : function(deleteNode, prefix) {
		var trNode = deleteNode.parent().get(0);
		var tdNodes = $(trNode).children();
		var credName = $(tdNodes[0]).text();
		var gaList = $(tdNodes[1]).text();
		var selCredentialsPrefix = prefix;
		if (prefix == "#editCredentialDlg") {
			selCredentialsPrefix = "#editResource";
		}
		var credentialsJSON = unescape($(selCredentialsPrefix + " #selectedCredentials").val());
		var selCredentials = new Array();
		if (credentialsJSON) {
			selCredentials = eval('(' + credentialsJSON + ')');
		}
		for (var i = 0; i < selCredentials.length; i++) {
			if (selCredentials[i].name == credName && selCredentials[i].grantingAuthority == gaList) {
				selCredentials.splice(i, 1);
				break;
			}
		}
		$(selCredentialsPrefix + " #selectedCredentials").val(escape(JSON.stringify(selCredentials, null)));
		deleteNode.parent().remove();
		// if a CE is deleted the credential expression will revert to the default format
		$(prefix + " #logicalCX").val(this.getDefaultLogicalCredExpr(prefix));
		$(prefix + " #editLogicalCX").button("option", "disabled", selCredentials.length <= 1 ? true : false);
		$(prefix + " #logicalCX").prop("disabled", true);
		$(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
	},
	
	searchMetadata : function(ctrl, prefix) {
		var theThis = this;
		ctrl.doTimeout('findMD', 1000, function() {
			theThis.clearCredentialProps(prefix);
			var searchName = this.val();
			searchName = $.trim(searchName);
			if (!searchName) {
				$(prefix + " #foundMDs").empty();
				$(prefix + " #foundMDs").hide();
			} else {
				$.getJSON("/" + Credential.basePath + "/cxBuilder/findCredentialMetaData?searchName=" + encodeURI(searchName),
	    			function(data) {
    					if (data) {
	    					if (data.count == 0) {
		    					$(prefix + " #foundMDs").append("<option>" + g_notFoundOption + "</option");
		    					
		    				} else {
		    					$.each(data.mds, function(i) {
			    					var mdName = data.mds[i].name + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + data.mds[i].ta;
			    					var encodedMD = escape(JSON.stringify(data.mds[i], null));
			    					var optionHTML = "<option name=\"" + mdName + "\" value=\"" + encodedMD + "\">" + mdName + "</option>";
			    					$(prefix + " #foundMDs").append(optionHTML);
		    					});
		    					// $("#mdProps").val(JSON.stringify(data, null));	
			    			}
			    			$(prefix + " #foundMDs").show();
			    			$(prefix + " #foundMDs").focus();
	    				} else {
		    				$(prefix + " #foundMDs").empty();
		    				$(prefix + " #foundMDs").hide();
		    			}
					});			
			}						
		});
	},
	
	hideMetadataSearchResults : function(prefix) {
		$(prefix + " #foundMDs").empty();
		$(prefix + " #foundMDs").hide();
		$(prefix + " #findMD").val("");				
	},
	
	showAddCredentialElement : function(prefix, show) {
		this.hideMetadataSearchResults(prefix);
		this.clearCredentialProps(prefix);
		if (show) {
			$(prefix + " #newCredential").show();
			$(prefix + " #newBtnBar").hide();
		} else {
			$(prefix + " #newCredential").hide();
			$(prefix + " #newBtnBar").show();
		}
	}, 

	populateCredentialElements : function(selCredentials) {
		if (selCredentials) {
			$("#editCredentialDlg #credentials").empty();
			var tableHeaderHTML = "<tr><th>Name</th><th>Granting Authorities</th><th></th></tr>";
			$("#editCredentialDlg #credentials").append(tableHeaderHTML);
			for (i = 0; i < selCredentials.length; i++) {
				var tableRowHTML = i % 2 ? "<tr style='background-color: #ffffff;'>" : "<tr style='background-color: #e9f2fe;'>";
				tableRowHTML += "<td>" + selCredentials[i].name + "</td>";
//				tableRowHTML += "<td>" + selCredentials[i].ta + "</td>";
				tableRowHTML += "<td>" + selCredentials[i].grantingAuthority + "</td>";
				tableRowHTML += "<td class='delete'></td></tr>";
				$("#editCredentialDlg #credentials").append(tableRowHTML);
			}
//			$("#editCredentialDlg #selectedCredentials").val(escape(JSON.stringify(selCredentials, null)));
			$("#editResource #selectedCredentials").val(escape(JSON.stringify(selCredentials, null)));
		}
	},
	
	getDefaultLogicalCredExpr : function(prefix) {
		var selCredentialsPrefix = prefix;
		if (prefix == "#editCredentialDlg") {
			selCredentialsPrefix = "#editResource";
		}
		var selCredentials = new Array();
		var credentialsJSON = unescape($(selCredentialsPrefix + " #selectedCredentials").val());
		if (credentialsJSON) {
			selCredentials = eval('(' + credentialsJSON + ')');
		}
		var joinTypeSelector = prefix == "#editCredentialDlg" ? prefix + " #editJoinType" : prefix + " #joinType";
		var logicalCX = "";
		for (i = 0; i < selCredentials.length; i++) {
			if (logicalCX) {
				logicalCX += " " + $(joinTypeSelector + " option:selected").attr("value");
			}
			logicalCX += " " + this.getLogicalCredElement(selCredentials[i]);
		}
		return logicalCX;
	},
	
	getLogicalCredElement : function(credElement, fullyQualifed) {
		fullyQualified = typeof fullyQualified !== 'undefined' ? fullyQualified : false;
		if (!credElement) {
			return "";
		}
		var GAs = credElement.grantingAuthority.split(",");
		var gaNames = "";
		$.each(GAs, function(i) {
			var gaName = null;
			if (fullyQualified) {
				gaName = GAs[i];
			} else {	
				var gaParts = GAs[i].split("/");
				if (gaParts && gaParts.length) {
					gaName = gaParts[gaParts.length - 1];
				}
				if (gaNames) {
					gaNames += ",";
				}
			}
			gaNames += gaName;
		});
		return gaNames ? credElement.name + "[" + gaNames + "]" : credElement.name;
	},
	
	populateParameters : function(credentialParams, prefix) {
		if (credentialParams) {
			$(prefix + " #paramsTable").empty();
			var tableHeaderHTML = "<tr><th>Name</th><th>Display Name</th><th>Type</th><th>In Credential Expression</th><th>Value</th></tr>";
			$(prefix + " #paramsTable").append(tableHeaderHTML);
			$.each(credentialParams, function(i) {
				var bgStyle = i % 2 ? " style='background-color: #ffffff;'" : " style='background-color: #e9f2fe;'";
				var paramRowHTML = "<tr" + bgStyle + ">";
				paramRowHTML += "<td>" + credentialParams[i].name + "</td>";
				paramRowHTML += "<td>" + credentialParams[i].displayName + "</td>";
				paramRowHTML += "<td>" + credentialParams[i].type + "</td>";
				var paramName = Utils.getSafeIdFromName(credentialParams[i].name);
				var checked = "";
				if (credentialParams[i].mapping.isRequired) {
					checked = "checked='checked'";
				}
				paramRowHTML += "<td><input type='checkbox' class='paramMapRequiredInput' name='paramRequired' id='paramRequired' " + checked + "/></td>";
				var showValue = "";
				if (!credentialParams[i].mapping.isRequired || credentialParams[i].type === "rns:data-token") {
					showValue = "style='display:none'";
				}
				// render the static value cell
				paramRowHTML += "<td><div id='showValue' " + showValue +"><table cellspacing='0' cellpadding='5' width='100%'" + bgStyle + "><tr" + bgStyle + "><td width='50%'>";
				var staticChecked = "";
				var dynamicChecked = "";
				var staticValue = "";
				var dynamicValue = "";
				var staticDisabled = "";
				var dynamicDisabled = "";
				if (credentialParams[i].mapping.isStatic) {
					staticChecked = "checked='checked'";
					dynamicDisabled = "disabled='disabled'";
					staticValue = credentialParams[i].mapping.value;
				} else {
					dynamicChecked = "checked='checked'";
					staticDisabled = "disabled='disabled'";
					dynamicValue = credentialParams[i].mapping.value;
				}
				paramRowHTML += "<input type='radio' class='paramMapValueType' id='staticParamValue' name='" + paramName + "_type' value='static' " + staticChecked + " style='margin-bottom:5px;'/><label for='staticParamValue'>Static Value:</label>";
				paramRowHTML += "<input type='text' name='paramValue' id='paramValue' value='" + staticValue + "'" + staticDisabled + "  size='20' /></td>";
				paramRowHTML += "<td width='50%'><input type='radio' class='paramMapValueType' id='dynamicParamValue' name='" + paramName + "_type' value='dynamic' " + dynamicChecked + " style='margin-bottom:5px;'/><label for='dynamicParamValue'>From Request Parameter:</label>";
				paramRowHTML += "<input type='text' name='requestParam' id='requestParam' value='" + dynamicValue + "'" + dynamicDisabled + " size='20' /></td>";
				paramRowHTML += "</tr></table></div></td></tr>";
				$(prefix + " #paramsTable").append(paramRowHTML);
			});
		}
	},

	
	getParameterMapping : function(prefix) {
		var paramMapping = new Array();
		var tbody = $(prefix + " #paramsTable").children()[0];
		var paramRows = $(tbody).children();
		for (var i = 1; i < paramRows.length; i++) {
			var paramRow = paramRows[i]; // $("#paramsTable tr:nth-child(" + i + 1 + ")")[0]
			var paramName = $("td:first", paramRow).text();
			var paramDisplayName = $($("td:nth-child(2)", paramRow)[0]).text();
			var paramType = $($("td:nth-child(3)", paramRow)[0]).text();
			var isRequired = $("#paramRequired", paramRow).prop("checked");
			var isStatic = $("#staticParamValue", paramRow).prop("checked");
			var value = isRequired ? isStatic ?  $("#paramValue", paramRow).val() : $("#requestParam", paramRow).val() : "";
			paramMapping.push({name: paramName, displayName: paramDisplayName, type: paramType, mapping: {isRequired: isRequired, isStatic: isStatic, historyKey: true, value: value}});
		}
		var paramMappingJSON = JSON.stringify(paramMapping, null);
		return paramMappingJSON;
	},
	
	onClickEditLogicalCX : function(prefix) {
		var logicalCXDisabled = $(prefix + " #logicalCX").prop("disabled");
		if (logicalCXDisabled) {
			$(prefix + " #logicalCX").prop("disabled", false);
			$(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-disk'});
			$(prefix + " #editLogicalCX").focus();
		} else {
			// we need to parse the change in logical expression, if the parse succeeds then
			// update logical CX else throw an error
			var selCredentialsPrefix = prefix;
			if (prefix == "#editCredentialDlg") {
				selCredentialsPrefix = "#editResource";
			}
			var selectedCredentials = unescape($(selCredentialsPrefix + " #selectedCredentials").val());
			var logicalCredExpr = $(prefix + " #logicalCX").val();
			$.post("/" + this.basePath + "/cxBuilder/validateLogicalCredExpr", {selectedCredentials: selectedCredentials, logicalCredExpr: logicalCredExpr},
				function(data) {
					if (data) {
						if (!data.parseResult) {
							Utils.showMessageBox("Edit Credential Expression", "Parse Error: " + data.errorMsg + " at Position: " + data.errorOffset, "error");
							$(prefix + " #logicalCX").val(Credential.getDefaultLogicalCredExpr(prefix));
						}	
						$(prefix + " #logicalCX").prop("disabled", true);
						$(prefix + " #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
					}
				}, "json");
		}
	}
	
};

