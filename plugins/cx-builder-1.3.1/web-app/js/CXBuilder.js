var CXBuilder = {
	selectedResource : null,
	
	selectedResourceId : null,
	
	selectedMappingCredential : null,
	
	basePath : null,
	
	clearResourceConfigProps : function(prefix) {
		$(prefix + " #resourceConfigTable #name").val("");
		$(prefix + " #hostname").val("");
		$(prefix + " #portStr").val("");
		$(prefix + " #path").val("");
		$(prefix + " #useWrapper").removeAttr("checked");
		$(prefix + " #displayTypeDownload").prop("selected", true);
		$(prefix + " #stylesheet").val("");
		$(prefix + " #resourceType option[value='app']").prop("selected", true);
		this.onChangeResourceType(prefix);
	},
	
	createOrUpdateResource : function(isNew) {
		var selectorPrefix = isNew ? "#newResource" : "#editResource";
		var credentialSelectorPrefix = isNew ? "#newResource" : "#editCredentialDlg";
	    if ($(selectorPrefix + " #useWrapper").is(":checked")) {
	    	$(selectorPrefix + " #useWrapper").val("true");
		} else {
			$(selectorPrefix + " #useWrapper").val("false");
		}
	    var selCredentials = unescape($(selectorPrefix + " #selectedCredentials").val());
	    $(selectorPrefix + " #selectedCredentials").val(selCredentials);
	    var paramMappingJSON = this.getParameterMapping(credentialSelectorPrefix);
	    $(selectorPrefix + " #paramMapping").val(paramMappingJSON);
	    var displayType = $(selectorPrefix + " #displayType").val();
	    if (!displayType) {
	    	$(selectorPrefix + " #displayType").val("download");
	    }
	    if (selectorPrefix == "#editResource") {
	    	$(selectorPrefix + " #joinType").val($(credentialSelectorPrefix + " #editJoinType option:selected").attr("value"));
	    	$(selectorPrefix + " #frequencyType").val($(credentialSelectorPrefix + " #editFrequencyType option:selected").attr("value"));
	    }
		var logicalCX = $(credentialSelectorPrefix + " #logicalCX").val();
		$(selectorPrefix + " #logicalCredExpr").val(logicalCX);
		$(selectorPrefix + " form").submit();
	},

	populateParameterMapping : function(paramMappingJSONStr, prefix) {
		this.selectedMappingCredential = null;
		$(prefix + " #credElements").empty();
		var paramMappingJSON = eval('(' + paramMappingJSONStr + ')');
		var theResource = this;
		if (paramMappingJSON) {
			$.each(paramMappingJSON, function(i) {
				var optionName = Credential.getLogicalCredElement(paramMappingJSON[i]);
				var paramsJSONStr = escape(JSON.stringify(paramMappingJSON[i], null));
				var selected = "";
				if (i == 0) {
					selected = "selected='selected'";
					Credential.populateParameters(paramMappingJSON[i].parameters, prefix);
					theResource.selectedMappingCredential = optionName;
					$(prefix + " #frequencyType option[value='" + paramMappingJSON[i].frequencyType + "']").attr("selected", "selected");
					$(prefix + " #maxAge").val(paramMappingJSON[i].maxAge);
				}
				
				var optionHTML = "<option name='" + Utils.getSafeIdFromName(optionName) + "' value='" + paramsJSONStr + "' " + selected + ">" + optionName + "</option>";
				$(prefix + " #credElements").append(optionHTML);
			});
		}
	},
	
	updateSelCredentialParamMapping : function(prefix) {
		if (this.selectedMappingCredential) {
			var paramsJSONStr = Credential.getParameterMapping(prefix);
			var paramsJSON = eval('(' + paramsJSONStr + ')');
			var freqType = $(prefix + " #frequencyType option:selected").val();
			var maxAge = $(prefix + " #maxAge").val();
			// get the previous option value and update the parameters, frequencyType and maxAge values only
			var prevCredParamMappingStr = unescape($(prefix + " #credElements option[name=" + Utils.getSafeIdFromName(this.selectedMappingCredential) + "]").val()); 
			var prevCredParamMapping = eval('(' + prevCredParamMappingStr + ')');
			prevCredParamMapping.parameters = paramsJSON;
			prevCredParamMapping.frequencyType = freqType;
			prevCredParamMapping.maxAge = maxAge;
//			var prevCredParamMapping = { name: prevCredName, tsNames: prevTSNames, parameters: paramsJSON, frequencyType: freqType, maxAge: maxAge};
			prevCredParamMappingStr = escape(JSON.stringify(prevCredParamMapping, null));
			$(prefix + " #credElements option[name=" + Utils.getSafeIdFromName(this.selectedMappingCredential) + "]").attr("value", prevCredParamMappingStr);
		}
	},
	
	onChangeParameterMapping : function(prefix) {
		this.updateSelCredentialParamMapping(prefix);
		var selCredMappingJSONStr = unescape($(prefix + " #credElements option:selected").attr("value"));
		var selCredMappingJSON = eval('(' + selCredMappingJSONStr + ')');
		Credential.populateParameters(selCredMappingJSON.parameters, prefix);
		var freqType = selCredMappingJSON.frequencyType ? selCredMappingJSON.frequencyType : "SameSession";
		$(prefix + " #frequencyType option[value='" + freqType + "']").attr("selected", "selected");
		$(prefix + " #maxAge").val(selCredMappingJSON.maxAge ? selCredMappingJSON.maxAge : "20");
		this.selectedMappingCredential = $(prefix + " #credElements option:selected").text();
	},
	
	getParameterMapping : function(prefix) {
		this.updateSelCredentialParamMapping(prefix);
		var paramMapping = "[";
		var paramOptions = $(prefix + " #credElements").children();
		$.each(paramOptions, function(i, paramOption) {
			var credParamJSONStr = unescape($(paramOption).attr("value"));
			if (i != 0) {
				paramMapping += ", ";
			}
			paramMapping += credParamJSONStr;
		});
		paramMapping += "]";
		return paramMapping;
	},
	
	viewCredentialExpr : function() {
		var selectedCredentials = unescape($("#newResource #selectedCredentials").val());
		var paramMapping = this.getParameterMapping("#newResource");
		var joinType = $("#newResource #joinType option:selected").attr("value");
		var logicalCredExpr = $("#newResource #logicalCX").val();
		$.post("/" + this.basePath + "/cxBuilder/viewCredentialExpression", {selectedCredentials: selectedCredentials, paramMapping: paramMapping, joinType: joinType, logicalCredExpr: logicalCredExpr},
			function(data) {
				if (data) {
			        var credElementStr = JSON.stringify(data, null, 4);
			        $("#credExprJSON").text(credElementStr);
			        var credElementDlg = $('#credExpr').dialog({autoOpen: false, title:'Credential Expression', modal:true,
						width:640, minWidth:400, maxWidth:1024, height:480, minHeight:300, maxHeight:768,
						buttons:{'OK': function() {$(this).dialog('close'); }}});
			        credElementDlg.dialog("open");
				}
			}, "json");			
	},
	
	updateCredentialExpr : function() {
		var selectedCredentials = unescape($("#editResource #selectedCredentials").val());
		var paramMapping = this.getParameterMapping("#editCredentialDlg");
		var joinType = $("#editCredentialDlg #editJoinType option:selected").attr("value");
		var logicalCredExpr = $("#editCredentialDlg #logicalCX").val();
		$.post("/" + this.basePath + "/cxBuilder/updateCredentialExpression/" + this.selectedResource.name, {selectedCredentials: selectedCredentials, paramMapping: paramMapping, joinType: joinType, logicalCredExpr: logicalCredExpr},
			function(data) {
				if (data) {
					if (!data.valid) {
						Utils.showMessageBox("Edit Credentials", data.message, "error");
					}
					var credExpr = data.credentialExpr;
					var resJSON = data.resourceJSON;
			        var credElementStr = JSON.stringify(credExpr, null, 4);
			        $("#savedCredExpr").text(credElementStr);
			        CXBuilder.selectedResource.logicalCredExpr = resJSON.logicalCredExpr;
			        CXBuilder.selectedResource.joinType = resJSON.joinType;
			        CXBuilder.setCredentialExpr(resJSON.credentials);
				}
			}, "json");
	},
	
	updateParameterMapping : function() {
		var currSelCredentialsJSON =  unescape($("#editResource #selectedCredentials").val());
		$.post("/" + this.basePath + "/cxBuilder/parameterMapping", {selectedCredentials: currSelCredentialsJSON},
			function(data) {
				if (data) {
	        		var paramMappingJSONStr = JSON.stringify(data, null);
	        		CXBuilder.populateParameterMapping(paramMappingJSONStr, "#editCredentialDlg");
				}
		}, "json");
	},
	
	setCredentialExpr : function(selCredentials) {
		Credential.populateCredentialElements(selCredentials);
		this.updateParameterMapping();
		$("#editCredentialDlg #editJoinType option[value='" + this.selectedResource.joinType + "']").attr("selected", "selected");
		if (this.selectedResource.logicalCredExpr) {
			$("#editCredentialDlg #logicalCX").val(this.selectedResource.logicalCredExpr);
		} else {
			$("#editCredentialDlg #logicalCX").val(Credential.getDefaultLogicalCredExpr("#editCredentialDlg"));
		}
		$("#editCredentialDlg #editLogicalCX").button("option", "icons", {primary:'ui-icon-pencil'});
		$("#editCredentialDlg #editLogicalCX").button("option", "disabled", selCredentials.length <= 1 ? true : false);
		$("#editCredentialDlg #logicalCX").prop("disabled", true);
	},

	showSelectCredentialsStep : function(prefix) {
		$(prefix + " #resourceConfig").hide();
		$(prefix + " #parameterMapping").hide();
		$(prefix + " #paramConfig").hide();
		$(prefix + " #selectCredentials").show();
	},
	
	showParameterMappingStep : function(prefix) {
		$(prefix + " #resourceConfig").hide();
		$(prefix + " #selectCredentials").hide();
		$(prefix + " #paramConfig").hide();
		$(prefix + " #parameterMapping").show();
	},
	
	showPrev : function(prefix, currPage) {
		if (currPage == "ParameterMapping") {
			this.showSelectCredentialsStep(prefix);
			currPage = "SelectCredentials";
		}
		return currPage;
	},
	
	showNext : function(prefix, currPage) {
		if (currPage == "SelectCredentials") {
			this.showParameterMappingStep(prefix);
			currPage = "ParameterMapping";
			// see if we should populate the Parameter Mapping
			var paramMapping = $(prefix + " #paramMapping").val();
			if (!paramMapping) {
				var selCredentialsJSON = unescape($(prefix + " #selectedCredentials").val());
				$.getJSON("/" + this.basePath + "/cxBuilder/parameterMapping", {selectedCredentials: selCredentialsJSON},
						function(data) {
							if (data) {
				        		var paramMappingJSONStr = JSON.stringify(data, null);
				        		CXBuilder.populateParameterMapping(paramMappingJSONStr, prefix);
							}
						});
						
			}
		} else if (currPage == "ParameterMapping") {					
			this.createOrUpdateResource(true);
			$(prefix).dialog('close');
		}
		return currPage;
	}
		
};


/// Event Handler. Moved from GSP page and replaced with 'live' versions so can work strictly through JS include
var g_notFoundOption = "No Credential Metadata found";

$("#newResource #credentialTable .delete").live("click", function() {
	var theThis = $(this);
	Utils.setMessageBox("The Credential will be deleted. Are you sure?", "warning");
	var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:'Confirm Delete Credential', model:true,
		width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
		buttons:{'Cancel': function() {$(this).dialog('close'); },
			'Delete': function() {
				Credential.removeCredential(theThis, "#newResource");
				$(this).dialog('close');
			}}});
	confirmDlg.dialog("open");
	return false;
});

$("#editCredentialDlg #credentialTable .delete").live("click", function() {
	var theThis = $(this);
	Utils.setMessageBox("The Credential will be deleted. Are you sure?", "warning");
	var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:'Confirm Delete Credential', model:true,
		width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
		buttons:{'Cancel': function() {$(this).dialog('close'); },
			'Delete': function() {
				Credential.removeCredential(theThis, "#editCredentialDlg");
				$(this).dialog('close');
			}}});
	confirmDlg.dialog("open");
	return false;
});

$("#newResource #newCredential #saveCred").live("click", function(event) {
	Credential.addCredential("#newResource");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #newCredential #saveCred").live("click", function(event) {
	Credential.addCredential("#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#newResource #credentialTable #newCred").live("click", function(event) {
	Credential.showAddCredentialElement("#newResource", true);
	Utils.preventDefault (event);
});

$("#editCredentialDlg #credentialTable #newCred").live("click", function(event) {
	Credential.showAddCredentialElement("#editCredentialDlg", true);
	Utils.preventDefault (event);
});

$("#newResource #newCredential #cancelAddCred").live("click", function(event) {
	Credential.showAddCredentialElement("#newResource", false);
	Utils.preventDefault (event);
});

$("#editCredentialDlg #newCredential #cancelAddCred").live("click", function(event) {
	Credential.showAddCredentialElement("#editCredentialDlg", false);
	Utils.preventDefault (event);
});

$("#newResource #findMD").live("keyup", function(event) {
	Credential.searchMetadata($(this), "#newResource");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #findMD").live("keyup", function(event) {
	Credential.searchMetadata($(this), "#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#newResource #foundMDs").live("blur", function(event) {
	Credential.hideMetadataSearchResults("#newResource");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #foundMDs").live("blur", function(event) {
	Credential.hideMetadataSearchResults("#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#newResource #foundMDs").live("click", function(event) {
	// populate the Credential Element Configuration area	
	var mdJSONStr = unescape($("#newResource #foundMDs option:selected").attr("value"));
	if (mdJSONStr != g_notFoundOption) {
		Credential.setCredentialProps("#newResource", mdJSONStr);
	}
	Credential.hideMetadataSearchResults("#newResource");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #foundMDs").live("click", function(event) {
	// populate the Credential Element Configuration area	
	var mdJSONStr = unescape($("#editCredentialDlg #foundMDs option:selected").attr("value"));
	if (mdJSONStr != g_notFoundOption) {
		Credential.setCredentialProps("#editCredentialDlg", mdJSONStr);
	}
	Credential.hideMetadataSearchResults("#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#newResource #credElements").live("change", function(event) {
	CXBuilder.onChangeParameterMapping("#newResource");
	Utils.preventDefault (event);
});	

$("#editCredentialDlg #credElements").live("change", function(event) {
	CXBuilder.onChangeParameterMapping("#editCredentialDlg");
	Utils.preventDefault (event);
});	

$("#newResource #credElementParams").live("change", function(event) {
	Credential.onChangeParameter("#newResource");
	Utils.preventDefault (event);
});	

$("#editCredentialDlg #credElementParams").live("change", function(event) {
	Credential.onChangeParameter("#editCredentialDlg");
	Utils.preventDefault (event);
});	

$("#editCredentialDlg #step3Link").live("click", function(event) {
	g_currPage = "SelectCredentials";
	CXBuilder.showSelectCredentialsStep("#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #step4Link").live("click", function(event) {
	g_currPage = "ParameterMapping";
	CXBuilder.updateParameterMapping();
	CXBuilder.showParameterMappingStep("#editCredentialDlg");
	Utils.preventDefault (event);
});

$("#newResource #grantingAuthorities input[type='checkbox']").live("click", function(event) {
	var ga = $(this).val();
	var checked = $(this).is(":checked");
	Credential.updateGrantingAuthorities("#newResource", ga, checked);
//	event.stopPropagation();
//	Utils.preventDefault (event);
});

$("#editCredentialDlg #grantingAuthorities input[type='checkbox']").live("click", function(event) {
	var ga = $(this).val();
	var checked = $(this).is(":checked");
	Credential.updateGrantingAuthorities("#editCredentialDlg", ga, checked);
//	event.stopPropagation();	
//	Utils.preventDefault (event);
});

$(".paramMapValueType").live("click", function(event) {
	var trNode = $(this).parent().parent();
	var currNodeId = $(this).attr("id");
	var isStatic = currNodeId == "staticParamValue";
	if (isStatic) {
		$("#paramValue", trNode).prop("disabled", false);
		$("#requestParam", trNode).prop("disabled", true);
		$("#requestParam", trNode).val("");
	} else {
		$("#paramValue", trNode).prop("disabled", true);
		$("#paramValue", trNode).val("");
		$("#requestParam", trNode).prop("disabled", false);
	}
});

$(".paramMapRequiredInput").live("click", function(event) {
	var trNode = $(this).parent().parent();
	var isRequired = $("#paramRequired", trNode).prop("checked");
	var type = $($("td:nth-child(3)", trNode)[0]).text();
	if (isRequired && type != "rns:data-token") {
		$("#showValue", trNode).show();
	} else {
		$("#showValue", trNode).hide();
	}
});

$("#newResource #editLogicalCX").live("click", function(event) {
	Credential.onClickEditLogicalCX("#newResource");
	Utils.preventDefault (event);
});

$("#editCredentialDlg #editLogicalCX").live("click", function(event) {
	Credential.onClickEditLogicalCX("#editCredentialDlg");
	Utils.preventDefault (event);
});


$("#newResource #viewCredExpr").live("click", function(event) {
	CXBuilder.viewCredentialExpr();
	Utils.preventDefault (event);
});

