<% if (isNew) {
	out << "<input type='hidden' id='selectedCredentials' name='selectedCredentials' value=''/>\n"
	out << "<input type='hidden' id='logicalCredExpr' name='logicalCredExpr' value=''/>\n"
} %>
  <table id="navBarTable" cellspacing='0' cellpadding='0' width='100%' >
  	<tr>
 		<td class="navBar" valign="top">
 			<g:render template='navBar' model='${[selected:"selectCredentials", isNew:isNew]}' />
 		</td>
 		<td valign="top"> 
			<table id="credentialTable" cellspacing="0" cellpadding="5" width="100%">
			<!-- 
				<tr>
					<td><p>A resource is protected through the evaluation of a credential expression. A credential expression is made up of one or more credential elements.</p></td>
				</tr>
			 -->
				<tr>
					<td style="padding-left:20px">Default Join Operator:&nbsp;&nbsp;
					<%  
						def joinTypeId = isNew ? 'joinType' : 'editJoinType';
					%>
						<select id="<%= joinTypeId %>" name="<%= joinTypeId %>" title="Select the default Join Operator that is used to join the credential elements. The structure of the credential expression can be modified below.">
							<option value="And">And</option>
							<option value="Or">Or</option>
							<option value="OrderedAnd">Ordered And</option>
							<option value="OrderedOr">Ordered Or</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px">Credential Expression:&nbsp;&nbsp;
					    <textarea name="logicalCX" id="logicalCX" rows="3" cols="100" style="height:40px;margin:0px;padding:0px;margin-right:-5px" disabled="disabled" title="The Credential Expression in a logical expression form. Click on the button on the right to change the logical expression"></textarea>
						<button	id="editLogicalCX" title="Edit Credential Expression" style="height:24px;width:24px;vertical-align:top;"></button>
					</td>	 
				</tr>
				<tr>
					<td valign="top">
				        <div class="sectionBody withShadow">
				        	<div class="sectionHeader sectionHeaderShadow">Credential Elements
				        		<div title="The list of Credential Elements in the Credential Expression" class="helpBackground ui-icon ui-icon-help" style="margin-top:2px"></div>
				        	</div> 
				        	<table id="credentials" cellspacing="0" cellpadding="5">
				        		<tr>
					        		<th>Name</th>
					        		<th>Granting Authorities</th>
					        		<th></th>
				        		</tr>
				        	</table>
				        </div>
					</td>
				</tr>
				<tr id="newBtnBar">
					<td align="right">
						<div class="buttons" align="right">
							<button id="newCred" title="Displays the UI Widget to find and add a Credential Element">Add Credential Element</button>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="newCredential" style="display:none">
							<table id="credentialConfigTable" cellspacing="0" cellpadding="5" width="100%" style="margin-top:20px">
								<tr>
									<td>Search for Credential Meta Data
										<input type='text' id='findMD' name='findMD' value='' size='100' style='margin-top:5px;width:100%' title="Type in characters in Credential Metadata names, minimum 4 characters, the list below will be populated with matching Credential Metadatas. The search is case-insensitive"/>
										<select id='foundMDs' size='5' style='width:100%; scroll:auto;display:none'>
										</select>
									</td>
								</tr>
						   		<tr>
						   			<td>
										<div class='sectionBody'>
							        		<div class='sectionHeader'>Credential Element Configuration</div>
								        	<table cellspacing="0" cellpadding="5" width="100%">
								        		<tr>
										   			<td>Credential Name:</td>
										   			<td><input type="text" name="credentialName" id="credentialName" value="" size="100" style="width:90%" readonly="readonly" title="" /></td>
							        			</tr>
								        		<tr>
										   			<td>Trust Authority:</td>
										   			<td><input type="text" name="ta" id="ta" value="" size="100" style="width:90%" readonly="readonly"/></td>
							        			</tr>
							        		<!-- 	
								        		<tr>
										   			<td>Description:</td>
										   			<td><textarea name="desc" id="desc" rows="3" cols="100" style="height:40px;margin:0px;padding:0px" readonly="readonly"></textarea></td>
							        			</tr>
							        		 -->	
								        		<tr>
										   			<td>Granting Authorities:</td>
										   			<td><ul name="grantingAuthorities" id="grantingAuthorities" style="width:90%;height:60px;overflow-y:scroll;"></ul></td>
							        			</tr>
						        			</table>
					        			</div>
				        			</td>
			        			</tr>
			       			</table>
							<div class="buttons" align="right">
								<button id="cancelAddCred" title="Close the Add Credential Element widget">Cancel</button>				
								<button id="saveCred" title="Add the Credential Element to the Credential Expression">Add Credential Element</button>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
