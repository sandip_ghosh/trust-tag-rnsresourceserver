<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Credential Expression</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'dotimer.js') }"></script>        
        <script type="text/javascript" src="${resource(dir: 'js', file: 'Utils.js') }"></script>
 		<script type="text/javascript" src="${resource(dir: 'js', file: 'Credential.js') }"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'CXBuilder.js') }"></script>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/start/jquery-ui.css" type="text/css" rel="stylesheet" media="all" />
        <link href="${resource(dir: 'css', file: 'cxbuilder.css') }" type="text/css" rel="stylesheet" />
    </head> 
	<script type="text/javascript">
	
		function updateFormAction(resourceName) {
			var formAction = "/" + $("#basePath").val() + "/cxBuilder/saveCredentialService/" + resourceName;
			$("form").attr("action", formAction);
		}
	
		$(document).ready(function(){
			var g_currPage = "SelectCredentials";
//			var g_notFoundOption = "No Credential Metadata found";

			$("button").button();
			$( document ).tooltip({
				 position: {
				 	my: "center bottom-20",
				 	at: "center top",
				 	using: function( position, feedback ) {
				 		$( this ).css( position );
				 		$( "<div>" )
				 		.addClass( "arrow" )
				 		.addClass( feedback.vertical )
				 		.addClass( feedback.horizontal )
				 		.appendTo( this );
				 	}
				 }
			});
			$("#editCredentialDlg #grantingAuthorities").menu();
			Credential.initEditLogicalCXButton("#editCredentialDlg");
			
			$("#save").click(function(event) {
				$("#testCX").val("false");
				CXBuilder.createOrUpdateResource(false);
				Utils.preventDefault (event);
			});

			$("#test").click(function(event) {
				$("#testCX").val("true");
				CXBuilder.createOrUpdateResource(false);
				Utils.preventDefault(event);
			});
			
			$("#editResource #editCredExpr").click(function(event) {
				g_currPage = "SelectCredentials";
//				Credential.populateCredentialElements(CXBuilder.selectedResource.credentials);
				$("#editCredentialDlg #editJoinType option[value = '" + CXBuilder.selectedResource.joinType + "']").prop("selected", true);
				$("#editCredentialDlg #parameterMapping").hide();
				$("#editCredentialDlg #selectCredentials").show();
				var editCredentialDlg = $('#editCredentialDlg').dialog({autoOpen: false, title:'Edit Credential Configuration', modal:true, 
					width:1024, minWidth:1024, height:600, minHeight:600,
					buttons:{'Cancel': function() {$(this).dialog('close'); },
						'Prev' : function() {
							if (g_currPage == "ParameterMapping") {
								$("#editCredentialDlg #parameterMapping").hide();
								$("#editCredentialDlg #selectCredentials").show();
								g_currPage = "SelectCredentials";
							}
						},						
						'Next': function() {
							if (g_currPage == "SelectCredentials") {
								g_currPage = "ParameterMapping";
								CXBuilder.updateParameterMapping();
								$("#editCredentialDlg #selectCredentials").hide();
								$("#editCredentialDlg #parameterMapping").show();
							} else if (g_currPage == "ParameterMapping") {					
								CXBuilder.updateCredentialExpr();
								$("#test").show();
								$(this).dialog('close');
							}				
						}
					}});
				editCredentialDlg.dialog("open");
				Utils.preventDefault ( event );
			});
			
			// initializes the selected Resource object
			CXBuilder.selectedResourceId = $("#selResourceId").val();
			var resourceJSON = $("#selResourceJSON").val();
			CXBuilder.selectedResource = eval('(' + resourceJSON + ')');
			CXBuilder.basePath = $("#basePath").val();
			Credential.basePath = $("#basePath").val();
			CXBuilder.setCredentialExpr(CXBuilder.selectedResource.credentials);
			$.getJSON("/" + CXBuilder.basePath + "/cxBuilder/viewSavedCredentialExpr", {id: CXBuilder.selectedResourceId},
					function(data) {
						if (data) {
					        var credExprStr = JSON.stringify(data, null, 4);
					        $("#editResource #savedCredExpr").text(credExprStr);
						}
			});
			updateFormAction($("#resourceName").val());
	    });

	</script>
	<body>
		<g:hasErrors bean="${ flash.errorObj }">
			<div class="error" style="margin-bottom:20px">
				<g:renderErrors bean="${ flash.errorObj }" as="list" />
			</div>
		</g:hasErrors>
		<input type="hidden" name="selResourceId"  id="selResourceId" value="${ resource.id}"/>
		<input type="hidden" name="selResourceJSON" id="selResourceJSON" value="${ resource.outputJSON().encodeAsHTML() }"/>
		<input type="hidden" name="resourceName" id="resourceName" value="${resourceName }" />
		<input type="hidden" name="basePath" id="basePath" value="${basePath}"/>		
        <div id="editResource">
           	<form action="/${basePath}/cxBuilder/saveCredentialService" method="post">
           	    <input type="hidden" id="selectedCredentials" name="selectedCredentials" value="" />
           	    <input type="hidden" name="paramMapping" id="paramMapping" value="" />
           	    <input type="hidden" name="joinType" id="joinType" value="" />
           	    <input type="hidden" name="logicalCredExpr" id="logicalCredExpr" value="" />
           	    <input type="hidden" name="invalidateSessionOnSave" id="invalidateSessionOnSave" value="${invalidateSessionOnSave}" />
           	    <input type="hidden" name="testCX" id="testCX" value="false"/>
 	        	<div class="sectionBody withShadow">
	        		<div class="sectionHeader sectionHeaderShadow">Credential Expression</div>
	        		<textarea id="savedCredExpr" readonly="readonly"></textarea>
		   			<div class="buttons" style="margin-left:20px">
		   				<button id="editCredExpr">Edit Credential Expression</button>
		   			</div>
	        	</div>
				<div id="editCredentialDlg" style="display:none">	
		        	<div id="selectCredentials">
		        		<g:render template="selectCredentials" model="[isNew:false]"/>
		        	</div>
		        	<div id="parameterMapping" style="display:none">
		        		<g:render template="parameterMapping" model="[isNew:false]"/>
		        	</div>
        		</div>
				<div class="buttons" align="left" style="margin-left:50px;">
					<button id="save">Save</button>
					<g:if test="${enableTest }">
						<button id="test" style="display:none">Test</button>
					</g:if>
				</div>
			</form>
        </div>
		<div id="messageBox" style="display:none">
			<div id="message"></div>
		</div>
	</body>
</html>		    
