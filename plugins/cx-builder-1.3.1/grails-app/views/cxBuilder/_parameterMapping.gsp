<% if (isNew) {
	out << "<input type='hidden' id='paramMapping' name='paramMapping' value=''/>"
} %>
  <table id="navBarTable" cellspacing='0' cellpadding='0' width='100%' >
  	<tr>
 		<td class="navBar" valign="top">
 		<g:render template='navBar' model='${[selected:"parameterMapping", isNew:isNew]}' />
 		</td>
 		<td valign="top">
			<table id="credentialConfigTable" cellspacing="0" cellpadding="5" width="100%">
				<tr>
					<td style="padding-left:20px">Credential Element:&nbsp;&nbsp;
						<select id="credElements" name="credElements" title="Configure the Parameter Mapping and Frequency Settings for each Credential Element by selecting each of them from this drop down."></select>
					</td>
				</tr>
				<tr>
					<td>
						<div class="sectionBody withShadow">
							<div class="sectionHeader sectionHeaderShadow">Credential Element Configuration</div>
							<table id="paramMappingTable" cellspacing="0" cellpadding="5" width="100%">
							<!-- 
								<tr>
									<td colspan="2"><p>The Parameter Mapping specifies how values are assigned to credential element parameters. The values may be static values or dynamic values substituted from request parameters.
									 		A required credential element parameter is embedded in the credential expression and is used in the initial evaluation of the credential element.
									 		A non-required credential element parameter may or may not be required in the evaluation of the credential element, if required then parameter value input will be requested through a Trust Broker Display request.</p></td>
								</tr>
							 -->	
								<tr>
									<td>
										<div class="sectionBody">
											<div class="sectionHeader">Parameter Mapping
												<div class="helpBackground ui-icon ui-icon-help" title="Specify how values are assigned to Credential Element parameters. The values may be static values or dynamic values substituted from request parameters."></div>
											</div>
											<table id="paramsTable" cellspacing="0" cellpadding="5" width="100%">
												<tr>
													<th>Name</th>
													<th>Type</th>
													<th>In Credential Expression</th>
													<th>Value</th>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="sectionBody">
											<div class="sectionHeader">Frequency Settings
												<div class="helpBackground ui-icon ui-icon-help" title="Specify the duration for which a granted credential is valid for. The user will be challenged for the credentials again after the duration expires"></div>
											</div>
											<table id="frequencyTable" cellspacing="0" cellpadding="5" width="100%">
											<!-- 
												<tr>
													<td colspan="2"><p>The Frequency Settings specifies the condition and duration for which a granted credential is valid for. The credential will be requested for again after the condition and duration expires</p></td>
												</tr>
											 -->	
												<tr>
													<td>Frequency Type:</td>
													<td>
														<select id="frequencyType" name="frequencyType>">
															<option value="SameSession">Same Session</option>
														</select>
													</td>
												</tr>
												<tr>
													<td>Expires after (in minutes):</td>
													<td><input type="text" name="maxAge" id="maxAge" value="" size="20" style="width:90%"/></td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<% if (isNew) {
						out << "<tr>\n";
						out << "    <td align='left'>\n";
						out << "        <div class='buttons' style='margin-left:10px;'>\n";
						out << "			<button id='viewCredExpr' title='View the Credential Expression in its JSON representation'>View Credential Expression</button>\n";
						out << "		</div>\n";
						out << "	</td>\n";
						out << "</tr>"	
					}	
				 %>
			</table>
		</td>
	</tr>
</table>
