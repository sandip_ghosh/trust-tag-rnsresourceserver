package com.resilient.as

import java.util.Map

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONException
import org.codehaus.groovy.grails.web.json.JSONObject

import java.net.URI;
import java.net.URISyntaxException;

import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers
import com.resilient.as.cxbuilder.util.CredentialExpressionParser

class CxBuilderController {

	def parameterMapping = {
		JSONArray credentialJSONs = new JSONArray();
		if (params.id) {
			def res = BaseResource.get(params.id)
			if (res) {
				CredentialExpression credExpr = res.credentialExpr
				for (CredentialElement cred: credExpr.credentialElements) {
					def credJSONStr = new JSON(cred).toString();
					JSONObject paramsJSON = new JSONObject(CredentialExpressionHelpers.buildCredentialParamsJSON(credJSONStr))
					credentialJSONs.add(paramsJSON)
				}
			}
		} else if (params.selectedCredentials) {
			JSONArray selCredArray = new JSONArray(params.selectedCredentials);
			for (int i = 0; i < selCredArray.size(); i++) {
				JSONObject selCredentialObj = selCredArray.get(i);
				String credName = selCredentialObj.getString("name");
				String grantingAuth = selCredentialObj.getString("grantingAuthority");
				CredentialElement cred = new CredentialElement(name: credName, grantingAuthority: grantingAuth);
				String frequencyType = "";
				int maxAge = 0;
				try {
					frequencyType = selCredentialObj.getString("frequencyType");
					maxAge = Integer.parseInt(selCredentialObj.getString("maxAge"));
					maxAge = maxAge / (1000 * 60)
				} catch (JSONException ex) {
					// the frequencyType and maxAge was not set in the credential expression builder so set these values to the default values
					frequencyType = CredentialElement.DEFAULT_FREQUENCY_TYPE;
					maxAge = CredentialElement.DEFAULT_MAX_AGE_MINS;
				} catch (NumberFormatException ex) {
					throw new NumberFormatException("Failed to parse the 'Expires After' value to a number. Please enter a numeric value");
				}
				cred.frequencyType = frequencyType;
				cred.maxAge = maxAge;
				if (selCredentialObj.has("parameterMapping")) {
					JSONArray paramMapping = selCredentialObj.getJSONArray("parameterMapping");
					if (paramMapping) {
						cred.parameterMapping.addAll(paramMapping);
					}
				}
				def credJSONStr = new JSON(cred).toString();
				JSONObject paramsJSON = new JSONObject(CredentialExpressionHelpers.buildCredentialParamsJSON(credJSONStr))
				credentialJSONs.add(paramsJSON)
			}
		}
		render credentialJSONs.toString()
	}
	
	def viewCredentialExpression = {
		def res = new ReservedResource()
		res.properties = params
		_createCredentialsFromFormData(res, params, false);
		render res.credentialExpr()
	}

	def updateCredentialExpression = {
		def out = ""
		def resJSONStr = ""
		def credExprStr = ""
		try {
			def res = new ReservedResource()
			res.properties = params
			_createCredentialsFromFormData(res, params, false);
			resJSONStr = res.outputJSON()
			credExprStr = res.credentialExpr()
			out = "{\"valid\": true, \"credentialExpr\": " + credExprStr + ", \"resourceJSON\": " + resJSONStr + "}"
		} catch (Exception ex) {
			log.error("Exception occurred in updating the Credential Expression. Reason: " + ex.getLocalizedMessage())
			def errMsg = "Failed to update the Credential Expression. Reason: " + ex.getLocalizedMessage()
			def res = BaseResource.findByNameIlike(params.id)
			if (res) {
				resJSONStr = res.outputJSON()
				credExprStr = res.credentialExpr()
			}
			out = "{\"valid\": false, \"message\": \"" +  errMsg + "\", \"credentialExpr\": " + credExprStr + ", \"resourceJSON\": " + resJSONStr + "}"
		} finally {
			log.info("The out: " + out)
			render out
		}
	}

	def viewSavedCredentialExpr = {
		def res
		def resId = null
		if (params.id instanceof Long) {
			res = BaseResource.get(params.id)
		} else {
			try {
				resId = Integer.parseInt(params.id)
				res = BaseResource.get(resId)
			} catch(NumberFormatException ex) {
				res = BaseResource.findByNameIlike(params.id)
			}
		}
		if (res) {
			render res.credentialExpr()
		} else {
		    render ""
        }
	}
	
	def editCredentialService = {
		def afterTest = false
		if (params.afterTest && Boolean.valueOf(params.afterTest)) {
			afterTest = true
		}
		def resName = afterTest ? params.id + "_TEST" : params.id
		def res = BaseResource.findByNameIlike(resName)
		if (!res) {
			res = new ReservedResource()
			res.name = resName
		}
		def invalidateSessionOnSave = false
		if (params.invalidateSessionOnSave) {
			invalidateSessionOnSave = Boolean.valueOf(params.invalidateSessionOnSave)
		}
		def basePath = _getBasePath()
		def resourceName = params.id
		return [resource : res, basePath: basePath, invalidateSessionOnSave: invalidateSessionOnSave, resourceName: resourceName, enableTest: invalidateSessionOnSave]
	}
	
	def saveCredentialService = {
		def testCX = false
		def resName = params.id
		def testResName = resName + "_TEST"
		// If we are saving the Resource through the test button
		// append the "_TEST" name to the resource.
		// if saving the Resource through the save button and the "_TEST" is in the resource name then save without the suffix
		if (params.testCX && Boolean.valueOf(params.testCX)) {
			testCX = true
		} else  {
			_deleteResource(testResName)
		}
		
		def res = BaseResource.findByNameIlike(testCX ? testResName : resName)
		if (!res) {
			res = new ReservedResource()
			res.name = testCX ? testResName : resName
		}
		try {
			CredentialExpression credExpr = res.credentialExpr
			if (credExpr && credExpr.credentialElements && credExpr.credentialElements.size()) {
				for(CredentialElement credential : credExpr.credentialElements) {
					credential.delete();
				}
			}
			if (credExpr) {
				credExpr.delete();
				res.credentialExpr = null;
			}
			_createCredentialsFromFormData(res, params, true);
			res.containerToken = null
			
			if (!res.save(flush:true)) {
				flash.errorObj = res;
			}
		} catch (Exception ex) {
			Object[] args = new Object[1];
			args[0] = ex.getLocalizedMessage();
			res.errors.reject("com.resilient.resourceserver.Resource.saveException", args, "Exception occurred while attempting to save the Resource.") ;
			flash.errorObj = res;
		} finally {
			def invalidateSessionOnSave = Boolean.valueOf(params.invalidateSessionOnSave)
			if (invalidateSessionOnSave) {
				def sessionIdParam = "sessionId"
				if (params.sessionIdParam) {
					sessionIdParam = params.sessionIdParam
				}
				def parametersParam = "parameters"
				if (params.parametersParam) {
					parametersParam = params.parametersParam
				}
				if (session.getAttribute("authCredential")) {
					session.setAttribute("authCredential", null)
				}
				session.setAttribute(sessionIdParam, null)
				session.setAttribute(parametersParam, null)
			}
			if (testCX && !flash.errorObj) {
				def basePath = _getBasePath()
				def redirectURL = "/" + basePath + "/relyingParty/testCredentials/" + resName
				redirect(url: redirectURL)
			} else {
				redirect(action:'editCredentialService', id: res.name);
			}
		}
	}

	def findCredentialMetaData = {
		def partialMDName = params.searchName
		def jsonResult = CredentialExpressionHelpers.findCredentialElementMetaDataJSON(partialMDName)
		if (jsonResult)
			render jsonResult
		else
			render ""
	}

	def validateLogicalCredExpr = {
		if (params.logicalCredExpr && params.selectedCredentials) {
			JSONArray selCredArray = new JSONArray(params.selectedCredentials)
			CredentialExpressionParser cxParser = new CredentialExpressionParser(params.logicalCredExpr, selCredArray)
			boolean parseOK = cxParser.parseCredentialExpr()
			JSONObject result = new JSONObject()
			result.put("parseResult", parseOK)
			if (!parseOK) {
				result.put("errorMsg", cxParser.getErrorMessage())
				result.put("errorOffset", cxParser.getErrorOffset())
			}
			render result.toString()
		} else {
			render ""
		}
	}

	private void _createCredentialsFromFormData(BaseResource res, Map params, boolean persist) throws Exception {
		if (params.selectedCredentials) {
			res.credentialExpr = new CredentialExpression(joinType: params.joinType, logicalCredExpr: params.logicalCredExpr)
			JSONArray selCredArray = new JSONArray(params.selectedCredentials);
			for (int i = 0; i < selCredArray.size(); i++) {
				JSONObject selCredentialObj = selCredArray.get(i);
				String credName = selCredentialObj.getString("name");
				String grantingAuths = selCredentialObj.getString("grantingAuthority");
				String logicalCredName = _getLogicalCredentialName(credName, grantingAuths);
				def credential = new CredentialElement(name: credName, grantingAuthority: grantingAuths);
				if (params.paramMapping) {
					JSONArray paramMappings = new JSONArray(params.paramMapping);
					for (int cred = 0; cred < paramMappings.size(); cred++) {
						JSONObject credParamsObj = paramMappings.getJSONObject(cred);
						String paramMappingCredName = _getLogicalCredentialName(credParamsObj.getString("name"), credParamsObj.getString("grantingAuthority"));
						if (paramMappingCredName.equals(logicalCredName)) {
							String frequencyType = "";
							int maxAge = 0;
							try {
								frequencyType = credParamsObj.getString("frequencyType");
								maxAge = Integer.parseInt(credParamsObj.getString("maxAge"));
                                // if maxAge is > 35791 Java int will overflow when multiplied by 60000
                                if (maxAge > 35791) {
                                    maxAge = 35791;
                                }
                                if (maxAge < 0) {
                                    maxAge = CredentialElement.DEFAULT_MAX_AGE;
                                } else {
								    maxAge = maxAge * 1000 * 60
                                }
							} catch (JSONException ex) {
								// the frequencyType and maxAge was not set in the credential expression builder so set these values to the default values
								frequencyType = CredentialElement.DEFAULT_FREQUENCY_TYPE;
								maxAge = CredentialElement.DEFAULT_MAX_AGE;
							} catch (NumberFormatException ex) {
								throw new NumberFormatException("Failed to parse the 'Expires After' value to a number. Please enter a numeric value");
							}
							credential.frequencyType = frequencyType;
							credential.maxAge = maxAge;
							JSONArray paramsArray = credParamsObj.getJSONArray("parameters");
							for (int param = 0; param < paramsArray.size(); param++) {
								JSONObject paramObj = paramsArray.getJSONObject(param);
								JSONObject paramMapObj = paramObj.getJSONObject("mapping");
								String paramValue = paramMapObj.getString("value") ? paramMapObj.getString("value") : ""
								boolean isRequired = paramMapObj.getBoolean("isRequired")
								String paramType = paramObj.getString("type")
								boolean isStatic = paramMapObj.getBoolean("isStatic")
								boolean historyKey = true
								if (paramType == CredentialExpressionHelpers.DATA_TOKEN_TYPE) {
									isStatic = false
									paramValue = paramObj.getString("name")
									historyKey = false;
								}
								def paramMap = new ParameterMap(name: paramObj.getString("name"), displayName: paramObj.getString("displayName"), isRequired: isRequired, isStatic: isStatic, value: paramValue, historyKey : historyKey)
								if (persist) {
									if ((paramValue.isEmpty() || paramValue.equals("null")) && isRequired && !paramType.equals(CredentialExpressionHelpers.DATA_TOKEN_TYPE)) {
										throw new Exception("The value is not specified for parameter '" + paramObj.getString("displayName") + "' that is required before evaluating the credential expression")
									}
									credential.addToParameterMapping(paramMap);
								} else {
									credential.parameterMapping.add(paramMap);
								}
								
							}
							break;
						}
					}
				}

				if (persist) {
					res.credentialExpr.addToCredentialElements(credential);
				} else {
					res.credentialExpr.credentialElements.add(credential);
				}
			}
		}

	}
	
	private String _getLogicalCredentialName(String credName, String grantingAuths) {
		CredentialElement credElement = new CredentialElement(name: credName, grantingAuthority: grantingAuths)
		return credElement.logicalName()
	}
	
	def getCredentialFrequency = {
		def model = []
		def res = BaseResource.findByNameIlike(params.id)
		if (res && res.credentialExpr && res.credentialExpr.credentialElements.size()) {
			CredentialElement cred = res.credentialExpr.credentialElements.get(0)
			model = [timeout: cred.maxAge]
		}
		render model as JSON
	}

    def deleteCredential = {
		def status = _deleteResource(params.id)
        def result = [status: status]
        render result as JSON
    }

	private String _getBasePath() {
		String basePath = ""
		String url = request.getRequestURL().toString()
		try {
			URI reqUri = new URI(url)
			String path = reqUri.getPath()
			if (path.startsWith("/")) {
				path = path.substring(1);
			}
			String[] urlParts = path.split("/")
			basePath = urlParts[0]
		} catch (URISyntaxException ex) {
			// just swallow basePath will be an empty string
		}
		return basePath
	}
	
	private boolean _deleteResource(String resName) {
		boolean success = false
		BaseResource res = BaseResource.findByNameIlike(resName)
		if (res) {
			CredentialExpression credExpr = res.credentialExpr
			if (credExpr && credExpr.credentialElements && credExpr.credentialElements.size()) {
				for(CredentialElement credential : credExpr.credentialElements) {
					credential.delete();
				}
			}
			res.delete()
			success = true
		}
		return success
	}

}
