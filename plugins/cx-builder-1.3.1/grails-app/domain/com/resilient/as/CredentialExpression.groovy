package com.resilient.as

import grails.converters.JSON

import java.util.List

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers
import com.resilient.as.cxbuilder.util.CredentialExpressionParser

class CredentialExpression {

	List credentialElements = []
	static hasMany = [ credentialElements : CredentialElement ]

	String joinType = "And"
	String logicalCredExpr
	
	static mapping = {
		table "rs_credential_expression"
		id generator:"org.hibernate.id.enhanced.SequenceStyleGenerator",
			params:[sequence_name:'rate_id_sequence', initial_value:1, increment_size:1]
	}

	static belongsTo = [ resource : BaseResource]
	
    static constraints = {
		joinType (inList: ["And", "Or", "OrderedAnd", "OrderedOr", "Not"])
		credentialElements (minSize: 1)
		logicalCredExpr (nullable: true, maxSize:255)
	}
	
	def String logicalCredExpr() {
		List<CredentialElement> lstCredentials = credentialElements.asList();
		if (lstCredentials.size() == 1) {
			return lstCredentials.get(0).logicalName();
		}
		if (logicalCredExpr) {
			JSONObject credExprJSON = null;
			JSONArray credElements = new JSONArray();
			for(CredentialElement cred: lstCredentials) {
				JSONObject credElementJSON = new JSONObject(new JSON(cred).toString());
				credElements.add(credElementJSON);
			}
			CredentialExpressionParser cxParser = new CredentialExpressionParser(logicalCredExpr, credElements);
			cxParser.parseCredentialExpr()
			logicalCredExpr = cxParser.getParsedExpression();
		} else {
			def logicalCX = ""
			for(CredentialElement cred: lstCredentials) {
				if (!logicalCX.isEmpty()) {
					logicalCX += " " + joinType + " "
				} 
				logicalCX += cred.logicalName()
			}
			logicalCredExpr = logicalCX
		}
		return logicalCredExpr 
	}

	def String credentialExpr() {
		List<CredentialElement> lstCredentials = credentialElements.asList();
		if (lstCredentials.size() == 1) {
			return lstCredentials.get(0).credentialExpr();
		}
		JSONObject credExprJSON = null;
		if (logicalCredExpr) {
			JSONArray credElements = new JSONArray();
			for(CredentialElement cred: lstCredentials) {
				JSONObject credElementJSON = new JSONObject(new JSON(cred).toString());
				credElements.add(credElementJSON);
			}
			CredentialExpressionParser cxParser = new CredentialExpressionParser(logicalCredExpr, credElements);
			credExprJSON = cxParser.getCredentialExprJSON();
		} else {
			credExprJSON = new JSONObject();
			credExprJSON.put("nodeName", joinType)
			JSONArray credElements = new JSONArray();
			for(CredentialElement cred: lstCredentials) {
				def credElementJSON = new JSON(cred);
				String credElementJSONStr = credElementJSON.toString();
				credElements.add(CredentialExpressionHelpers.buildCredentialElement(credElementJSONStr));
			}
			credExprJSON.put("children", credElements);
		}
		return credExprJSON.toString(4);
	}
	
	def valid() {
		for (CredentialElement ce : credentialElements) {
			if (!ce.valid()) {
				return false
			}
		}
		return true
	}
}
