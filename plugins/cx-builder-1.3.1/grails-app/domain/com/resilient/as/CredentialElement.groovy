package com.resilient.as

import grails.converters.JSON

import java.util.Date
import java.util.Set

import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.cxbuilder.util.CredentialExpressionHelpers

class CredentialElement {

	public static String DEFAULT_FREQUENCY_TYPE = "SameSession"
	public static int DEFAULT_MAX_AGE = 20 * 60 * 1000
	public static int DEFAULT_MAX_AGE_MINS = 20

	String name
	Date lastUpdated
	String grantingAuthority
	String frequencyType
	int maxAge

	static mapping = {
		table "rs_credential_element"
		id generator:"org.hibernate.id.enhanced.SequenceStyleGenerator",
			params:[sequence_name:'rate_id_sequence', initial_value:1, increment_size:1]
	}

	Set parameterMapping = []
	static hasMany = [parameterMapping : ParameterMap]
//	static belongsTo = [ resource : Resource]
	
	static constraints = {
		name (blank: false, nullable: false)
		grantingAuthority (blank: false)
		frequencyType (inList: ["SameSession"])
	}

	def parameterNames() {
		return CredentialExpressionHelpers.getParameterNames(name)
	}
	
	def credentialExpr() {
		def jsonObj = new JSON(this)
		def jsonStr = jsonObj.toString(false)
		return CredentialExpressionHelpers.buildCredentialElement(jsonStr).toString(4);
	}
	
	def credentialMetaData() {
		return CredentialExpressionHelpers.getCredentialElementMetaDataJSON(this.name, false)
	}
	
	def paramValuesJSON() {
		def paramValuesJSONObj = new JSON(parameterMapping)
		return paramValuesJSONObj.toString(false)
	}
	 
	def trustAuthority() {
		JSONObject credMDJSONObj = new JSONObject(credentialMetaData())
		return credMDJSONObj.getString("ta");
	}

	def logicalName() {
		String[] grantingAuthArray = grantingAuthority.split(",");
		String tsNames = "";
		for (int i = 0; i < grantingAuthArray.length; i++) {
			String[] gaParts = grantingAuthArray[i].split("/");
			String tsName = gaParts[gaParts.length - 1];
			if (tsNames.length() > 0) {
				tsNames += ",";
			}
			tsNames += tsName;
		}
		return name + "[" + tsNames + "]";
	}
	
	def valid() {
		boolean isValid = CredentialExpressionHelpers.getCredentialElementMetaData(name) != null
//		if (!isValid)
//			log.error("Failed to find the Credential Metadata with name: " + name)
		return isValid
	}
}
