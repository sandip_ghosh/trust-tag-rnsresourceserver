package com.resilient.as

import grails.converters.JSON

import java.util.Date

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject


class BaseResource {
	String name
	String type = "rnsReserved"
	String displayType = "download"
	Date lastUpdated
	String hostname = ""
	int port = 0
	String path = ""
	boolean useWrapper = false
	CredentialExpression credentialExpr

	static transients = ['containerToken']
	String containerToken;

	static constraints = {
		name (blank: false, nullable: false, matches:"^[a-zA-Z_][a-zA-Z0-9_]*", validator: { val, obj ->
				def similarRes = BaseResource.findByNameIlike(val)
				return !similarRes || obj.id == similarRes.id
			})
		type (inList: ["rnsReserved"])
		displayType (inList: ["download"])
	}
	
	static mapping = {
		table "rs_resource"
		id generator:"org.hibernate.id.enhanced.SequenceStyleGenerator",
			params:[sequence_name:'rate_id_sequence', initial_value:1, increment_size:1]
	}

	def String credentialExpr() {
		return credentialExpr != null ? credentialExpr.credentialExpr()	: ""
	}
	
	def String logicalCredExpr() {
		return credentialExpr != null ? credentialExpr.logicalCredExpr()	: ""
	}

	def outputJSON() {}

	def typeString() {}
	
	def resourceURLTemplate(boolean useWrapper) { return "" }
	
	def valid() {
		boolean isValid = credentialExpr != null ? credentialExpr.valid() : true
//		if (!isValid) {
//			log.error("The Resource: " + name + " is not valid because a credential metadata referenced in the Credential Expression could not be found")
//		}
		return isValid
	}
}
