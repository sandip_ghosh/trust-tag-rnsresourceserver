package com.resilient.as

import java.util.Date;

class ParameterMap {

	String name
	String displayName
	String value
	boolean isRequired
	boolean isStatic
	boolean historyKey = true
	Date dateCreated
	
	static transients = ['type']
	String type;
	
	static belongsTo = [ credential : CredentialElement]

    static constraints = {
		name (blank: false)
    	value (nullable: true) 
	}
	
	static mapping = {
		table "rs_parameter_map"
		sort dateCreated:"asc"
		id generator:"org.hibernate.id.enhanced.SequenceStyleGenerator",
			params:[sequence_name:'rate_id_sequence', initial_value:1, increment_size:1]
	}
}
