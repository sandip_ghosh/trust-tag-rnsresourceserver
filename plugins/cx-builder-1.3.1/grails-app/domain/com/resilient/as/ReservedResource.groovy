package com.resilient.as

import grails.converters.JSON

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class ReservedResource extends BaseResource {

    static constraints = {
		type (inList: ["rnsReserved"])
		displayType (inList: ["download"])
    }
	
	ReservedResource() {
		type = "rnsReserved"
		displayType = "download"
		hostname = ""
		port = 0
		path = ""
		useWrapper = false	
	}
	
	def typeString() {
		return "Reserved"
	}

	def outputJSON() {
		JSONObject outputJSONObj = new JSONObject();
		outputJSONObj.put("name", name);
		outputJSONObj.put("type", type);
		if (credentialExpr) {
			outputJSONObj.put("joinType", credentialExpr.joinType);
			outputJSONObj.put("logicalCredExpr", credentialExpr.logicalCredExpr);
			def credExprJSON = new JSON(credentialExpr);
			JSONObject credExprJSONObj = new JSONObject(credExprJSON.toString(false));
			JSONArray credArray = new JSONArray(credExprJSONObj.getJSONArray("credentialElements"));
			for (int cred = 0; cred < credArray.size(); cred++) {
				JSONObject credObj = credArray.get(cred);
				CredentialElement tempCred = new CredentialElement(name: credObj.getString("name"))
				credObj.put("ta", tempCred.trustAuthority())
			}
			outputJSONObj.put("credentials", credArray);
		} else {
			outputJSONObj.put("joinType","And");
			JSONArray credArray = new JSONArray();
			outputJSONObj.put("credentials", credArray)
		}
		return outputJSONObj.toString();
	}
}
