var Resource = {
	selectedRow : null,
	
	selectedRowColor : null,
	
	selectedResource : null,
	
	selectedResourceId : null,
	
	selectedMappingCredential : null,
	
	clearSelectedCredentials : function () {
		var selOptions = $("#editResource #selCredentials").children();
		$.each(selOptions, function(i, selOption) {
			var credentialId = $(selOption).attr("value");
			var selOption = "<option value=\"";
			selOption += $("#editResource #selCredentials option[value=" + credentialId + "]").attr("value") + "\">";
			selOption += $("#editResource #selCredentials option[value=" + credentialId + "]").text() + "</option>";
			$("#editResource #availCredentials").append(selOption);
			$("#editResource #selCredentials option[value=" + credentialId + "]").remove();
		});
	},
	
	clearResourceConfigProps : function(prefix) {
		$(prefix + " #resourceConfigTable #name").val("");
		$(prefix + " #hostname").val("");
		$(prefix + " #portStr").val("");
		$(prefix + " #path").val("");
		$(prefix + " #useWrapper").removeAttr("checked");
		$(prefix + " #displayTypeDownload").prop("selected", true);
		$(prefix + " #stylesheet").val("");
		$(prefix + " #resourceType option[value='data']").prop("selected", true);
		this.onChangeResourceType(prefix);
	},
	
	selectResource : function(newSelRow) {
		if (newSelRow != this.selectedRow) {
			if (this.selectedRow) {
				$(this.selectedRow).css("background-color", this.selectedRowColor);
			}
			this.selectedRowColor = $(newSelRow).css("background-color");
			$(newSelRow).css("background-color", "#79c9ec");
			var resName = $("td:first", newSelRow).text();
			resName = Utils.getSafeIdFromName(resName);
			this.selectedResourceId = $("#" + resName + "_id").val();
			var resourceJSON = $("#" + resName + "_props").val();
			this.selectedResource = eval('(' + resourceJSON + ')');
			CXBuilder.selectedResource = eval('(' + resourceJSON + ')');
			this.selectedRow = $(newSelRow);
			
			if (this.selectedResource.type == "rnsReserved") {
				$("#editResource").hide();
			} else {
				$("#editResource").show();
				$("#editResource input[name='id']").attr("value", this.selectedResourceId);
				$("#editResource input[name='name']").attr("value", this.selectedResource.name);
				$("#editResource input[name='hostname']").attr("value", this.selectedResource.type == "noProxy" ? "" : this.selectedResource.hostname);	
				$("#editResource input[name='portStr']").attr("value", this.selectedResource.type == "noProxy" ? "" : this.selectedResource.port);
				$("#editResource input[name='path']").attr("value", this.selectedResource.type == "noProxy" ? "" : this.selectedResource.path);
				$("#editResource #resourceType option[value='" + this.selectedResource.type + "']").attr("selected", "selected");
				$("#editResource #trustTagType").val(this.selectedResource.trustTagType);
				$("#editResource #trustTagClientOnly").prop("checked", this.selectedResource.trustTagType == "client_only");
				$("#editResource #trustTagClientServer").prop("checked", this.selectedResource.trustTagType == "client_server");
				$("#editResource #trustTagScriptURI").val(this.selectedResource.trustTagPostGrantURI);
				this.onChangeResourceType("#editResource");
				this.onChangeDisplayType("#editResource", this.selectedResource.displayType, false);
				$("#editResource #displayTypeDownload").prop("checked", this.selectedResource.displayType == "download");
				$("#editResource #displayTypeRawData").prop("checked", this.selectedResource.displayType == "rawData");
				$("#editResource #displayTypeDisplay").prop("checked", this.selectedResource.displayType == "display");
				$("#editResource #displayTypeRedirect").prop("checked", this.selectedResource.displayType == "redirect");
				Parameter.initParameters(this.selectedResource.parameters);
				Parameter.showParameters("#editResource");
				CXBuilder.setCredentialExpr(this.selectedResource.credentials);
				$.getJSON("/RNSResourceServer/cxBuilder/viewSavedCredentialExpr", {id: this.selectedResourceId},
						function(data) {
							if (data) {
						        var credExprStr = JSON.stringify(data, null, 4);
						        $("#editResource #savedCredExpr").text(credExprStr);
							}
				});
			}
		}
	},

	getInitialSelectRow : function(selectedResourceId) {
		var tbody = $("#resources").children()[0];
		var resRows = $(tbody).children();
		for (var i = 1; i < resRows.length; i++) {
			var resRow = resRows[i];
			var resName = $("td:first", resRow).text();
			resName = resName.toLowerCase()
			resName = Utils.getSafeIdFromName(resName);
			var resId = $("#" + resName + "_id").val();
			if (selectedResourceId === resId) {
				return resRow;
			}
		}
		return null;
	},
	
	createOrUpdateResource : function(isNew) {
		var selectorPrefix = isNew ? "#newResource" : "#editResource";
		var credentialSelectorPrefix = isNew ? "#newResource" : "#editCredentialDlg";
//	    if ($(selectorPrefix + " #useWrapper").is(":checked")) {
//	    	$(selectorPrefix + " #useWrapper").val("true");
//		} else {
//			$(selectorPrefix + " #useWrapper").val("false");
//		}
	    var selCredentials = unescape($(selectorPrefix + " #selectedCredentials").val());
	    $(selectorPrefix + " #selectedCredentials").val(selCredentials);
	    var paramMappingJSON = CXBuilder.getParameterMapping(credentialSelectorPrefix);
	    $(selectorPrefix + " #paramMapping").val(paramMappingJSON);
	    var displayType = $(selectorPrefix + " #displayType").val();
	    if (!displayType) {
	    	$(selectorPrefix + " #displayType").val("download");
	    }
	    var trustTagType = $(selectorPrefix + " #trustTagType").val();
	    if (!trustTagType) {
	    	$(selectorPrefix + " #trustTagType").val("client_only");
	    } else if (trustTagType === "client_server") {
	    	$(selectorPrefix + " #trustTagPostGrantURI").val($(selectorPrefix + " #trustTagScriptURI").val());
	    }
	    if (selectorPrefix == "#editResource") {
	    	$(selectorPrefix + " #joinType").val($(credentialSelectorPrefix + " #editJoinType option:selected").attr("value"));
	    	$(selectorPrefix + " #frequencyType").val($(credentialSelectorPrefix + " #editFrequencyType option:selected").attr("value"));
	    }
		if (Parameter.parameters && Parameter.parameters.length) {
			var paramsJSON = JSON.stringify(Parameter.parameters, null);
			$(selectorPrefix + " #parametersJSON").val(paramsJSON);
		}
		var logicalCX = $(credentialSelectorPrefix + " #logicalCX").val();
		$(selectorPrefix + " #logicalCredExpr").val(logicalCX);
		$(selectorPrefix + " form").submit();
	},

	onChangeResourceType : function(prefix) {
		var selResourceType = $(prefix + " #resourceType option:selected").attr("value");
		if (selResourceType == "data") {
			$(prefix + " #resourceProxyURLs").show();
			$(prefix + " #assetLocation").show();
			$(prefix + " #displaySettings").show();
			$(prefix + " #relyingPartyURL").hide();
			$(prefix + " #trustTagSettings").hide();
		} else {
			$(prefix + " #resourceProxyURLs").hide();
			$(prefix + " #assetLocation").hide()
			$(prefix + " #displaySettings").hide();
			$(prefix + " #rpURLs").empty();
			$(prefix + " input[name='hostname']").val("");
			$(prefix + " input[name='portStr']").val("");
			$(prefix + " input[name='path']").val("");
			$(prefix + " #relyingPartyURL").show();
			$(prefix + " #trustTagSettings").show();
		}
		this.onChangeResourceName(prefix);		
	},
	
	onChangeResourceName : function(prefix) {
		var theThis = this;
		var resObj = $(prefix + " #name");
		var resName = $.trim(resObj.val());
		var resType = $(prefix + " #resourceType option:selected").attr("value");
		if (resType == "data") {
			resObj.doTimeout('updateRPURL', 1000, function() {
				$.getJSON("/RNSResourceServer/resource/resourceProxyURLTemplate",
					{ resourceName: resName, resourceType: resType},
	    			function(data) {
						if (data) {
							$(prefix + " #rpURLs").empty();
							var rpURLsText = "API URL: " + data.apiURL + "<br/>" + "Web URL: " + data.webURL;
							$(prefix + " #rpURLs").append(rpURLsText);
						}
					});
				
			});
		} else {
			resObj.doTimeout('updateASURI', 1000, function() {
				var rpURI = '<p>' + location.protocol + "//" + location.host + "/RNSResourceServer/credentialRequestor/requestCredentials/" + resName + '</p>';
				$(prefix + " #relyingPartyURI").empty();
				$(prefix + " #relyingPartyURI").append(rpURI);
			});
			var trustTagType = $(prefix + " #trustTagType").val();
			if (!trustTagType) {
				trustTagType = "client_only";
			}
			this.onChangeScriptURI(prefix, trustTagType);
		}
	},
	
	onChangeScriptURI : function(prefix, trustTagType) {
		$(prefix + " #trustTagType").val(trustTagType);
		var resObj = $(prefix + " #name");
		var resName = $.trim(resObj.val());
		var trustTagScriptURI = $(prefix + " #trustTagScriptURI").val();
		if (trustTagType == "client_server") {
			$(prefix + " #trustTagScriptURI").prop("disabled", false);
		} else {
			$(prefix + " #trustTagScriptURI").prop("disabled", true);
			$(prefix + " #trustTagScriptURI").val("");			
		}
		resObj.doTimeout('updateTrustTag', 1000, function() {
			var trustTagHTML = '<SCRIPT type="text/javascript" src="' + location.protocol + '//' + location.host + '/RNSResourceServer/TrustTag/js?resource=' + resName;
			trustTagHTML += trustTagType == "client_server" ? "&serverScript=true" : "&serverScript=false";
			if (trustTagType == "client_server") {
				trustTagHTML += "&serverScriptURI=" + encodeURIComponent(trustTagScriptURI); 
			}
			trustTagHTML += "\"></SCRIPT>" 
			var encodedTrustTagHTML = '<p>' + $('<div/>').text(trustTagHTML).html() + '</p>';	
			$(prefix + " #trustTagHTML").empty();
			$(prefix + " #trustTagHTML").append(encodedTrustTagHTML);	
		});
	},
	
	onChangeDisplayType : function(prefix, displayType, prompt) {
		if (displayType == "redirect" && prompt) {
			Utils.showMessageBox("Asset Retrieval Settings", "This option is not secure. It requires the Asset to be publicly accessible. Recommended only to be used for demo purposes", "warning");
		}
		$(prefix + " #displayType").val(displayType);
		$(prefix + " #stylesheet").prop("disabled", displayType != "display");
		
	},
	
	
	showResourceConfigStep : function(prefix) {
		$(prefix + " #selectCredentials").hide();
		$(prefix + " #parameterMapping").hide();
		$(prefix + " #paramConfig").hide();
		$(prefix + " #resourceConfig").show();
	},
	
	showParameterConfigStep : function(prefix) {
		$(prefix + " #selectCredentials").hide();
		$(prefix + " #parameterMapping").hide();
		$(prefix + " #resourceConfig").hide();
		$(prefix + " #paramConfig").show();
	},
	
	showSelectCredentialsStep : function(prefix) {
		$(prefix + " #resourceConfig").hide();
		$(prefix + " #parameterMapping").hide();
		$(prefix + " #paramConfig").hide();
		$(prefix + " #selectCredentials").show();
	},
	
	showParameterMappingStep : function(prefix) {
		$(prefix + " #resourceConfig").hide();
		$(prefix + " #selectCredentials").hide();
		$(prefix + " #paramConfig").hide();
		$(prefix + " #parameterMapping").show();
	},

	showPrev : function(prefix, currPage) {
//		if (currPage == "ParameterConfig") {
//			this.showResourceConfigStep(prefix);
//			currPage = "ResourceConfig";
//		} else
		if (currPage == "SelectCredentials") {
//			this.showParameterConfigStep(prefix);
//			currPage = "ParameterConfig";
			this.showResourceConfigStep(prefix);
			currPage = "ResourceConfig";
		} else if (currPage == "ParameterMapping") {
			this.showSelectCredentialsStep(prefix);
			currPage = "SelectCredentials";
		}
		return currPage;
	},
	
	showNext : function(prefix, currPage) {
		if (currPage == "ResourceConfig") {
//			this.showParameterConfigStep(prefix);
//			currPage = "ParameterConfig";
//		} else if (currPage == "ParameterConfig") {
			this.showSelectCredentialsStep(prefix);
			currPage = "SelectCredentials";
		} else if (currPage == "SelectCredentials") {
			this.showParameterMappingStep(prefix);
			currPage = "ParameterMapping";
			// see if we should populate the Parameter Mapping
			var paramMapping = $(prefix + " #paramMapping").val();
			if (!paramMapping) {
				var selCredentialsJSON = unescape($(prefix + " #selectedCredentials").val());
				$.getJSON("/RNSResourceServer/cxBuilder/parameterMapping", {selectedCredentials: selCredentialsJSON},
						function(data) {
							if (data) {
				        		var paramMappingJSONStr = JSON.stringify(data, null);
				        		CXBuilder.populateParameterMapping(paramMappingJSONStr, prefix);
							}
						});
						
			}
		} else if (currPage == "ParameterMapping") {					
			this.createOrUpdateResource(true);
			$(prefix).dialog('close');
		}
		return currPage;
	}
		
};

/* ------------------------------------------------------------------------------------------------------------
 * Event handlers moved from RNSResourceServer index page
 */
var g_currPage = "ResourceConfig";
var g_notFoundOption = "No Credential Metadata found";
			

$("#resources tr").live("click", function(event) {
	$(".error:visible").hide();
	Resource.selectResource($(this));
	event.preventDefault();
});

$("#newResource #step1Link").live("click", function(event) {
	g_currPage = "ResourceConfig";
	Resource.showResourceConfigStep("#newResource");
	event.preventDefault();
});

$("#newResource #step2Link").live("click", function(event) {
	g_currPage = "ParameterConfig";
	Resource.showParameterConfigStep("#newResource");
	event.preventDefault();
});

$("#newResource #step3Link").live("click", function(event) {
	g_currPage = "P";
	CXBuilder.showSelectCredentialsStep("#newResource");
	event.preventDefault();
});

$("#newResource #step4Link").live("click", function(event) {
	g_currPage = "ParameterMapping";
	CXBuilder.showParameterMappingStep("#newResource");
	// see if we should populate the Parameter Mapping
	var paramMapping = $("#newResource #paramMapping").val();
	if (!paramMapping) {
		var selCredentialsJSON = unescape($("#newResource #selectedCredentials").val());
		$.getJSON("/RNSResourceServer/cxBuilder/parameterMapping", {selectedCredentials: selCredentialsJSON},
				function(data) {
					if (data) {
		        		var paramMappingJSONStr = JSON.stringify(data, null);
		        		CXBuilder.populateParameterMapping(paramMappingJSONStr, "#newResource");
					}
				});
				
	}
	event.preventDefault();
});

$("#resources .delete").live("click", function(event) {
	Resource.selectResource($(this).parent());
	var theThis = $(this);
	Utils.setMessageBox("The Resource will be deleted. Are you sure?", "warning");
	var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:'Confirm Delete Resource', model:true,
		width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
		buttons:{'Cancel': function() {$(this).dialog('close'); },
			'Delete': function() {
				if (Resource.selectedResourceId) {
					window.location = "/RNSResourceServer/resource/delete?id=" + Resource.selectedResourceId;
				}
				
			}}});
	confirmDlg.dialog("open");
	event.preventDefault();
});

$("#newResource #resourceType").live("change", function(event) {
	Resource.onChangeResourceType("#newResource");
	event.preventDefault();
});

$("#editResource #resourceType").live("change", function(event) {
	Resource.onChangeResourceType("#editResource");
	event.preventDefault();
});

$("#newResource #name").live("keyup", function(event) {
	Resource.onChangeResourceName("#newResource");
	event.preventDefault();
});

$("#editResource #name").live("keyup", function(event) {
	Resource.onChangeResourceName("#editResource");
	event.preventDefault();
});

$("#newResource #trustTagScriptURI").live("keyup", function(event) {
	var trustTagType = $("#newResource #trustTagType").val();
	Resource.onChangeScriptURI("#newResource", trustTagType);
	event.preventDefault();
});

$("#editResource #trustTagScriptURI").live("keyup", function(event) {
	var trustTagType = $("#editResource #trustTagType").val();
	Resource.onChangeScriptURI("#editResource", trustTagType);
	event.preventDefault();
});

$("#newResource #trustTagClientOnly").live("click", function(event) {
	Resource.onChangeScriptURI("#newResource", "client_only");
//	event.preventDefault();
});

$("#editResource #trustTagClientOnly").live("click", function(event) {
	Resource.onChangeScriptURI("#editResource", "client_only");
//	event.preventDefault();
});

$("#newResource #trustTagClientServer").live("click", function(event) {
	Resource.onChangeScriptURI("#newResource", "client_server");
//	event.preventDefault();
});

$("#editResource #trustTagClientServer").live("click", function(event) {
	Resource.onChangeScriptURI("#editResource", "client_server");
//	event.preventDefault();
});

$("#newResource #displayTypeDownload").live("click", function(event) {
	Resource.onChangeDisplayType("#newResource", "download", true);
//	event.preventDefault();
});

$("#editResource #displayTypeDownload").live("click", function(event) {
	Resource.onChangeDisplayType("#editResource", "download", true);
//	event.preventDefault();
});

$("#newResource #displayTypeDisplay").live("click", function(event) {
	Resource.onChangeDisplayType("#newResource", "display", true);
//	event.preventDefault();
});

$("#editResource #displayTypeDisplay").live("click", function(event) {
	Resource.onChangeDisplayType("#editResource", "display", true);
//	event.preventDefault();
});

$("#newResource #displayTypeRawData").live("click", function(event) {
	Resource.onChangeDisplayType("#newResource", "rawData", true);
//	event.preventDefault();
});

$("#editResource #displayTypeRawData").live("click", function(event) {
	Resource.onChangeDisplayType("#editResource", "rawData", true);
//	event.preventDefault();
});

/*
$("#newResource #displayTypeRedirect").live("click", function(event) {
	Resource.onChangeDisplayType("#newResource", "redirect", true);
	return false;
});
*/

$("#editResource #displayTypeRedirect").live("click", function(event) {
	Resource.onChangeDisplayType("#editResource", "redirect", true);
//	event.preventDefault();
});



