var Parameter = {
	parameters : null,
	
	initParameters : function(paramsJSON) {
		if (paramsJSON && paramsJSON.length) {
			this.parameters = new Array();
			for (var i=0; i < paramsJSON.length; i++) {
				var paramObj = {name: paramsJSON[i].name, 
								type: paramsJSON[i].type, 
								label: paramsJSON[i].label, 
								description: paramsJSON[i].description, 
								validationType: paramsJSON[i].validationType, 
								customValidation: paramsJSON[i].customValidation, 
								picklistValues: paramsJSON[i].picklistValues};
				this.parameters.push(paramObj);
			}
		} else {
			this.parameters = null;
		}
	},
	
	showParameters : function(prefix) {
		$(prefix + " #parameters").empty();
		var tableHeaderHTML = "<tr><th>Name</th><th>Type</th><th>Display Name</th><th>Description</th><th>Validation</th><th></th></tr>";
		$(prefix + " #parameters").append(tableHeaderHTML);
		if (!this.parameters || !this.parameters.length) {
			return;
		}
		for (i = 0; i < this.parameters.length; i++) {
			var tableRowHTML = i % 2 ? "<tr style='background-color: #ffffff;'>" : "<tr style='background-color: #e9f2fe;'>";
			tableRowHTML += "<td>" + this.parameters[i].name + "</td>";
			tableRowHTML += "<td>" + $(prefix + " #paramType option[value='" + this.parameters[i].type + "']").text() + "</td>";
			tableRowHTML += "<td>" +  this.parameters[i].label + "</td>";				
			tableRowHTML += "<td>" + this.parameters[i].description + "</td>";
			tableRowHTML += "<td>" + $(prefix + " #validationType option[value='" + this.parameters[i].validationType + "']").text() + "</td>";
			tableRowHTML += "<td class='delete'></td></tr>";
			$(prefix + " #parameters").append(tableRowHTML);
		}
	},

	clearParameterProps : function(prefix) {
		$(prefix + " #paramDetails #paramName").val("");
		$(prefix + " #paramDetails #paramType option[value = 'text']").prop("selected", true);
		$(prefix + " #paramDetails #picklistValuesSpan").hide();
		$(prefix + " #paramDetails #picklistValues").val("");
		$(prefix + " #paramDetails #paramLabel").val("");
		$(prefix + " #paramDetails #paramDesc").val("");
		$(prefix + " #paramDetails #validationType option[value = 'none']").prop("selected", true);
		$(prefix + " #paramDetails #customValidationSpan").hide();
		$(prefix + " #paramDetails #customValidation").val("");
		$(prefix + " #paramDetails #validationType").prop("disabled", false);
	},
	
	deleteParameter : function(prefix, deleteNode) {
		var trNode = deleteNode.parent().get(0);
		var tdNodes = $(trNode).children();
		var paramName = $(tdNodes[0]).text();
		for (var i = 0; i < this.parameters.length; i++) {
			if (this.parameters[i].name == paramName) {
				this.parameters.splice(i, 1);
				break;
			}
		}
		this.showParameters(prefix);
	},
	
	confirmDeleteParameter : function(prefix, deleteNode) {
		Utils.setMessageBox("The Parameter will be deleted. Are you sure?", "warning");
		var confirmDlg = $('#messageBox').dialog({autoOpen: false, title:'Confirm Delete Parameter', model:true,
			width:600, minWidth:600, maxWidth:600, height:180, minHeight:180, maxHeight:180,
			buttons:{'Cancel': function() {$(this).dialog('close'); },
				'Delete': function() {
					Parameter.deleteParameter(prefix, deleteNode);
					$(this).dialog('close');
				}}});
		confirmDlg.dialog("open");
	},
	
	showParameterDetail : function(prefix, show) {
		if (show) {
			$(prefix + " #topBtnBar").hide();
			$(prefix + " #paramDetails").show();
			$(prefix + " #bottomBtnBar").show();
			this.clearParameterProps(prefix);
		} else {
			$(prefix + " #paramDetails").hide();
			$(prefix + " #bottomBtnBar").hide();
			$(prefix + " #topBtnBar").show();
		}
	},
	
	addParameter : function(prefix) {
		var newParam = new Object();
		var name = $(prefix + " #paramName").val();
		if (!name) {
			Utils.showMessageBox("Add Parameter", "Please specify a name for the new Parameter.", "warning");
			return;
		}
		newParam["name"] = name;
		var displayName = $(prefix + " #paramLabel").val();
		if (!name) {
			Utils.showMessageBox("Add Parameter", "Please specify a Display Name for the new Parameter.", "warning");
			return;
		}
		newParam["label"] = displayName;
		newParam["type"] = $(prefix + " #paramType").val();
		if (newParam.type == "picklist") {
			var picklistValues = $(prefix + " #picklistValues").val();
			if (!picklistValues) {
				Utils.showMessageBox("Add Parameter", "Please specify values for the Picklist parameter type.", "warning");
				return;
			}
			newParam["picklistValues"] = picklistValues; 
		}
		newParam["description"] = $(prefix + " #paramDesc").val();
		newParam["validationType"] = $(prefix + " #validationType").val();
		if (newParam.validationType == "custom") {
			var customValidation = $(prefix + " #customValidation").val();
			if (!customValidation) {
				Utils.showMessageBox("Add Parameter", "Please specify the custom validation regular expression", "warning");
				return;
			}
			newParam["customValidation"] = customValidation; 
		}
		// check if the parameter name is unique
		if (this.parameters) {
			for (var i = 0; i < this.parameters.length; i++) {
				if (this.parameters[i].name === newParam.name) {
					Utils.showMessageBox("Add Parameter", "The parameter name is not unique. The parameter name must be unique.", "warning");
					return;
				}
			}
		} else {
			this.parameters = new Array();
		}
		this.parameters.push(newParam);
		this.showParameterDetail(prefix, false);
		this.showParameters(prefix);
	},
	
	onChangeParamType : function(prefix) {
		var paramType = $(prefix + " #paramType").val();
		if (paramType == "picklist") {
			$(prefix + " #paramDetails #picklistValuesSpan").show();
		} else {
			$(prefix + " #paramDetails #picklistValuesSpan").hide();
			$(prefix + " #paramDetails #picklistValues").val("");
		}
		if (paramType == "text") {
			$(prefix + " #paramDetails #validationType").prop("disabled", false);
			$(prefix + " #paramDetails #validationType option[value='none']").prop("selected", true);
		} else {
			$(prefix + " #paramDetails #validationType").prop("disabled", true);
		}
	},
	
	onChangeValidationType : function(prefix) {
		var validationType = $(prefix + " #validationType").val();
		if (validationType == "custom") {
			$(prefix + " #paramDetails #customValidationSpan").show();
		} else {
			$(prefix + " #paramDetails #customValidationSpan").hide();
			$(prefix + " #paramDetails #customValidation").val("");
		}
	}
	
}

// ----- Parameter Event handlers -----------------------

$("#newResource #saveParam").live("click", function(event) {
	Parameter.addParameter("#newResource");
	event.preventDefault();
});

$("#editResource #saveParam").live("click", function(event) {
	Parameter.addParameter("#editResource");
	event.preventDefault();
});

$("#newResource #addParam").live("click", function(event) {
	Parameter.showParameterDetail("#newResource", true);
	event.preventDefault();
});

$("#editResource #addParam").live("click", function(event) {
	Parameter.showParameterDetail("#editResource", true);
	event.preventDefault();
});

$("#newResource #cancelSaveParam").live("click", function(event) {
	Parameter.showParameterDetail("#newResource", false);
	event.preventDefault();
});

$("#editResource #cancelSaveParam").live("click", function(event) {
	Parameter.showParameterDetail("#editResource", false);
	event.preventDefault();
});

$("#newResource #parameters .delete").live("click", function() {
	var deleteNode = $(this);
	Parameter.confirmDeleteParameter("#newResource", deleteNode);
});

$("#editResource #parameters .delete").live("click", function() {
	var deleteNode = $(this);
	Parameter.confirmDeleteParameter("#editResource", deleteNode);
});

$("#newResource #paramDetails #paramType").live("change", function(event) {
	Parameter.onChangeParamType("#newResource");
	event.preventDefault();
});

$("#editResource #paramDetails #paramType").live("change", function(event) {
	Parameter.onChangeParamType("#editResource");
	event.preventDefault();
});

$("#newResource #paramDetails #validationType").live("change", function(event) {
	Parameter.onChangeValidationType("#newResource");
	event.preventDefault();
});

$("#editResource #paramDetails #validationType").live("change", function(event) {
	Parameter.onChangeValidationType("#editResource");
	event.preventDefault();
});

// ----- END Parameters Event handlers ------------------
