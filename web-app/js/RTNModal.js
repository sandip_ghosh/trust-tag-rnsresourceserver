String.prototype.format = function () {
  var args = arguments;
  return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined' ? (args[number] == null ? "": args[number]): match;
  });
};

HTMLElement.prototype.getAncestorBy = function (name) {
  var self = this;
  while (self) {
    if (self.nodeType === 1 && ( self.id===name || self.hasClass(name) ) ) {
      return self;
    }
    self = self.parentNode;
  }
  return null;
};

HTMLElement.prototype.hasClass = function () {
  var args = arguments;
  var classNames = this.className.split(" ");
  for (var c=0; c<classNames.length; c++) {
    for (var i=0; i<args.length; i++) {
      if (classNames[c] === args[i]) {
        return true;
        break;
      }
    }
  }
  return false;
};

HTMLElement.prototype.addClass = function () {
  var args = arguments;
  for (var i=0; i<args.length; i++) {
    if (!this.hasClass(args[i])) {
      if (this.className === "") {
        this.className = args[i];
      } else {
        this.className = (this.className+" "+args[i]);  
      }
    }
  }
  return true;
};

HTMLElement.prototype.removeClass = function () {
  var args = arguments;
  for (var i=0; i<args.length; i++) {
    this.className = this.className.replace(args[i],"").trim();
    return true;
    break;
  }
  return false;
};

RTNModal = (function() {

   var RTNModal = function (customSettings) {
    
    this.addEvents();
    this.init(customSettings);
    
  };
  
  RTNModal.prototype.init = function (customSettings) {
    
    this.dom = {};
    
    this.settingsDefaults = {
      transitionTime: .35,
      id: 'tnModal',
      spinnerId: 'tnModalSpinner',
      contentId: 'tnModalContent',
      contentInnerId: 'tnmodalContentInner',
      headerId: 'tnModalHeader',
      footerId: 'tnModalFooter',
      maskId: 'tnModalMask',
      headerTitleId: 'tnModalTitle',
      formSubmitButtonId: 'rnsFormSubmit',
      headerBarId: 'tnHeaderBar',
      borderRadius: 100,
      height: 200, 
      width: 200, 
      padding: 25,
      opacity: 0,
      contentOpacity: 1,
      contentTop: 25,
      contentLeft: 25,
      headerOpacity: 0,
      footerOpacity: 0,
      headerHeight: 60,
      footerHeight: 60,
      contentHeight: 100,
      contentWidth: 100,
      contentColor: '#fff',
      statusColorSuccess: '#33ffcc',
      statusColorFailure: '#ff0033',
      bg: '#000',
      headerBg: '#000',
      footerBg: '#000',
      maskBg: '#fff',
      maskOpacity: 0.0,
      shadowBlur: 20,
      shadowColor: 'rgba(0,0,0,.7)',
      maskDisplay: 'none',
      borderWidth: 0,
      borderColor: 'transparent',
      borderType: 'solid',
      top: '50%',
      left: '50%',
      cursor: 'auto',
      statusHTML: '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC6NJREFUeNrsXQ2UFlUZvt+uohtL7kKQCCkSFCiEygKSQhRHMxEoVMwO/qAcUfSUqSkGBNZSJKL4EygdE7HfI62KpQgoCKgrEKHAiglJbogCCeuuuyK7bPdxn+l7d5z5vrkzc+f7OvWe8x7uzjd/95n3vj/PvTOk+j/QrP4v4eWIHF+/rdYBWsu0flFrN61dtLbX2k7rp7Ru19qT+4/RerXWf2jdpnWT1kqtH/yvAFiodbDW0VrP0tqH2zJJD62lWvdrfYfHSWnUulHrWq1Pal2t9XBSHUolNIQB1EStF2ntGOL4M7W+oLWD1n1Z9t2t9VGti7T+xXbHCmw+HK3n0zI2a70uJHiOFUL+pbUmy76dtX5X6watz2sdZbOfBZaA+yb902KtZ8RwzuNE+z2D44ZqfULrVj7MVL4D2FvrKq2Paf1SjOftJNrvhzi+Fx/mGq2n5iOAbbSW0+qGWrDqo0S7NsJ5MBrWaZ0RVwCNA8AedPBTCKQNOVq0ayKeC8BN1/oiU6ecAtiLka7McrD70ELqNYCBZlQuAXyDSa1tkX6vXYznLaa//kGuAGzSekMCAMpKo72FQHq71l+EidKmAOICA13bVmh93DKA1aJ9vKVrTAoDoimAs7V+y2P7tSy1bMkO/nss62Nbco3Wu20BOFnrja5tQ/jv2xaHMmrNLWx3T8BdTLIBIKzupx4pBVKXz7O9kA45bvmbsO4BCREepXECeKLWXwnfUOz6/UHBqIxnZI5TXhLt0xMiWdrHBWAbMhslYluJK6n9Ci3R+ftCrfUxdmZVDgB0/GwHVxVkDOCPtPZ3bTtGtPeJ/Yaz/YrWCfRdUQVc359F0t5NJSufZtkXCkCUOTd5bD9WtN8VfuN3Kk07of3DGDrwrHhIoxMErp3Ic4HBKWEAnOdjvp1Fe6dod2QQcSx0ltafR+zIAtG+KEEAawWQKB3v88sP/QAEd/Y1n98+o7WI7Tddv4F5rhDAI/W5K2QndmldwnY/FTMNFbDyKRIszpigAALpqVku0N0HQEXgFwtmBvnhzBCduJc+0EnUk5QGF4CQn3jh5QXgiExj3gUgEuiPPH4/T+tvtR7Jv6cyy28M2IF3OWwgYKPHJQxgvQgijvT2skIvAIM4/5P57+EMeR/cwBMiJbifD+dAgPPPEsPoZpcl2JYm4QPbun67PhuA4PUGB7jIaaL9aob9vkGywUlMlzGX25LhmCoW9ZCuWq9K2Pr2qvS06Gddv8EXnpQJwCsDXkTmhhuz7IsHso4BBvK61kGuCCufPnLIQ/x7TsLWB/mnaHfx+H2CH4DwVxcEvEh3US+uC7A/6uUXBZMDHzORQ1oSsj8TpRsm0Meq5GWHaHtRZ5cI394KwGFMUZShFW4IGBzaMcWZJ6zqKfrTOUyaZ4hycUEOwGtwAfgFnzRuqBeAIwwvNkBY03qD467h/mUi57qJFtfEbfMTLNvgc69Qaa5xivitq88xI70AHG544WGi/ZzhsbA6LAqaK9idZsGElCYEHghizF8/JMpSKVhSss0n120FYKk7ugSQIaLiWB7i5lE/Xy5SIkew8uAcRvrfaD1oCbwHmCI1ZdhnO9mmXWIb+MkH3QCWKXN6v0ilGWkECFNKfz+H7cvCKkeImvOvTKA/x2pmXUwMD+QtFZxB36NaWOqFHKVghe52A9g35I04vuAQiYSmgMe9wfRmvQBvpdY/EbixKk3S7mU9PYjR//tan1HROMcFhsejJh9PV9UsRtB/AOwV8kYGunLIIkauG1z5lJQnCcbr/PsUgtdREAd/YDScolovLNpJv4kh3oEjAPM0v2dC3xDwvleG7C9A+zqzibkfEwdcH7hMfXLhYjZZTorJb+iijlwkeLwGRtv54imOZM1cnOE6qApWE6QKWqSfpAg4EuBOfChHClqqjqMF5zJdpHQJS0zngT6t9VwHwCoWy0FlA51rtmHQhg8HN/s9wd7A8m8hw1FocN3D9IXLCSpcQI1KRkASf1v8jSVzfY4QqYNJJyYE9CEf0eQPunKrh5U/35hJClhLny7u5TXVMo2wjW7hLUbNPTFGcFj2l13bPk61HABNJqvX8IaDykHR+avIDca1PKOAAehkn99rWSXV8z5KCAb+7mngM8/xKOvaSgBNFuysDtHRExilk2SVZb9KPaxnZsBUBsZ1p8f2YgngWEbGfoyiXTPkhbtDdKR3DsDLJkiHDtAP++WXsNjHfbKUlATwUaoM152IvjMPXMMhsS/EzXZQ+Sm3af2qauEtP3QRBmCmpvpQWo578F2s2BTS0vykn8pfOVqAN4pJ+wkBsoMGCeBanqSKVUI1860Gmjks8RihSETfCXiDxcyh8lXmivaNKvgCpgMSwFpGmqCMDALC+QFr03LVejI+n6RapRdE9VVmC+R3yVKu2vDCYJbvCWDmZzOBzle5XaVnFU3v800JYFWIi19HFuYsV8ROqTTl/YKLCson2S1oqS4h3MxWCeArIW9iIEs1cHgvUVEB3MHfPyDnlo9SLhLpycr8FY1XJZlQyvQkrhdvGulTttEiV4Qs3WwJSr4+vM9uvM+jDMtZpDr7HcD2q8zzu6YiF+TgCU1y5Vm5lptVeiJspiF4kM0OCyUt7tmYbxIRfZx44tPyBLwlKr1oCTOLF4c4xwpZjDuy1MLN3qnSLwrexaCSS6lj8HP6Pk+Fe4NzqReAKxkA4hT4iftFdTNOBVsbY0smi5TtavXJd16CyB4lGO0CV/n2Rws3jZzxUrZ3qhZKKxdfulhKi4Mcp8ItuVPEqMkLQMgvLd08Fgv1FMTFHQmDh7L0Cj64FPtZEvJcrTByA4gZsUoLHUA9jHkIZ0nHrZZ8rh8x8h1BjmDonhvyXJXEyBdAx/HbkNNUetkaOoX5he0JADhdRE1wknMiBkWVDUCM8S2WOjNepV+lAr84Wpl9A8FUMD3qvGEFFmmxCr9cbqtXjCjwybJnWOzUPSo9hVrF4VRv4TovC78H0uPXKtq7dtOVx/do/Eq3ChIFNqSQgaSv6OiFynutdVhBpTBSPBiwLudFON8aYqKCAtjMhLPREogYTlgbeCL/foo+8VAM54ZfPVulJ+CxnC7Km6TA4Fq/1CsTeYBoM9viUO5KJseZc3gsBkvcQdLCYcsxWXZvxPucTYtWpgAq+sJNFkHswQjpLObGqv5RPj4RoKyn3/QiJjDBPkRUGhiyjyizlQ9u2aQivCunaA3IoWotgogpw+dVejXoM7SivRw2WCOIRZCdWXphEh0T85erllUIiv56iMj1RtDPRvkMSx37nnFEBP342BimACmLQO4kg/N3YZ39spSXABLfycI7eQ45egFBjwJeM89TkW3HoARqRQJ0VDeyNWUiGGSrzd/jEHPAm8iKJ+oHgGYEAc8EQMXie75lEDF7t0qZr9fGyJhF5qcw4j2gjz8OurMphY/UZqFlENsq82nQFKucqPKwMnyx0RTAw8zu51kGsTLEfUUla8GyXKkMqbYwk0jNfErTlD1eL8z6m7BkMPpwG/1nk+nBUWbhyhmp6iwl2abSOWSqcjGDRihjiDqNiUiFN5Y2xgxgf8P9MZFv+t1C3PMgMjahJY55YMypDmYUjFo7ryY780iIYThNBVtR1sh7HazCrciIHUCnYgHLDNJ0reGxKMsWsUNYuP50iOEEUO5j8n2r8ucY8YBO5T6xsD9xf0N1M4cSKpetWSwGNNb19HeXuSLvcBINk7NUP3iNFsvTHJ6vntZ1PCsUp6rZQn89TMVMFtv8jjQeDpbAYdXTGXzisM6lLAvdH6woJPC3uHxgJQPWc6LiwDdtLiVImG8BDfYQ96t2nXMgH5aVj3Mn9SHu3uxYnY8VXUbN9m1ADM0i5U/LHySI5SohSepT8K95bINV4g31Mw1ICq/XI5o4LEGLLVEJr37I5X9GgI6OZCpxEq0UhAImvTuRECghQO/TeqFvc/jjtVPwgxss5aJ5DyCkhsFimfovlX8LMAANCHqMVuHBbgAAAABJRU5ErkJggg==" style="height: 40px; width: 40px; margin: 0 0 5px 0;"/><br/><b>TRUST NETWORK</b><br/><i id="rnsStatusText" style="font-weight: 200; font-size: .8em; letter-spacing: 1px; line-height: 16px; color: #33ffcc; -webkit-transition: all .2s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: all .2s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: all .2s cubic-bezier(0.785, 0.135, 0.150, 0.860); opacity: 1;">{0}</i>',
      headerBarStyle: 'position: absolute; top: 0; left: 0; height: 40px; width: 100%; padding: 0; margin: 0; background-color: #000; display: {0}',
      headerBarContents: '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC6NJREFUeNrsXQ2UFlUZvt+uohtL7kKQCCkSFCiEygKSQhRHMxEoVMwO/qAcUfSUqSkGBNZSJKL4EygdE7HfI62KpQgoCKgrEKHAiglJbogCCeuuuyK7bPdxn+l7d5z5vrkzc+f7OvWe8x7uzjd/95n3vj/PvTOk+j/QrP4v4eWIHF+/rdYBWsu0flFrN61dtLbX2k7rp7Ru19qT+4/RerXWf2jdpnWT1kqtH/yvAFiodbDW0VrP0tqH2zJJD62lWvdrfYfHSWnUulHrWq1Pal2t9XBSHUolNIQB1EStF2ntGOL4M7W+oLWD1n1Z9t2t9VGti7T+xXbHCmw+HK3n0zI2a70uJHiOFUL+pbUmy76dtX5X6watz2sdZbOfBZaA+yb902KtZ8RwzuNE+z2D44ZqfULrVj7MVL4D2FvrKq2Paf1SjOftJNrvhzi+Fx/mGq2n5iOAbbSW0+qGWrDqo0S7NsJ5MBrWaZ0RVwCNA8AedPBTCKQNOVq0ayKeC8BN1/oiU6ecAtiLka7McrD70ELqNYCBZlQuAXyDSa1tkX6vXYznLaa//kGuAGzSekMCAMpKo72FQHq71l+EidKmAOICA13bVmh93DKA1aJ9vKVrTAoDoimAs7V+y2P7tSy1bMkO/nss62Nbco3Wu20BOFnrja5tQ/jv2xaHMmrNLWx3T8BdTLIBIKzupx4pBVKXz7O9kA45bvmbsO4BCREepXECeKLWXwnfUOz6/UHBqIxnZI5TXhLt0xMiWdrHBWAbMhslYluJK6n9Ci3R+ftCrfUxdmZVDgB0/GwHVxVkDOCPtPZ3bTtGtPeJ/Yaz/YrWCfRdUQVc359F0t5NJSufZtkXCkCUOTd5bD9WtN8VfuN3Kk07of3DGDrwrHhIoxMErp3Ic4HBKWEAnOdjvp1Fe6dod2QQcSx0ltafR+zIAtG+KEEAawWQKB3v88sP/QAEd/Y1n98+o7WI7Tddv4F5rhDAI/W5K2QndmldwnY/FTMNFbDyKRIszpigAALpqVku0N0HQEXgFwtmBvnhzBCduJc+0EnUk5QGF4CQn3jh5QXgiExj3gUgEuiPPH4/T+tvtR7Jv6cyy28M2IF3OWwgYKPHJQxgvQgijvT2skIvAIM4/5P57+EMeR/cwBMiJbifD+dAgPPPEsPoZpcl2JYm4QPbun67PhuA4PUGB7jIaaL9aob9vkGywUlMlzGX25LhmCoW9ZCuWq9K2Pr2qvS06Gddv8EXnpQJwCsDXkTmhhuz7IsHso4BBvK61kGuCCufPnLIQ/x7TsLWB/mnaHfx+H2CH4DwVxcEvEh3US+uC7A/6uUXBZMDHzORQ1oSsj8TpRsm0Meq5GWHaHtRZ5cI394KwGFMUZShFW4IGBzaMcWZJ6zqKfrTOUyaZ4hycUEOwGtwAfgFnzRuqBeAIwwvNkBY03qD467h/mUi57qJFtfEbfMTLNvgc69Qaa5xivitq88xI70AHG544WGi/ZzhsbA6LAqaK9idZsGElCYEHghizF8/JMpSKVhSss0n120FYKk7ugSQIaLiWB7i5lE/Xy5SIkew8uAcRvrfaD1oCbwHmCI1ZdhnO9mmXWIb+MkH3QCWKXN6v0ilGWkECFNKfz+H7cvCKkeImvOvTKA/x2pmXUwMD+QtFZxB36NaWOqFHKVghe52A9g35I04vuAQiYSmgMe9wfRmvQBvpdY/EbixKk3S7mU9PYjR//tan1HROMcFhsejJh9PV9UsRtB/AOwV8kYGunLIIkauG1z5lJQnCcbr/PsUgtdREAd/YDScolovLNpJv4kh3oEjAPM0v2dC3xDwvleG7C9A+zqzibkfEwdcH7hMfXLhYjZZTorJb+iijlwkeLwGRtv54imOZM1cnOE6qApWE6QKWqSfpAg4EuBOfChHClqqjqMF5zJdpHQJS0zngT6t9VwHwCoWy0FlA51rtmHQhg8HN/s9wd7A8m8hw1FocN3D9IXLCSpcQI1KRkASf1v8jSVzfY4QqYNJJyYE9CEf0eQPunKrh5U/35hJClhLny7u5TXVMo2wjW7hLUbNPTFGcFj2l13bPk61HABNJqvX8IaDykHR+avIDca1PKOAAehkn99rWSXV8z5KCAb+7mngM8/xKOvaSgBNFuysDtHRExilk2SVZb9KPaxnZsBUBsZ1p8f2YgngWEbGfoyiXTPkhbtDdKR3DsDLJkiHDtAP++WXsNjHfbKUlATwUaoM152IvjMPXMMhsS/EzXZQ+Sm3af2qauEtP3QRBmCmpvpQWo578F2s2BTS0vykn8pfOVqAN4pJ+wkBsoMGCeBanqSKVUI1860Gmjks8RihSETfCXiDxcyh8lXmivaNKvgCpgMSwFpGmqCMDALC+QFr03LVejI+n6RapRdE9VVmC+R3yVKu2vDCYJbvCWDmZzOBzle5XaVnFU3v800JYFWIi19HFuYsV8ROqTTl/YKLCson2S1oqS4h3MxWCeArIW9iIEs1cHgvUVEB3MHfPyDnlo9SLhLpycr8FY1XJZlQyvQkrhdvGulTttEiV4Qs3WwJSr4+vM9uvM+jDMtZpDr7HcD2q8zzu6YiF+TgCU1y5Vm5lptVeiJspiF4kM0OCyUt7tmYbxIRfZx44tPyBLwlKr1oCTOLF4c4xwpZjDuy1MLN3qnSLwrexaCSS6lj8HP6Pk+Fe4NzqReAKxkA4hT4iftFdTNOBVsbY0smi5TtavXJd16CyB4lGO0CV/n2Rws3jZzxUrZ3qhZKKxdfulhKi4Mcp8ItuVPEqMkLQMgvLd08Fgv1FMTFHQmDh7L0Cj64FPtZEvJcrTByA4gZsUoLHUA9jHkIZ0nHrZZ8rh8x8h1BjmDonhvyXJXEyBdAx/HbkNNUetkaOoX5he0JADhdRE1wknMiBkWVDUCM8S2WOjNepV+lAr84Wpl9A8FUMD3qvGEFFmmxCr9cbqtXjCjwybJnWOzUPSo9hVrF4VRv4TovC78H0uPXKtq7dtOVx/do/Eq3ChIFNqSQgaSv6OiFynutdVhBpTBSPBiwLudFON8aYqKCAtjMhLPREogYTlgbeCL/foo+8VAM54ZfPVulJ+CxnC7Km6TA4Fq/1CsTeYBoM9viUO5KJseZc3gsBkvcQdLCYcsxWXZvxPucTYtWpgAq+sJNFkHswQjpLObGqv5RPj4RoKyn3/QiJjDBPkRUGhiyjyizlQ9u2aQivCunaA3IoWotgogpw+dVejXoM7SivRw2WCOIRZCdWXphEh0T85erllUIiv56iMj1RtDPRvkMSx37nnFEBP342BimACmLQO4kg/N3YZ39spSXABLfycI7eQ45egFBjwJeM89TkW3HoARqRQJ0VDeyNWUiGGSrzd/jEHPAm8iKJ+oHgGYEAc8EQMXie75lEDF7t0qZr9fGyJhF5qcw4j2gjz8OurMphY/UZqFlENsq82nQFKucqPKwMnyx0RTAw8zu51kGsTLEfUUla8GyXKkMqbYwk0jNfErTlD1eL8z6m7BkMPpwG/1nk+nBUWbhyhmp6iwl2abSOWSqcjGDRihjiDqNiUiFN5Y2xgxgf8P9MZFv+t1C3PMgMjahJY55YMypDmYUjFo7ryY780iIYThNBVtR1sh7HazCrciIHUCnYgHLDNJ0reGxKMsWsUNYuP50iOEEUO5j8n2r8ucY8YBO5T6xsD9xf0N1M4cSKpetWSwGNNb19HeXuSLvcBINk7NUP3iNFsvTHJ6vntZ1PCsUp6rZQn89TMVMFtv8jjQeDpbAYdXTGXzisM6lLAvdH6woJPC3uHxgJQPWc6LiwDdtLiVImG8BDfYQ96t2nXMgH5aVj3Mn9SHu3uxYnY8VXUbN9m1ADM0i5U/LHySI5SohSepT8K95bINV4g31Mw1ICq/XI5o4LEGLLVEJr37I5X9GgI6OZCpxEq0UhAImvTuRECghQO/TeqFvc/jjtVPwgxss5aJ5DyCkhsFimfovlX8LMAANCHqMVuHBbgAAAABJRU5ErkJggg==" style="height: 32px; width: 32px; margin: 4px 5px 4px 5px;"/><span style="position:absolute; top: 0, left: 0, right:0, bottom: 0; margin-top: 10px; overflow: hidden; text-overflow: ellipsis; color: rgba(255,255,255,.7); font-family: \"HelveticaNeue-Light\", \"Helvetica Neue Light\", \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif; font-size: 12px;"><b>TRUST NETWORK</b></span><input style="position: relative; float: right; margin-top: 10px" type="button" id="rnsLogout" value="Logout"/><span style="position: relative; float:right; top: 0; left: 0; bottom: 0; right: -1; padding: 0 10px 0 0; margin-top: 10px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; color: rgba(255,255,255,.7); font-family: \"HelveticaNeue-Light\", \"Helvetica Neue Light\", \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif; font-size: 12px;">{0}</span>',
      scale: 0, 
      maskStyle: 'z-index: 99999; position: fixed; top: 0; left: 0; right: 0; bottom: 0; -webkit-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); background-color: {1}; opacity: {2}; display: {3}; cursor: pointer',
      headerHTML: '<div style="float: right; width: 25%; overflow: hidden; text-align: right; height: inherit; position: relative;"><span style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; padding: 0 10px 0 0; margin: 0 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; color: rgba(255,255,255,.7);">{0}</span></div><div style="float: left; width: 25%; overflow: hidden; height: inherit; position: relative;"><span style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; padding: 0 0 0 10px; margin: 0 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; color: rgba(255,255,255,.7);">{1}</span></div><div style="overflow: hidden; text-align: center; font-weight: 100; letter-spacing: 1px; font-size: 2em; width: 50%; height: inherit; position: relative;"><span id="{3}" style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; padding: 0 5px 0 5px; margin: 0 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{2}</span></div>',
      footerStyle: '-webkit-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); background-color: {4}; position: absolute; left: 0; bottom: 0; right: 0; height: {1}px; opacity: {2}; -webkit-border-bottom-right-radius: {3}px; -webkit-border-bottom-left-radius: {3}px; -moz-border-radius-bottomright: {3}px; -moz-border-radius-bottomleft: {3}px; border-bottom-right-radius: {3}px; border-bottom-left-radius: {3}px;',
      style: 'z-index: 100000; -webkit-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); padding: 0; -moz-box-shadow: 0px 0px {8}px 0px {9}; -webkit-box-shadow: 0px 0px {8}px 0px {9}; box-shadow: 0px 0px {8}px 0px {9}; width: {1}px; height: {2}px; overflow: hidden; background: {7}; position: fixed; top: {10}; left: {11}; margin: -{3}px 0 0 -{4}px; border-radius: {5}px; opacity: 1; -webkit-transform: scale({6}); -moz-transform: scale({6}); -ms-transform: scale({6}); -o-transform: scale({6}); transform: scale({6}); cursor: {12}; border: {13}px {14} {15}',
      spinnerStyle: 'position: absolute; top: 4px; left: 0; height:180px; width:180px; margin:0 auto; position:relative;',
      contentStyle: '-webkit-transition: opacity {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: opacity {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: opacity {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); position: absolute; top: {1}px; left: {2}px; height: {3}px; width: {4}px; display: table; font-size: 1em; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; opacity: {5}; color: {6}; overflow: scroll;',
      contentInnerStyle: 'display: table-cell; vertical-align: middle; text-align: center; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: moz-none; -ms-user-select: none; user-select: none;',
      headerStyle: 'position: absolute; top: 0; left: 0; right: 0; -webkit-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -moz-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); -o-transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); transition: all {0}s cubic-bezier(0.785, 0.135, 0.150, 0.860); height: {1}px; line-height: {1}px; width: 100%; background: {4}; color: white; font-weight: 900; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 10px; opacity: {2}; -webkit-border-top-left-radius: {3}px; -webkit-border-top-right-radius: {3}px; -moz-border-radius-topleft: {3}px; -moz-border-radius-topright: {3}px; border-top-left-radius: {3}px; border-top-right-radius: {3}px;',
      footerFormButtons: '<div style="float: left; position: relative; width: 50%; height: 100%; padding: 0; margin: 0; color: #fff;">&nbsp;</div><div style="float: left; position: relative; height: 100%; width: 50%; padding: 0; margin: 0; color: #fff;"><input id="{0}" style="" type="submit" class="disabled" value="Continue" style="height: 40px; width: 200px;"></div>'
    };
    
    // apply user settings
    if (customSettings) {
      for (var key in customSettings) { 
        if (this.settingsDefaults.hasOwnProperty(key)) { 
          this.settingsDefaults[key] = customSettings[key]; 
        } 
      }
    }
    
    this.settings = {};
    
    this.setDefaults();
    
    this.setContentDimensionsByModal();
    
    // create modal element container
    var self = this, maskElement = document.createElement('div'), modalElement = document.createElement('div');
    
    maskElement.id = this.settings.maskId;
    maskElement.setAttribute('style', this.settings.maskStyle.format(
      this.settings.transitionTime,
      this.settings.maskBg,
      this.settings.maskOpacity,
      this.settings.maskDisplay
    ));
    
    // set modal id
    modalElement.id = this.settings.id;
    
    // style modal
    modalElement.setAttribute('style', 
      this.settings.style.format(
        this.settings.transitionTime,
        this.settings.width, 
        this.settings.height, 
        (this.settings.height/2), 
        (this.settings.width/2),
        (this.settings.height/2),
        this.settings.opacity,
        this.settings.bg,
        this.settings.shadowBlur,
        this.settings.shadowColor,
        this.settings.top,
        this.settings.left,
        this.settings.cursor,
        this.settings.borderWidth,
        this.settings.borderType,
        this.settings.borderColor
      )
    );
    
    // create inner modal spinner
    var modalSpinner = document.createElement('div');
    modalSpinner.id = this.settings.spinnerId;
    modalSpinner.setAttribute('style', this.settings.spinnerStyle);
    //modalSpinner.className = 'rns-spinner active';
    
    var modalHeader = document.createElement('div');
    modalHeader.id = this.settings.headerId;
    modalHeader.setAttribute('style',this.settings.headerStyle.format(this.settings.transitionTime, this.settings.headerHeight, this.settings.headerOpacity, this.settings.borderRadius, this.settings.headerBg));
    modalHeader.innerHTML = this.settings.headerHTML.format('ADMIN', 'TRUST NETWORK', 'Credential Identity', this.settings.headerTitleId);
    
    var modalFooter = document.createElement('div');
    modalFooter.id = this.settings.footerId;
    modalFooter.setAttribute('style', this.settings.footerStyle.format(this.settings.transitionTime, this.settings.footerHeight, this.settings.footerOpacity, this.settings.borderRadius, this.settings.footerBg));
    modalFooter.innerHTML = this.settings.footerFormButtons.format(this.settings.formSubmitButtonId);
    
    var headerBar = document.createElement('div');
    headerBar.id = this.settings.headerBarId;
    headerBar.setAttribute('style', this.settings.headerBarStyle.format('none'));
    headerBar.innerHTML = this.settings.headerBarContents.format('');
    
    // create inner modal content container
    var modalContent = document.createElement('div');
    modalContent.id = this.settings.contentId;
    modalContent.setAttribute('style', 
      this.settings.contentStyle.format(
        this.settings.transitionTime, // transition time
        this.settings.contentTop, // top
        this.settings.contentLeft, //left position
        this.settings.contentHeight, // height
        this.settings.contentWidth, // width
        this.settings.contentOpacity, // opacity
        this.settings.contentColor // color
      )
    );
    
    // create inner content paragraph 
    var modalContentInner = document.createElement('div');
    modalContentInner.id = this.settings.contentInnerId;
    modalContentInner.setAttribute('style', this.settings.contentInnerStyle.format())
    modalContentInner.innerHTML = this.settings.statusHTML.format('PROTECTED');
    
    modalContent.appendChild(modalContentInner);
    modalElement.appendChild(modalHeader);
    modalElement.appendChild(modalFooter);
    modalElement.appendChild(modalSpinner);
    modalElement.appendChild(modalContent);
    document.body.appendChild(maskElement);
    document.body.appendChild(modalElement);
    document.body.appendChild(headerBar);
    
    setTimeout(function () {
      
      self.dom.element = document.getElementById(self.settings.id);
      self.dom.spinnerElement = document.getElementById(self.settings.spinnerId);
      self.dom.contentElement = document.getElementById(self.settings.contentId);
      self.dom.contentInnerElement = document.getElementById(self.settings.contentInnerId);
      self.dom.headerElement = document.getElementById(self.settings.headerId);
      self.dom.footerElement = document.getElementById(self.settings.footerId);
      self.dom.maskElement = document.getElementById(self.settings.maskId);
      self.dom.titleElement = document.getElementById(self.settings.headerTitleId);
      self.dom.formSubmitButtonElement = document.getElementById(self.settings.formSubmitButtonId);
      
    }, 1);
    
  };
  
  RTNModal.prototype.setDefaults = function () {

    for ( var key in this.settingsDefaults ) {
      if (this.settingsDefaults.hasOwnProperty(key)) {
        this.settings[key] = this.settingsDefaults[key];
      }
    }
    
  };
  
  RTNModal.prototype.setStatusTo = function (type, status) {
  
    var self = this;
    
    if (!this.dom.statusTextElement) {
      
      this.dom.contentInnerElement.innerHTML = this.settings.statusHTML.format(status);
    
      delete this.dom.statusTextElement;
      
      setTimeout(function () {
        
        self.dom.statusTextElement = document.getElementById('rnsStatusText');
        
      }, 1);
    
    } else if (this.dom.statusTextElement.parentNode) {
        
      this.setStatusTextTo(type, status);
      
    } 
    
  };
  
  RTNModal.prototype.setStatusTextTo = function (type, status) {
    
    var self = this, color;
    
    if (this.dom.statusTextElement && this.dom.statusTextElement.parentNode) {
    
      if (this.dom.statusTextElement.style.opacity==1) {
      
        this.dom.statusTextElement.style.opacity = 0;
        
        setTimeout(function () {
        
          switch (type) {
            case 'success':
              color = self.settings.statusColorSuccess;
              break;
            case 'failure':
              color = self.settings.statusColorFailure;
          }
        
          self.dom.statusTextElement.style.color = color;
          self.dom.statusTextElement.innerHTML = status;
          self.dom.statusTextElement.style.opacity = 1;
        
        }, 200);
        
      }
    
    }
    
  };

  RTNModal.prototype.showMask = function (callback) {
    
    var self = this;
      
    this.settings.maskDisplay = 'block';
    this.setMaskStyle();
  
    setTimeout(function () {
    
      self.settings.maskOpacity = 0.7;
      
      self.setMaskStyle();
      
      self.handleCallbackWithTransitionTime(callback);
      
    }, 1);
    
  };
  
  RTNModal.prototype.hideMask = function () {
    
    var self = this;
    
    this.settings.maskOpacity = 0.0;
    
    this.setMaskStyle();
    
    this.setDefaults();
    
    setTimeout(function () {
    
      self.settings.maskDisplay = 'none';
      
      self.setMaskStyle();
      
    }, (1000*this.settings.transitionTime) );
    
  };
  
  RTNModal.prototype.setMaskStyle = function () {
    
    this.dom.maskElement.setAttribute('style', this.settings.maskStyle.format(
      this.settings.transitionTime,
      this.settings.maskBg,
      this.settings.maskOpacity,
      this.settings.maskDisplay
    ));
    
  };
  
  RTNModal.prototype.show = function (useMask, callback) {
    
    var self = this;
    
    this.settings.opacity = 1;
    this.settings.scale = 1;
    if (useMask) {
          this.showMask(function () {
        self.update();
      });
    } else {
      this.update();
    }
    this.visible = true;
    
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.hide = function (callback) {
    
    var self = this;
    
    this.setDefaults();
    this.hideMask();
    this.update();
    this.visible = false;
    
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.setContentDimensionsByModal = function (callback) {
    
    var subHeight = 0;
    
    if ( this.settings.footerOpacity > 0 ) {
      subHeight += this.settings.footerHeight;
    }
    
    if ( this.settings.headerOpacity > 0 ) {
      subHeight += this.settings.headerHeight;
      this.settings.contentTop = ( this.settings.padding + this.settings.headerHeight );
    } else {
      this.settings.contentTop = this.settings.padding;
    }
    
    subHeight += ( this.settings.padding * 2 );
    
    this.settings.contentHeight = ( this.settings.height - subHeight );

    this.settings.contentLeft = this.settings.padding;
    this.settings.contentWidth = ( this.settings.width - ( this.settings.padding * 2 ) );
    
    this.handleCallback(callback);
    
  };
  
  RTNModal.prototype.setTitleText = function (text) {
    
    this.dom.titleElement.innerHTML = text;
    
  };
  
  RTNModal.prototype.update = function (callback) {
    
    var self = this;
    
    this.setContentDimensionsByModal();
    this.updateModalHeaderStyle();
    this.updateModalFooterStyle();
    this.updateModalContainerStyle(function () {
      self.updateModalContentStyle();
    });
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.updateModalHeaderStyle = function (callback) {
    
    var radius;
    
    if ( this.settings.borderWidth > 0 ) {
      radius = 0;
    } else {
      radius = this.settings.borderRadius;
    }
    
    this.dom.headerElement.setAttribute('style', 
      this.settings.headerStyle.format(
        this.settings.transitionTime,
        this.settings.headerHeight,
        this.settings.headerOpacity,
        radius,
        this.settings.headerBg
      )
    );
    
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.updateModalFooterStyle = function (callback) {
    
    var radius;
    
    if ( this.settings.borderWidth > 0 ) {
      radius = 0;
    } else {
      radius = this.settings.borderRadius;
    }
    
    this.dom.footerElement.setAttribute('style', 
      this.settings.footerStyle.format(
        this.settings.transitionTime,
        this.settings.footerHeight,
        this.settings.footerOpacity,
        radius,
        this.settings.footerBg
      )
    );
    
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.updateModalContainerStyle = function (callback) {
  
    var topMargin, leftMargin;
    
    if ( this.settings.borderWidth > 0 ) {
      topMargin = ( ( this.settings.height / 2 ) + this.settings.borderWidth );
      leftMargin = ( ( this.settings.width / 2 ) + this.settings.borderWidth );
    } else {
      topMargin = ( this.settings.height / 2 );
      leftMargin = ( this.settings.width / 2 );
    }
    
    this.dom.element.setAttribute('style', 
      this.settings.style.format(
        this.settings.transitionTime, // transition time
        this.settings.width, // width
        this.settings.height, // height
        topMargin, // negative top margin
        leftMargin, // negative left margin
        this.settings.borderRadius, // border radius
        this.settings.opacity, // opacity
        this.settings.bg, // background
        this.settings.shadowBlur, // blur amount
        this.settings.shadowColor,
        this.settings.top,
        this.settings.left,
        this.settings.cursor,
        this.settings.borderWidth,
        this.settings.borderType,
        this.settings.borderColor
      )
    );
    
    this.handleCallbackWithTransitionTime(callback);
    
  };
  
  RTNModal.prototype.updateModalContentStyle = function (callback) {
    
    this.dom.contentElement.setAttribute('style', 
      this.settings.contentStyle.format(
        this.settings.transitionTime, // transition time
        this.settings.contentTop, // top
        this.settings.contentLeft, //left position
        this.settings.contentHeight, // height
        this.settings.contentWidth, // width
        this.settings.contentOpacity, // opacity
        this.settings.contentColor // color
      )
    );
    
    this.handleCallbackWithTransitionTime(callback);
    
  };

  RTNModal.prototype.showSpinner = function () {
  
    this.dom.spinnerElement.className = 'rns-spinner active';
    
  };
  
  RTNModal.prototype.hideSpinner = function () {
  
    this.dom.spinnerElement.className = '';
    
  };
  
  RTNModal.prototype.showContent = function (callback) {
    
    this.settings.contentOpacity = 1;
    this.updateModalContentStyle(callback);
    
  };
  
  RTNModal.prototype.hideContent = function (callback) {
    
    this.settings.contentOpacity = 0;
    this.updateModalContentStyle(callback);
    
  };
  
  RTNModal.prototype.changeContentTo = function (text, callback) {
    
    this.dom.contentInnerElement.innerHTML = text;
    
    // always remove this to ensure it's updated via setStatusTo/Text
    delete this.dom.statusTextElement;
    
    this.handleCallback(callback);
    
  };
  
  RTNModal.prototype.hideHeader = function (callback) {
    
    this.settings.headerOpacity = 0;
    this.updateModalHeaderStyle(callback);
    
  };
  
  RTNModal.prototype.showHeader = function (callback) {
    
    this.settings.headerOpacity = 1;
    this.updateModalHeaderStyle(callback);
    
  };
  
  RTNModal.prototype.showFooter = function (callback) {
    
    this.settings.footerOpacity = 1;
    this.updateModalFooterStyle(callback);
    
  };
  
  RTNModal.prototype.hideFooter = function (callback) {
    
    this.settings.footerOpacity = 0;
    this.updateModalFooterStyle(callback);
    
  };
  
  RTNModal.prototype.showHeaderBar = function (parameters) {
	  var headerBar = document.getElementById(this.settings.headerBarId);
	  headerBar.innerHTML = this.settings.headerBarContents.format(parameters);
	  headerBar.setAttribute('style', this.settings.headerBarStyle.format('block'));
  };
  
  RTNModal.prototype.hideHeaderBar = function () {
	  var headerBar = document.getElementById(this.settings.headerBarId);
	  headerBar.innerHTML = this.settings.headerBarContents.format('');
	  headerBar.setAttribute('style', this.settings.headerBarStyle.format('block'));
  };

  RTNModal.prototype.handleCallbackWithTransitionTime = function (callback) {
    
    if (callback) {
      setTimeout(function () {
        callback.call(this);
      }, ( 1000 * this.settings.transitionTime ) );
    }
    
  };
  
  RTNModal.prototype.handleCallback = function (callback) {
    
    if (callback && this.visible) {
      setTimeout(function () {
        callback.call(this);
      }, 1);
    }
    
  };
  
  RTNModal.prototype.addEvents = function () {
  
    var self = this; 
    
    // prevent body scrolling when modal is visible
    document.addEventListener('mousewheel', function (e) {
      
      self.handleBodyScroll(e);
      
    });
    
    document.addEventListener('scroll', function (e) {
      
      self.handleBodyScroll(e);
      
    });
    
  };
  
  RTNModal.prototype.handleSearchResultClick = function (item) {
    
    console.log(item);
    
  };
  
  RTNModal.prototype.handleBodyScroll = function (e) {
    
    if ( this.visible ) {
      
      e.preventDefault();
      
    }
    
  };
  
  RTNModal.prototype.handleLogin = function () {
  
    console.log('handleLogin()');
    
  };

  return RTNModal;
  
})();