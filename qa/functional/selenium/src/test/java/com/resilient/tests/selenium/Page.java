package com.resilient.tests.selenium;


import org.openqa.selenium.WebDriver;



public class Page {
	
	
	// no need to put a locator for this thing, this has nothing to do with anything on a page, it is the entirety of the page
	protected WebDriver driver;
	
	public Page(WebDriver driver){
		this.driver=driver;
	}
	
	
	// now that we have the driver set inside this object, why do we need the browser thing?
	// i don't think we need it, so lets comment this out, or hell lets overload the damn thing
    public void verify(FFBrowser browser) {
    	String pg = (browser != null && browser.getCurrentUrl() != null) ? browser.getCurrentUrl() : "null";
    	//System.out.println(">   - inside Page.verify(Browser browser): currentUrl=["+pg+"]");
  
    }
    
  //  public void verify(IEBrowser browser) {
  //  	String pg = (browser != null && browser.getCurrentUrl() != null) ? browser.getCurrentUrl() : "null";
    	//System.out.println(">   - inside Page.verify(Browser browser): currentUrl=["+pg+"]");
    	// i like the idea of having a little shout out of where we are (address bar wise)
    	// more simply, we would show the damn url of this page.
  //  }
    
    public void verify() {
    	String pg = (this.driver != null) ? this.driver.getCurrentUrl() : "null";
    	//System.out.println(">   - inside Page.verify(): currentUrl=["+pg+"]");
    }
    
    public WebDriver getDriver() {
    	return driver;
    }
   
}

