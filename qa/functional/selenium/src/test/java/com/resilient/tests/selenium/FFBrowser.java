package com.resilient.tests.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FFBrowser {

    private WebDriver driver;
    private String serverPort;
    private String hostName;

    public FFBrowser(String hostName, String serverPort) {
        this.driver = new FirefoxDriver();
        this.serverPort = serverPort;
        this.hostName = hostName;
    }
    
    public void getFromOutside(String completeUrl) {
    	driver.get(completeUrl);
    }

    public void get(String url) {
    	String totalUrl = this.hostName+":"+serverPort+"/"+url;
    	//System.out.println(">   - going to try and do driver.get() with url: -->"+totalUrl+"<--");
        driver.get(totalUrl);
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public WebElement findElement(By selector) {
        return driver.findElement(selector);
    }

    public List<WebElement> findElements(By selector) {
        return driver.findElements(selector);
    }

    public WebElement waitForElement(final By selector) {
    	
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        
        return wait.until(new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver webDriver) {
                return driver.findElement(selector);
            }
        });
    }

    public String getBodyClass() {
        return waitForElement(By.tagName("body")).getAttribute("class");
    }
    
    
    // this is kind of dumb, but a Browser 'shows' a web page, so that's why it's called this
    public <T extends Page> T shows(Class<T> pageClass) {
        T page = PageFactory.initElements(driver, pageClass);
        page.verify(this);
        return page;
    }

    public void refreshScreen() {
    
    	//System.out.println(">   - going to try and do driver.get() with url: -->"+totalUrl+"<--");
        driver.navigate().refresh();
    }
    public void stop() {
        try {
            driver.quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

