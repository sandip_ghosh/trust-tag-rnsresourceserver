package com.resilient.tests.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.resilient.resourceserver.util.Constants;
import com.resilient.resourceserver.util.Helpers;

/**
 * TestApp Page  
 * Functions associated with TestApp Page --Login
 * 
 * @author ulakumarappan
 *
 */
public class TestAppPage extends Page {
	
	@FindBy(how=How.XPATH, using="//html//body//h1")
    public WebElement getUserHTML;
	
	//fix this might not be right
	@FindBy(how=How.NAME,using="username")
	 private WebElement userName;
	 @FindBy(how=How.CLASS_NAME,using="button")
	 private WebElement submitBtn;

	
	 /**
	  * RNSCredential Page view 
	  * @param driver
	  */
	 public TestAppPage(WebDriver driver) {
			super(driver);
		}
	 
	 public void enterUserName(String sCred){
		 //System.out.println("RSCredentialPage:  Inside addNewCred " + sCred + bSave);
		// 
		// Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		userName.clear();
		userName.sendKeys(sCred);
	//	System.out.println("RSCredentialPage:  Copied credential text in inputbox " + sCred);
		Helpers.rest(Constants.I_DEF_TIMEOUT);
		submitBtn.click();
	 }
}