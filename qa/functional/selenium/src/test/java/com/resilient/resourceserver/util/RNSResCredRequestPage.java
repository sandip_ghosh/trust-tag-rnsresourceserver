package com.resilient.resourceserver.util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.resilient.tests.selenium.Page;

/**
 * RNSCredential Page  
 * */

public class RNSResCredRequestPage extends Page {
	
	@FindBy(how=How.ID_OR_NAME,using="requestCredentials")
	 public WebElement reqCredBtn;
	
	@FindBy(how=How.ID_OR_NAME,using="userid")
	 public WebElement userInputID;
	
	
	public RNSResCredRequestPage(WebDriver driver) {
		super(driver);
	}
	
	public void logUser(String id){
		userInputID.clear();
		userInputID.sendKeys(id);
		Helpers.rest(Constants.I_DEF_TIMEOUT);
		
		reqCredBtn.click();
	}
	

}
