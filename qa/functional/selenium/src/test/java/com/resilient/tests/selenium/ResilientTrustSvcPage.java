package com.resilient.tests.selenium;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.JavascriptExecutor;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import com.resilient.tests.selenium.Page;
import com.resilient.tests.selenium.CriqPage;
import com.resilient.resourceserver.util.Constants;
import com.resilient.resourceserver.util.ResConstants;
import com.resilient.resourceserver.util.Helpers;


public class ResilientTrustSvcPage extends Page {

	@FindBy(how=How.NAME, using="password")
    public WebElement inPass;
	
	@FindBy(how=How.XPATH, using="//html//body//table//tbody//tr[3]//td[2]//input")
	public WebElement signIn;
	
	@FindBy(how=How.XPATH, using="//html//body//h1")
	public WebElement heading1;
	@FindBy(how=How.XPATH, using="//html//body//h2")
	public WebElement heading2;
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Post of")
	public WebElement PostSimpLnk;
	
	CriqPage crPg;
	
	public ResilientTrustSvcPage(WebDriver driver) {
		super(driver);
	}
	
	public void LoginCorrect() {
		 inPass.clear();
		 inPass.sendKeys(ResConstants.SIMPLE_PWD);
		//return PageFactory.initElements(this.driver, ResilientTrustSvcPage.class);
		 signIn.click();
	}
	
	public void LoginInCorrect() {
		 inPass.clear();
		 inPass.sendKeys(ResConstants.SIMPLE_BAD);

		 signIn.click();
	}
	
	public String getHeading(int iHead){
		String sReturn = null;
		if (iHead == 1)
			sReturn = heading1.getText();
		   else 
			   sReturn = heading2.getText();
		return sReturn;
	}

	
	public void iFrameLogin(String pass){
		
		System.out.println("inside iFrameLogin " + pass);
		WebElement theframe = null;
		List<WebElement> frames = driver.findElements(By.tagName("iframe"));
		for(WebElement we : frames) {
			String tagid = we.getAttribute("id");
			//  fxwfr0
			if("tbDisplay".equals(tagid)) {
				theframe = we;
				break;
			}
		}
		
		driver.switchTo().frame(theframe);
		
		List<WebElement> texts = driver.findElements(By.tagName("input"));
		List<WebElement> passwords = new ArrayList<WebElement>();
		for(WebElement we : texts) {
			String ttype = we.getAttribute("type");
			if("password".equals(ttype)) {
				passwords.add(we);
			}
		}
		
		if(passwords.size() == 1) {
			for(WebElement pwtb : passwords) {
				pwtb.click();
				pwtb.clear();
				pwtb.sendKeys(pass);
			}
		} else {
			System.out.println(">   - [problem]: we didn't get password box, password is INCORRECT!");
		}

		try {
			// now click the save button to get out of here
			//WebElement signbtn = driver.findElement(By.name("btnSubmit"));
			signIn.click();
			Helpers.rest(Constants.I_DEF_TIMEOUT+700);
			
		} catch(Exception na) {
			na.printStackTrace();
		}
		
		// done with pop-up window, so get going
		driver.switchTo().defaultContent();
	}
	}
