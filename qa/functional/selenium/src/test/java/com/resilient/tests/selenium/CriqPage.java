package com.resilient.tests.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CriqPage extends Page{
	@FindBy(how=How.XPATH, using="//html//body//h1")
	public WebElement headPageIndex;
	@FindBy(how=How.XPATH, using="//html//body//h2")
	public WebElement headTestApp;
	
	
	public CriqPage(WebDriver driver) {
		super(driver);
	}
	
	public String getIndexHead(){
		return headPageIndex.getText();
	}

	public String getTestAppHead(){
		return headTestApp.getText();
	}
}
