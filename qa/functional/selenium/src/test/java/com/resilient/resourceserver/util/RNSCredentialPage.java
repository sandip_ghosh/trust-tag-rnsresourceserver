package com.resilient.resourceserver.util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.resilient.tests.selenium.Page;

/**
 * RNSCredential Page  
 * Functions associated with Credential Page -- creation, updation, deletion of Credentials
 * Functions to get the attributes of a credential - Name, Parameters, Usecount, Last updated, Granting Authority
 * @author ulakumarappan
 *
 */
public class RNSCredentialPage extends Page {
	
	@FindBy(how=How.ID_OR_NAME,using="new")
	 private WebElement addNewCredBtn;
	 @FindBy(how=How.ID_OR_NAME,using="save")
	 private WebElement saveEdtCredBtn;

	 @FindBy(how=How.ID, using="newCredentialExpr")
	 private WebElement copyCredExp;
	 @FindBy(how=How.ID, using="selCredential")
	 private WebElement editCredExp;
	
	 /**
	  * RNSCredential Page view 
	  * @param driver
	  */
	 public RNSCredentialPage(WebDriver driver) {
			super(driver);
		}

	 /**
	 * Add credential with string passed, and save creation if true
	 * @param sCred  - credential name
	 * @param bSave -- boolean to save
	 */
	 public void addNewCred(String sCred, boolean bSave){
		 //System.out.println("RSCredentialPage:  Inside addNewCred " + sCred + bSave);
		 addNewCredBtn.click();
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		copyCredExp.clear();
		copyCredExp.sendKeys(sCred);
	//	System.out.println("RSCredentialPage:  Copied credential text in inputbox " + sCred);
		Helpers.rest(Constants.I_DEF_TIMEOUT);
		
		if (bSave == true) { 			
			//System.out.println("RSCredentialPage:  Going to Click Add Credential button");
			Helpers.clickButton(this, 4);
			Helpers.rest(Constants.I_DEF_TIMEOUT);

		} else {				
			System.out.println("RSCredentialPage: Going to click Cancel button");
			Helpers.clickButton(this, 3);
			
		}
	}
	 /**
	  * Find and Delete the credential passed in, and save the deletion, if confirmed with true
	  * @param sCred  -- credential string
	  * @param bDelete  -- boolean to save delete
	  */
	 public void delCred(String sCred, boolean bDelete){
		//System.out.println("Inside Delete Credential: " + sCred);
		boolean didFind = Helpers.findCred(this, sCred);
		if ( didFind == true )
			{
				Helpers.confirmDel(this, bDelete);
			}
		else {
			System.out.println("DELCRED:: Did not find the Credential " + sCred + " to Delete");
		}			
}
	 
	/**
	 * Find the credential to be edited first. Then replace with string passed in. Save if set true.
	 * There is NO CANCEL button for EDITS  ONly SAVE
	 * @param sCred  =  stringname for credential to be edited
	 * @param sNewCred  =  string for the changes to be made to the credential
	 * @param bEdit  = boolean to save edit
	 */
	public void edtCred(String sCred, String sNewCred, boolean bEdit){
	//System.out.println(">  - Inside Edit Credential: ");
		String sCredit = Helpers.findCredStr(this,sCred);
//		System.out.println("Make sure this is the credential to edit Back into edtCred method: " + sCredit);	
//		System.out.println("This is the new string to replace: " + sNewCred);

		editCredExp.clear();
		editCredExp.sendKeys(sNewCred);
		System.out.println(">  - Sending Edit Credential: ");

		//System.out.println("Copied new edits - make sure frequency is Edited value " + editCredExp.getText());
		Helpers.rest(Constants.I_DEF_TIMEOUT +200);
	
		if (bEdit == true) { 
			//List<WebElement> allB = driver.findElements(By.tagName("button"));
			//List<WebElement> allB = driver.findElements(By.id("save"));

			saveEdtCredBtn.click();    // Error 404   - Cannot Save at times

			// save via TAB keys
		/*	WebElement element = driver.findElement(By.id("save")); 
			if (element != null){
				System.out.println("ready to tab over to save button");
			element.sendKeys(Keys.TAB); 
			Helpers.rest(Constants.I_DEF_TIMEOUT);
			System.out.println("In save button");
			element.sendKeys(Keys.ENTER);
			Helpers.rest(Constants.I_DEF_TIMEOUT);}
			System.out.println("CR on save button");  
			*/
			// did not save - has OLD VALUE STILL - There is no error in the string
			/*if (driver.findElement(By.id("save")) != null){
				driver.navigate().forward();   // tab to Save Button    // Getting 404 Error
				//driver.findElement(By.id("save")).click();}
				if (saveEdtCredBtn.isSelected()) {saveEdtCredBtn.click(); 	rest(3222);} 
				else {System.out.println("not selected");}
		*/

		}
	}
	
	/**
	 * 
	 * @return int   count of credentials on the page
	 * @throws Exception
	 */
		public int getCount() throws Exception 
		{
			return Helpers.getEntityRowCount(this, "credentials");
		}
	/**
	 * 	
	 * @param rowNum
	 * @return String  name(column 1) of credential table in rowNum passed in
	 * @throws Exception
	 */
		public String getName(int rowNum) throws Exception {
			return Helpers.getEntityFieldValue(this, "credentials", rowNum, 1);
		}
	
	/**
	 * 		 
	 * @param rowNum
	 * @return String granting authority name from column 2 in credential table
	 * @throws Exception
	 */
		public String getGrantingAuthorities(int rowNum) throws Exception {
			return Helpers.getEntityFieldValue(this, "credentials", rowNum, 2);
		}
	
	/**
	* 
	* @param rowNum
	* @return String array of parameters in credential(column 3)
	* @throws Exception
	*/
		public String[] getParameters(int rowNum) throws Exception {
			String paramNames = Helpers.getEntityFieldValue(this, "credentials", rowNum, 3);
			return paramNames.split(",");
		}
		
	/**
	 * 
	 * @param rowNum
	 * @return int  credential usage count (column 4 in credential table)ß
	 * @throws Exception
	 */
		public int getUseCount(int rowNum) throws Exception {
			String useCountStr = Helpers.getEntityFieldValue(this, "credentials", rowNum, 4);
			return Integer.parseInt(useCountStr);
		}
	
	/**
	* 
	* @param rowNum
	* @return String  Date credential was last updated
	* @throws Exception
	*/
		public String getLastUpdateDate(int rowNum) throws Exception {
			return Helpers.getEntityFieldValue(this, "credentials", rowNum, 5);
		}
	
}
