package com.resilient.resourceserver.util;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.JavascriptExecutor;

import com.resilient.tests.selenium.Page;


public class RNSResourcePage extends Page {
	
	@FindBy(how=How.XPATH, using="//input[@id='resourcesTab']")
	protected WebElement resTab;
	

	 @FindBy(how=How.ID_OR_NAME,using="new")
	    public WebElement addNewResBtn;
	 
	 @FindBy(how=How.ID, using="newResource")
	    public WebElement resConfigDialog;
	 
	 // Wizard PAGE 1  Resource Configurations
	 @FindBy(how=How.ID, using="resources")
	    public WebElement resTable;
	 @FindBy(how=How.ID,using="name")
	    protected WebElement appNameInput;
	
	@FindBy(how=How.ID, using="resourceType")
	protected WebElement selResType;

	@FindBy(how=How.ID, using="hostname")
	protected WebElement resHost;
	@FindBy(how=How.ID, using="portStr")
	protected WebElement resPort;
	@FindBy(how=How.ID, using="path")
	protected WebElement resPath;
	@FindBy(how=How.ID, using="useWrapper")  //checkbox
	protected WebElement useWrap;
	
//	WebElement resPort;
//	WebElement resHost;
//	WebElement resPath;
//	 WebElement delResBtn;  // does not work
//	 WebElement canResBtn;
	
	@FindBy(how=How.XPATH, using="//div[@class='ui-dialog-buttonset']")
    public WebElement wizBtns;
	
	// Wizard PAGE 2 Credential Configurations
	@FindBy(how=How.ID_OR_NAME, using="joinType")   //select
	protected WebElement credJoinOp;	
	@FindBy(how=How.ID, using="credentials")
	protected WebElement credTbl;
	@FindBy(how=How.ID, using="newCred")
	protected WebElement credElemAddBtn;
	
	// Wizard PAGE - Add Credential MD
	@FindBy(how=How.ID, using="findMD")
	protected WebElement credMDfind;
	@FindBy(how=How.ID, using="credentialName")   //readonly
	protected WebElement credName;
	@FindBy(how=How.ID, using="ta")                 //readonly
	protected WebElement credTA;
	@FindBy(how=How.ID, using="desc")               //readonly
	protected WebElement credDesc;
	@FindBy(how=How.ID, using="grantingAuthority")  //select
	protected WebElement credGA;
	
	@FindBy(how=How.ID, using="saveCred")
	protected WebElement saveCredBtn;
	@FindBy(how=How.ID, using="cancelAddCred")
	protected WebElement canCredBtn;

	
	//Wizard PAGE 4 - Credential Parameter Mapping / Frequency Setting
	@FindBy(how=How.ID, using="credElements")  //select
	protected WebElement credElems;
	@FindBy(how=How.ID, using="paramsTable")  
	protected WebElement credParamTbl;
	@FindBy(how=How.ID, using="frequencyType")  //select
	protected WebElement credFreq;
	@FindBy(how=How.ID, using="maxAge")  //select
	protected WebElement credMAge;
	@FindBy(how=How.ID,using="viewCredExpr")
	protected WebElement credViewBtn;
	
	@FindBy(how=How.ID, using="editResource")
	protected WebElement resEditConfig;   //div id
	
	@FindBy(how=How.ID, using="savedCredExpr")  // readonly
	protected WebElement credExprDisplay;
	
	@FindBy(how=How.ID, using="editCredExpr")
	protected WebElement credEditBtn;
	@FindBy(how=How.ID, using="save")
	protected WebElement saveResBtn;
	
	@FindBy(how=How.ID, using="delete")   // change to XPATH
	protected WebElement resDel;
	////////////////////////////

	
	public RNSResourcePage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement getResTab(){
		return resTab;
	}
	public String getTabTitle(){
		String resTabVal = resTab.getAttribute("value");
		System.out.println("resTab value = " + resTabVal);
		return resTabVal;
	}
	 //works only on Edit Resource - New Resource is Hidden
 	public void addNewRes(String sAppName,String sAvailCred,String sJoin, String sResType, String sResHost, String sResPort,String sResPath, boolean bSave) 
 	{
		 System.out.println("RSCredentialPage:  Inside addNewRes ");
		 addNewResBtn.click();
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 if (sAppName !=null){
			 WebElement wName = driver.findElement(By.cssSelector("#newResource #name"));
		//	WebElement wName = driver.findElement(By.xpath("//html//body//div[8]//div[2]//form//div//table//tbody//tr//td//input")); //@id=newResource]"));
			if (wName != null)
				{   wName.click();  
					System.out.println("Name is not null - we found it"); 
				}
				if(wName.isEnabled()) 
				{
					wName.clear();
					wName.sendKeys(sAppName); //click(); 
				}
				Helpers.rest(Constants.I_DEF_TIMEOUT);
		 }
			 // controls  should be visible now
		WebElement resHost = driver.findElement(By.cssSelector("#newResource #hostname"));
			// WebElement resHost = driver.findElement(By.xpath("//html//body//div[8]//div[2]//form//div//table//tbody//tr[2]//td//div[2]//table//tbody//tr[2]//td[2]//input"));
		WebElement resPort = driver.findElement(By.cssSelector("#newResource #portStr"));
		// WebElement resPort = driver.findElement(By.xpath("//html//body//div[8]//div[2]//form//div//table//tbody//tr[2]//td//div[2]//table//tbody//tr[3]//td[2]//input"));
		WebElement resPath = driver.findElement(By.cssSelector("#newResource #path"));
		// WebElement resPath = driver.findElement(By.xpath("//html//body//div[8]//div[2]//form//div//table//tbody//tr[2]//td//div[2]//table//tbody//tr[4]//td[2]//input"));
		WebElement useWrap = driver.findElement(By.cssSelector("#newResource #useWrapper"));	 
		// WebElement useWrap = driver.findElement(By.xpath("//html//body//div[8]//div[2]//form//div//table//tbody//tr[2]//td//div[2]//table//tbody//tr[5]//td"));
		WebElement resType = driver.findElement(By.xpath("//html//body//div[9]//div[2]//form//div//table//tbody//tr[2]//td//div[2]//table//tbody//tr//td[2]//select"));  // does not work
		//	 WebElement canResBtn = driver.findElement(By.xpath("//html//body//div[7]//div[11]//div//button"));
		 
		 // Set all Resource config details
		// WebElement resHost = driver.findElement(By.xpath("//html//body//div[7]//div[2]//form//table//tbody//tr[3]//td//div[2]//table//tbody//tr[2]//td[2]//input"));
		 resHost.clear();
		 resHost.sendKeys(sResHost);
		 //System.out.println("set Host = " + resHost.getText() );
		 
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		// WebElement resPort = driver.findElement(By.xpath("//html//body//div[7]//div[2]//form//table//tbody//tr[3]//td//div[2]//table//tbody//tr[3]//td[2]//input"));
		 resPort.clear();
		 resPort.sendKeys(sResPort);
		 //System.out.println("set Port = " + resPort.getText() );
		 
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		// WebElement resPath = driver.findElement(By.xpath("//html//body//div[7]//div[2]//form//table//tbody//tr[3]//td//div[2]//table//tbody//tr[4]//td[2]//input"));
		 resPath.clear();
		 resPath.sendKeys(sResPath);
		// System.out.println("set Path = " + resPath.getText() );
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		 /// NEeed to be fixed  - incorrect
		 List<WebElement> appTypes = driver.findElements(By.xpath("//select[@id='resourceType']"));
		 //System.out.println("COunt of resourceTypes = " + appTypes.size());
		 int ind = sResType.indexOf(sResType);
		 String sRType = appTypes.get(ind).getText();    // always gets Application  - whole string
		 
		 appTypes.set(ind, null);
		 //System.out.println("set ResType = " + sRType );
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		 WebElement nextPg = driver.findElement(By.xpath("/html//body//div[9]//div[11]//div//button[3]"));
		 nextPg.click();
		 
		 // Choose Credential  - Builder wizard page 1
	
		 WebElement credJoinType = driver.findElement(By.cssSelector("#newResource #joinType"));
		 
		 WebElement credElemAddBtn = driver.findElement(By.cssSelector("#newResource #newCred"));
		 //WebElement credElemAddBtn = driver.findElement(By.xpath("/html//body//div[8]//div[2]//form//div[2]//table//tbody//tr[4]//td//div//button"));
		 credElemAddBtn.click();
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		 WebElement credMDfind = driver.findElement(By.cssSelector("#newResource #findMD"));
		// WebElement credMDfind = driver.findElement(By.xpath("/html//body//div[8]//div[2]//form//div[2]//table//tbody//tr[5]//td//div//table//tbody/tr//td//input"));
		 credMDfind.sendKeys(sAvailCred);
		 Helpers.rest(Constants.I_DEF_TIMEOUT + 700);
		 
		 WebElement credMDfound = driver.findElement(By.cssSelector("#newResource #foundMDs"));
		 //WebElement credMDfound = driver.findElement(By.xpath("/html//body//div[8]//div[2]//form//div[2]//table//tbody//tr[5]//td//div//table//tbody/tr//td//select"));
	
		 if ((credMDfind !=null)) { //&& (credMDfound.isDisplayed())){ 
			 System.out.println("inside credfind");

			 List<WebElement> allOptions = credMDfound.findElements(By.xpath("//select[@id='foundMDs']"));
			 int iCnt = allOptions.size();
			 
			 System.out.println("number of creds "+ iCnt);
			 if (allOptions == null || iCnt == 0)
			 {
				 System.out.println("No credentials list");
			 }
			 else 
			 {
				 for (WebElement option : allOptions) 
				 {

				     credMDfound.sendKeys(sAvailCred);
				     credMDfound.click();
				     break;

				 }
			 }
		 }

		 Helpers.rest(Constants.I_DEF_TIMEOUT + 700);
		 WebElement saveCredBtn = driver.findElement(By.cssSelector("#newResource #saveCred"));

		 saveCredBtn.click();
		 
		 nextPg.click();
		 

		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 // on to parameter page
		 WebElement fromReqParam = 
				 driver.findElement(
						 By.cssSelector("#newResource #dynamicParamValue"));

		 fromReqParam.click();
		 WebElement reqParamValue = driver.findElement(By.cssSelector("#newResource #requestParam"));

		 reqParamValue.clear();
		 reqParamValue.sendKeys("userid");
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 
		 WebElement credMAge = driver.findElement(By.cssSelector("#newResource #maxAge"));

		 if (credMAge.isDisplayed()) {
		 credMAge.sendKeys("120000");
		 }
		 Helpers.rest(Constants.I_DEF_TIMEOUT + 1000);
		 
		 nextPg.click();
		 
		 //credViewBtn.click();  /// not viewing 
		 Helpers.rest(Constants.I_DEF_TIMEOUT + 500);
		 saveResBtn.click();
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		//}  // shoudl be here????
		
}

	
	/***
	 * Delete Resource
	 * @param sAppName
	 * @param bAct
	 */
	public void delRes(String sAppName, boolean bAct){
	//	WebElement delResBtn = driver.findElement(By.xpath("//html//body//div[9]//div[11]//div//button[2]"));  // does not work
	//	WebElement canResBtn = driver.findElement(By.xpath("//html//body//div[9]//div[11]//div//button"));
		System.out.println("Inside Delete Resource: ");
		//System.out.println("> ---   ");
		boolean didFind = findResAndDel(sAppName); 
		if (didFind==false) {
			System.out.println("Nothing to delete - App not found");} 
		else {
		confirmDel(bAct); }
	}
	
	/***
	 * Find a Resource
	 * @param sApp
	 * @return
	 */
	public boolean findResAndDel(String sApp){
		System.out.println("Inside Find Resource");

		boolean gotIt = false;

		List<WebElement> tabs = driver.findElements(By.xpath("//table[@id='resources']")); //("tr"));
		int iRCount = tabs.size();  // heading is counted as a row? if size is 0-no Resources, it does not get in here.  then will fail.
		if(tabs == null || iRCount == 0) {
			System.out.println(">   - we didn't get any tr's so we fail to find resource by name, return null");
		}
		else
		{
			//int iCnt = 0;
			System.out.println(">   - got few tr's so we can find Resource by name: " + iRCount);
			//first row is table name Resources; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : tabs) 
			{
				WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				for (WebElement trow : ctRows)
				{
					String strow =  trow.getText();
//					System.out.println("Current row data inside table data: " +strow);
					if (strow.startsWith(sApp)) 
					{
						// System.out.println("Got the right Resource to Delete");
						trow.click();  // clicking right ? or we.click() ?
						WebElement wDel= we.findElement(By.className("delete"));
						if (wDel != null) 
						{//System.out.println(">   - going to delete Resource");
							wDel.click();  //delete now   //goto confirm dialog
							gotIt = true;
							break;
						}	else continue;
					} // for trow}	
				}//for2

			}// for1
			//	System.out.println("This is the count for loop: "+ iCnt);
		}// else
		return gotIt;
	} // method
	
	
		
	// Edit page
	public String findCred(String sCred, String sCredBox){
		// List<WebElement> lAvailCreds = driver.findElements(By.xpath("/html//body//div[7]//div[2]//form//table//tbody//tr//td//div[2]//select/option"));
		//List<WebElement> selCreds = driver.findElements(By.xpath("/html//body//div[7]//div[2]//form//table//tbody//tr[2]//table//tbody//tr//td[3]//div[2]//select//option"));
		int iCnt=0;
		String sCrStr = "";
		List<WebElement> lAvailCreds;
		if (sCredBox == "avail"){
		 lAvailCreds = driver.findElements(By.xpath("//select[@id='availCredentials']"));
		 iCnt = lAvailCreds.size();
		 System.out.println("Size of Available credentials box is : " + iCnt); }
		
		 if (iCnt > 0){ 
		 List<WebElement> allOptions = driver.findElements(By.tagName("option"));
		 for (WebElement option : allOptions) {
			 String selVal = option.getText(); // option.getAttribute("value");
		 
		     //System.out.println(String.format("Value is: %s", selVal));
		     if (sCred.contentEquals(selVal)){ System.out.println("Found the right credential on Available side"); 
		     	
		     	sCrStr = sCred; break;
		     	}
		  //   option.click();
		  
		 }
		/// else {
			// System.out.println("AvailCredentials is null");}
		 }
	
		else{
		 List<WebElement> lSelCreds = driver.findElements(By.xpath("//select[@id='selCredentials']"));
		 int isCnt=0;
		 System.out.println("Size of Selected credentials box is : " + lSelCreds.size());
		 if (isCnt > 0){ 
			 List<WebElement> allOptions = driver.findElements(By.tagName("option"));
			 for (WebElement option : allOptions) {
				 String selVal = option.getText(); // option.getAttribute("value");
			 
			     //System.out.println(String.format("Value is: %s", selVal));
			     if (sCred.contentEquals(selVal)){ //System.out.println("Found the right credential on Selected side"); 
			     	
			     	sCrStr = sCred; break;
			     	}
			  //   option.click();
			  
			 }
		}
		}
		 System.out.println("Credential is = " + sCrStr);
		return sCrStr; 
}
	public String findSelCred(String sApp){
		String selVal="";
		String sRs= findRes(sApp);
		List<WebElement> lSelCreds = driver.findElements(By.xpath("//select[@id='selCredentials']"));
		 int isCnt=lSelCreds.size();
		// System.out.println("Size of Selected credentials box is : " + isCnt);
		 if (isCnt > 0){ 
			
			 for (WebElement option : lSelCreds) {
				 
				 List<WebElement> wRows = option.findElements(By.tagName("option"));
				 for (WebElement trow : wRows){
						
				 selVal = trow.getText();
			 //System.out.println("Selected credential is = " + selVal + selTxt);
			     //System.out.println(String.format("Value is: %s", selVal));
			    // if (sCred.contentEquals(selVal)){ System.out.println("Found the right credential on Selected side"); 
			     	
			  //   	sCrStr = selVal; //break;
				 }	}
			 }
		 return selVal;  // could be a list of credentials  for now only a String

	}
	public String findRes(String sApp){

		String gotIt = null;

		List<WebElement> tabs = driver.findElements(By.xpath("//table[@id='resources']")); //("tr"));
		int iRCount = tabs.size();  // heading is counted as a row? if size is 0-no Resources, it does not get in here.  then will fail.
		if(tabs == null || iRCount == 0) {
			System.out.println(">   - we didn't get any tr's so we fail to find resource by name, return null");
		}
		else
		{
			//System.out.println(">   - got few tr's so we can find Resource by name: " + iRCount);
			//first row is table name Resources; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : tabs) 
			{   //System.out.println(">  - we.getText: " + we.getText());
				WebElement rowData = we.findElement(By.tagName("td"));
				//System.out.println(">> -- row data in table is : " + rows.subList(2,4));
				
				//	System.out.println(">    - Current data in the table: " + rowData.getText() + "what we want is: " + sApp);
		
					List<WebElement> rows = we.findElements(By.tagName("tr"));
				//	System.out.println("size of rows within each row is: " + rows.size());
						for (WebElement trow : rows){
							String strow =  trow.getText();
				//			System.out.println("Current row data inside table data: " +strow);
						if (strow.startsWith(sApp)) 
						{ //System.out.println("Got the right Resource to Edit");
						trow.click();  // clicking right ? or we.click() ?
						gotIt = strow;
						//System.out.println("Sending rowdata found: " + gotIt);  // DO NOT DELETE  - DEBUGGINg
						break;
						}else continue;
					} // for trow}		
	
			} // for we
		}// else
		return gotIt;

	} // end method
	

	public String findPort(String sApp){ 
		String sPrt = ""; 
		String sName = findRes(sApp);
		if (sName != null){ 
			sPrt=resPort.getAttribute("value");
			//System.out.println ("PORT IS= " + sPrt);
		}
		return sPrt;
	}
	
	public String findHost(String sApp){ 
		String sHst = ""; 
		String sName = findRes(sApp);
		if (sName != null){ 
			sHst=resHost.getAttribute("value");
			//System.out.println ("HOST IS= " + sHst);
		}	
		return sHst;
	}
	
	public String findPath(String sApp){ 
		String sPat = ""; 
		String sName = findRes(sApp);
		if (sName != null){ 
			sPat=resPath.getAttribute("value");
			//System.out.println ("PATH IS= " + sPat);
		}	
		return sPat;}
	
	public String findResDetails(String sApp){ 
		String sAll = ""; String sName = ""; String sCred=""; String sHost=""; String sPort=""; String sPath=""; String sUp="";
		sName = findRes(sApp);
		sCred = findSelCred(sApp);  // selected
		sHost = findHost(sApp);
		sPort = findPort(sApp);
		sPath = findPath(sApp);
		sUp = findUpdTime(sApp);
		sAll = "RESOURCE= "+sName+" CREDENTIAL= "+sCred+" HOST= "+sHost+" PORT= "+sPort+" PATH= "+sPath+" UPDATED LAST= "+sUp;
		
		return sAll;}
	
	public String findUpdTime(String sApp){
//System.out.println(">>. -------Inside FIND UPDATE TIME -----");
		String gotIt = "";
		List<WebElement> tabs = driver.findElements(By.xpath("//table[@id='resources']")); 
		int iRCount = tabs.size();  // heading is counted as a row? if size is 0-no Resources, it does not get in here.  then will fail.
		if(tabs == null || iRCount == 0) {
			System.out.println(">   - we didn't get any tr's so we fail to find resource by name, return null");
		}
		else
		{

			//System.out.println(">   - got few tr's so we can check Resource Update by name: " + iRCount);
			//first row is table name Resources; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : tabs) 
			{

				WebElement rowData = we.findElement(By.tagName("td"));
				//System.out.println(">> -- row data in table is : " + rows.subList(2,4));
				
			//		System.out.println(">    - Current Row data in the table: " + rowData.getText());
					List<WebElement> rows = we.findElements(By.tagName("tr"));
			//		System.out.println("size of rows within each row is: " + rows.size());
						for (WebElement trow : rows){
					
							String strow =  trow.getText();
						//	System.out.println("Current row data inside table data: " +strow);     // edited name
						if (strow.startsWith(sApp)){// just try to get timestamp? instead of whole row?
						we.click();
						gotIt = strow;
						break;
					}else continue;
						}
			}
		}return gotIt;
	}
/*	public void edtResName(String sName, String sRname, boolean bSave){
		String sFound = findRes(sName);
		if ((sFound !=null) && (sFound.startsWith(sName)) && (bSave == true)){
			System.out.println("In edtResName:  Found Resource, ready to update Name now");
			sName.replace(sName, sRname);  // I need to do this  otherwise will pick first one
			appNameInput.clear();
			appNameInput.sendKeys(sRname);  
			saveBtn.click();
			// check the updated time for the new name
				String sUp = findUpdTime(sRname);
				System.out.println("Updated Resource details for " + sRname +" is -- " +sUp);
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
	}	
	
	public void edtResCred(String sApp, String sCred, String sRname, boolean bSave){
		System.out.println("> --- Entered edtResCred with App,Cred, newCred: " + sApp + ", " + sCred + ", " + sRname);
		
		String sSOpt = ""; 
		String sAOpt = "";
		String sFoundRes = findRes(sApp);
		if (sFoundRes !=null) {
		String sFoundCred = findCred(sCred,"avail");
		System.out.println("> --- Credential Name: " + sFoundCred);
		if ((sFoundCred !=null) && (sFoundCred.startsWith(sCred)) && (bSave == true)){
			System.out.println("In edtResCred:  Found Resource, ready to update Credential now");
			//sName.replace(sName, sRname);  // do I need to do this?
			List<WebElement> wAvailCreds = driver.findElements(By.xpath("//select[@id='availCredentials']"));
						
			for (WebElement we : wAvailCreds){
				WebElement rowData = we.findElement(By.tagName("option"));
				String rData= rowData.getText();
				sAOpt = rowData.getAttribute("value");
				System.out.println(">    - Current Row data in Available Credentials is: " + rData + " and the Option value is: " + sAOpt);
				if (rData.contentEquals(sRname)){
					rowData.click();
					aCredBtn.click();  // add new one
					System.out.println(">    - Just clicked to Add New credential");
					Helpers.rest(Constants.I_DEF_TIMEOUT);
					break;
				}
			}
			System.out.println("Should check out Selected Credentials now");
			List<WebElement> wSelCreds = driver.findElements(By.xpath("//select[@id='selCredentials']"));
			for (WebElement wSel : wSelCreds){
				System.out.println("Selected credentials has " + wSelCreds.size() + " items");
				WebElement selData = wSel.findElement(By.tagName("option"));
				String rsData= selData.getText();
				sSOpt = selData.getAttribute("value");
				System.out.println(">    - Current Row data in Selected Credentials is: " + rsData + " and the Option value is: " + sSOpt);
				if (rsData.contentEquals(sCred)){
					selData.click();
					rCredBtn.click();  // remove old one
					Helpers.rest(Constants.I_DEF_TIMEOUT);
					System.out.println(">    - Just clicked to Remove old credential");
					break;
				}
			}
			//if (bSave == true){
				saveBtn.click();
				System.out.println(">    - Just Save credential Edits");
			// check the updated time for the new name
				String sUp = findUpdTime(sApp);
				System.out.println("Updated Resource details for " + sRname +" is -- " +sUp);
				System.out.println(">    - Just checking the credentials drop down box now ----------->");
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
		} // found the right Resource App first.
		if (findCred(sRname,"avail").startsWith(sCred)) { System.out.println("swapped the credentials around");}
		if (findCred(sCred,"").startsWith(sRname)) {System.out.println("swapped the credentials around");}
	}	

	public void edtResType(String sName, String sOtype, String sRType, boolean bSave){

		System.out.println("Entered EdtResType: " + sName + " " + sOtype + " " + sRType );
		String sFound = findRes(sName);
		if ((sFound !=null) && (sFound.startsWith(sName)) && (bSave == true)){
			System.out.println("In edtResType:  Found Resource, ready to update Type now");
			//sName.replace(sName, sName);  // I need to do this  otherwise will pick first one
				String sType="";
				int ind=0;
			List<WebElement> sTypes = driver.findElements(By.xpath("//select[@id='resourceType']"));
			System.out.println("Count of Types = " + sTypes.size());
			
			for (WebElement wSel : sTypes){
				sType = wSel.getText(); // returns whole string Application Data No Proxy
				//sType = wSel.getAttribute("name");  // returns type
			//	ind = sRType.indexOf(sRType);
			//	  sType = sTypes.get(ind).getText();
			//				 sTypes.set(ind, null);
				System.out.println("This is the Resource Type now: " + sType + " and we want "+ sRType);
				if (sType.contains(sRType)){
						System.out.println("Found the Res Type: " + sType);
						String sSub = sType.substring(12);
					wSel.sendKeys(sSub);
					System.out.println("set ResType = " + sType );
					break;
			} else continue;
		}
			Helpers.rest(Constants.I_DEF_TIMEOUT);
			
			saveBtn.click();
			// check the updated time for the new name
				String sUp = findUpdTime(sName);
				System.out.println("Updated Resource details for " + sName +" is -- " +sUp);
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
	}	

	public void edtResHost(String sName, String sRname, boolean bSave){
		String sFound = findRes(sName);
		if ((sFound !=null) && (sFound.startsWith(sName)) && (bSave == true)){
			System.out.println("In edtResHost:  Found Resource, ready to update Hostname now");
			//sName.replace(sName, sRname);  // I need to do this  otherwise will pick first one
			resHost.clear();
			resHost.sendKeys(sRname);  
			saveBtn.click();
			// check the updated time for the new name
				String sUp = findUpdTime(sRname);
				System.out.println("Updated Resource details for " + sRname +" is -- " +sUp);
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
	}	
	public void edtResPort(String sName, String iPort, boolean bSave){
		String sFound = findRes(sName);
		if ((sFound !=null) && (sFound.startsWith(sName)) && (bSave == true)){
			System.out.println("In edtResHost:  Found Resource, ready to update Port now");
			//sName.replace(sName, sRname);  // I need to do this  otherwise will pick first one
			resPort.clear();
			resPort.sendKeys(iPort);  // should be integer value - using string for automation
			saveBtn.click();
			// check the updated time for the new name
				String sUp = findUpdTime(sName);
				System.out.println("Updated Resource details for " + sName +" is -- " +sUp);
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
	}	
	public void edtResPath(String sName, String sRname, boolean bSave){
		String sFound = findRes(sName);
		if ((sFound !=null) && (sFound.startsWith(sName)) && (bSave == true)){
			System.out.println("In edtResHost:  Found Resource, ready to update Path now");
			//sName.replace(sName, sRname);  // I need to do this  otherwise will pick first one
			resPath.clear();
			resPath.sendKeys(sRname);  
			saveBtn.click();
			// check the updated time for the new name
				String sUp = findUpdTime(sName);
				System.out.println("Updated Resource details for " + sName +" is -- " +sUp);
		}
		else { // No cancel button for Edits.
			System.out.println("Cancelling Edit --- did not find the right Resource to edit");
			}
		Helpers.rest(Constants.I_DEF_TIMEOUT);
	}
	*/

	
	/** Confirm Deletion
	 * 
	 * @param bDelete
	 */
	public void confirmDel(boolean bDelete){
		
		System.out.println(">   - Inside confirmDelete: ");
		//String sWarning = "The Credential will be deleted. Are you sure?";
		WebElement sConf = driver.findElement(By.className("warning"));
		System.out.println(">   - This is the confirmation text: "+ sConf.getText());
		// leave these here - DO NOT TOUCH --- can cause invocation exception.....
		 WebElement delResBtn = driver.findElement(By.xpath("//html//body//div[9]//div[11]//div//button[2]"));  
		 WebElement canResBtn = driver.findElement(By.xpath("//html//body//div[9]//div[11]//div//button"));
		if (bDelete == true){
			//clickButton(4);  // hopefully delete 
			delResBtn.click();
			Helpers.rest(Constants.I_DEF_TIMEOUT);
		}
		else //clickButton(3);  // for cancel
			canResBtn.click();
	}
	
	public String getErrorText() {

		WebElement errorBlock = null;
		List<WebElement> errors = driver.findElements(By.className("error"));
		if(errors != null && errors.size() > 0) {
			System.out.println(">   - you have this many errors: "+errors.size());
			errorBlock = errors.get(0);		
		}
			
		String errorMess = "";
		if(errorBlock != null) {
			errorMess = errorBlock.getText();
		}		
		return errorMess;
	}
	 
	public void clickButton(int Button){
		//	System.out.println(">   - start: RNSResourcePage:  Inside clickButton " + Button);
			WebElement clink = null;
			
			List<WebElement> allButns = driver.findElements(By.tagName("button"));
			System.out.println("number of buttons: " + allButns.size());
		
			if(allButns == null || allButns.size() == 0) {
				System.out.println(">   - error: button not available, null or zero buttons available");
				return;
			} else {
				
				clink = allButns.get(Button); 
			}

			Helpers.rest(Constants.I_DEF_TIMEOUT);
			try {
				clink.click();
			} catch(Exception xu) {
				System.out.println(">   - 2nd click got exception: ");
				xu.printStackTrace();
			}
			System.out.println(">   - inside clickButton(): button number["+Button+"] was just clicked on page: "+driver.getTitle());

		}
	 

	/**
	 * 
	 * @return int   count of credentials on the page
	 * @throws Exception
	 */
		public int getCount() throws Exception 
		{
			return Helpers.getEntityRowCount(this, "resources");
		}
	
	/**
	* 	
	* @param rowNum
	* @return String  name(column 1) of credential table in rowNum passed in
	* @throws Exception
	*/
			public String getName(int rowNum) throws Exception {
				return Helpers.getEntityFieldValue(this, "resources", rowNum, 1);
			}
}
