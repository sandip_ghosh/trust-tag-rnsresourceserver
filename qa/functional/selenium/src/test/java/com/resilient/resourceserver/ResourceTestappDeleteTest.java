/* Copyright (c) 2010, 2012 Resilient Network Systems
 * All Rights Reserved 
*/
package com.resilient.resourceserver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

import com.resilient.resourceserver.util.Constants;
import com.resilient.resourceserver.util.ResConstants;
import com.resilient.resourceserver.util.Helpers;
import com.resilient.resourceserver.util.RNSResCredRequestPage;
import com.resilient.resourceserver.util.RNSResourcePage;
//import com.resilient.resourceserver.util.ResilientTrustSvcPage;
import com.resilient.tests.selenium.ResilientTrustSvcPage;
import com.resilient.tests.selenium.FFBrowser;


/*******
 * Testcase 101 
 * Launch RNSResourceServer UI from host and port read in from properties file
 * Login with Simple Login Credential 
 * Create a Resource Testapp; protect with Simple Credential offered by TA
 * Delete Resource during cleanup
 * @author ulakumarappan
 *
 */
public class ResourceTestappDeleteTest {

	FFBrowser browser;
	RNSResCredRequestPage RsCredPg;
	RNSResourcePage RsResPg;
	ResilientTrustSvcPage RttsPg;
	
	@Before
	public void setUp() throws Exception {
		
		 String scheme=Helpers.getPropValue("SCHEME");
		 String host=Helpers.getPropValue("HOST");
		 String portnum=Helpers.getPropValue("PORTNUM");
		 String loginURL = scheme+host;
		 browser = new FFBrowser(loginURL, portnum);
		 browser.get(Constants.S_RES_PAGE);
// Login credentials for RESOURCE SRVR
		 RsCredPg = browser.shows(RNSResCredRequestPage.class);
		 Helpers.rest(Constants.I_DEF_TIMEOUT);
		 RsCredPg.logUser(Constants.S_QA_USER);
		   	Helpers.rest(Constants.I_DEF_TIMEOUT+700);
		   	RttsPg = browser.shows(ResilientTrustSvcPage.class);
		   	RttsPg.iFrameLogin(Constants.S_QA_USER);
//Logged in correct - show the Resource Page now		   	
		   	RsResPg = browser.shows(RNSResourcePage.class); 
//Add resource Testapp
		   	RsResPg.addNewRes(ResConstants.sAppName, 
					ResConstants.sAvailCred, 
					ResConstants.sJoin, 
					ResConstants.sResType, 
					ResConstants.sResHost, 
					ResConstants.sResPort, 
					ResConstants.sResPath, true);  //  at times gives 404 error
		   	Helpers.rest(Constants.I_DEF_TIMEOUT); 

/*  	int numRes = RsResPg.getCount();
	assertEquals("Did not see the expected credential count", 1, numRes);
	String expectedResName = RsResPg.getName(1);
	assertEquals("The name does not match", expectedResName, Helpers.getPropValue("RESOURCE_SIMPLE_TESTAPP"));  */

	}
	
	@Test
	public void ResDelete101() throws Exception{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Positive Full Resource Addition   - make it a parameterized method - so can have creation of many Resources
		RsResPg.delRes(ResConstants.sAppName, true);   // Delete
       	Helpers.rest(Constants.I_DEF_TIMEOUT); 
	}
	
	
	@After
	public void tearDown() throws Exception {
		// after completion of the end - end, enable this.
 		browser.stop();
	}

}
