package com.resilient.resourceserver.util;

public class ResConstants {

		final static public String S_ASERVER = "http://localhost";
		final static public String S_PORT = "8080";
		
		final static public String S_RES_PAGE = "RNSResourceServer/resource";
		final static public String S_RES_PAGE_IND = "RNSResourceServer/resource/index";
		final static public String S_RES_PAGE_L = "RNSResourceServer/resource/index?status=requestCredential&sessionId=&parameters={S_QUOTEuseridS_QUOTE%3AS_QUOTEanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanywhereanyS_QUOTE}";
			
		final static public String SIMPLE_PWD = "Joe.Left@foo.com";
		final static public String SIMPLE_BAD = "Joel.eft@foo.com";
		final static public String GRANTED_TESTAPP = "Test Application";
		
		final static public String sAppName="testapp";
		final static public String sAvailCred="spec.resilient.com/Credentials.SimpleLogin";
		final static public String sJoin="AND";
		final static public String sResType="Application";
		final static public String sResHost="localhost";
		final static public String sResPort="8080";
		final static public String sResPath="/";
		final static public String sOper="Add";
		final static public String S_QUOTE = "\"";
		
		// ERROR VALIDATION STRINGS
		final static public String sPortEmpty= "Failed to convert property value of type java.lang.String to required type int for property port; nested exception is java.lang.IllegalArgumentException: Could not parse number: Unparseable number: "+S_QUOTE+"lll"+S_QUOTE;
		final static public String sCredEmpty = "Property [credentials] of class [class com.resilient.resourceserver.Resource] with value [[]] is less than the minimum size of [1]";
		final static public String sHostEmpty = "Failed to save the Resource. The hostname cannot be empty";
		final static public String sPathEmpty = "Failed to save the Resource. The path cannot be empty";
		final static public String sNameEmpty = "Failed to save the Resource. The Resource name cannot be empty";
		final static public String sNameExists = "Failed to save the Resource. The Resource name has to be unique";
		
		final static public String CREDENTIAL_SIMPLOGIN_TESTAPP = "{ \n" +
				"	\"nodeName\" : \"CredentialElement\", \n" +
				"	\"credentialIdentifier\" : { \n" + 
					"		\"name\" : \"spec.resilient.com//Credentials.SimpleLogin\" \n" +
				"	}, \n" +
				"	\"grantingAuthorities\" : [ [ { \n" +
				"		\"uri\" : \"rtn://localhost:2911/resilient_test_trust_service\" \n" +
				"	} ] ], \n" +
				"	\"parameters\" : [ \n" +
				"		{ \n " +
				"			\"historyKey\" : true, \n" +
				"			\"key\"        : \"user id\", \n" +
				"			\"value\"      : \"<userid>\" \n" +
				"		} \n" +
				"	], \n" +	
				"	\"frequency\" :  { \n" +
				"		\"nodeName\" : \"sameSession\", \n" +
				"		\"maxAge\" : 60000 \n" +
				"	}  \n" +
				"}";

}
