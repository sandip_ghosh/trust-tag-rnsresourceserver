package com.resilient.resourceserver.util;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.resilient.tests.selenium.Page;


public final class Helpers {
	public static Properties _props;
	
	public static String getPropValue(String propName) throws Exception {
		if (_props == null) {
			_props = new Properties();
			String currentDir = System.getProperty("user.dir");
			//System.out.println("CurrentDir = " + currentDir);
			_props.load(new FileInputStream(currentDir+Constants.PROPS_FILE));
		}
		return _props.getProperty(propName);
	}

	//FIX THIS Failed tests:   CredEdit101(com.resilient.resourceserver.CredentialEditTest): 
	//The parameter name does not match expected:<[Ljava.lang.String;@6d46b6db> but was:<[Ljava.lang.String;@3d8f1be9>
	public static String[] getPropValueArray(String propName) throws Exception {
		if (_props == null) {
			_props = new Properties();
			String currentDir = System.getProperty("user.dir");
			//System.out.println("CurrentDir = " + currentDir);
			_props.load(new FileInputStream(currentDir+Constants.PROPS_FILE));
		}
		String[] sParam1 = (_props.getProperty(propName)).split(",");
		int iLen = sParam1.length;
		String[]sParam2 = new String[iLen];

		for (int i=0; i<iLen; i++)
		{
			sParam2[i]=sParam1[i];
			System.out.println("String in final array of parameters is " + sParam2[i]);
		}
		
	 return sParam2;
	}
	 public static String getErrorText(Page p) {

		WebElement errorBlock = null;
		List<WebElement> errors = p.getDriver().findElements(By.className("error"));
		if(errors != null && errors.size() > 0) {
			System.out.println(">   - you have this many errors: "+errors.size());
			errorBlock = errors.get(0);		
		}
			
		String errorMess = "";
		if(errorBlock != null) {
			errorMess = errorBlock.getText();
		}		
		return errorMess;
	}
		
	 // Currently no NAME for the buttons - 0=? 1=NEW, 2=CONVERT_FORMAT, 3=CANCEL, 4=SAVE/Delete/Add  // could be random???
	public static void clickButton(RNSCredentialPage p, int Button){

			//System.out.println(">   - start: RSCredentialPage:  Inside clickButton " + Button);
			WebElement clink = null;
			
			List<WebElement> allButns = p.getDriver().findElements(By.tagName("button"));
			//System.out.println("number of buttons: " + allButns.size());
		
			if(allButns == null || allButns.size() == 0) {
				//System.out.println(">   - error: button not available, null or zero buttons available");
				return;
			} else {
				
				clink = allButns.get(Button); 
			}

			rest(3555);
			try {
				clink.click();
			} catch(Exception xu) {
				System.out.println(">   - 2nd click got exception: ");
				xu.printStackTrace();
			}
		//	System.out.println(">   - inside clickButton(): button number["+Button+"] was just clicked on page: "+p.getDriver().getTitle());

		}
	
	public static int getEntityRowCount(Page p, String entityId) throws Exception {
		//System.out.println("Inside getCredCount");
		// this gets the Heading row always will be size 1
		String xpathTpl = getPropValue("ENTITY_TABLE_XPATH");
		String xpath = String.format(xpathTpl, entityId);
		List<WebElement> rows = p.getDriver().findElements(By.xpath(xpath)); 
		if (rows == null || rows.size() == 0){
			Assert.fail("There should be a heading row");
			System.out.println("no creds (yet) in the table");
		//	iSize = 0;
		}
		return rows.size() - 1;   // subtract 1 for the last row that has the "New" button;
	}
	
	
	/**
	 * Returns the specified column value from the entities table (Credential or Resource) 
	 * 
	 * @param p - The Page object for the entity, e.g. RNSCredentialPage
	 * @param entityId - The entity ID that identifies the entities list table, e.g. "credentials"
	 * @param rowNum - The 1 based index of the row we are searching for
	 * @param colNum - The 1 based index of the column we are searching for
	 * @return The text at the colNum column of the rowNum row of the entities list table
	 * 
	 * @throws Exception
	 */
	public static String getEntityFieldValue(Page p, String entityId, int rowNum, int colNum) throws Exception {
//		String xpathTpl = getPropValue("ENTITY_TABLE_XPATH");
//		String xpath = String.format(xpathTpl, entityId);
//		List<WebElement> rows = p.getDriver().findElements(By.xpath(xpath));
//		Assert.assertNotNull("Did not find any rows", rows);
//		Assert.assertTrue("Did not find the expected rows", rows.size() >= rowNum);
//		// The header row is row 0, so do not subtract 1
//		WebElement row = rows.get(rowNum);
//		List<WebElement> cells = row.findElements(By.tagName("td"));
//		Assert.assertNotNull("Did not find any cells", cells);
//		Assert.assertTrue("Did not find the expected cell", cells.size() >= colNum);
//		WebElement fieldCol = cells.get(colNum - 1);
//		Assert.assertNotNull("the field column is null", fieldCol);
//		return fieldCol.getText();

		String xpathTpl = getPropValue("ENTITY_FIELD_VALUE_XPATH");
		// The header row is row 1, so should add 1
		//String xpath = String.format(xpathTpl, "credentials", rowNum + 1, colNum);
		String xpath = String.format(xpathTpl, entityId, rowNum+1, colNum);
		List<WebElement> elements = p.getDriver().findElements(By.xpath(xpath));
		Assert.assertTrue("Could not find the entity field value", elements.size() == 1);
		return elements.get(0).getText();
	}
	
	public static int getCredCount(RNSCredentialPage p) throws Exception 
	{
		//System.out.println("Inside getCredCount");
		// this gets the Heading row always will be size 1
		int iSize=0;
		String xpathTpl = getPropValue("ENTITY_TABLE_XPATH");
		String xpath = String.format(xpathTpl, "credentials");
		List<WebElement> tabs = p.getDriver().findElements(By.xpath(xpath)); 
		if (tabs == null || tabs.size() == 0){
			Assert.fail("There should be a heading row");
			System.out.println("no creds (yet) in the table");
		//	iSize = 0;
		}
		else {
			for(WebElement we : tabs) 
			{
		//		WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				iSize = ctRows.size();
		//		System.out.println("rows inside credential table = " +iSize);
			} 
		}
		return iSize;   // send 1 always or subtract heading row;
	}
	
	public static int getResCount(RNSResourcePage p) throws Exception 
	{
		//System.out.println("Inside getCredCount");
		// this gets the Heading row always will be size 1
		int iSize=0;
		String xpathTpl = getPropValue("ENTITY_TABLE_XPATH");
		String xpath = String.format(xpathTpl, "resources");
		List<WebElement> tabs = p.getDriver().findElements(By.xpath(xpath)); 
		if (tabs == null || tabs.size() == 0){
			System.out.println("no resources (yet) in the table");
		//	iSize = 0;
		}
		else {
			for(WebElement we : tabs) 
			{
		//		WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				iSize = ctRows.size();
		//		System.out.println("rows inside credential table = " +iSize);
			} 
		}
		return iSize;   // send 1 always or subtract heading row;
	}
	public static boolean findCred(RNSCredentialPage p, String sCred){
		//System.out.println("Inside FindCredential " + sCred);

		boolean gotIt = false;

		List<WebElement> tabs = p.getDriver().findElements(By.xpath("//table[@id='credentials']")); 
		int iRCount = tabs.size();
		if(tabs == null || iRCount == 0) {
			System.out.println(">   - fail, we didn't get any tr's so we fail to find cred by name, you are done, return null");
		}
		else 
		{
			//System.out.println(">   - got few tr's so we can find cred by name: " + iRCount);
			//first row is table name Credential; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : tabs) 
			{
				WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				for (WebElement trow : ctRows){
					String strow =  trow.getText();
		//			System.out.println("Current row data inside table data: " +strow);
				if (strow.startsWith(sCred)) 
				{// System.out.println("Got the right Resource to Delete");
				trow.click();  // clicking right ? or we.click() ?
				WebElement wDel= we.findElement(By.className("delete"));
				if (wDel != null) {//System.out.println(">   - going to delete Credential");
				wDel.click();  //delete now   //goto confirm dialog
				gotIt = true;
					break;
				}else continue;
			} // for trow}	
				}
	
			}
		//	System.out.println("This is the count for loop: "+ iCnt);
		}// else
		return gotIt;
	} // method
	
	public static String getSysDate(){
    	//date validation  - get current date 
    	SimpleDateFormat formatter = new SimpleDateFormat ("dd MMM yyyy");
       	Date date = new Date();
       	String sWantedate = formatter.format(date);
       	return sWantedate;
	}
	
	public static void confirmDel(RNSCredentialPage p, boolean bDelete){
		//System.out.println(">   - Inside confirmDelete: ");
		WebElement sConf = p.getDriver().findElement(By.className("warning"));
		System.out.println(">   - This is the confirmation text: "+ sConf.getText());
		if (bDelete == true){
			clickButton(p, 4);  // hopefully delete 
			rest(2222);
		}
		else clickButton(p,3);  // for cancel
	}
	public static void rest(long restmillis) {
		try {
        	Thread.sleep(restmillis);
        } catch(Exception fu) {
        	
        }
	}
	public static String findCredStr(RNSCredentialPage p,String sCred){
		String gotIt = "";

		List<WebElement> rows = p.getDriver().findElements(By.tagName("tr"));
		int iRCount = rows.size();
		if(rows == null || iRCount == 0) {
			System.out.println(">   - fail, we didn't get any tr's so we fail to find cred by name, you are done, return null");
		}
		else
		{

			//System.out.println(">   - got few tr's so we can find cred by name: " + iRCount);
			//first row is table name Credential; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : rows.subList(2,4)) 
			{
					String rowData = we.findElement(By.tagName("td")).getText();
				
			//		System.out.println(">    - Current Row data in the table: " + rowData);
					if (rowData.contains(sCred)) {
			//			System.out.println(">   - going to select right Credential to Edit " + sCred);
						
			//			System.out.println(">  -rowData getText: " + we.getText());
						// it is getting all the credentials in the block or table - I need only the one row to be selected
						if (we.getText().startsWith(sCred))
							
						{ //System.out.println("got the right one to edit");
						we.click();  // am I clicking the right one? - clickin the first row's delete image- which is not correct
			//			System.out.println("clicked the right one to edit");
						Helpers.rest(Constants.I_DEF_TIMEOUT);
						//all these do not work
						
						WebElement wSel= we.findElement(By.xpath("//div[@class='sectionBody']"));
							if (wSel != null) {
								System.out.println(">   - going to edit Credential");
							
							}
							gotIt= we.getText();
							break; }
					} 
					else continue;
			}
		
		}// else
		return gotIt;
	} // method
}
