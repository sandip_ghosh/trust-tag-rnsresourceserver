package com.resilient.as.trusttag.websocket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.websocket.Constants;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.WsOutbound;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.resilient.commonutils.httputils.HttpClientUtil;
import com.resilient.commonutils.httputils.IHttpClientUtil;


public class TrustTagWebSocketServlet extends WebSocketServlet {
	static final long serialVersionUID = 1L;

	// set the default idle timeout to 10 minutes
	private static final int DEFAULT_NETWORK_IDLETIMEOUT_INTERVAL = 1000 * 60 * 10; 
	
	private static final Logger LOG = Logger.getLogger(TrustTagWebSocketServlet.class.getName());

	private Map<UUID, TrustTagMessageInbound> m_inboundConnections = new ConcurrentHashMap<UUID,TrustTagMessageInbound>();
	
	@SuppressWarnings("unchecked")
	@Override
	protected StreamInbound createWebSocketInbound(String subProtocol, HttpServletRequest request) {
		UUID wsRequestCtx = UUID.randomUUID();
		String asAPIBaseURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
		TrustTagMessageInbound mi = new TrustTagMessageInbound(wsRequestCtx, asAPIBaseURL);
		String sessionId = request.getParameter("sessionId");
		if (sessionId != null) {
			mi.m_sessionId = sessionId;
		}
		String parameters= request.getParameter("parameters");
		if (parameters != null) {
			try {
				JSONObject paramsObj = new JSONObject(parameters);
				mi.m_parameters = paramsObj;
			} catch (JSONException ex) {
				LOG.severe("Failed to parse the parameters parameter to a JSON Object. Reason: " + ex.getLocalizedMessage());
			}
		}
		m_inboundConnections.put(wsRequestCtx, mi);
		return mi;
	}
	
	void removeInboundConnection(UUID reqCtx) {
		TrustTagMessageInbound mi = m_inboundConnections.get(reqCtx);
		if (mi != null) {
			mi.shutdownPendingTasks();
			m_inboundConnections.remove(reqCtx);
		}
	}
	
	JSONObject executeCredentialInput(String resourceName, String asAPIBaseURL) {
		int respCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		String message = "";
		JSONObject respJSON = null;
		try {
			String reqURL = asAPIBaseURL + "/RNSResourceServer/credentialRequestor/inputCredentials/" + resourceName;
			IHttpClientUtil httpClientUtil = new HttpClientUtil();
			httpClientUtil.setupRequest(IHttpClientUtil.Method.GET, reqURL);
			IHttpClientUtil.IResponse resp = httpClientUtil.executeRequest();
			respCode = resp.getResponseCode();
			if (respCode == HttpServletResponse.SC_OK) {
				String respData = resp.getResponseString(-1);
				respJSON = new JSONObject(respData);
				respJSON.put("responseCode", HttpServletResponse.SC_OK);
			} else if (respCode == HttpServletResponse.SC_NOT_FOUND) {
				message = "The resource : " + resourceName + " is not defined in the Access Server";
				LOG.warning(message);
			}
		} catch (Exception ex) {
			respCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			message = "Unknown Error occurred in executing inputCredentials API. Reason: " + ex.getLocalizedMessage();
			LOG.severe(message);
		} finally {
			if (respCode != HttpServletResponse.SC_OK) {
				try {
					respJSON = new JSONObject();
					respJSON.put("responseCode", respCode);
					respJSON.put("message", message);
				} catch (JSONException ex) {
					LOG.severe("Failed to create the JSON response for the inputCredentials API. Reason: " + ex.getLocalizedMessage());
				}
			}
			return respJSON;
		}
	}
	

	@SuppressWarnings("unchecked")
	JSONObject executeCredentialRequest(UUID reqCtx) {
		TrustTagMessageInbound mi = m_inboundConnections.get(reqCtx);
		if (mi != null) {
			int respCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			String reqCredentialURL = mi.m_ASAPIBaseURL + "/RNSResourceServer/credentialRequestor/requestCredential/" + mi.m_resourceName;
			String message = "";
			JSONObject respJSON = null;
			try {
				JSONObject postData = new JSONObject();
				if (mi.m_sessionId != null) {
					postData.put("sessionId", mi.m_sessionId);
				}
				if (mi.m_parameters != null) {
					postData.put("parameters", mi.m_parameters);
				}
				String callbackURL = mi.m_ASAPIBaseURL + "/RNSResourceServer/evaluateCredentials/checkDecision?contextId=" + reqCtx.toString();
				postData.put("callbackURL", callbackURL);
				IHttpClientUtil httpClientUtil = new HttpClientUtil();
				httpClientUtil.setupRequest(IHttpClientUtil.Method.POST, reqCredentialURL);
				httpClientUtil.setPostData(postData.toString());
				IHttpClientUtil.IResponse resp = httpClientUtil.executeRequest();
				respCode = resp.getResponseCode();
				if (respCode == HttpServletResponse.SC_OK) {
					String respData = resp.getResponseString(-1);
					respJSON = new JSONObject(respData);
					mi.m_decision = respJSON.getString("decision");
					if (mi.m_decision.equals("TB_DISPLAY")) {
						Map<UUID, TrustTagMessageInbound> relyingPartyState = null;
						if (getServletContext().getAttribute("RelyingPartyState") == null) {
							relyingPartyState = new HashMap<UUID, TrustTagMessageInbound>();
							getServletContext().setAttribute("RelyingPartyState", relyingPartyState);
						} else if (getServletContext().getAttribute("RelyingPartyState") instanceof Map<?, ?>){
							relyingPartyState = (Map<UUID, TrustTagMessageInbound>)getServletContext().getAttribute("RelyingPartyState");
						}
						relyingPartyState.put(reqCtx, mi);
					}
					if (respJSON.has("sessionId")) {
						mi.m_sessionId = respJSON.getString("sessionId");
					}
					respJSON.put("responseCode", HttpServletResponse.SC_OK);
				} else if (respCode == HttpServletResponse.SC_UNAUTHORIZED) {
					mi.m_decision = "DENY";
					message = "Authentication failed for the requested credentials";
				} else {
					mi.m_decision = "ERROR";
					message = "There was an error in executing the requestCredential API";
				}
			} catch (Exception ex) {
				LOG.severe("Exception occurred in executing the requestCredential API. Reason: " + ex.getLocalizedMessage());
			} finally {
				if (respCode != HttpServletResponse.SC_OK) {
					try {
						respJSON = new JSONObject();
						respJSON.put("responseCode", respCode);
						respJSON.put("message", message);
						respJSON.put("decision", mi.m_decision);
					} catch (JSONException ex) {
						LOG.severe("Failed to create the JSON response for the inputCredentials API. Reason: " + ex.getLocalizedMessage());
					}
				}
				return respJSON;
			}
		} else {
			return null;
		}
	}

	public void closeConnections() throws IOException {
		for (Map.Entry<UUID, TrustTagMessageInbound> entry : m_inboundConnections.entrySet()) {
			TrustTagMessageInbound mi = entry.getValue();
			mi.shutdownPendingTasks();
			if (mi.getWsOutbound() != null) {
				mi.getWsOutbound().close(Constants.STATUS_SHUTDOWN, null);
			}
		}
		m_inboundConnections.clear();
	}
	
	final class TrustTagMessageInbound extends MessageInbound {
		
		private UUID m_requestContext;
		private ScheduledExecutorService m_idleShutdownExecutor;
		String m_ASAPIBaseURL;
		String m_resourceName;
		String m_sessionId;
		JSONObject m_parameters;
		String m_decision;
		long m_credentialExpiryTime = 0;
		
		TrustTagMessageInbound(UUID requestCtxId, String asAPIBaseURL) {
			m_requestContext = requestCtxId;
			m_ASAPIBaseURL = asAPIBaseURL;
			m_idleShutdownExecutor = Executors.newSingleThreadScheduledExecutor();
		}
		
		@Override
		protected void onClose(int status) {
			m_idleShutdownExecutor.shutdownNow();
			removeInboundConnection(m_requestContext);
		}
		
		@Override
		protected void onBinaryMessage(ByteBuffer arg0) throws IOException {
			LOG.warning("Received a binary message, the websocket servlet can only process text stream messages");
			throw new UnsupportedOperationException("Binary message not supported.");
		}
		
		@Override
		protected void onTextMessage(CharBuffer msgBuf) throws IOException {
			try {
				JSONObject msgJSON = new JSONObject(msgBuf.toString());
				if (msgJSON.has("resourceName")) {
					m_resourceName = msgJSON.getString("resourceName");
					JSONObject respJSON = null;
					if (msgJSON.has("requestContextId")) {
						String recvdReqCtxId = msgJSON.getString("requestContextId");
						if (this.m_requestContext.toString().equals(recvdReqCtxId)) {
							if (msgJSON.has("parameters")) {
								m_parameters = msgJSON.getJSONObject("parameters");
							}
							// see if the decision is grant and within the expiry time
							if (m_decision != null && m_decision.equals("GRANT") && System.currentTimeMillis() < m_credentialExpiryTime) {
								respJSON = new JSONObject();
								respJSON.put("decision", "GRANT");
							} else {
								respJSON = executeCredentialRequest(m_requestContext);
							}
							respJSON.put("status", "requestCredentials");
						} else {
							LOG.warning("Received a message with a request Context Id that does not match the Request Context Id of this MessageInbound");
						}
					} else if (m_sessionId != null || m_parameters != null) {
						// this is the case of getting the MessageInbound from the cookie
						if (m_decision != null && m_decision.equals("GRANT") && System.currentTimeMillis() < m_credentialExpiryTime) {
							respJSON = new JSONObject();
							respJSON.put("decision", "GRANT");
//							respJSON.put("status", "requestCredentials");
						} else {
							respJSON = executeCredentialRequest(m_requestContext);
							respJSON.put("requestContextId", m_requestContext);
//							respJSON.put("status", "requestCredentials");
						}
					} else {
						respJSON = executeCredentialInput(m_resourceName, m_ASAPIBaseURL);
					}
					if (respJSON != null) {
						respJSON.put("requestContextId", m_requestContext.toString());
						CharBuffer charBuf = CharBuffer.wrap(respJSON.toString().toCharArray());
						getWsOutbound().writeTextMessage(charBuf);
					}
				} else {
					LOG.warning("Relying Party Websocket servlet received a message without the resourceName specified. Ignoring message");
				}
			} catch (JSONException ex) {
				LOG.severe("Exception occurred while parsing the message into JSON. Reason: " + ex.getLocalizedMessage());
			}
			startNetworkIdleTimer();
		}
	
		int evaluateCredentialsOnRestart() {
			int grantGoodForSeconds = -1;
			try {
				JSONObject respJSON = executeCredentialRequest(m_requestContext);
				if (respJSON != null) {
					respJSON.put("requestContextId", m_requestContext);
					if (respJSON.get("decision").equals("GRANT")) {
						String reqURL = m_ASAPIBaseURL + "/RNSResourceServer/cxBuilder/getCredentialFrequency/" + m_resourceName;
						IHttpClientUtil httpClientUtil = new HttpClientUtil();
						httpClientUtil.setupRequest(IHttpClientUtil.Method.GET, reqURL);
						IHttpClientUtil.IResponse resp = httpClientUtil.executeRequest();
						int respCode = resp.getResponseCode();
						if (respCode == HttpServletResponse.SC_OK) {
							String respData = resp.getResponseString(-1);
							JSONObject cxExpiryJSON = new JSONObject(respData);
							long grantGoodFor = cxExpiryJSON.getLong("timeout");
							m_credentialExpiryTime = System.currentTimeMillis() + grantGoodFor;
							respJSON.put("grantGoodFor", grantGoodFor);
							grantGoodForSeconds = (int)(grantGoodFor/1000 + 1);
						}	
					}
					CharBuffer charBuf = CharBuffer.wrap(respJSON.toString().toCharArray());
					getWsOutbound().writeTextMessage(charBuf);
				}
			} catch (Exception ex) {
				LOG.severe("Exception occurred trying to execute requestCredential API a second time. Reason: " + ex.getLocalizedMessage());
			} finally {
				return grantGoodForSeconds;
			}
		}
		
		public void shutdownPendingTasks() {
			m_idleShutdownExecutor.shutdownNow();
		}

		// Schedules an idle timeout to occur after the current message is processed.
		// when the idle timeout expires, it closes the websocket connection
		private synchronized void startNetworkIdleTimer() {
			final WsOutbound theOutbound = getWsOutbound();
			// remove all pending runnables
			if (m_idleShutdownExecutor instanceof ScheduledThreadPoolExecutor) {
				BlockingQueue<Runnable> pendingQueue = ((ScheduledThreadPoolExecutor)m_idleShutdownExecutor).getQueue();
				for (Runnable task: pendingQueue) {
					((ScheduledThreadPoolExecutor) m_idleShutdownExecutor).remove(task);
				}
				pendingQueue.clear();
			}
			m_idleShutdownExecutor.schedule(new ShutdownWSOutboundTask(theOutbound), DEFAULT_NETWORK_IDLETIMEOUT_INTERVAL, TimeUnit.MILLISECONDS);
		}
		
		private class ShutdownWSOutboundTask implements Runnable {
			
			private WsOutbound m_theOutbound;

			ShutdownWSOutboundTask(WsOutbound theOutbound) {
				m_theOutbound = theOutbound;
			}
			
			@Override
			public void run() {
			  try {
				  LOG.info("Closing the RelyingParty websocket connection due to idle timeout");
				  if (m_theOutbound != null) {
					  m_theOutbound.close(Constants.STATUS_CLOSE_NORMAL, null);
					  removeInboundConnection(m_requestContext);
				  }
			  } catch (IOException ioe) {
				  LOG.severe("Error occurred in closing the connection due to idle timeout, reason: " + ioe.getLocalizedMessage());
			  }
			}
			
		}
	}	
	
}
