package com.resilient.as.trusttag.websocket;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;

import com.resilient.as.trusttag.websocket.TrustTagWebSocketServlet.TrustTagMessageInbound;

public class TrustTagCookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(TrustTagCookieServlet.class.getName());
	
	private void setContextCookie(HttpServletRequest request, HttpServletResponse response, String appName, String cookieVal, int maxAge) {
		Cookie ctxCookie = null;
		if (request.getCookies() != null && request.getCookies().length > 0) {
			for (Cookie c : request.getCookies()) {
				if (c.getName().contentEquals("RNSTrustTag." + appName)) {
				    ctxCookie = c;
				    break;
				}
		    }
		}
		if (ctxCookie == null) {
			ctxCookie = new Cookie("RNSTrustTag." + appName, cookieVal);
			ctxCookie.setMaxAge(maxAge);
			ctxCookie.setPath("/");
			response.addCookie(ctxCookie);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String reqCtxId = request.getParameter("contextId");
		String resName = request.getParameter("resourceName");
		String action = request.getParameter("action");
		if (reqCtxId != null && !reqCtxId.isEmpty() && getServletContext().getAttribute("RelyingPartyState") != null) {
			try {
				if (getServletContext().getAttribute("RelyingPartyState") instanceof Map<?,?>) {
					Map<UUID, TrustTagMessageInbound> relyingPartyState = (Map<UUID, TrustTagMessageInbound>)getServletContext().getAttribute("RelyingPartyState");
					if (relyingPartyState.get(UUID.fromString(reqCtxId)) != null) {
						TrustTagMessageInbound mi = relyingPartyState.get(UUID.fromString(reqCtxId));
						if (mi != null) {
							int maxAge = mi.evaluateCredentialsOnRestart();
							if (maxAge != -1) {
								JSONObject cookieValJSON = new JSONObject();
								cookieValJSON.put("sessionId", mi.m_sessionId);
								cookieValJSON.put("parameters", mi.m_parameters);
								String cookieVal = cookieValJSON.toString();
								setContextCookie(request, response, mi.m_resourceName, cookieVal, maxAge);
							}
						}
						relyingPartyState.remove(mi);
					}
				}
				// write a blank page so the cookie gets set in the browser
				response.setContentType("text/html");
				response.getWriter().write("<html><head></head><body></body></html>");
			} catch (Exception ex) {
				LOG.severe("Failed to invoke the requestCredential API a second time and send decision to the client. Reason: " + ex.getLocalizedMessage());
			}
		} else if (resName != null && !resName.isEmpty()) {
			try {
				Cookie ctxCookie = null;
				if (request.getCookies() != null && request.getCookies().length > 0) {
					for (Cookie c : request.getCookies()) {
						if (c.getName().contentEquals("RNSRelyingParty." + resName)) {
						    ctxCookie = c;
						    break;
						}
				    }
				}
				if (action != null && action.equals("checkStatus")) {
					JSONObject respJSON = new JSONObject();
					if (ctxCookie != null) {
						String cookieVal = ctxCookie.getValue();
						JSONObject cookieValJSON = new JSONObject(cookieVal);
						respJSON.put("status", "GRANTED");
						respJSON.put("sessionId", cookieValJSON.getString("sessionId"));
						respJSON.put("parameters", cookieValJSON.getJSONObject("parameters"));
					} else {
						respJSON.put("status", "inputCredentials");
					}
					response.setContentType("application/json");
					response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
					response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
					response.setDateHeader("Expires", 0);
					String respData = "respJSON(" + respJSON.toString() + ")";
					response.getWriter().write(respData);
				} else if (action != null && action.equals("logout")) {
//					if (ctxCookie != null) {
//						response.setContentType("text/html");
						JSONObject respJSON = new JSONObject();
						respJSON.put("status", "SUCCESS");
						Cookie cookie = new Cookie("RNSRelyingParty." + resName, "");
						cookie.setMaxAge(0);
						cookie.setPath("/");
						response.addCookie(cookie);
						response.setContentType("application/json");
						response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
						response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
						response.setDateHeader("Expires", 0);
						String respData = "respJSON({status: SUCCESS})";
						response.getWriter().write(respData);
//						response.getWriter().write("<html><head></head><body></body></html>");
//					}
				}
			} catch (Exception ex) {
				LOG.severe("Failed to evaluate the checkStatus API. Reason: " + ex.getLocalizedMessage());
			}
		}
	}

}
