package com.resilient.resourceserver.websocket;


import javax.servlet.ServletException;

import com.resilient.tn.api.client.ClientSessionManager;
import com.resilient.tn.config.ClientConfigurator;
import com.resilient.tn.msg.MsgDispatchAgent;
import com.resilient.tn.msgHandler.MsgHandler;
import com.resilient.tn.network.MsgDeliveryAgent;
import com.resilient.tn.network.websocket.TNSDKWebSocketServlet;
import com.resilient.tn.network.websocket.WebSocketDispatchAgent;

public class PrimaryCSMWebSocketServlet extends	TNSDKWebSocketServlet {

	static final long serialVersionUID = 1L;
	
    public void init() throws ServletException {
    	Object configObj = this.getServletContext().getAttribute("Configurator");
    	if (configObj != null && configObj instanceof ClientConfigurator) {
    		this.m_msgHandler = (MsgHandler)((ClientConfigurator)configObj).getInstance(ClientSessionManager.class);
    		this.m_deliveryAgent = ((ClientConfigurator)configObj).getInstance(MsgDeliveryAgent.class);
    		setupNetworkReference((ClientConfigurator)configObj);
    	}
    }	
    
	protected MsgDispatchAgent getDispatchAgent() {
		MsgHandler theMsgHandler = this.m_msgHandler;
		Object configObj = this.getServletContext().getAttribute("Configurator");
    	if (configObj != null && configObj instanceof ClientConfigurator) {
    		theMsgHandler = (MsgHandler)((ClientConfigurator)configObj).getInstance(ClientSessionManager.class);
    	}
		return new WebSocketDispatchAgent(theMsgHandler);
	}
	
}
