package com.resilient.ts.util;

import java.util.Properties;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;

public class ConfigUtils {
	
	public static Properties loadPropertiesFile(Class forClass, String propFileName) throws IOException {
		Properties configProps = new Properties();
		loadPropertiesFile(forClass, configProps, propFileName);
		return configProps;
	}
	
	public static String getConfigFileLocation(String propFileName) {
		String rnsHome = System.getenv("RNS_HOME");
		if (rnsHome == null) {
			return null;
		}
		String configFileName = rnsHome + "/config/" + propFileName;
		if (new File(configFileName).exists()) {
			return configFileName;
		} 
		return null;
	}
	
	private static void loadPropertiesFile(Class forClass, Properties configProps, String propFileName) throws IOException {
    	String propFileFullPath = getConfigFileLocation(propFileName);
	    try {
			if (propFileFullPath != null) {
				configProps.load(new FileReader(propFileFullPath));
			} else {
				configProps.load(forClass.getResourceAsStream(propFileName));
	        	//configProps.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName));
			}
	    } catch (Exception e) {
			if (propFileFullPath == null)
				throw new IOException("Failed to load properties from classpath '" + propFileName + "'", e);
			else
				throw new IOException("Failed to load properties from file '" + propFileFullPath + "'", e);
	    }
	}

}
