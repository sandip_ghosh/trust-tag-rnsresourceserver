package com.resilient.resourceserver

import grails.test.*

class CredentialRequestorTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

	void testConstraints() {
		def credReq = new CredentialRequestor(decision: "GRANT")
		mockForConstraintsTests(CredentialRequestor, [credReq])
		assertTrue credReq.validate()
		
		def noDecision = new CredentialRequestor()
		assertFalse noDecision.validate()
		assertEquals "nullable", noDecision.errors["decision"]
	}
	
    void testCreateCredentialRequestor() {
		UUID sessionId = UUID.randomUUID()
		def credReq = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [credReq])
		def savedCredReq = credReq.save()
		assertNotNull "failed to save the credential requestor", savedCredReq
		assertEquals "the saved decision is not equal to the one created", credReq.decision, savedCredReq.decision
		assertEquals "the saved session id is not equal to the one created", credReq.sessionId, savedCredReq.sessionId
		def credReqId = savedCredReq.id
		assertTrue "The credential requestor id should not be 0", credReqId != 0
    }
	
	void testGetCredentialRequestor() {
		UUID sessionId = UUID.randomUUID()
		def credReq = new CredentialRequestor(decision: "DENY", sessionId: sessionId)
		mockDomain(CredentialRequestor, [credReq])
		def savedCredReq = credReq.save()
		assertNotNull "failed to save the credential requestor", savedCredReq
		def credReqId = savedCredReq.id
		def gotCredReq = CredentialRequestor.get(credReqId)
		assertNotNull "Failed to get the credential requestor from persistence", gotCredReq
		assertEquals "The saved session id does not match the one retrieved from persistence", savedCredReq.sessionId, gotCredReq.sessionId;
		assertEquals "The decision does not match", credReq.decision, gotCredReq.decision
		gotCredReq.delete()
		gotCredReq = CredentialRequestor.get(credReqId)
		assertNull "should not be able to retrieve deleted credential requestor", gotCredReq
	}
}
