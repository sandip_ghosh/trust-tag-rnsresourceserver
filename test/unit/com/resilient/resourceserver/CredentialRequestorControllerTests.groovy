package com.resilient.resourceserver

import grails.test.*

import javax.servlet.http.HttpServletRequest

import org.apache.commons.logging.Log
import org.codehaus.groovy.grails.plugins.codecs.MD5Codec
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.BaseResource
import com.resilient.as.ParameterMap

import org.springframework.mock.web.MockServletContext

import com.resilient.tn.config.ClientConfigurator;

class CredentialRequestorControllerTests extends ControllerUnitTestCase {
	static RESOURCE1 = new Resource (name: "Mediator", type: "data", hostname: "localhost", port: 8080, path: "/Mediator/studies/{studyid}")
	static PARAM1 = new ParameterMap(name: "user id", value: "userid", displayName: "User ID", isRequired: true, isStatic: false)
	static PARAM2 = new ParameterMap(name: "password", value: "", displayName: "Password", isRequired: false, isStatic: true)

	def logMessages = []
	
	protected void setUp() {
        super.setUp()
		def settings = new RSSettings(tbHostname: 'localhost', tbPort: 2909, defaultMessageTimeout: 100000, trustSessionTimeout: 100000, retryCount: 3, retryInterval: 10)
		mockDomain(RSSettings, [settings])
		settings.save()
		def crServiceControl = mockFor(RSSettingsService)
		crServiceControl.demand.getInstance(1..1) { -> settings }
		crServiceControl.demand.getPrimaryClientConfigurator(1..1) { -> null }
		crServiceControl.demand.getSecondaryClientConfigurator(0..1) { -> null }
		this.controller.RSSettingsService = crServiceControl.createMock()
//		this.controller.RSSettingsService = new RSSettingsService()
		
    }

    protected void tearDown() {
        super.tearDown()
    }

	/**
	* Verifies we get a 200 response and correct response body if Resource is present and the request body is correct
	*/
	void testInputCredentialsAPI_OK() {
		setupResource("data")
		this.controller.params.id = RESOURCE1.name
		def responseWriter = this.controller.inputCredentials()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertTrue "The response should have the inputsNeeded array", responseJSON.has("inputsNeeded")
		JSONArray requiredInputs = responseJSON.getJSONArray("inputsNeeded")
		assertNotNull "The response should have required input parameters", requiredInputs
		assertEquals "There should be one required input parameter", requiredInputs.size(), 1
		JSONObject requiredInputParam = requiredInputs.get(0)
		assertNotNull "The response should have 1 input needed parameter", requiredInputParam
		assertEquals "The name of the required input param is wrong", requiredInputParam.getString("name"), PARAM1.value
		assertEquals "The label of the required input param is wrong", requiredInputParam.getString("label"), PARAM1.displayName
	}


	/**
	 * Verifies we get 404 response code if the Resource is not present
	 */
    void testInputCredentialsAPI_404() {
		setupResource("data")		
		this.controller.params.id = "foobar"
		def responseWriter = this.controller.inputCredentials()
		def responseBody = responseWriter.out.lock.toString() 
		assertEquals "The response code should be 404", this.controller.response.status, 404
		assertNotNull "The response should not be null", responseBody
		assertEquals "The response body should be empty", responseBody, "[]"
    }

	/**
	 * Verifies we get 200 response code and correct response body for TB_DISPLAY decision
	 */
	void testRequestCredentialAPI_TBD() {
		setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
//		this.controller.request.setMethod("POST")
		this.controller.request.setContent(requestBody.toString().getBytes())
		def sessionId = UUID.randomUUID().toString()
		def tbdContextId = UUID.randomUUID().toString()
		def expectedCRObj = new CredentialRequestor(decision: "TB_DISPLAY", trustBrokerDisplayURL: "http://localhost:8082/?contextId=" + tbdContextId, sessionId: sessionId)
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The trust broker display URL is not correct", responseJSON.getString("trustBrokerDisplayURL"), expectedCRObj.trustBrokerDisplayURL
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId
		crServiceControl.verify()
	}

	/**
	* Verifies we get 200 response code and correct response body for GRANT decision for a HTTP/REST resource
	*/
	void testRequestCredentialAPI_GRANT() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId)
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId
		assertTrue "The response does not have the resource config info", responseJSON.has("resourceConfig")
		JSONObject resourceConfig = responseJSON.getJSONObject("resourceConfig")
		assertNotNull "The resource config object should not be null", resourceConfig
		assertEquals "The resource name is not correct", resourceConfig.getString("name"), res.name
		assertEquals "The resource type is not correct", resourceConfig.getString("type"), res.type
		assertEquals "The hostname is not correct", resourceConfig.getString("hostname"), res.hostname
		assertEquals "The port is not correct", resourceConfig.getInt("port"), res.port
		assertEquals "The path is not correct", resourceConfig.getString("path"), res.path
		assertEquals "The display type is not correct", resourceConfig.getString("displayType"), res.displayType
		crServiceControl.verify()
	}

	/**
	* Verifies we get 200 response code and correct response body for GRANT decision for a PEP resource (ICIX use case)
	*/
	void testRequestCredentialAPI_GRANT_NoProxy() {
		def res = setupResource("noProxy")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId.toString())
		this.controller.params.id = RESOURCE1.name
//		this.controller.request.setMethod("POST")
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId.toString()
		assertFalse "The response does not have the resource config info", responseJSON.has("resourceConfig")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 401 response code and correct response body for DENY decision
	*/
	void testRequestCredentialAPI_DENY() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "foo.bar")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID()
		requestBody.put("sessionId", sessionId.toString())
		this.controller.params.id = RESOURCE1.name
//		this.controller.request.setMethod("POST")
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "DENY")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 401", this.controller.response.status, 401
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 404 response code and correct response body if resource is not present
	*/
	void testRequestCredentialAPI_404() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID()
		requestBody.put("sessionId", sessionId.toString())
		this.controller.params.id = "foobar"
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 404", this.controller.response.status, 404
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 400 response code and correct response body if the request body has errors
	*/
	void testRequestCredentialAPI_400() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 400", this.controller.response.status, 400
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 401 response code and response body with TIMEOUT decision if the Trust Broker is not available
	*/
	void testRequestCredentialAPI_Timeout() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "TIMEOUT", message: "Timed out while waiting for the response from Trust Broker")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(3..3) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 401", this.controller.response.status, 401
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		assertTrue "There should be an exception message", responseJSON.has("message")
		assertEquals "The exception message is not what was expected", responseJSON.getString("message"), expectedCRObj.message
		crServiceControl.verify()
	}

	/**
	* Verifies we get 500 response code and correct response body if there are errors executing the TNSDK operations
	*/
	void testRequestCredentialAPI_500() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR", message: "ClientException: some bogus exception")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 500", this.controller.response.status, 500
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		assertTrue "There should be an exception message", responseJSON.has("message")
		assertEquals "The exception message is not what was expected", responseJSON.getString("message"), expectedCRObj.message
		crServiceControl.verify()
	}

	/**
	* Verifies we get 500 response code and correct response body if there is an exception in the API
	*/
	void testRequestCredentialAPI_Exception() {
		def res = setupResource("data")
		this.controller.params.id = RESOURCE1.name
		def expectedCRObj = new CredentialRequestor(decision: "ERROR")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 500", this.controller.response.status, 500
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		assertTrue "There should be an exception message", responseJSON.has("message")
		crServiceControl.verify()
	}

	/**
	* Simulates the second call to requestCredential after TBD and verifies the response code is 200 and the response body is correct
	*/
	void testRequestCredentialAPI_PostTBD() {
		def res = setupResource("data")
		// simulate first requestCredential call
		def sessionId = UUID.randomUUID().toString()
		def tbdContextId = UUID.randomUUID().toString()
		def expectedCRObj = new CredentialRequestor(decision: "TB_DISPLAY", trustBrokerDisplayURL: "http://localhost:8082/?contextId=" + tbdContextId, sessionId: sessionId)
		mockDomain(CredentialRequestor, [expectedCRObj])
		expectedCRObj = expectedCRObj.save()
		// simulate restart
		this.controller.params.tsid = sessionId
		this.controller.params.decision = "GRANT"
		this.controller.restart()
		
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("userid", "john.left")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		requestBody.put("sessionId", sessionId)
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		expectedCRObj = CredentialRequestor.findBySessionId(sessionId)
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId.toString()
		assertTrue "The response does not have the resource config info", responseJSON.has("resourceConfig")
		JSONObject resourceConfig = responseJSON.getJSONObject("resourceConfig")
		assertNotNull "The resource config object should not be null", resourceConfig
		assertEquals "The resource name is not correct", resourceConfig.getString("name"), res.name
		assertEquals "The resource type is not correct", resourceConfig.getString("type"), res.type
		assertEquals "The hostname is not correct", resourceConfig.getString("hostname"), res.hostname
		assertEquals "The port is not correct", resourceConfig.getInt("port"), res.port
		assertEquals "The path is not correct", resourceConfig.getString("path"), res.path
		assertEquals "The display type is not correct", resourceConfig.getString("displayType"), res.displayType
		crServiceControl.verify()
	}

	/**
	* Verifies we get 200 response code and correct response body for TB_DISPLAY decision
	*/
	void testLogoutAPI_TBD() {
		setupResource("data")
		JSONObject requestBody = new JSONObject()
		def sessionId = UUID.randomUUID().toString()
		def tbdContextId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		mockDomain(CredentialRequestor, [])
		def expectedCRObj = new CredentialRequestor(decision: "TB_DISPLAY", trustBrokerDisplayURL: "http://localhost:8082/?contextId=" + tbdContextId, sessionId: sessionId)
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The trust broker display URL is not correct", responseJSON.getString("trustBrokerDisplayURL"), expectedCRObj.trustBrokerDisplayURL
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId
		crServiceControl.verify()
	}

	/**
	* Verifies we get 200 response code and correct response body for GRANT decision
	*/
	void testLogoutAPI_GRANT() {
		def res = setupResource("noProxy")
		JSONObject requestBody = new JSONObject()
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId.toString())
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		crServiceControl.verify()
	}

	/**
	* Verifies we get 401 response code and correct response body for DENY decision
	*/
	void testLogoutAPI_DENY() {
		def res = setupResource("noProxy")
		JSONObject requestBody = new JSONObject()
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId.toString())
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "DENY", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 401", this.controller.response.status, 401
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		crServiceControl.verify()
	}

	/**
	* Verifies we get 404 response code and correct response body if resource is not present
	*/
	void testLogoutAPI_404() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID()
		requestBody.put("sessionId", sessionId.toString())
		this.controller.params.id = "foobar"
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR", message: "The specified resource could not be found")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 404", this.controller.response.status, 404
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertTrue "The message should be present", responseJSON.has("message")
		assertEquals "The message is not correct", responseJSON.getString("message"), expectedCRObj.message
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 400 response code and correct response body if the request body has errors
	*/
	void testLogoutAPI_400() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		def sessionId = UUID.randomUUID()
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR", message: "The 'sessionId' and 'callbackURL' parameters must be passed in the request to logout API")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 400", this.controller.response.status, 400
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertTrue "The message should be present", responseJSON.has("message")
		assertEquals "The message is not correct", responseJSON.getString("message"), expectedCRObj.message
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		crServiceControl.verify()
	}

	/**
	* Verifies we get 500 response code and correct response body if there are errors executing the TNSDK operations
	*/
	void testLogoutAPI_500() {
		def res = setupResource("data")
		JSONObject requestBody = new JSONObject()
		def sessionId = UUID.randomUUID()
		requestBody.put("sessionId", sessionId.toString())
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "ERROR", message: "ClientException: some bogus exception")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 500", this.controller.response.status, 500
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		assertTrue "There should be an exception message", responseJSON.has("message")
		assertEquals "The exception message is not what was expected", responseJSON.getString("message"), expectedCRObj.message
		crServiceControl.verify()
	}
	
	/**
	* Verifies we get 500 response code and correct response body if there is an exception in the API
	*/
	void testLogoutAPI_Exception() {
		def res = setupResource("data")
		this.controller.params.id = RESOURCE1.name
		def expectedCRObj = new CredentialRequestor(decision: "ERROR")
		mockDomain(CredentialRequestor, [expectedCRObj])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 500", this.controller.response.status, 500
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertFalse "There should not be a session Id in the response", responseJSON.has("sessionId")
		assertTrue "There should be an exception message", responseJSON.has("message")
		crServiceControl.verify()
	}

	/**
	* Simulates the second call to requestCredential after TBD and verifies the response code is 200 and the response body is correct
	*/
	void testLogoutAPI_PostTBD() {
		def res = setupResource("data")
		// simulate first requestCredential call
		def sessionId = UUID.randomUUID().toString()
		def tbdContextId = UUID.randomUUID().toString()
		def expectedCRObj = new CredentialRequestor(decision: "TB_DISPLAY", trustBrokerDisplayURL: "http://localhost:8082/?contextId=" + tbdContextId, sessionId: sessionId)
		mockDomain(CredentialRequestor, [expectedCRObj])
		expectedCRObj = expectedCRObj.save()
		// simulate restart
		this.controller.params.tsid = sessionId
		this.controller.params.decision = "GRANT"
		this.controller.restart()
		
		JSONObject requestBody = new JSONObject()
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		requestBody.put("sessionId", sessionId)
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		expectedCRObj = CredentialRequestor.findBySessionId(sessionId)
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.logout(0..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.logout()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		crServiceControl.verify()
	}

	public void testBasicAuditTrailLog() {
		RSSettings settings = this.controller.RSSettingsService.getInstance()
		settings.auditLogLevel = "basic"
		settings.save()
		def crSettingsServiceControl = mockFor(RSSettingsService)
		crSettingsServiceControl.demand.getInstance(1..1) { -> settings }
		crSettingsServiceControl.demand.getPrimaryClientConfigurator(1..1) { -> null }
		crSettingsServiceControl.demand.getSecondaryClientConfigurator(0..1) { -> null }
		this.controller.RSSettingsService = crSettingsServiceControl.createMock()

		setupCustomLogger(this.getClass())
		def res = setupResource("noProxy")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("username", "sony.powell")
		paramsObj.put("password", "sony")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId)
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId
//		assertEquals "Should have two entries in logMessages", logMessages.size(), 2
//		assertEquals "The first log entry is not correct"
		crServiceControl.verify()

	}	
	
	public void testDetailAuditTrailLog() {
		RSSettings settings = this.controller.RSSettingsService.getInstance()
		settings.auditLogLevel = "detailed"
		settings.save()
		def crSettingsServiceControl = mockFor(RSSettingsService)
		crSettingsServiceControl.demand.getInstance(1..1) { -> settings }
		crSettingsServiceControl.demand.getPrimaryClientConfigurator(1..1) { -> null }
		crSettingsServiceControl.demand.getSecondaryClientConfigurator(0..1) { -> null }
		this.controller.RSSettingsService = crSettingsServiceControl.createMock()
		
		loadCodec(MD5Codec.class)
		setupCustomLogger(this.getClass())
		def res = setupResource("noProxy")
		JSONObject requestBody = new JSONObject()
		JSONObject paramsObj = new JSONObject()
		paramsObj.put("username", "sony.powell")
		paramsObj.put("password", "sony")
		requestBody.put("parameters", paramsObj)
		requestBody.put("callbackURL", "http://www.resilient-networks.com")
		def sessionId = UUID.randomUUID().toString()
		requestBody.put("sessionId", sessionId)
		this.controller.params.id = RESOURCE1.name
		this.controller.request.setContent(requestBody.toString().getBytes())
		def expectedCRObj = new CredentialRequestor(decision: "GRANT", sessionId: sessionId)
		mockDomain(CredentialRequestor, [])
		def crServiceControl = mockFor(CredentialRequestorService)
		crServiceControl.demand.requestCredential(1..1) { Resource resource, HttpServletRequest req, ClientConfigurator cc -> expectedCRObj }
		this.controller.credentialRequestorService = crServiceControl.createMock()
		def responseWriter = this.controller.requestCredential()
		def responseBody = responseWriter.out.lock.toString()
		assertEquals "The response code should be 200", this.controller.response.status, 200
		assertNotNull "The response should not be null", responseBody
		JSONObject responseJSON = new JSONObject(responseBody)
		assertNotNull "The response body is not valid JSON", responseJSON
		assertEquals "The decision is not correct", responseJSON.getString("decision"), expectedCRObj.decision
		assertEquals "The session ID is not correct", responseJSON.getString("sessionId"), sessionId
//		assertEquals "Should have two entries in logMessages", logMessages.size(), 2
//		assertEquals "The first log entry is not correct"
		crServiceControl.verify()

	}
	
	private void setupCustomLogger(Class clazz) {

		def pos = clazz.name.lastIndexOf('.')
		if (pos != -1) pos = clazz.name.lastIndexOf('.', pos - 1)
		def shortName = clazz.name.substring(pos + 1)

		def outputLog = {String key, def msg, Throwable t = null ->
			def logMsg = "$key (${shortName}): $msg"
			println logMsg
			logMessages.add(logMsg)
			if (t) {
				println "       Exception thrown - ${t.message}"
			}
		}
		
		// Dynamically inject a mock logger that simply prints the
		// log message (and optional exception) to stdout.
		def myMockLogger = [
			fatal: { def msg, Throwable t = null ->
				outputLog('FATAL', msg, t)
			},
			error: { def msg, Throwable t = null ->
				outputLog('ERROR', msg, t)
			},
			warn: { def msg, Throwable t = null ->
				outputLog('WARN', msg, t)
			},
			info: { def msg, Throwable t = null ->
				outputLog('INFO', msg, t)
			},
			debug: { def msg, Throwable t = null ->
				outputLog('DEBUG', msg, t)
			},
			trace: { String msg, Throwable t = null -> },
			isFatalEnabled: {-> true},
			isErrorEnabled: {-> true},
			isWarnEnabled: {-> true},
			isInfoEnabled: {-> true},
			isDebugEnabled: {-> true},
			isTraceEnabled: {-> false} ] as Log
		
		clazz.metaClass.getLog = {-> myMockLogger }
	}
	
	private void flushLog() {
		logMessages = []
	}
	
	private Resource setupResource(String resourceType) {
		mockDomain(Parameter, [PARAM1, PARAM2])
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE, parameterMapping: [PARAM1, PARAM2])
		mockDomain(CredentialElement, [cred])
		def savedCred = cred.save()
		
		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])
		
		def res = new Resource (name: RESOURCE1.name, type: resourceType, credentialExpr: credExpr)
		if (resourceType == "data") {
			res.hostname = RESOURCE1.hostname
			res.port = RESOURCE1.port
			res.path = RESOURCE1.path
			res.displayType = "download"
		} else {
			res.hostname = ""
			res.port = 0
			res.path = ""
		}
		mockDomain(BaseResource, [res])
//		credExpr.resource = res
		def savedRes = res.save()
		assertNotNull "Failed to save the resource", savedRes
		return savedRes
	}	
	
}
