package com.resilient.resourceserver

import grails.test.*

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import com.resilient.as.BaseResource
import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.ParameterMap
import com.resilient.ta.TrustAuthorityDiscoveryServiceFactory

class ResourceTests extends GrailsUnitTestCase {
	static String RESOURCE_NAME = "MEDIATOR"
	static String RESOURCE_TYPE = "data";
	static String RESOURCE_HOSTNAME = "localhost"
	static int RESOURCE_PORT = 8080
	static String RESOURCE_PATH = "/Mediator/studies/{studyid}"
	
    protected void setUp() {
        super.setUp()
		TrustAuthorityDiscoveryServiceFactory.setTestContext(true)
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		def credExpr = new CredentialExpression(credentialElements: [cred])
		def res = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		mockForConstraintsTests( Resource)
		mockDomain(BaseResource, [res])
		assertTrue res.validate()
		
		def noPropsRes = new Resource()
		assertFalse noPropsRes.validate()
//		assertEquals "nullable", noPropsRes.errors["name"]
//		assertEquals "nullable", noPropsRes.errors["hostname"]
//		assertEquals "nullable", noPropsRes.errors["path"]
		assertEquals "nullable", noPropsRes.errors["credentialExpr"]
		
		def noCredentialRes = new Resource (name: "foo", type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH)
		assertFalse noCredentialRes.validate()
		assertEquals "nullable", noCredentialRes.errors["credentialExpr"]
		
		def invalidTypeRes = new Resource (name: "foobar", type: "web-service", hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH)
		assertFalse invalidTypeRes.validate()
		assertEquals "inList", invalidTypeRes.errors["type"]

//		def invalidNoProxyRes = new Resource(name: "test", type: "noProxy")
//		assertFalse invalidNoProxyRes.validate()
//		assertEquals "minSize", noPropsRes.errors["credentials"]
//		
//		def validNoProxyRes = new Resource(name: "test", type: "noProxy", credentials: [cred])
//		assertFalse validNoProxyRes.validate()
//		assertEquals "minSize", validNoProxyRes.errors["credentials"]
		
		def notUniqueRes = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		assertFalse notUniqueRes.validate()
		assertEquals "validator", notUniqueRes.errors["name"]
		
		def invalidCharsRes = new Resource (name: "This Resource has *&^ chars", type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		assertFalse invalidCharsRes.validate()
		assertEquals "matches", invalidCharsRes.errors["name"]

    }
	
	void testCreateResource() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])
		def savedCred = cred.save()
		
		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])
		
		def res = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		mockDomain(BaseResource, [res])
		mockDomain(Resource, [res])
//		credExpr.resource = res
		def savedRes = res.save()
		assertNotNull "Failed to save the resource", savedRes
		
		def resId = savedRes.id
		assertTrue "The resource id should not be 0", resId != 0
		def lastUpdateCalDate = Calendar.getInstance()
		lastUpdateCalDate.setTime(savedRes.lastUpdated)
		def today = Calendar.getInstance()
		assertEquals "The last update date is not correct", lastUpdateCalDate.get(Calendar.DAY_OF_MONTH), today.get(Calendar.DAY_OF_MONTH)
		
		// make sure all the resource properties are as expected
		assertEquals "The resource name is not correct", savedRes.name, RESOURCE_NAME
		assertEquals "The resource type is not correct", savedRes.type, RESOURCE_TYPE
		assertEquals "The resource hostname is not correct", savedRes.hostname, RESOURCE_HOSTNAME
		assertEquals "The resource port is not correct", savedRes.port, RESOURCE_PORT
		assertEquals "The resource path is not correct", savedRes.path, RESOURCE_PATH
		assertNotNull "The credential expression should not be null", savedRes.credentialExpr
		assertEquals "The number of credentials is not correct", savedRes.credentialExpr.credentialElements.size(), 1
		assertEquals "The credential is not correct", savedRes.credentialExpr.credentialElements.iterator().next(), savedCred
	}
	
	void testGetResource() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])
		def savedCred = cred.save()
		
		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		mockDomain(BaseResource, [res])
		mockDomain(Resource, [res])
		def savedRes = res.save()
		assertNotNull "Failed to save the resource", savedRes
		def resId = savedRes.id
		def gotRes = Resource.get(resId)
		assertNotNull "Failed to get the resource from persistence", gotRes
		assertEquals "The resource name saved does not match the one retrieved from persistence", savedRes.name, gotRes.name;
		assertEquals "The resource type saved does not match the one retrieved from persistence", savedRes.type, gotRes.type;
		assertEquals "The resource hostname saved does not match the one retrieved from persistence", savedRes.hostname, gotRes.hostname;
		assertEquals "The resource port saved does not match the one retrieved from persistence", savedRes.port, gotRes.port;
		assertEquals "The resource path saved does not match the one retrieved from persistence", savedRes.path, gotRes.path;
		assertNotNull "The credential expression should not be null", gotRes.credentialExpr
		assertEquals "The resource number of credentials saved does not match the one retrieved from persistence", savedRes.credentialExpr.credentialElements.size(), gotRes.credentialExpr.credentialElements.size();
		assertEquals "The resource credentials saved does not match the one retrieved from persistence", savedRes.credentialExpr.credentialElements.iterator().next(), gotRes.credentialExpr.credentialElements.iterator().next();
	}

	void testUpdateDeleteResource() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])
		def savedCred = cred.save()

		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		mockDomain(BaseResource, [res])
		mockDomain(Resource, [res])
		def savedRes = res.save()
		assertNotNull "Failed to save the resource", savedRes
		def resId = savedRes.id
		def gotRes = Resource.get(resId)
	
		gotRes.name = "Updated_Name"
		def updatedRes = gotRes.save()
		assertNotNull "Failed to update the resource", updatedRes
		assertEquals "The resource name did not get updated", updatedRes.name, "Updated_Name"
		
		gotRes.credentialExpr = null
		def failedUpdateRes = gotRes.save()
		assertNull "The resource save should fail due to validation failure", failedUpdateRes
		
		// Delete the resource
		gotRes.delete()
		gotRes = Resource.get(resId)
		assertNull "Should not get the resource after delete", gotRes
	}
	
	void testDeepJSONConversion() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])
		def savedCred = cred.save()

		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res = new Resource (name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
		mockDomain(BaseResource, [res])
		mockDomain(Resource, [res])
		def savedRes = res.save()
		assertNotNull "Failed to save the resource", savedRes
		def resId = savedRes.id
		def gotRes = Resource.get(resId)
		assertNotNull "Failed to get the resource from persistence", gotRes

		def resJSON = gotRes.outputJSON()
		assertNotNull "Did not get the output JSON for the resource", resJSON
		JSONObject resJSONObj = new JSONObject(resJSON) 
		assertNotNull "Failed to parse the JSON output to a JSON object", resJSONObj
		assertEquals "The name does not match the expected name", resJSONObj.getString("name"), RESOURCE_NAME
		assertEquals "The type does not match the expected type", resJSONObj.getString("type"), RESOURCE_TYPE
		assertEquals "The hostName does not match the expected hostname", resJSONObj.getString("hostname"), RESOURCE_HOSTNAME
		assertEquals "The port does not match the expected port", resJSONObj.getInt("port"), RESOURCE_PORT
		assertEquals "The path does not match the expected path", resJSONObj.getString("path"), RESOURCE_PATH
		JSONArray credArray = resJSONObj.getJSONArray("credentials")
		assertNotNull "The credentials array is null", credArray
		assertEquals "There should be one credential in the array", credArray.size(), 1
		JSONObject credJSONObj = credArray.getJSONObject(0)
		assertNotNull "The credential in the credentials array is null", credJSONObj
		assertEquals "The credential name does not match the expected name", credJSONObj.getString("name"), Constants.CREDENTIAL_NAME
		assertEquals "The granting authority does not match the expected granting authority", credJSONObj.getString("grantingAuthority"), Constants.GRANTING_AUTHORITY
		assertEquals "The frequency type does not match the expected frequency type", credJSONObj.getString("frequencyType"), Constants.FREQUENCY_TYPE
		assertEquals "The max age does not match the expected max age", credJSONObj.getInt("maxAge"), Constants.MAX_AGE
	}
	
	void testCredentialExpr() {
//		def userParam1 = new ParameterMap(name: "user id", value: "userid", isRequired: true, isStatic: false)
//		def passwordParam1 = new ParameterMap(name: "password", value: "", isRequired: false, isStatic: true)
//		def cred1 = new ASCredential(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE, parameterMapping: [userParam1, passwordParam1])
//		def userParam2 = new ParameterMap(name: "user id", value: "john.left@resilient.com", isRequired: true, isStatic:true)
//		def passwordParam2 = new ParameterMap(name: "password", value: "john.left@resilient.com", isRequired: true, isStatic:true)
//		def cred2 = new ASCredential(name: Constants.CREDENTIAL_NAME2, grantingAuthority: Constants.GRANTING_AUTHORITY2, frequencyType: "SameSession", maxAge: Constants.MAX_AGE, parameterMapping: [userParam2, passwordParam2])
//		def credExpr = new CredentialExpression(joinType: "And", credentials: [cred1, cred2])
//		def res = new Resource(name: RESOURCE_NAME, type: RESOURCE_TYPE, hostname: RESOURCE_HOSTNAME, port: RESOURCE_PORT, path: RESOURCE_PATH, credentialExpr: credExpr)
//		def credExprStr = res.credentialExpr()
//		System.out.println "Credential Expression: " + credExprStr
//		assertNotNull "The credential expression built should not be null", credExprStr
//		JSONObject credExprJSON = new JSONObject(credExprStr)
//		assertNotNull "The credential expression is not valid JSON", credExprJSON
//		assertEquals "The node name is not correct", credExprJSON.getString("nodeName"), "And"
//		JSONArray credElementsArray = credExprJSON.getJSONArray("children")
//		assertNotNull "Did not find the children array of credential elements", credElementsArray
//		assertEquals "There should be two child credential elements", credElementsArray.size(), 2
//		JSONObject credElementJSON1 = credElementsArray.get(0)
//		JSONObject credElementJSON2 = null
//		// sets don't return in definite order so its hard to make these tests reliable
//		// so check the name and make sure the rest of the params are as expected
//		if (credElementJSON1.getJSONObject("credentialIdentifier").get("name").equals(Constants.CREDENTIAL_NAME)) {
//			credElementJSON2 = credElementsArray.get(1)
//		} else {
//			credElementJSON2 = credElementsArray.get(0)
//			credElementJSON1 = credElementsArray.get(1)
//		}
//		def credIndentifier = credElementJSON1.getJSONObject("credentialIdentifier")
//		assertEquals "The credential element1 name does not match expected name", credIndentifier.get("name"), Constants.CREDENTIAL_NAME
//		def grantingAuthorities = credElementJSON1.getJSONArray("grantingAuthorities").get(0)
//		def grantingAuthObj = grantingAuthorities.get(0)
//		assertEquals "The granting authority is not correct", grantingAuthObj.get("uri"), Constants.GRANTING_AUTHORITY
//		def parameters = credElementJSON1.getJSONArray("parameters")
//		assertEquals "The number of parameters is not correct", parameters.length(), 1
//		def parameterObj = parameters.get(0)
//		assertEquals "The parameter name is not correct", parameterObj.get("key"), "user id"
//		assertEquals "The parameter value is not correct", parameterObj.get("value"), "<userid>"
//		credIndentifier = credElementJSON2.getJSONObject("credentialIdentifier")
//		assertEquals "The credential element2 name does not match expected name", credIndentifier.get("name"), Constants.CREDENTIAL_NAME2
//		grantingAuthorities = credElementJSON2.getJSONArray("grantingAuthorities").get(0)
//		grantingAuthObj = grantingAuthorities.get(0)
//		assertEquals "The granting authority is not correct", grantingAuthObj.get("uri"), Constants.GRANTING_AUTHORITY2
//		parameters = credElementJSON2.getJSONArray("parameters")
//		assertEquals "The number of parameters is not correct", parameters.length(), 2
//		parameterObj = parameters.get(0)
//		assertEquals "The parameter name is not correct", parameterObj.get("key"), "user id"
//		assertEquals "The parameter value is not correct", parameterObj.get("value"), "john.left@resilient.com"
//		parameterObj = parameters.get(1)
//		assertEquals "The parameter name is not correct", parameterObj.get("key"), "password"
//		assertEquals "The parameter value is not correct", parameterObj.get("value"), "john.left@resilient.com"
	}
	
}
