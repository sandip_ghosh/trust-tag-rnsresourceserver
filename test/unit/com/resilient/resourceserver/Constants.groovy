package com.resilient.resourceserver

class Constants {

	static String CREDENTIAL_EXPR_OLD = "{TC=\"spec.resilient.com/Credentials.SimpleLogin\" Parameters[userid=\"Joe.Left@foo.com\"] TSP=\"rtn://localhost:2911/resilient_test_trust_service\"}"
	static String CREDENTIAL_NAME = "spec.resilient.com/Credentials.SimpleLogin"
	static String GRANTING_AUTHORITY = "rtn://localhost:2911/resilient_test_trust_service"
	static String CREDENTIAL_NAME2 = "spec.resilient.com/Credentials.SimpleLogin2"
	static String GRANTING_AUTHORITY2 = "rtn://localhost:2912/resilient_test_trust_service"
	static String FREQUENCY_TYPE = "SameSession"
	static int MAX_AGE = 20
	static int MAX_AGE_MILLIS = 1200000
	static String MAX_AGE_PARAM = "20"
	static String CREDENTIAL_EXPR_NEW = "{ \n" +
										"	\"nodeName\" : \"CredentialElement\", \n" +
										"	\"credentialIdentifier\" : { \n" +
										  "		\"name\" : \"spec.resilient.com/Credentials.SimpleLogin\" \n" +
										"	}, \n" +
										"	\"grantingAuthorities\" : [ [ { \n" +
										"		\"uri\" : \"rtn://localhost:2911/resilient_test_trust_service\" \n" +
										"	} ] ], \n" +
										"	\"parameters\" : [ \n" +
										"		{ \n " +
										"			\"historyKey\" : true, \n" +
										"			\"key\"        : \"userid\", \n" +
										"			\"value\"      : \"<userid>\" \n" +
										"		} \n" +
										"	], \n" +
										"	\"frequency\" : [ { \n" +
										"		\"nodeName\" : \"SameSession\", \n" +
										"		\"maxAge\" : 60000 \n" +
										"	} ] \n" +
										"}"

}
