package com.resilient.resourceserver

import grails.test.*

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

import org.codehaus.groovy.runtime.DefaultGroovyMethods

import com.resilient.ta.TrustAuthorityDiscoveryServiceFactory

import com.resilient.as.CredentialElement
import com.resilient.as.CredentialExpression
import com.resilient.as.BaseResource
import com.resilient.as.ParameterMap

import com.resilient.as.relyingparty.RelyingPartyUtils

class ResourceControllerTests extends ControllerUnitTestCase {
	static RESOURCE1 = new Resource (name: "Mediator", type: "data", hostname: "localhost", port: 8080, path: "/Mediator/studies/{studyid}")
	static RESOURCE2 = new Resource (name: "RedCap", type: "app", hostname: "redcap.resilient-networks.com", port: 80, path: "/redcap2/index.php")

	protected void setUp() {
		super.setUp()
		TrustAuthorityDiscoveryServiceFactory.setTestContext(true)
		def settings = new RSSettings(tbHostname: 'localhost', tbPort: 2909, defaultMessageTimeout: 100000, trustSessionTimeout: 100000)
		mockDomain(RSSettings, [settings])
		settings.save()
		this.controller.RSSettingsService = new RSSettingsService()
		DefaultGroovyMethods.mixin(ResourceController, [RelyingPartyUtils])
	}

	protected void tearDown() {
		super.tearDown()
	}

	void testIndex() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])

		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		def res2 = new Resource (name: RESOURCE2.name, type: RESOURCE2.type, hostname: RESOURCE2.hostname, port: RESOURCE2.port, path: RESOURCE2.path)
		res1.credentialExpr = credExpr
		res2.credentialExpr = credExpr

		mockDomain(Resource, [res1, res2])

		def model = this.controller.index()

		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resourcess model should have the Resources created", model["resources"]?.size(), 2
	}

	void testCreateValidationErrors() {
		// the path and credential are not set
		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		mockDomain(BaseResource, [res1])
		mockDomain(Resource, [res1])
		this.controller.params.name = RESOURCE2.name
		this.controller.params.type = RESOURCE2.type
		this.controller.params.hostname = RESOURCE2.hostname
		this.controller.params.port = RESOURCE2.port
		this.controller.update()
		assertNotNull "The flash object should be set with the validation error", this.controller.flash
		assertNotNull "The flash object should be set with the validation error", this.controller.flash.errorObj
		assertTrue "There shoud be validation errors", this.controller.flash.errorObj.errors.hasErrors()
		assertEquals "There should be two validation error", this.controller.flash.errorObj.errors.getErrorCount(), 1
	}

/*	
	void testNoProxyValidationErrors() {
		// the path and credential are not set
		mockDomain(Resource, [])
		this.controller.params.name = "testapp"
		this.controller.params.type = "noProxy"
		this.controller.update()
		assertNotNull "The flash object should be set with the validation error", this.controller.flash
		assertNotNull "The flash object should be set with the validation error", this.controller.flash.errorObj
		assertTrue "There shoud be validation errors", this.controller.flash.errorObj.errors.hasErrors()
		assertEquals "There should be two validation error", this.controller.flash.errorObj.errors.getErrorCount(), 1
		
		this.controller.params.name = "testapp"
		this.controller.params.type = "noProxy"
		this.controller.params.selectedCredentials = "[{name: 'spec.resilient.com/Credentials.SimpleLogin', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2911/resilient_test_trust_service'}]"
		this.controller.params.frequencyType = Constants.FREQUENCY_TYPE
		this.controller.params.maxAge = Constants.MAX_AGE_PARAM
		this.controller.update()
		assertNull "There were errors in creating a resource", this.controller.flash.errorObj
		def model = this.controller.index()
		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resources model should have the Resources created", model["resources"]?.size(), 1
	}
*/	
	void testUpdateValidationErrors() {
		// violate the unique constraint while updating Resource
		mockDomain(CredentialElement, [])

		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		def res2 = new Resource (name: RESOURCE2.name, type: RESOURCE2.type, hostname: RESOURCE2.hostname, port: RESOURCE2.port, path: RESOURCE2.path)
		mockDomain(Resource, [res1, res2])

		this.controller.params.id = res2.id
		this.controller.params.name = res1.name
		this.controller.params.selectedCredentials = "[{name: 'spec.resilient.com/Credentials.SimpleLogin', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2911/resilient_test_trust_service'}]"
		this.controller.params.frequencyType = Constants.FREQUENCY_TYPE
		this.controller.params.maxAge = Constants.MAX_AGE_PARAM
		this.controller.params.port = res1.port
		this.controller.update()
		assertNotNull "The flash object should be set with the validation error", this.controller.flash
		assertNotNull "The flash object should be set with the validation error", this.controller.flash.errorObj
		assertTrue "There shoud be validation errors", this.controller.flash.errorObj.errors.hasErrors()
		assertEquals "There should be one validation error", this.controller.flash.errorObj.errors.getErrorCount(), 1
	}

	void testCreateUpdate() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])

		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		res1.credentialExpr = credExpr

		mockDomain(BaseResource, [res1])
		mockDomain(Resource, [res1])
		this.controller.params.name = RESOURCE2.name
		this.controller.params.type = RESOURCE2.type
		this.controller.params.hostname = RESOURCE2.hostname
		this.controller.params.port = RESOURCE2.port
		this.controller.params.path = RESOURCE2.path
		this.controller.params.selectedCredentials = "[{name: '" + Constants.CREDENTIAL_NAME + "', ta: 'blah/blah', grantingAuthority: '" + Constants.GRANTING_AUTHORITY + "', frequencyType: '" + Constants.FREQUENCY_TYPE + "', maxAge: '" + Constants.MAX_AGE_PARAM + "'}]"
		this.controller.update()

		assertNull "There were errors in creating a resource", this.controller.flash.errorObj
		def model = this.controller.index()
		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resources model should have the Resources created", model["resources"]?.size(), 2

		// now update the credential on RESOURCE2 to have cred1 and cred2
		this.controller.params.id = res1.id
		this.controller.params.name = RESOURCE1.name
		this.controller.params.type = RESOURCE1.type
		this.controller.params.hostname = RESOURCE1.hostname
		this.controller.params.port = RESOURCE1.port
		this.controller.params.path = RESOURCE1.path
		this.controller.params.selectedCredentials = "[{name: 'spec.resilient.com/Credentials.SimpleLogin', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2911/resilient_test_trust_service', frequencyType: '" + Constants.FREQUENCY_TYPE + "', maxAge: '" + Constants.MAX_AGE_PARAM + "'}, \
                                                       {name: 'spec.resilient.com/Credentials.SimpleLogin', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2912/resilient_test_trust_service', frequencyType: '" + Constants.FREQUENCY_TYPE + "', maxAge: '" + Constants.MAX_AGE_PARAM + "'}]"
		this.controller.update()

		assertNull "There were errors in creating a resource", this.controller.flash.errorObj

		model = this.controller.index()
		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resources model should have the Resources created", model["resources"]?.size(), 2
		def updatedResource = model["resources"]?.getAt(0)
		assertEquals  "The updated resource should have 2 credentials", updatedResource.credentialExpr.credentialElements.size(), 2
	}

	void testDelete() {
		def cred = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		mockDomain(CredentialElement, [cred])

		def credExpr = new CredentialExpression(credentialElements: [cred])
		mockDomain(CredentialExpression, [credExpr])

		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		def res2 = new Resource (name: RESOURCE2.name, type: RESOURCE2.type, hostname: RESOURCE2.hostname, port: RESOURCE2.port, path: RESOURCE2.path)
		res1.credentialExpr = credExpr
		res2.credentialExpr = credExpr
		mockDomain(Resource, [res1, res2])
		mockDomain(BaseResource, [res1, res2])

		def model = this.controller.index()
		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resources model should have the Resources created", model["resources"]?.size(), 2

		this.controller.params.id = res1.id
		this.controller.delete()
		assertNull "There were errors in creating a resource", this.controller.flash.errorObj
		model = this.controller.index()
		assertNotNull "The resources model is null", model["resources"]
		assertEquals "The resources model should have the Resources created", model["resources"]?.size(), 1
		def remainingResource = model["resources"]?.getAt(0)
		assertEquals  "The remaining resource should be resource 2", remainingResource.name, res2.name
	}

	void testCascadeDeleteCredentials() {
		def cred1 = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		def cred2 = new CredentialElement(name: Constants.CREDENTIAL_NAME, grantingAuthority: Constants.GRANTING_AUTHORITY2, frequencyType: Constants.FREQUENCY_TYPE, maxAge: Constants.MAX_AGE)
		
		mockDomain(CredentialElement, [cred1, cred2])

		def credExpr = new CredentialExpression(credentialElements: [cred1, cred2])
		mockDomain(CredentialExpression, [credExpr])
		def res1 = new Resource (name: RESOURCE1.name, type: RESOURCE1.type, hostname: RESOURCE1.hostname, port: RESOURCE1.port, path: RESOURCE1.path)
		res1.credentialExpr = credExpr
		mockDomain(Resource, [res1])
		mockDomain(BaseResource, [res1])

//		mockDomain(Resource, [])
//		mockDomain(BaseResource, [])
//		this.controller.params.name = RESOURCE1.name
//		this.controller.params.type = RESOURCE1.type
//		this.controller.params.hostname = RESOURCE1.hostname
//		this.controller.params.port = RESOURCE1.port
//		this.controller.params.path = RESOURCE1.path
//		this.controller.params.selectedCredentials = "[{name: 'spec.resilient.com/Credentials.SimpleLogin', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2911/resilient_test_trust_service', frequencyType: '" + Constants.FREQUENCY_TYPE + "', maxAge: '" + Constants.MAX_AGE_PARAM + "'}, \
//                                                       {name: 'spec.resilient.com/Credentials.SimpleLogin2', ta: 'blah/blah', grantingAuthority: 'rtn://localhost:2912/resilient_test_trust_service', frequencyType: '" + Constants.FREQUENCY_TYPE + "', maxAge: '" + Constants.MAX_AGE_PARAM + "'}]"
//		this.controller.update()

		assertNull "There were errors in creating a resource", this.controller.flash.errorObj
		def model = this.controller.index()
		assertNotNull "The resources in the model is null", model["resources"]
		assertEquals "The resources in the model should have the Resources created", model["resources"]?.size(), 1
		def addedResource = model["resources"]?.getAt(0)
		assertEquals  "The updated resource should have 2 credentials", addedResource.credentialExpr.credentialElements.size(), 2
		def resId = addedResource.id

		this.controller.params.id = resId
		this.controller.delete()

		assertNull "There were errors in deleting a resource", this.controller.flash.errorObj
		model = this.controller.index()
		assertNotNull "The resources in the model is null", model["resources"]
		assertEquals "The resources in the model should have no Resources", model["resources"]?.size(), 0
		def credIds = CredentialElement.list()
		assertNotNull "The credentials in the model is null", credIds
		assertEquals "The credentials in the model should have no Credentials", credIds.size(), 0
	}

}
