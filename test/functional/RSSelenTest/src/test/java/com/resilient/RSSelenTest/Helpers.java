package com.resilient.RSSelenTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public final class Helpers {
	 public static String getErrorText(Page p) {

		WebElement errorBlock = null;
		List<WebElement> errors = p.driver.findElements(By.className("error"));
		if(errors != null && errors.size() > 0) {
			System.out.println(">   - you have this many errors: "+errors.size());
			errorBlock = errors.get(0);		
		}
			
		String errorMess = "";
		if(errorBlock != null) {
			errorMess = errorBlock.getText();
		}		
		return errorMess;
	}
		
	 // Currently no NAME for the buttons - 0=? 1=NEW, 2=CONVERT_FORMAT, 3=CANCEL, 4=SAVE/Delete/Add  // could be random???
	public static void clickButton(RNSCredentialPage p, int Button){

			//System.out.println(">   - start: RSCredentialPage:  Inside clickButton " + Button);
			WebElement clink = null;
			
			List<WebElement> allButns = p.driver.findElements(By.tagName("button"));
			//System.out.println("number of buttons: " + allButns.size());
		
			if(allButns == null || allButns.size() == 0) {
				//System.out.println(">   - error: button not available, null or zero buttons available");
				return;
			} else {
				
				clink = allButns.get(Button); 
			}

			rest(3555);
			try {
				clink.click();
			} catch(Exception xu) {
				System.out.println(">   - 2nd click got exception: ");
				xu.printStackTrace();
			}
		//	System.out.println(">   - inside clickButton(): button number["+Button+"] was just clicked on page: "+p.driver.getTitle());

		}
	public static int getCredCount(RNSCredentialPage p)
	{
		//System.out.println("Inside getCredCount");
		// this gets the Heading row always will be size 1
		int iSize=0;
		List<WebElement> tabs = p.driver.findElements(By.xpath("//table[@id='credentials']")); 
		if (tabs == null || tabs.size() == 0){
			System.out.println("no creds (yet) in the table");
		//	iSize = 0;
		}
		else {
			for(WebElement we : tabs) 
			{
		//		WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				iSize = ctRows.size();
		//		System.out.println("rows inside credential table = " +iSize);
			} 
		}
		return iSize;   // send 1 always or subtract heading row;
	}
	
	public static int getResCount(RNSResourcePage p)
	{
		//System.out.println("Inside getCredCount");
		// this gets the Heading row always will be size 1
		int iSize=0;
		List<WebElement> tabs = p.driver.findElements(By.xpath("//table[@id='resources']")); 
		if (tabs == null || tabs.size() == 0){
			System.out.println("no creds (yet) in the table");
		//	iSize = 0;
		}
		else {
			for(WebElement we : tabs) 
			{
		//		WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				iSize = ctRows.size();
		//		System.out.println("rows inside credential table = " +iSize);
			} 
		}
		return iSize;   // send 1 always or subtract heading row;
	}
	public static boolean findCred(RNSCredentialPage p, String sCred){
		//System.out.println("Inside FindCredential " + sCred);

		boolean gotIt = false;

		List<WebElement> tabs = p.driver.findElements(By.xpath("//table[@id='credentials']")); 
		int iRCount = tabs.size();
		if(tabs == null || iRCount == 0) {
			System.out.println(">   - fail, we didn't get any tr's so we fail to find cred by name, you are done, return null");
		}
		else 
		{
			//System.out.println(">   - got few tr's so we can find cred by name: " + iRCount);
			//first row is table name Credential; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : tabs) 
			{
				WebElement ctabData = we.findElement(By.tagName("td"));
				List<WebElement> ctRows = we.findElements(By.tagName("tr"));
				for (WebElement trow : ctRows){
					String strow =  trow.getText();
		//			System.out.println("Current row data inside table data: " +strow);
				if (strow.startsWith(sCred)) 
				{// System.out.println("Got the right Resource to Delete");
				trow.click();  // clicking right ? or we.click() ?
				WebElement wDel= we.findElement(By.className("delete"));
				if (wDel != null) {//System.out.println(">   - going to delete Credential");
				wDel.click();  //delete now   //goto confirm dialog
				gotIt = true;
					break;
				}else continue;
			} // for trow}	
				}
	
			}
		//	System.out.println("This is the count for loop: "+ iCnt);
		}// else
		return gotIt;
	} // method
	
	public static void confirmDel(RNSCredentialPage p, boolean bDelete){
		//System.out.println(">   - Inside confirmDelete: ");
		WebElement sConf = p.driver.findElement(By.className("warning"));
		System.out.println(">   - This is the confirmation text: "+ sConf.getText());
		if (bDelete == true){
			clickButton(p, 4);  // hopefully delete 
			rest(2222);
		}
		else clickButton(p,3);  // for cancel
	}
	public static void rest(long restmillis) {
		try {
        	Thread.sleep(restmillis);
        } catch(Exception fu) {
        	
        }
	}
	public static String findCredStr(RNSCredentialPage p,String sCred){
		String gotIt = "";

		List<WebElement> rows = p.driver.findElements(By.tagName("tr"));
		int iRCount = rows.size();
		if(rows == null || iRCount == 0) {
			System.out.println(">   - fail, we didn't get any tr's so we fail to find cred by name, you are done, return null");
		}
		else
		{

			System.out.println(">   - got few tr's so we can find cred by name: " + iRCount);
			//first row is table name Credential; 2nd row is column headings; last row is New Button 
			// want only in between
			for(WebElement we : rows.subList(2,4)) 
			{
					String rowData = we.findElement(By.tagName("td")).getText();
				
			//		System.out.println(">    - Current Row data in the table: " + rowData);
					if (rowData.contains(sCred)) {
			//			System.out.println(">   - going to select right Credential to Edit " + sCred);
						
						System.out.println(">  - getText: " + we.getText());
						// it is getting all the credentials in the block or table - I need only the one row to be selected
						if (we.getText().startsWith("RNS."+sCred))
						{ System.out.println("got the right one to edit");
						we.click();  // am I clicking the right one? - clickin the first row's delete image- which is not correct
			//			System.out.println("clicked the right one to edit");
						rest(222);
						//all these do not work
						//	WebElement wSel= we.findElement(By.tagName("textarea"));
							//WebElement wSel= we.findElement(By.id("selCredential"));
						//WebElement wSel= we.findElement(By.className("sectionBody"));
						WebElement wSel= we.findElement(By.xpath("//div[@class='sectionBody']"));
							if (wSel != null) {System.out.println(">   - going to edit Credential");
							
							//edit here now
							}
							gotIt= we.getText();
							break; }
					} //}
					else continue;
			}
		//	System.out.println("This is the count for loop: "+ iCnt);
		}// else
		return gotIt;
	} // method
}
