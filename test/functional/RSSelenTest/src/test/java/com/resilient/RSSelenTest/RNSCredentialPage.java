package com.resilient.RSSelenTest;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//import com.resilient.RSSelenTest.Page;

public class RNSCredentialPage extends Page {
	
	@FindBy(how=How.ID_OR_NAME,using="new")
	 private WebElement addNewCredBtn;
	 @FindBy(how=How.ID_OR_NAME,using="save")
	 private WebElement saveEdtCredBtn;

	 @FindBy(how=How.ID, using="newCredentialExpr")
	 private WebElement copyCredExp;
	 @FindBy(how=How.ID, using="selCredential")
	 private WebElement editCredExp;
	
	 public RNSCredentialPage(WebDriver driver) {
			super(driver);
		}

	 public void addNewCred(String sCred, boolean bSave){
			// System.out.println("RSCredentialPage:  Inside addNewCred " + sCred + bSave);
		 addNewCredBtn.click();
			 Helpers.rest(1555);
			 
			copyCredExp.clear();
			copyCredExp.sendKeys(sCred);
		//	System.out.println("RSCredentialPage:  Copied credential text in inputbox " + sCred);

			Helpers.rest(1555);
			
			if (bSave == true) { 
				
				//System.out.println("RSCredentialPage:  Going to Click Add Credential button");
				Helpers.clickButton(this, 4);

				Helpers.rest(1222);

			} else {
				
				System.out.println("RSCredentialPage: Going to click Cancel button");
				Helpers.clickButton(this, 3);
				
			}
		}
	 
	 public void delCred(String sCred, boolean bDelete){
			//System.out.println("Inside Delete Credential: " + sCred);
			
			boolean didFind = Helpers.findCred(this, sCred);
			if ( didFind == true )
				{
					Helpers.confirmDel(this, bDelete);
				}
			else {
				System.out.println("DELCRED:: Did not find the Credential " + sCred + " to Delete");
			}
			
	}
	 
		// There is NO CANCEL button for EDITS  ONly SAVE
	public void edtCred(String sCred, String sNewCred, boolean bEdit){
		System.out.println(">  - Inside Edit Credential: ");
			String sCredit = Helpers.findCredStr(this,sCred);
			System.out.println("> ---   ");
			System.out.println("Make sure this is the credential to edit Back into edtCred method: " + sCredit);
			System.out.println("> ---   ");
			System.out.println("This is the new string to replace: " + sNewCred);
			System.out.println("> ---   ");
			editCredExp.clear();
			editCredExp.sendKeys(sNewCred);
			System.out.println(">  - Sending Edit Credential: ");
			//rest(2222);
			//System.out.println("Copied new edits - make sure frequency is Edited value " + editCredExp.getText());
			Helpers.rest(3222);
		//	replaceElem(sNewCred);
			if (bEdit == true) { 
				
				System.out.println("RSCredentialPage:  Going to Click Save Edit Credential button");
				//clickButton(3);  // does not work
				//List<WebElement> allB = driver.findElements(By.tagName("button"));
				//List<WebElement> allB = driver.findElements(By.id("save"));
				//int iSz = allB.size();
				//System.out.println("Number of buttons= " + iSz );
				
			//	allB.get(iSz).click();
				//saveEdtCredBtn.click();
				//saveEdtCredBtn.submit();  // does not seem to work - Tried all buttons not right context?
				
				WebElement element = driver.findElement(By.id("save")); 
				if (element != null){
					System.out.println("ready to tab over to save button");
				element.sendKeys(Keys.TAB); 
				Helpers.rest(244);
				System.out.println("In save button");
				element.sendKeys(Keys.ENTER);
				Helpers.rest(222);}
				System.out.println("CR on save button");  // did not save - has OLD VALUE STILL - There is no error in the string
				/*if (driver.findElement(By.id("save")) != null){
					driver.navigate().forward();   // tab to Save Button    // Getting 404 Error
					//driver.findElement(By.id("save")).click();}
					if (saveEdtCredBtn.isSelected()) {saveEdtCredBtn.click(); 	rest(3222);} 
					else {System.out.println("not selected");}
			*/
		//		System.out.println(">   - window should now be gone, button add just clicked!  here is the title: "+driver.getTitle());
				//driver.switchTo().activeElement();
					
			//	}} else {}	
			}
		}
}
