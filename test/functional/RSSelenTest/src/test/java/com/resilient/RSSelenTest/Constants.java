package com.resilient.RSSelenTest;


public class Constants {
	final static public String S_ASERVER = "http://localhost";
	final static public String S_PORT = "8080";
	
	final static public String S_RES_PAGE = "RNSResourceServer/resource";
	final static public String S_CRED_PAGE = "RNSResourceServer/credential";
	
	final static public String S_QUOTE = "\"";
	final static public String S_SLASH = "/";
	final static public String S_LCURLY = "{";
	final static public String S_RCURLY = "}";
	final static public String S_COLON = ":";
	final static public String S_LSQRBKT = "[";
	final static public String S_RSQRBKT = "]";
	final static public String S_COMMA = ",";
	final static public String S_LABKT = "<";
	final static public String S_RABKT = ">";
	
	// RoleCheck credential bits
	final static public String sCredID = S_LCURLY +"credentialIdentifier"+S_COLON;
	final static public String sCredNM = S_LCURLY + S_QUOTE+"name"+S_QUOTE+S_COLON+S_QUOTE+"RNS.RoleCheck"+S_QUOTE+S_RCURLY+S_COMMA;
	final static public String sCredND = S_QUOTE+"nodeName"+S_QUOTE + S_COLON +S_QUOTE+"CredentialElement"+S_QUOTE+S_COMMA;
	final static public String sCredGR = S_QUOTE+"grantingAuthorities"+S_QUOTE+S_COLON;
	final static public String sURI = S_LSQRBKT+S_LSQRBKT+S_LCURLY+S_QUOTE+"uri"+S_QUOTE+S_COLON+S_QUOTE+"rtn://qafe.resilient-networks.com:2909/RNS_LDAPTS_Default/RNS.RoleCheck"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_RSQRBKT+S_COMMA;
	final static public String sFreq = S_QUOTE+"frequency"+S_QUOTE+S_COLON+S_LSQRBKT+S_LCURLY+S_QUOTE+"maxAge"+S_QUOTE+S_COLON+"60000"+S_COMMA+S_QUOTE+"type"+S_QUOTE+S_COLON+S_QUOTE+"sameSession"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_COMMA;
	final static public String sParm1 = S_QUOTE+"parameters"+S_QUOTE+S_COLON+S_LSQRBKT+S_LCURLY+S_QUOTE+"historyKey"+S_QUOTE+S_COLON+"true"+S_COMMA+S_QUOTE+"value"+S_QUOTE+S_COLON+S_QUOTE+S_LABKT+"Role"+S_RABKT+S_QUOTE+S_COMMA+S_QUOTE+"key"+S_QUOTE+S_COLON+S_QUOTE+"Role"+S_QUOTE+S_RCURLY+S_COMMA;
	final static public String sParm2 = S_LCURLY+S_QUOTE+"historyKey"+S_QUOTE+S_COLON+"true"+S_COMMA+S_QUOTE+"value"+S_QUOTE+S_COLON+S_QUOTE+S_LABKT+"userid"+S_RABKT+S_QUOTE+S_COMMA+S_QUOTE+"key"+S_QUOTE+S_COLON+S_QUOTE+"userid"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_RCURLY;

	// SimpleLogin credential bits
	final static public String sSimpCID = S_QUOTE+"credentialIdentifier"+S_QUOTE+S_COLON+S_LCURLY+S_QUOTE+"name"+S_QUOTE+S_COLON+S_QUOTE;
	final static public String sSimpNM = "spec.resilient.com"+S_SLASH+"Credentials.SimpleLogin";
	final static public String sSimpNEND = S_QUOTE+S_RCURLY+S_COMMA;
	final static public String sSimpURI = S_LSQRBKT+S_LSQRBKT+S_LCURLY+S_QUOTE+"uri"+S_QUOTE+S_COLON+S_QUOTE+"rtn://localhost:2911/resilient_test_trust_service"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_RSQRBKT+S_COMMA;
	final static public String sSimpParm1 = S_QUOTE+"parameters"+S_QUOTE+S_COLON+S_LSQRBKT+S_LCURLY+S_QUOTE+"historyKey"+S_QUOTE+S_COLON+"true"+S_COMMA;
	final static public String sSimpParm2 = S_QUOTE+"key"+S_QUOTE+S_COLON+S_QUOTE+"user id"+S_QUOTE+S_COMMA;
	final static public String sSimpParm3 = S_QUOTE+"value"+S_QUOTE+S_COLON+S_QUOTE+S_LABKT+"userid"+S_RABKT+S_QUOTE+S_COMMA+S_QUOTE+"key"+S_QUOTE+S_COLON+S_QUOTE+"userid"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_RCURLY;
	final static public String sSimpFreq = S_QUOTE+"frequency"+S_QUOTE+S_COLON+S_LCURLY+S_QUOTE+"nodeName"+S_QUOTE+S_COLON+S_QUOTE+"sameSession"+S_QUOTE+S_COMMA+S_QUOTE+"maxAge"+S_QUOTE+S_COLON+"60000"+S_RCURLY+S_RCURLY;
	
	// constructed credentials
	final static public String S_CR_EXP = "Somecredential";  // Bogus Credential
	final static public String S_RNS_ROCK_CRED = sCredID+sCredNM+sCredND+sCredGR+sURI+sFreq+sParm1+sParm2;    //RNS.RoleCheck -Good Credential
	final static public String S_SIMPLE_LOG_CRED = S_LCURLY+sCredND+sSimpCID+sSimpNM+sSimpNEND+sCredGR+sSimpURI+sSimpParm1+sSimpParm2+sSimpParm3+sSimpFreq;   // SimpleLogin Credential
	final static public String S_DEL_CRED = "RNS.QA_IRBTS";
	final static public String S_DEL_CRED2 = "RNS.RoleCheck";
	final static public String S_DEL_CRED1 = "spec.resilient.com";
	
	//ERROR STRINGS  - put in a file
	final static String SEARCH_FOR_FAIL_SAVE_STR = "Failed to save the Credential. The credential expression is not a valid credential expression.";
	private static String sLCurly;
	String S_ERROR = null;
	final static String SEARCH_FOR_DEL_DEPEND_STR = "The Credential Expression is in use, it cannot be deleted.";
	
	//Edit credential bits
    final static public String sFreq2 = S_QUOTE+"frequency"+S_QUOTE+S_COLON+S_LSQRBKT+sLCurly+S_QUOTE+"maxAge"+S_QUOTE+S_COLON+"40000"+S_COMMA+S_QUOTE+"type"+S_QUOTE+S_COLON+S_QUOTE+"sameSession"+S_QUOTE+S_RCURLY+S_RSQRBKT+S_COMMA;
    final static public	String sNewRNSRockCred = sCredID+sCredNM+sCredND+sCredGR+sURI+sFreq2+sParm1+sParm2;

}
