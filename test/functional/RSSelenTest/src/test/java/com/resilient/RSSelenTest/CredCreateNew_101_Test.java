package com.resilient.RSSelenTest;

 import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.thoughtworks.selenium.*;


/*******
 * Launch RNSResourceServer UI from host and port read in from properties file
 * Create a valid Simple Login Credential and assert if count = 1
 * Delete it during cleanup
 * Issues:  Leaving System.out.println statements to debug later ;   Assert covers some verification but not all ; Can have one class for 1 test only as is; SUREFIRE is not aggregating all .xml - have to open each individual result
 * @author ulakumarappan
 *
 */
public class CredCreateNew_101_Test extends TestCase{

	FFBrowser browser;
	RNSCredentialPage RsCredPg;
	String sError = null;
	
	@BeforeClass
	public void setUp() throws Exception {
		System.out.println("************ CREATE TEST 101: SetUp() **********");
		String $LOGINURL="";
		String $PORTNUM="";
		try{
			Properties props = new Properties();
			String currentDir = System.getProperty("user.dir");
			//System.out.println("CurrentDir = " + currentDir);
			props.load(new FileInputStream(currentDir+"/src/test/props/RNSResource_ServerPort.properties"));
			$LOGINURL = props.getProperty("$LOGINURL");
			$PORTNUM = props.getProperty("$PORTNUM");
		}catch(Exception e){
			e.printStackTrace();
		}

		browser = new FFBrowser($LOGINURL, $PORTNUM);
		browser.get(Constants.S_CRED_PAGE);
		System.out.println(">-------------------- ------------------------------------------------- --------------<");
	}

	@Test
	public void testCredCreate101_Test(){
    	System.out.println("************ CREATE TEST 101: New SimpleLogin testCredCreate101_Test()  - Should PASS SAVE **********");
    	RsCredPg = browser.shows(RNSCredentialPage.class);
    	RsCredPg.addNewCred(Constants.S_SIMPLE_LOG_CRED, true);
    	Helpers.rest(1111);
    	
    	int iC= Helpers.getCredCount(RsCredPg);
    	//System.out.println("Cred count = " + iC);
    	assertEquals(2, iC);   //  heading is considered 1 row in table
     	//sError = Helpers.getErrorText(RsCredPg);
    	
    	//if(sError == null){System.out.println("************ CREATE TEST3 PASS: " + sSimpleLogCred + " CREDENTIAL ADD SAVED ");}
    	//else 
    		//{ System.out.println("~~~~~~~~~~~~~ Verify Error Message for Credential =  " + sError+ "~~~~~~~~~~~~~");	}
    	
    	System.out.println(">-------------------- ------------------------------------------------- --------------<");
		}
	
	@AfterClass
	public void tearDown() throws Exception {
		System.out.println("************ CREATE TEST 101: TearDown()   - SHOULD DELETE SIMPLE LOGIN CREDENTIAL CREATED **********");
		RsCredPg.delCred(Constants.S_DEL_CRED1, true);
		Helpers.rest(1111);
		
		int iC= Helpers.getCredCount(RsCredPg);
    	//System.out.println("Cred count = " + iC);
		assertEquals(1,iC);    // heading is considered 1 row in table
		System.out.println(">-------------------- ------------------------------------------------- --------------<");
		browser.stop();
	}

}
